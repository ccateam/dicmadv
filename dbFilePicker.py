import tables
import numpy as np
import scipy
import scipy.misc
import os
import shutil
import matplotlib.pyplot as plt
from matplotlib import cm



def dicFiles(func,tablePath):
    return func(tablePath)


# modified by NIMA needed to be aproved by CCA. 
# CCA Jul 2016 looked into it

def dicFilesWrtMem(func,tablePath,referenceIndex=0,**kwds):
    """sends **kwds to func"""
    filenames=func(tablePath,**kwds)
    # taking away any extension check 2016 Jul CCA
    
    dfs = filenames[:] 
    #dfs= filenames[len(filenames)/2:] #only for EXP 13 LP16 :NS 
    #dfs = filenames[referenceIndex+1:] CCA correction
    dfs.remove(dfs[referenceIndex])
    #print filenames
    #udf = [dfs[referenceIndex],]*len(dfs)
    udf=[filenames[referenceIndex],]*len(dfs)
    #udf= filenames[:len(filenames)/2] #only for EXP 13 LP16 :NS
    print udf
    print dfs
    return (udf,dfs)



if __name__=="__main__":
       
    tablePath = '/home/can/meta/db/mdmdb.h5'
    npyImDir='/home/can/MDM/exp2/exp2npyMicroIms/'
    
    def pickFile(tablePath):
        fid = tables.open_file(tablePath, mode='r')
        imTable = fid.root.exprGroup.exprDataTable    
        rows = [
            x.nrow for x in imTable.iterrows() if
            x['expNo']==3 
            and x['imagingSet']=='microTop'
            and x['fileName'][-4:]=='.tif'
            and x['loadPt']==0 #and x['gridNo']<12
            ]    
        files = [x['fileName'] for x in imTable.iterrows() if x.nrow in rows]
        fid.close()
        return files
    
    print pickFile(tablePath)
    
    dicFilesWrtMem(pickFile,tablePath,referenceIndex=0)

