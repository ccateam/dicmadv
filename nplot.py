import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import postProcess as pp
reload(pp)
import genericUtility.matplotlibstyles as gms
reload(gms)

import logging
logger = logging.getLogger(__name__)
    
def histProcessor(f, percent=True,normalizetoFreq=False,fixedrange=None,
                    timesStd=None,symm=False, numLevels=100,_stats='sane',**kwds):
    """
    _stats = 'sane': in historgram-range stats, 'raw':entire range stats
    kwds is to be used for sending additional pars to plt.hist
    variables in kwds have precedence. Namely, if you send them they will 
    override any computed values to be sent plt.hist
    """
    #initialize kwds for plt.hist
    histKwds={}
    #1) take of nans from data
    msk = np.logical_not(np.isnan(f))
    ff = f[msk]
    # 2) Multiply by 100 for percent strain description if needed
    if percent==True:
        ff=ff*100.
    # direct average and standard deviation
    av,_std= np.average(ff),np.std(ff)
    # check the percent of nan data points lost
    # dataComplete = 0.8 is 20% lost to nans
    dum = np.ones_like(msk)
    totalPoints = np.sum(dum.astype(float))
    dataComplete = np.sum(msk.astype(float))/totalPoints
    #
    if normalizetoFreq:
        # if there are 10 points, they will all have weights 0.1
        # weights is sent to plt.hist
        #numbers in the y axis are answer to "what percent of points do you have in this bin?"
        histKwds['weights'] = np.ones_like(ff)/totalPoints *100. # converting to percent freq
    else:
        histKwds['weights'] = np.ones_like(ff)
    #
    
    try:
        mincon , maxcon = fixedrange
    except TypeError:
        rng = _std * timesStd
        mincon,maxcon = av-rng, av + rng
        if symm:
            rng = max(abs(mincon),abs(maxcon))
            mincon,maxcon = -rng, rng
    histKwds['bins']=numLevels
    histKwds['range'] = (mincon, maxcon)
    #ff still contains outliers; if mincon and maxcon are sanity limits as expected
    msksane = np.logical_and(ff>mincon,ff<maxcon)
    ffsane = ff[msksane]
    avsane,_stdsane= np.average(ffsane),np.std(ffsane)
    # at this point datacomplete is fraction of data points left after nans and outliers are gone.
    dataCompletesane = np.sum(msksane.astype(float))/totalPoints
    if _stats=='raw':
        histTxt = 'av %s\nstd%s\ncp%s'%(av,_std,round(dataComplete*100,1))
    elif _stats=='sane':
        histTxt = 'av %s\nstd%s\ncp%s'%(avsane,_stdsane,round(dataCompletesane*100,1))
    histKwds.update(kwds)

    return ff,histKwds,histTxt



#to do: it is good to create a leveling object that auto creates legend

def levelMaker(f, leveling):
    """
    f = data matrix
    leveling = dictionary with possible items "fixedrange" or ("timesStd","symm")
                            and "numLevels"
    creates a colorbar level dictionary for matplotlib contourf
    """
    av,mn,_std,inval= pp.mean_std(f)
    try:
        levels = leveling['fixedList']
        levelsD = {'levels':levels}
    except KeyError:
        try:
            mincon,maxcon = leveling['fixedrange']
        except KeyError:
            rng = _std*leveling['timesStd']
            mincon,maxcon = av-rng, av + rng
            if leveling['symm']:
                rng = max(abs(mincon),abs(maxcon))
                mincon,maxcon = -rng, rng
        
        levels = np.linspace(mincon,maxcon, leveling['numLevels']+1)
        _norm = mpl.colors.Normalize(vmin=mincon, vmax=maxcon)
        # I could not make use of _norm really.
        levelsD = {'levels':levels}
    print levelsD
    return levelsD, av, mn, _std
    
def defplot(x,y,f,defPlotMode,contourKwds):
    if contourKwds['alpha']=='nofill':
        fn=plt.contour
        del contourKwds['alpha']
    else:
        fn=plt.contourf
    if defPlotMode=='undef':
        fn(x, y, f, **contourKwds)
    elif defPlotMode=='tripcolor':
	      
		fn=plt.tripcolor 
		triangles=mpl.tri.Triangulation(x.flatten(),y.flatten())
		fn(x.flatten(), y.flatten(), f.flatten(), edgecolors='k', alpha=1,  cmap=cm.hsv_r)
    elif defPlotMode=='def':
        fn(x+uu, y+vv, f,**contourKwds)
    elif defPlotMode=='defmedian':
        fn(x+np.median(uu), y+np.median(vv), f, **contourKwds)
    else:
        raise ValueError
    return


def overlay(x,y,var,im,defPlotMode,contourKwds,colorbar=True):
    plt.hold(True)
    plt.imshow(np.array(im),aspect='equal',cmap=cm.gray) 
    defplot(x,y,var,defPlotMode,contourKwds)
    if colorbar: plt.colorbar()
    #hideTicks()
    ax=plt.gca()
    ax.axes.set_xticks([])
    ax.axes.set_yticks([])
    plt.hold(False)

def masker(f,maskRange):
    if maskRange==None: return f
    mn,mx= maskRange
    g=f.copy()
    g[np.logical_and(g>mn,g<mx)]=nan
    return g
    
def hideTicks():
    """
    this does not hide ticks but ticklabels only
    ax.set_xticks([]) looks more practical
    """
    ax = plt.gca()
    axx,axy = ax.get_xticklabels() , ax.get_yticklabels() 
    plt.setp(axx,visible=False)
    plt.setp(axy,visible=False)
    return

def _imshowp(imdata,coords,origin,cmap=plt.cm.gray, ax=None, **kwds):
    """
    personalized imshow. Expects 1-channel data
    data : data matrix
    coords --> extent and origin -- > origin keywords are put as arguments to
    be forced out of the user. 
    coords: (xmin, xmax, ymin, ymax)is simply sent to extent variable of imshow
        that will scale and shift the axes as needed. This is used to put the image
        in our machine (spatial) coordinates. Once that is done contourf or any other
        plotting will work fine with  data x,y matches the machine coords
    origin: "upper" or "lower", it is another imshow keyword. Sending "upper" is
       our typical convention.
    """
    if ax==None:
        fig,ax = gms.yieldDefaultAxis()
    xmin, xmax, ymin, ymax = coords # just for half-checking
    ax.imshow(imdata,extent=coords, origin=origin, cmap=cmap,**kwds)
    return

def _contourfp(data,x,y,origin,extent,alpha,extend,
                invert_yaxis,cmap=plt.cm.RdBu_r,ax=None,onlyContours=False, 
                showColorbar=False,**kwds):
    """
    personalized contourf again forcing the user to input certain optional
    parameters of the function.
    
    extent = (xmin, xmax,ymin,ymax) much like imshow, if you use this send 
                 x=None, y=None
    x,y =  data locations with x and y matrices, if you use these , send
    extent as None
    
    contourf will put make extent inactive anyway if x and y matrices are sent.
    origin: [ *None* | 'upper' | 'lower' | 'image' ] contourf options. Typically
    will try to comply with imshow default upper.
     
    alpha: transparency level enforced to be input
    """
    if ax==None:
        fig,ax = gms.yieldDefaultAxis()

    try:
        x.dtype
        logger.warn("coordinates sent origin and extent parameters are ignored--also ignored in plt.contour(f)")
        if onlyContours:
            k = ax.contour(x,y,data,alpha=alpha,extend=extend,cmap=cmap,**kwds)
        else:
            k = ax.contourf(x,y,data,alpha=alpha,extend=extend,cmap=cmap,**kwds)
    except AttributeError:
        if onlyContours:
            k = ax.contour(data,origin=origin,extent=extent,alpha=alpha,extend=extend,cmap=cmap,**kwds)
        else:
            k = ax.contourf(data,origin=origin,extent=extent,alpha=alpha,extend=extend,cmap=cmap,**kwds)
    #
    if invert_yaxis:
        ax.invert_yaxis()
    ax.set_aspect('equal')    
    if showColorbar: 
        _kwds={}
        if showColorbar in ['horizontal','vertical']:
            _kwds['orientation']=showColorbar
        plt.colorbar(k,**_kwds)
    return
    
def dataplot(leveling,**kwds): 
    """ 
    high level function
    see _contourfp argument list for what should be input as kwds ; these are intentionally
    enforced though some have defaults under contourf
    """
    levelsD, av, mn, _std = levelMaker(kwds['data'], leveling)
    kwds.update(levelsD)
    try:# kwds contains invert_yaxis, run with whatever is sent
        kwds['invert_yaxis']
        _contourfp(**kwds)
    except KeyError: # if it is not sent that usually means dataplot is used on its own
        _contourfp(invert_yaxis=True,**kwds)
    return

def overlayplot(_imshowpKwds,leveling,_contourfpKwds,**kwds):
    """
    _imshowpKwds: dictionary of keywords to be passed onto _imshowp. (Contains all
                   the data as well as its placement and metric.)
    leveling, _contourfpKwds: to be passed on to dataplot. leveling is keywords for 
                            levelMaker, _contourfpKwds are for _contourfp
    
    Default rule: keep all positional arguments like coords, extent or x/y in the 
    same CS. This is typically our machine CS in mm.
    """
    try:
        ax = kwds['ax']
    except KeyError:
        fig,ax = gms.yieldDefaultAxis()
    
    cfp = _contourfpKwds.copy()
    #cfp['invert_yaxis']=False, no need in axis based operation
    _imshowp(ax=ax,**_imshowpKwds)
    dataplot(leveling,ax=ax,**cfp)
    
    return

def _saveFrame(fignum,dpi,filname,facecolor=None, hideTick=True):
    """
    saves a tight image by deleting axes and ticks
    """    
    plt.figure(fignum)
    ax=plt.gca()
    ax.set_aspect('equal')
    ax.set_frame_on(False)
    if hideTick:
        hideTicks()
    plt.savefig(filname,dpi=dpi,bbox_inches='tight',pad_inches=0,facecolor=facecolor)
    return
    
    
def plot(f,x=None, y=None, blow=1, figNum=30,
        leveling={'timesStd':2,'numLevels':8,'symm':True},colorbar=True, 
        contourKwds={'alpha':1.,'extend':'both','cmap':plt.cm.RdBu_r},
        maskRange=None,percentStrn=False, **kwds):
    """
    deprecated f = data matrix
    """
    print 'Deprecated; use dataplot and overlayplot instead!'
    nrow,ncol = f.shape
    #x,y = np.meshgrid(np.arange(ncol),np.arange(nrow))
    #x*=blow
    #y*=blow
    #xb,yb = np.meshgrid(np.arange(ncol)*blow,np.arange(nrow)*blow)
    #datb = np.ones_like(xb)
    levelsD, av, mn, _std = levelMaker(f, leveling)
    contourKwds.update(levelsD)
    plt.figure()
    print contourKwds
    #plt.imshow(datb,cmap=plt.cm.gray_r)
    k = plt.contourf(f,**contourKwds) 
    ax = plt.gca()
    
    ax.set_aspect('equal')
    if colorbar:
        plt.colorbar(k)
    #ax.set_frame_on(False)
    #hideTicks()
    return


