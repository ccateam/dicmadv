from dicTools import *
from scipy import *
import sys
import genericUtility.myStrTools as mst  ; reload(mst)
import FileGen as fg ; reload(fg)
import indRun ; reload(indRun)
from dicTools import *
import analysis as an ; reload(an)

# This is to be inherited.
class analysisCase:
    def __init__(self,imageDir,runDir,fbase):
        self.imageDir = imageDir
        self.runDir = runDir
        self.fbase=fbase
    def ppPars(self,**kwds):
        '''This will save the pars to set them, but for now first will set the
        fileName'''
        self.ppDict=kwds
    def npPars(self,**kwds):
        self.npDict=kwds
    def Pars(self,pppars,nppars):
        '''both pppars and nppars are dictionaries set by the user.'''
        self.ppPars(**pppars)
        self.npPars(**nppars)
    def nameMaker(self,**kwds):
        tempDict={}
        tempDict.update(self.ppDict)
        tempDict.update(self.npDict)
        tempDict.update(kwds)
        keyList = ['sub','gridx','gridy']
        self.name = self.fbase
        for key in keyList:
            try:
                val = tempDict[key]
                self.name = self.name+'_'+key+genStrMaker(val,'_')
            except KeyError:
                raise KeyError, 'Could not find the key %s in any dictionary'%(key,)

    def defAnalysis(self):
        self.an = an.defAnalysis(self.name,imageDir=self.imageDir,runDir=self.runDir)
    def setPars(self):
        self.an.setppAttrForAll(**self.ppDict)
        self.an.setnpAttr(**self.npDict)
    def defFiles(self,udf,dfs):
        self.an.setUndefFiles(udf)
        self.an.setDefFiles(dfs)
    def pickleSelf(self,name=''):
        import pickle
        if not name:
            name=self.name+'.pickle'
        pickle.dump(self,open(name,'w'))

class seqAnalysisCase(analysisCase):
    def defAnalysis(self):
        self.an = an.seqAnalysis(self.name,imageDir=self.imageDir,runDir=self.runDir)
    def defFiles(self,udf,dfs):
        self.an.setUndefFiles(udf)
        self.an.setDefFiles(dfs)



