from scipy import *
import numpy as np
import multiprocessing as mp
import os, sys, shutil, pickle, pdb
import os.path as osp
import pylab
from dicTools import *
import indRun ; reload(indRun)
import analysis ; reload(analysis)
import genericUtility.myFileTools as mft ; reload(mft)
import postProcess as pp ; reload(pp)
from PIL import Image, ImageDraw, ImageOps, ImageFont

import socket
from multiprocessing.managers import BaseManager
import multiprocessing as mp
import Pyro4
import Queue
import time
import glob
import shutil
D = {'tesla':'tesla.me.boun.edu.tr', 'navier':'navier.me.boun.edu.tr', 'young':'young.me.boun.edu.tr'}




class defAnalysisGroupMC(analysis.defAnalysisGroup):
    def __init__(self, groupSize,basename,relPath,ppRoot,imageDir=None,
                 fileRoot=None,impreprocessFunc=None,impreprocessFuncKwds={} ):
        """
        relPath = common relative path. ppRoot, fileRoot, etc. will be joined this to find final paths
        imageDir = central image location on the network drive (typically)
        fileRoot = shared location on the network drive for analysis input and output files
        ppRoot = after fileDir completes its task, all files are carried over and checked out for postprocessing. ppRoot + relPath corresponds to old runDir. This is where you work with the data results. Since it is a local decision we have not made it an environment variable. 
        """
        self.relPath=relPath
        if not imageDir:
            imageDir = os.environ['DICIMAGEPTH']
        if not fileRoot: 
            fileRoot = os.environ['DICCOMMONANROOT']
        self.fileDir = self.addRelPath(fileRoot)
        self.groupSize= groupSize
        self.analyses=[]
        for i in range(groupSize):
            name = basename+'part_%s'%(i,)
            self.analyses.append(analysis.defAnalysis(name=name,imageDir=imageDir, 
                                                      runDir=self.fileDir))
        self.ppDir = self.addRelPath(ppRoot)
        self.impreprocessFunc=impreprocessFunc
        self.impreprocessFuncKwds=impreprocessFuncKwds
        #self.impreprocessFuncKwds={}
        return
    
        # after create all files, need to fill a central queue
    def addRelPath(self,pth):
        fullpth = os.path.join(pth, self.relPath)
        if not(os.path.exists(fullpth)):
            from broadcast import makedirsFullPermission
            makedirsFullPermission(fullpth)
        else:
            print 'Warning! You might overwrite local files under this directory.'
        return fullpth
    
    def fillQueue(self,emptyQ=True,queueKwds = dict(address=('navier.me.boun.edu.tr', 50000), authkey='karamazof')):
        """ fillQueue fills the remote queue. The hard coded queue should be modified later. It is best to never change the queue service."""
        L=[]
        # all images in the entire analysis
        self.allimages = set() # sent won't allow multiplicity
        for an in self.analyses:
            for runObj in an.runObjs:
                p = runObj.pp
                L.append([p.imFiles[0],p.imFiles[1],runObj.ppFile])
                self.allimages.add(p.imFiles[0])
                self.allimages.add(p.imFiles[1])
        #
        class QueueManager(BaseManager): pass
        QueueManager.register('get_queue')
        self.queueKwds=queueKwds
        m = QueueManager(**self.queueKwds)
        m.connect()
        queue = m.get_queue()
        if not queue.empty():
            print "Warning! The queue is not empty to start with.\n"
            if emptyQ:
                print "emptying queue as requested\n"
                while not queue.empty():
                    print "pulling line\n"
                    print queue.get()
                
        for mem in L:
            print "putting line\n"
            print mem
            queue.put(mem)
            
        self.inputQsize= queue.qsize()
        return queue

    def setWorkers(self,workers=[('dicCnavier','navier.me.boun.edu.tr',7),('dicCmason2','mason2.me.boun.edu.tr',31)],nameServerCoords={'host':"navier.me.boun.edu.tr",'port':9005}):
        """ workers: a list triplets of registered proxy name, computer name, poolsize 
        DOES: creates the proxies of the identified worker processes on other computers
        
        """
        ns = Pyro4.locateNS(**nameServerCoords)
        uris=[]
        self.psizes = []
        for bcast,host,psize in workers: 
            uri= ns.lookup(bcast)
            uri.host=host
            uris.append(uri)
            self.psizes.append(psize)
        self.proxies = []
        for uri in uris:
            self.proxies.append(Pyro4.Proxy(uri))
        
    def runAllN(self,**kwds):
        """ activates all resources to start working on the queue."""
        # allow initiation first, in all.
        self.processes=[]
        for psize,proxy in zip(self.psizes,self.proxies):
            p = mp.Process(target=runproxy, 
                           args=(proxy,psize,self.relPath,self.allimages,
                                 self.impreprocessFunc, self.impreprocessFuncKwds,
                                 self.queueKwds))
            p.start()
            self.processes.append(p)
        # wait for all processes to finish
        for p in self.processes:
            p.join()

        
        return
     
    def transferResults(self,**kwds):
        """ from self.fileDir to self.ppDir for postprocessing"""
        from broadcast import copyFilesByExtension
        num = copyFilesByExtension(srcdir=self.fileDir,dstdir=self.ppDir,extension='*.rawdata')
        #
        print 'Input queue size was %s'%(self.inputQsize)
        print 'Found %s rawdata under %s and hopefully returned them to %s'%(num,self.fileDir,self.ppDir)
        return

    def checkStore(self,enforceStore=False,suppressShowProblem=False,**kwds):
        """ 
        run operation is replaced a bunch of network distribution operations in this mode.
        So, once all *.rawdata files are recovered into the directory self.ppDir, this operation will
        start.
        """
        os.chdir(self.ppDir)
        self.rawData()
        self.concatData()
        #
        p = pp.postprocess(self.rawdatamatrices,an=self.analyses[0],**kwds)
        #an is sent only for deformation files, so this should do.
        pres =  p.checkStatus()
        print pres
        if pres != 'CLEAN MINIMIZATION':
	    if not suppressShowProblem:
	        p.showProblem(**kwds)
            if not enforceStore:
	        raise ValueError, 'The analysis did not yield clean minimization. Will not store.'
	    else:
	        print 'Warning! The analysis did not yield clean minimization. Only storing because you want me to.'
	        del self.processes # this is necessary for pickle rejects to serialize mp processes
                self.pickleSelf(**kwds)
        else:
            del self.processes
            self.pickleSelf(**kwds)
        return p

def initproxy(pr,poolsize,relPath,allimages):
    pr.init(relPath,allimages)

def runproxy(pr, poolsize,relPath,allimages,impreprocessFunc, 
             impreprocessFuncKwds,queueKwds):
    pr.init(relPath,allimages,impreprocessFunc,impreprocessFuncKwds )
    pr.runPool(poolsize, queueKwds)
