#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 08:34:52 2019

@author: can
"""

import numpy as np
import scipy.interpolate as interp
import matplotlib.pyplot as plt

def central_LinInt(xc, yc, cc, dx=0.5,dy=0.5):
    """
    for lack of time dx and dy are inputs for now
    """
    #
    x = np.zeros(np.size(xc)+1)
    x[:-1] = xc - 0.5*dx
    x[-1] = x[-2] + dx
    #
    y = np.zeros(np.size(yc)+1)
    y[:-1] = yc - 0.5*dy
    y[-1] = y[-2] + dy
    
    c = cc.copy()
    c[1,1,:,:] = cc[1,1,:,:] - cc[0,1,:,:]*0.5*dx - cc[1,0,:,:]*0.5*dy
    return (x,y,c)
    
    
    

# 1-D
if 0:
    #               quad, lin, value 
    #c = np.array([[5,5],[2,-2],[0,3]])
    #              lin ,  value
    c = np.zeros((2,2,2,2))
    x = [0,0.5,1]
    y = [0,0.5,1]
    # These are the values
    c[1,1,:,:]=np.array([[0,1],[0,2]])
    # I tested all derivatives one-by-one 
    # u_x
    c[0,1,:,:]=np.array([[1,1],[1,1]])*1
    # u_y
    #c[1,0,:,:]=np.array([[1,1],[1,1]])
    # u_xy
    #c[0,0,:,:]=np.array([[1,1],[1,1]])*5
else:
    xc = np.array([0.25, 0.75])
    yc = np.array([0.25, 0.75])
    cc = np.zeros((2,2,2,2))
    cc[1,1,:,:] = np.array([[0.25,1.25],[0.25,2.25]])
    cc[0,1,:,:]=np.array([[1,1],[1,1]])*1
    x,y,c = central_LinInt(xc,yc,cc)
# values apply to the first two x'es
a = interp.NdPPoly(c,[x,y])

xp=np.linspace(0,1,num=101)
yp=np.linspace(0,1,num=101)

xp2,yp2 = np.meshgrid(xp,yp)
up2 = a((xp2,yp2))

#plt.contourf(up2)
# extraplation
#xp=np.linspace(-0.5,1,num=151)
#yp=a(xp)
#plt.plot(xp,yp)




