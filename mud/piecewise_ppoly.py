#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 08:34:52 2019

@author: can
"""

import numpy as np
import scipy.interpolate as interp
import matplotlib.pyplot as plt

def central_LinInt(xc, cc):
    dx = np.unique(np.diff(xc))
    if dx.size != 1:
        raise ValueError
    else:
        dx= dx[0]
    
    x = np.zeros(np.size(xc)+1)
    x[:-1] = xc - 0.5*dx
    x[-1] = x[-2] + dx
    c = cc.copy()
    c[1,:] = cc[1,:] - cc[0,:]*0.5*dx
    return (x,c)
    
    
    

# 1-D
if 1:
    #               quad, lin, value 
    #c = np.array([[5,5],[2,-2],[0,3]])
    #              lin ,  value
    c = np.array([[2,-2],[0,3]])
    x =[0,0.5,1]
else:
    xc = [0.25, 0.75]
    cc = np.array([[2,-2],[0.5,2.5]])
    x,c = central_LinInt(xc,cc)
# values apply to the first two x'es
a = interp.PPoly(c,x)
# no extrapolation
#xp=np.linspace(0,1,num=101)
# extraplation
xp=np.linspace(-0.5,1,num=151)
yp=a(xp)
plt.plot(xp,yp)




