import dicTools; reload(dicTools); from dicTools import *
from scipy import *
import scipy
import scipy.stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl
import os, sys, shutil, copy, pickle
import indRun ; reload(indRun)
import strainOp; reload(strainOp)
import genericUtility.myFileTools as mft ; reload(mft)
import genericUtility.datafit as dft ; reload(dft)
import numpy as np
import pylab
from PIL import Image, ImageDraw, ImageOps, ImageFont
import pdb
import os.path as osp
import warnings, logging
logger = logging.getLogger(__name__)


def plot_strains(f,undefFil,defFil,im2,im4,x,y,undefPlot=True,defPlotMode='def',cropAllowance=100,leveling={'timesStd':2,'numLevels':8,'symm':True},colorbar=True,contourKwds={'alpha':0.6,'extend':'both','cmap':cm.jet},maskRange=None,figstart=1, **kwds):
        """
        varType = 'small' works with inf. strain and rot
                  'Lag'   works with Lagrangian strain and large rotation (Lag since I want to leave room for Eulerian)
        leveling= a dictionary that adjusts the limits and levels of the contour plot; keys = timesStd (float) or fixedRange (doublet) , symm (bool), numLevels (int)
        if plotvar=='all': plots all ex, ey, exy, wxy in a 2x2 plot array
               =='ex' or ... plots just this one.
        """

        def levelMaker(f, leveling):
            av,mn,_std,inval= mean_std(f)
            try:
                mincon,maxcon = leveling['fixedrange']
            except KeyError:
                rng = _std*leveling['timesStd']
                mincon,maxcon = av-rng, av + rng
                if leveling['symm']:
                    rng = max(abs(mincon),abs(maxcon))
                    mincon,maxcon = -rng, rng
            
            levels = np.linspace(mincon,maxcon, leveling['numLevels']+1)
            _norm = mpl.colors.Normalize(vmin=mincon, vmax=maxcon)
            # I could not make use of _norm really.
            levelsD = {'levels':levels}
            print levelsD
            return levelsD, av, mn, _std

        def defplot(x,y,f,defPlotMode,contourKwds):
            if contourKwds['alpha']=='nofill':
                fn=plt.contour
                del contourKwds['alpha']
            else:
                fn=plt.contourf
            if defPlotMode=='undef':
                fn(x, y, f, **contourKwds)
            elif defPlotMode=='def':
                fn(x+uu, y+vv, f,**contourKwds)
            elif defPlotMode=='defmedian':
                fn(x+np.median(uu), y+np.median(vv), f, **contourKwds)
            else:
                raise ValueError
            return

        def overlay(x,y,var,im,defPlotMode,contourKwds,colorbar=True):
            plt.hold(True)
            plt.imshow(np.array(im),aspect='equal',cmap=cm.gray) 
            defplot(x,y,var,defPlotMode,contourKwds)
            if colorbar: plt.colorbar()
            #hideTicks()
            ax=plt.gca()
            ax.axes.set_xticks([])
            ax.axes.set_yticks([])
            plt.hold(False)
        
        def masker(f,maskRange):
            if maskRange==None: return f
            mn,mx= maskRange
            g=f.copy()
            g[np.logical_and(g>mn,g<mx)]=nan
            return g
        
        
        figNum=figStart
        #
        if undefPlot:
            fig = plt.figure(figNum)
            figNum+=1
            levelsD, av, mn, _std = levelMaker(f, leveling)
            contourKwds.update(levelsD)
            overlay(x,y,f,im2,'undef',contourKwds,colorbar=colorbar)
        
        if defPlotMode!=None:
            uu, vv= self.naned_uvcopy()
            fig = plt.figure(figNum)
            figNum+=1
            levelsD, av, mn, _std = levelMaker(f, leveling)
            contourKwds.update(levelsD)
            print contourKwds
            overlay(x,y,masker(f,maskRange),im4,defPlotMode,contourKwds,colorbar=colorbar)
        #
        return figNum
