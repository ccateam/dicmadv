from scipy import *
from scipy import stats
import sys,os,pylab,pickle,pdb
from PIL import Image 
import dicTools as dicTools ; reload(dicTools)
import genericUtility.myStrTools as mst  ; reload(mst)
import genericUtility.gridTools as ggt  ; reload(ggt)
import genericUtility.evaluateExcel as ex ;  reload(ex)
#import pathYield ; reload(pathYield)
from genericUtility.gridTools import MakeGrid
import FileGen as fg ; reload(fg)
import indRun as indRun ; reload(indRun)
import analysis as analysis ; reload(analysis)
import Script as Script  ; reload(Script)
from Script import analysisCase
import postProcess as postProcess ; reload(postProcess)
import genericUtility.emailer as em
import genericUtility.gridTools as gridTools ;  reload(gridTools)
#
	

def shift(df,udf, firstLevel=False,firstLevelKwds={'shrinkFactor':10}, **shiftKwds):
    """does a first level center based shift before doing the actually requested stuff with shiftKwds. Useful when say a 16 pt shift is done and the first point fails"""
    try:
        shiftKwds['ppparsMod']['pOrShift']
        shiftHandSpec=True
    except KeyError:
        shiftHandSpec=False
    #pdb.set_trace()
    if firstLevel:
        if shiftHandSpec:
            raise ValueError, "doesn't make sense for you to specify initial shift as well as asking to find it."
        a = _shift(df,udf,gridType='1',**firstLevelKwds)
        s2 = shiftKwds['shrinkFactor']
        s1 = firstLevelKwds['shrinkFactor']
        shftFloat = array(a) / float(s2) 
        shiftKwds['ppparsMod']['pORshift'] = (int(round(shftFloat[0])),int(round(shftFloat[1])))
    # now do the real level
    return _shift(df,udf, **shiftKwds)

def _shift(df, udf, shrinkFactor = 10, gridType = '4center',gridPts='None',showGrid=False,ppparsMod={},npparsMod={}, ret='median',imageDir=None, runDir=None,suppressShowProblem=False):
    """
    dfs = deformed file
    udf = undeformed fileqqq
    ret = median returns only the median u,v pair
        = full returns u and v
    gridPts = before shrinkFactor is applied, gridType='4specify' needed to take effect
    """
    
    if imageDir:      
        if not os.path.exists(imageDir): 
            raise IOError, "Directory %s sent for imageDir is not found."%(imageDir,)
    else:
        imageDir=os.getcwd()
    if runDir:      
        if not os.path.exists(runDir): 
            raise IOError, "Directory %s sent for imageDir is not found."%(runDir,)
    else:
        runDir=os.getcwd()
        
    dfs = [os.path.join(imageDir,df),]
    udf = [os.path.join(imageDir,udf),]

    
    dummy = Image.open(udf[0])
    sz=dummy.size
   
   
    del dummy
    
    newsz = (sz[0]/shrinkFactor, sz[1]/shrinkFactor)
    sub = 50
    
    if gridType=='1':
        stx = newsz[0]/2
        sty = newsz[1]/2
        gr = ggt.MakeGrid(stx,sty, sub,sub,1,1,gridtype='snakeLR',showGrid=False)
    elif gridType=='4':
        stx = newsz[0]/2-sub/2
        sty = newsz[1]/2-sub/2
        gr = ggt.MakeGrid(stx,sty, sub,sub,2,2,gridtype='snakeLR',showGrid=False)
    elif gridType=='4center':
        quarter_x, quarter_y = newsz[0]/4, newsz[1]/4
        gr = ggt.MakeGrid(quarter_x,quarter_y, 2*quarter_x,2*quarter_y,2,2,gridtype='snakeLR',showGrid=False)
    elif gridType=='4specify':
        gr=[(u/shrinkFactor, v/shrinkFactor) for u, v in gridPts]
    else:
        raise ValueError
    print gr
    pppars={'icoflag':1,'npoint':len(gr),'i0j0List':gr,'size':newsz ,'pORshift':[0,0]}
    nppars={'sub':[sub,sub],'nrt':[25,25],'nsb':[5,5],'imethod_flag':1, 'ftol':1.e-5}
    pppars.update(ppparsMod)
    nppars.update(npparsMod)
    
    
    rawDataColDict = {'X':0,'Y':1,'u':2,'v':3,'ex':4,'ey':5,'exy':6,'dudx':7,'dvdy':8,'dudy':9,'dvdx':10,'Theta':11,'w':12,'status':13, 'iter':14, 'corrcoeff':15}
    #shiftList = [[0,0],]
    olddir = os.getcwd()
    os.chdir(runDir)

    storeName = 'reduced.p'

    # class whose primary purpose is auto name defs.
    an = analysis.defAnalysis('grid',imageDir,runDir)
    an.setDefFiles(dfs)
    an.setUndefFiles(udf)
    an.createRunObjs()
    an.setppAttrForAll(**pppars); an.setnpAttr(**nppars)
    #an.setppAttrfromList('pORshift',shiftList)
    #
    #
    
    
    try:
        an.runCheckStore(numProc=1,enforceStore=True, colDict = rawDataColDict, pointSize=1,fontsize=8, drawCon=False, selfPickleName=storeName,resize=newsz,suppressShowProblem=suppressShowProblem)
    except OSError:
        message = "Sorry to say that gridScriptmodi crashed at %s. On second thought, I am just a machine so I'm not sorry. I'm not happy either; totally neutral here.; my name is %s not Marvin. Anyways filpoint is %s as it crashed, the stored files should be fine until this guy, so you are advised to readjust the list of files to be submitted starting with this one and resubmit."%(os.environ['HOSTNAME'],os.environ['HOSTNAME'],gridPt)
        em.send2UsualSuspects(msg=message)
    
    if showGrid:
        an.showGrid()

    #print checkExistingAnalysis(a)
    os.chdir(olddir)
    c = postProcess.postprocess(an.rawdatamatrices, colDict = rawDataColDict)
    c = postProcess.postprocess(an.rawdatamatrices, colDict = rawDataColDict)
    u=c.pickVar('u')
    v=c.pickVar('v')
    cmask =c.cleanMask
    numloads = u.shape[0]
    print 'numloads = %s'%(numloads,)
    for i in range(numloads):
        ui, vi, cm = u[i,:], v[i,:], cmask[i,:]
        medui, medvi = median(ui[cm]), median(vi[cm])
        stdui, stdvi = std(ui[cm]), std(vi[cm])
        print 'sent Shift: %s'%(list(pppars['pORshift']))
        print 'For load %s \n medu = %s medv=%s \n stdu = %s stdv =%s\n'%(i,medui,medvi,stdui,stdvi)
    if ret=='median':
        return (int(round(medui*shrinkFactor)), int(round(medvi*shrinkFactor)))
    elif ret=='full':
        return (   (ui*shrinkFactor).round(), (vi*shrinkFactor).round())


if __name__=="__main__":
    if 1:
        shfts = []
        for i in [5,6,11,12]:
            shft = shift(3, i, 0, shrinkFactor =5,gridType='4center',showGrid=True)
            shfts.append(shft)

        print shfts
    if 1:
        grids, centers = gridTools.MultiGrid((2,2), (180, 2450-180, 180, 2050-180) , 10, 10)
        shfts = []
        for i in [5,6,11,12]:
            shft = shift(3, i, 0, shrinkFactor =5,gridType='4specify',gridPts =centers, showGrid=True)
            shfts.append(shft)

        print shfts
