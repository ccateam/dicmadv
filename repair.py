#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 17:33:16 2019

@author: can
"""
import pickle, os
import matplotlib.pyplot as plt
from skimage.restoration import inpaint_biharmonic,inpaint
import logging

import dicmadv.quick  as quick

class frameRepair:
    """ used to repair poor subgroup calculations in defAnalysisGroup 
    applications"""
    def __init__(self, pfile,correlationNum=0):
        """
        pfile =  pickled file of an defAnalysisGroup object
        correlationNum = the load-comparison num in the object. 
                        We typically reverted to only one load comparison 
                        files for which the only valid number is 0.
        """
        self.pfile = pfile
        self.anGroup = pickle.load(open(pfile))
        self.corrNo = correlationNum
        return
    
    def detect(self,**kwds):
        """
        successLim = just checking the status. If success ratio by status falls
                     below this number analysis is redone for repair.
        Prepares self.redoIndices can be overriden as well.
        """
        self.redoIndices=[]
        for i, subgroup in enumerate(self.anGroup.analyses):
            if self.detectPoor(subgroup,**kwds):
                self.redoIndices.append(i)
            #
        return
    
    def detectPoor(self, subgroup,successLim=0.9):
        subgroup.check()
        logging.debug("%s"%(subgroup.successRate))
        if subgroup.successRate<successLim:
            return True
        else:
            return False
            
    def rerun(self,**kwds):
        for j in self.redoIndices :
            subgroup = self.anGroup.analyses[j]
            for strategyNo in range(100):  
                modified = self.modify(subgroup,strategyNo)
                if modified:
                    # run at the current location
                    subgroup.createAllFiles()
                    subgroup.runAll()
                    subgroup.rawData()
                    if self.detectPoor(subgroup,**kwds):
                        pass # go on to next strategy if there is one
                    else: # this means that the check succeeded
                        logging.debug("problem fixed")
                        break
                else:
                    logging.warn("couldn't fix the problem")
                    break
        return
            
    
    def modify(self,subgroup,strategyNo):
        """This is the major item that requires inheriting and is called under
        rerun. One can place a cascaded strategy """
        raise NotImplementedError
    
    def collectBack(self, rewrite=True):
        self.anGroup.concatData()
        if rewrite:
            filName = os.path.basename(self.pfile)
            self.anGroup.pickleSelf(selfPickleName=filName)
        return


class frameRepairIco(frameRepair):
    def modify(self,subgroup,strategyNo):
        """
        returns True if a modification could be made
        returns False when strategies are exhausted
        switches icoflag value in the first tier
        """
        ro =subgroup.runObjs[self.corrNo]
        if strategyNo==0:
            oldval = ro.pp.icoflag
            if oldval == 1:
                ro.pp.icoflag = 2
            elif oldval == 2:
                ro.pp.icoflag =1
            else:
                raise ValueError, "unexpected icoflag found"
        else:
            return False
        #
        return True 
        
        
    
if __name__=="__main__":
    pfile = '/home/can/MDM/exp5/LP22/65302530_dn_pc/an0.p'
    leveling = {'numLevels':8,'symm':True,'fixedrange':[-0.02,0.02]}
    quick.peek(pfile,leveling=leveling)
    repAn = frameRepairIco(pfile,correlationNum=0)
    repAn.detect()
    repAn.rerun()
    repAn.collectBack(rewrite=False)
    plt.figure()
    quick.peek(repAn.anGroup,  leveling=leveling)

