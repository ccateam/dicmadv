##############################################
# CCA 2019  
# The latest execution of legacy DIC routines
# that includes cascaded initial condition search
###############################################
import dicmadv.genericUtility.gridTools as ggt  ; reload(ggt)

import dicmadv.shiftFinder as shiftFinder; reload(shiftFinder)
import dicmadv.analysis as analysis ; reload(analysis)
import dicmadv.boundaryDet as bdet; reload(bdet)
import dicmadv.estimateGrid as estm; reload(estm)
import dicmadv.frameMatrix as frameMatrix ;  reload(frameMatrix)
import dicmadv.distribAnalysis as danalysis; reload(danalysis)


import os,time
import logging
import numpy as np
import skimage.io

imageDir = os.environ['DICIMAGEPTH']
db = os.environ['DICDBPTH']
#


def retFile(runDir, GRIDNO, exp, mode, loadDefList,
            shiftCorrespondence='noShift',**kwds):
    """
    GRIDNO: grid number in the deformed frame. No difference when 
            shiftCorrespondence='noShift' of course, you could call it in
            the deformed or undeformed frame.
    shfitCorrespondence: noShift, downRef, upRef
    governs if the reference files are to be picked from the
    same row or the one above or the one below
    
    """
    # running this global info on every single GRIDNO is not ideal 
    # but should not be slow; improve if need be
    nL = len(loadDefList)
    fgrids=[None,]*nL
    for i,load in enumerate(loadDefList):
        fgrids[i] = frameMatrix.frameGrid(expNo=exp,loadNo=load,mode=mode)
        fgrids[i].dataBaseOp(frameMatrix.f)
    #
    dfs=[fgrids[j].files[GRIDNO] for j in range(1,nL) ]
    if shiftCorrespondence=='noShift':
        udf = [fgrids[0].files[GRIDNO],]*len(dfs)
        modREFNO=GRIDNO
    elif shiftCorrespondence=='downRef':
        numrow = fgrids[0].numrow
        I,J = fgrids[0].retIJ(GRIDNO)
        if I==numrow-1: 
            raise ValueError, "you cannot ask for the first row correspondences to be below."
        else:
            modREFNO = fgrids[0].retGrid(I+1,J)
            udf = [fgrids[0].files[modREFNO],]*len(dfs)  
    elif shiftCorrespondence=="upRef":
        I,J = fgrids[0].retIJ(GRIDNO)
        if I==0: 
            raise ValueError, "you cannot ask for the first row correspondences to be above."
        else:
            modREFNO = fgrids[0].retGrid(I-1,J)
            udf = [fgrids[0].files[modREFNO],]*len(dfs)
    elif shiftCorrespondence=="rightRef":
        I,J = fgrids[0].retIJ(GRIDNO)
        numcol = fgrids[0].numcol #added by NS
        if J==numcol-1: #I==0: #added by NS if J==numcol-1 
            raise ValueError, "you cannot ask for the first row correspondences to be above."
        else:
            modREFNO = fgrids[0].retGrid(I,J+1)
            udf = [fgrids[0].files[modREFNO],]*len(dfs)

    else:
        raise NotImplementedError, "unknown shiftCorrespondence"
        
    #udf,dfs = filePicker.dicFilesWrtMem(pickFileDef, db,referenceIndex=0,gridNo=GRIDNO,loadList=loadDefList)
    return udf, dfs, modREFNO
    



def preprocessShift(runDir,GRIDNO,fgrid,udf,dfs,shiftCorrespondence='noShift',
                    gridspx=None, gridspy=None, groupShapeReg=None, 
                    groupShapeShift=None, margin=None, prepParsReg=None,
                    prepParsShift=None, **kwds):
     
    #
    _ij = fgrid.retIJ(GRIDNO)
    _phasex = np.remainder(-fgrid.Dx_pix[...,0], gridspx)
    _phasey = np.remainder(-fgrid.Dx_pix[...,1], gridspy)
    phasex,phasey = _phasex[_ij] , _phasey[_ij]
    
    if shiftCorrespondence == 'noShift':
        groupShape = groupShapeReg
    else:
        groupShape = groupShapeShift
    #
    numGroup=groupShape[0]*groupShape[1]
    numLoads = len(dfs)
    if numLoads!=1:
        raise NotImplementedError
    #newShift4ptList=[['pORshift',['dummy',]*numLoads ]]*numGroup
    shiftList=[]
    j=0   # load/comparison number, usually only one
    for df, ud in zip(dfs, udf):
        print ud,df
        analysis.copier(os.path.join(imageDir,ud),runDir)
        analysis.copier(os.path.join(imageDir,df),runDir)
        ud_im = skimage.io.imread(os.path.join(runDir,ud))
        df_im = skimage.io.imread(os.path.join(runDir,df))
        #
        if shiftCorrespondence == 'noShift':
            gridShift=(0,0)
            shift_x , shift_y = 0,0
            layers = bdet.gfFFTAnalysis(ud_im,df_im,**prepParsReg)
            _layer = layers[-1]
            _layer.interpNets(interpMode="extendedInterp",useCleans=False)
            inbool = bdet.matBoundary(ud_im, df_im, mappingLayer=_layer)
        elif shiftCorrespondence == 'downRef':
            # connection='up' does not sound good with 'downRef' 
            # but ok with the definitions made: a reference image that is down
            # has its "up" points in material overlap
            gridShift=(0,0)
            _res = bdet.patchAnalysis(ud_im,df_im, connection='up', 
                                     **prepParsShift)
            layers,choppedims, delta,bw = _res
            shift_x , shift_y = delta 
            im1b, im2b = choppedims
            _layer = layers[-1]
            _layer.interpNets(interpMode="extendedInterp",useCleans=True)
            inbool = bdet.matBoundary(im1b, im2b, mappingLayer=_layer)
        elif shiftCorrespondence == 'upRef':
            _res = bdet.patchAnalysis(ud_im,df_im, connection='down', 
                                     **prepParsShift)
            layers,choppedims, delta,bw = _res
            shift_x , shift_y = delta 
            gridShift= (-shift_x,-shift_y)
            im1b, im2b = choppedims
            _layer = layers[-1]
            _layer.interpNets(interpMode="extendedInterp",useCleans=True)
            inbool = bdet.matBoundary(im1b, im2b, mappingLayer=_layer)
        elif shiftCorrespondence == 'rightRef':
            gridShift=(0,0)
            _res = bdet.patchAnalysis(ud_im,df_im, connection='left', 
                                      **prepParsShift)
            layers,choppedims, delta,bw = _res
            shift_x , shift_y = delta 
            im1b, im2b = choppedims
            _layer = layers[-1]
            _layer.interpNets(interpMode="extendedInterp",useCleans=True)
            inbool = bdet.matBoundary(im1b, im2b, mappingLayer=_layer)
        else:
            raise NotImplementedError, "unknown shiftCorrespondence"
        #
        lim = estm.prepRectGridLims(inbool, margin=margin, shift=gridShift, delta=(gridspx,gridspy),phase=(phasex,phasey))
        grids,centers = ggt.nMultiGrid(groupShape,lim,gridspx,gridspy,gridtype='snakeUD',**kwds)
        newgrids= [ {'i0j0List':grid, 'npoint':len(grid)} for grid in grids]
        for i in range(numGroup):
            gridSt = grids[i][0]
            u = int(np.round(_layer.uif(*gridSt)) + shift_x )
            v = int(np.round(_layer.vif(*gridSt)) + shift_y)
            print u,v
            #newShift4ptList[i][1][j] = (u,v)
            shiftList.append(['pORshift',[(u,v)]])
        j+=1  
    logging.debug("shiftList from preprocess: ", shiftList)
    
    return shiftList , grids, centers, newgrids
    #bdet.combinedPlot(ud_im,df_im,inbool,grids=grids,saveFil=False)
    
def analyze(runBase,relPath,GRIDNO,showGrid,analyze,procInfo, udf,dfs,modREFNO,
            shiftCorrespondence='noShift',MC=False,pppars=None,nppars=None,
            shiftList=None, newgrids=None,rawDataColDict=None,
            impreprocessFunc=None,impreprocessFuncKwds=None,**kwds):
    """
    sends **kwds to runCheckStore
    """
    try:
        nameServerCoords =  procInfo["nameServerCoords"]
        workers = procInfo["workers"]
        queueKwds = procInfo["queueKwds"]
        MC = True # MC = multicomputer
    except KeyError:
        numProc = procInfo["numProc"]
        MC = False
    except TypeError:
        raise TypeError, "procInfo is supposed to be a dictionary"
        
    #
    if shiftCorrespondence == 'noShift':
        nm ='an%s.p'%(modREFNO,)
    elif shiftCorrespondence == 'downRef':
        nm = 'an%s_downRef.p'%(modREFNO,)
    elif shiftCorrespondence == 'upRef':
        nm = 'an%s_upRef.p'%(modREFNO,)
    elif shiftCorrespondence == 'rightRef':
        nm = 'an%s_rightRef.p'%(modREFNO,)         

    else:
        raise NotImplementedError, "unknown shiftCorrespondence"
    #
    numGroup = len(shiftList)
    #
    if MC:
        ans = danalysis.defAnalysisGroupMC(numGroup,nm,relPath=relPath, 
                                       ppRoot=runBase,imageDir=None,
                                       fileRoot=None,
                                       impreprocessFunc=impreprocessFunc,
                                       impreprocessFuncKwds=impreprocessFuncKwds)
    else:
        runDir = os.path.join(runBase,relPath)
        pickleName = os.path.join(runDir,nm)
        ans = analysis.defAnalysisGroup(numGroup,'exp',imageDir,runDir)
    #
    ans.setDefFiles(dfs)
    ans.setUndefFiles(udf)
    ans.createRunObjs()
    ans.setppAttrForAll(**pppars)
    ans.setnpAttr(**nppars)
    ans.setppAttrfromList_ind(argsList = shiftList, kwdsList=None)
    ans.setppAttrForAll_ind(argsList =None , kwdsList=newgrids)
    ans.createAllFiles() # recreate files

    if showGrid:
        ans.showGrid()
    if analyze:
        if MC:
            ans.fillQueue(emptyQ=True, queueKwds=queueKwds)
            ans.setWorkers(workers=workers,nameServerCoords=nameServerCoords)
            # process all
            ans.runAllN()
            # bring .rawdata results back to local directory
            ans.transferResults()
            pickleName = os.path.join(ans.ppDir,nm)
            p = ans.checkStore(enforceStore=True, colDict = rawDataColDict, 
                           pointSize=1,fontsize=8, drawCon=False, 
                           selfPickleName= pickleName,
                           suppressShowProblem=True,runMode=2,**kwds)
        else:
            ans.runCheckStore(enforceStore=True, colDict = rawDataColDict, 
                          pointSize=1,fontsize=8, drawCon=False, 
                          selfPickleName= pickleName,
                          suppressShowProblem=True,runMode=2,
                          impreprocessFunc=impreprocessFunc,
                          impreprocessFuncKwds=impreprocessFuncKwds,
                          numProc=numProc,**kwds)
    return

def entireProcess(exp,mode,loadDefList, runBase,relPath, GRIDNO,fgrid, 
                  justpreprocess=False,shiftCorrespondence='noShift',**kwds):
    runDir = os.path.join(runBase,relPath)
    udf, dfs, modREFNO = retFile(runDir,GRIDNO,exp,mode,loadDefList,
            shiftCorrespondence=shiftCorrespondence,**kwds)
    preOut = preprocessShift(runDir,GRIDNO=GRIDNO,udf=udf,dfs=dfs,
                             shiftCorrespondence=shiftCorrespondence,
                             fgrid=fgrid,**kwds)
    shiftList , grids, centers, newgrids = preOut
    if not justpreprocess:
        start_time = time.time()
        ans= analyze(runBase,relPath, GRIDNO, udf=udf,dfs=dfs,modREFNO=modREFNO,
                     shiftList=shiftList,newgrids=newgrids,
                     shiftCorrespondence=shiftCorrespondence, **kwds)
        print "analysis time is %s seconds"%(time.time()-start_time)
        
    else:
        ans=None
    os.system("paplay /usr/share/sounds/Oxygen-Im-Network-Problems.ogg")
    return ans
