import pylab
import numpy as np

def smartsplit(Ntotal, Nsections):
    """
    idea copied from numpy.array_split
    Allows nonuniform division but maximizes uniformity of division points by a 
    clever trick of distributing the excess.
    """
    Nsections = int(Nsections)
    if Nsections <= 0:
        raise ValueError('number sections must be larger than 0.')
    Neach_section, extras = divmod(Ntotal, Nsections)
    section_sizes = np.array([0]+extras * [Neach_section+1] + 
                               (Nsections-extras) * [Neach_section])
    
    div_points = section_sizes.cumsum()
    # trimming to section sizes and start points    
    return (section_sizes[1:] , div_points[:-1])

def nMultiGrid(numzone,limits, delta_x, delta_y, **kwds):
    """
    numzone = (numzonex,numzoney) number of zones to split the grid to
    limits = left, right, top, bottom in pixels of the overall grid
    delta_x, delta_y = grid pixel spacings in x and y
    kwds: passed on to makeGrid. Most important is gridtype
    #
    This is a multigrid that works the same as before when equal splitting
    was enforced. Gives a decent nonuniform splitting with no small remainder
    islands. 
    """
    left, right, top, bottom = limits
    numzonex, numzoney = numzone
    xspan, yspan =  right-left, bottom-top
    assert xspan % delta_x == 0
    assert yspan % delta_y == 0
    #        
    totnumgridPts_x = xspan/delta_x + 1
    totnumgridPts_y = yspan/delta_y + 1
    #
    xSizes , xCum = smartsplit(totnumgridPts_x, numzonex)
    xstgrid = left + xCum * delta_x
    ySizes , yCum = smartsplit(totnumgridPts_y, numzoney)
    ystgrid = top  + yCum * delta_y
    #
    grids, centers = [] , [] 
    for j in range(numzoney):
        numrow = ySizes[j] 
        for i in range(numzonex):
            numcol = xSizes[i]
            grids.append( MakeGrid(xstgrid[i],ystgrid[j], delta_x, delta_y, numcol, numrow, **kwds) )
            centers.append( (xstgrid[i] + numcol/2 * delta_x ,  ystgrid[j] + numrow/2 * delta_y) )
    return (grids, centers)    
    
def MultiGrid(numzone, limits , delta_x, delta_y, **kwds):
    left, right, top, bottom = limits
    numzonex, numzoney = numzone
    xspan, yspan =  right-left, bottom-top
    assert xspan % delta_x == 0
    assert yspan % delta_y == 0
    #assuming first and last points are at the limit
    totnumgridPts_x = xspan/delta_x + 1
    totnumgridPts_y = yspan/delta_y + 1
    assert totnumgridPts_x % numzonex==0
    assert totnumgridPts_y % numzoney==0
    zonenumgridPts_x = totnumgridPts_x / numzonex
    zonenumgridPts_y = totnumgridPts_y / numzoney
    numcol = zonenumgridPts_x
    numrow = zonenumgridPts_y
    # make the starting points grid
    xstgrid, ystgrid = np.meshgrid(range(numzonex), range(numzoney))
    xstgrid *= (numcol*delta_x)
    ystgrid *= (numrow*delta_y)
    xstgrid += left
    ystgrid += top
    #
    grids, centers = [] , [] 
    for i in range(numzonex):
        for j in range(numzoney):
            grids.append( MakeGrid(xstgrid[i,j],ystgrid[i,j], delta_x, delta_y, numcol, numrow, **kwds) )
            centers.append( (xstgrid[i,j] + numcol/2 * delta_x ,  ystgrid[i,j] + numrow/2 * delta_y) )
    return (grids, centers)

#written by NS 
#for manual gridding (group shape)
def ManGrds(gridlist,limits,**kwds):
    l=len(limits)
    D=np.asarray(gridlist[0])
    manGridLst=[]
    Cntrs=[]
    for i in range(l):
        G=D[(D[:,0]>limits[i][0]-1) & (D[:,0]<limits[i][1]+1) & (D[:,1]>limits[i][2]-1) & (D[:,1]<limits[i][3]+1)]
        Gaslist=G.tolist()
        assert (limits[i][1]-limits[i][0])%2==0
        assert (limits[i][3]-limits[i][2])%2==0
        centers=((limits[i][1]+limits[i][0])/2,(limits[i][3]+limits[i][2])/2)
        Cntrs.append(centers)
        manGridLst.append(Gaslist)
    return manGridLst,Cntrs
        
def MultiGrid2(numzone, limits , delta_x, delta_y,tol=1.e-6, **kwds):
    left, right, top, bottom = limits
    numzonex, numzoney = numzone
    numzonex_fractions = np.arange(numzonex+1).astype(float)/float(numzonex)
    numzoney_fractions = np.arange(numzoney+1).astype(float)/float(numzoney)
    xspan, yspan =  right-left, bottom-top
    xspanLimZone = numzonex_fractions * xspan + left
    yspanLimZone = numzoney_fractions * yspan + top
    xspanLimZone[-1] += 2*tol
    yspanLimZone[-1] += 2*tol
    assert xspan % delta_x == 0
    assert yspan % delta_y == 0
    #assuming first and last points are at the limit
    totnumgridPts_x = xspan/delta_x + 1
    totnumgridPts_y = yspan/delta_y + 1
    bigGrid = MakeGrid(left,top, delta_x, delta_y, totnumgridPts_x,totnumgridPts_y,gridtype='rowwise')
    bigGrid = np.array(bigGrid)
    grids, centers = [] , [] 
    for j in range(numzoney):
        for i in range(numzonex):
            xmin, xmax = xspanLimZone[i],xspanLimZone[i+1] 
            # worry about less than causing trouble
            ymin, ymax = yspanLimZone[j],yspanLimZone[j+1] 
            cond1 = bigGrid[:,0] > (xmin-tol)
            cond2 = bigGrid[:,0] < (xmax-tol)
            cond3 = bigGrid[:,1] > (ymin-tol)
            cond4 = bigGrid[:,1] < (ymax-tol)
            And=np.logical_and
            fincond = And(cond1, And(cond2, And(cond3,cond4)))
            zonePts = bigGrid[fincond,:]
            minzonex = zonePts[:,0].min()
            maxzonex = zonePts[:,0].max()
            numcol = int(round((maxzonex-minzonex)/delta_x)) + 1
            minzoney = zonePts[:,1].min()
            maxzoney = zonePts[:,1].max()
            numrow = int(round((maxzoney-minzoney)/delta_y)) + 1
            grids.append(MakeGrid(minzonex,minzoney, delta_x, delta_y, numcol, numrow,**kwds))
            centers.append( ( minzonex + numcol/2 * delta_x ,  minzoney + numrow/2 * delta_y) )
            #
    return (grids, centers)

def MakeGrid(y0,z0,delta_y,delta_z,numcolumn,numrow,gridtype='snakeUD',
             showGrid=False,verbose=False,reverseList=False,**kwds):
    """
    gridtype:
    rowwise = makes a carriage return at the end of each row, left right, left right op
    columnwise = column by column, returns to top for each
    snakeLR = first row left right, second row right left, etc...
    snakeUD = first column down, second column up, etc.
    RETURNS: a list of grids
    """
    w = np.arange(y0,y0+delta_y*numcolumn,delta_y)
    q = np.arange(z0,z0+delta_z*numrow,delta_z)
    x,y = np.meshgrid(w,q)
    if gridtype == 'rowwise':
        gridList = np.vstack((x.flatten(), y.flatten()))
    elif gridtype == 'columnwise':
        gridList = np.vstack((x.T.flatten(), y.T.flatten()))
    elif gridtype == 'snakeLR':
        toflip = np.arange(numrow/2)* 2 + 1
        for j in toflip:
            temp = np.atleast_2d(x[j,:].copy())
            x[j,:]=np.fliplr(temp)
        if verbose: print x
        gridList = np.vstack((x.flatten(), y.flatten()))
    elif gridtype == 'snakeUD':
        toflip = np.arange(numcolumn/2)* 2 + 1
        for j in toflip:
            temp=np.atleast_2d(y[:,j].copy())
            y[:,j]=np.fliplr(temp)
        if verbose: print y
        gridList = np.vstack((x.T.flatten(), y.T.flatten()))
    
    else:
        raise ValueError, 'unsupported grid type'
    if showGrid:
        pylab.plot(gridList[0,:],gridList[1,:],'o-')
        pylab.show()
    if reverseList:
        for v in range(len(gridList)):
            gridList[v]=gridList[v][::-1]
    
    return gridList.T.tolist()                           
               
def addGridSketches(ax,grids):
    # if a single grid is sent, still operate
    try: 
        grids[0][0][0]
    except IndexError: 
        gr = np.array(grids)
        ax.plot(gr[:,0],gr[:,1],'o-')
    else:
        for grid in grids:
            gr = np.array(grid)
            ax.plot(gr[:,0],gr[:,1],'o-')
    return



if __name__=='__main__':
    if 0:
        y0=0
        numcolumn=4
        delta_y=2
        z0=0
        numrow=7
        delta_z=2
        pylab.figure(1)
        print MakeGrid(y0,z0,delta_y,delta_z,numcolumn,numrow,gridtype='rowwise',showGrid=True)
        pylab.figure(2)
        print MakeGrid(y0,z0,delta_y,delta_z,numcolumn,numrow,gridtype='columnwise',showGrid=True)
        pylab.figure(3)
        print MakeGrid(y0,z0,delta_y,delta_z,numcolumn,numrow,gridtype='snakeLR',showGrid=True)
        pylab.figure(4)
        print MakeGrid(y0,z0,delta_y,delta_z,numcolumn,numrow,gridtype='snakeUD',showGrid=True)
        pylab.show()
    if 1:
        #gr = MakeGrid(180,180, 10,10,210,170,gridtype='snakeLR',showGrid=False)
        u=190
        grids, centers = MultiGrid((8,8), (u, 2450-u, u, 2050-u) , 10, 10)
        ngrids, ncenters = nMultiGrid((8,8), (u, 2450-u, u, 2050-u) , 10, 10)
        u = 180
        ngrids, ncenters = nMultiGrid((8,8), (u, 2450-u, u, 2050-u) , 10, 10)
        #grids2, centers2 = MultiGrid2((2,2), (180, 2450-180, 180, 2050-180) , 10, 10)
        #grids3, centers3 = MultiGrid2((4,4), (180, 2450-180, 180, 2050-180) , 10, 10)
