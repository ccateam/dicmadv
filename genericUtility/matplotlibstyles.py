# SEP 2010
#
#
from scipy import array
import copy
import pylab
import matplotlib.pyplot as plt

gray=array([0.5,0.5,0.5])
black='k'
red='r'
green='g'
blue='b'
cyan='c'
magenta='m'
purple='purple'
orange='orange'

styles1= [{'color':black,'marker':'o','markerfacecolor':'None',
'markeredgecolor':black,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':blue,'marker':'s','markerfacecolor':'None','markeredgecolor':blue,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':red,'marker':'d','markerfacecolor':'None','markeredgecolor':red,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':green,'marker':'^','markerfacecolor':'None','markeredgecolor':green,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':cyan,'marker':'h','markerfacecolor':'None','markeredgecolor':cyan,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':magenta,'marker':'<','markerfacecolor':'None','markeredgecolor':magenta,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':orange,'marker':'v','markerfacecolor':'None','markeredgecolor':orange,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':purple,'marker':'o','markerfacecolor':'None','markeredgecolor':purple,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':black,'marker':'s','markerfacecolor':'None',
'markeredgecolor':black,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':blue,'marker':'d','markerfacecolor':'None','markeredgecolor':blue,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':red,'marker':'^','markerfacecolor':'None','markeredgecolor':red,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':green,'marker':'h','markerfacecolor':'None','markeredgecolor':green,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':cyan,'marker':'<','markerfacecolor':'None','markeredgecolor':cyan,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':magenta,'marker':'v','markerfacecolor':'None','markeredgecolor':magenta,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':orange,'marker':'o','markerfacecolor':'None','markeredgecolor':orange,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':purple,'marker':'h','markerfacecolor':'None','markeredgecolor':purple,'linestyle':'-','markersize':10,'linewidth':1.},
         ]

styles2 = [{'color':black,'marker':'o','markerfacecolor':'None',
'markeredgecolor':black,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':blue,'marker':'s','markerfacecolor':'None','markeredgecolor':blue,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':red,'marker':'d','markerfacecolor':'None','markeredgecolor':red,'linestyle':'-','markersize':10,'linewidth':1.},
          {'color':green,'marker':'^','markerfacecolor':'None','markeredgecolor':green,'linestyle':'-','markersize':10,'linewidth':1.},
           ]

#no marker,black
styles3=[{'color':black,'linestyle':'-','linewidth':1.},{'color':black,'linestyle':'--','linewidth':1.},{'color':black,'linestyle':'.','linewidth':1.}]

def markerset(markers=['o','s','d','^','x','>','x','+'],baseDict={'color':black,'markerfacecolor':'w',
'markeredgecolor':black,'linestyle':'-','markersize':6,'linewidth':.5}):
    sty=[]
    for m in markers:
        k = baseDict.copy()
        k['marker']=m
        sty.append(k)
    return sty
        
def dash(sty):
    sty2=copy.copy(sty)
    for d in sty2:
        d['linestyle']='--'
    return sty2

def emptyMarker(sty):
    sty2=copy.copy(sty)
    for d in sty2:
        c = d['color']
        d['markerfacecolor']='None'
        d['markeredgecolor']=c
    return sty2

def fullMarker(sty):
    sty2=copy.copy(sty)
    for d in sty2:
        c = d['color']
        d['markerfacecolor']=c
        d['markeredgecolor']=c
    return sty2

def resizeMarker(sty,ratio):
    sty2=copy.copy(sty)
    for d in sty2:
        c = d['markersize']
        d['markersize']=ratio*c
    return sty2


def tripleAxes(xsize=7.,lsx=0.08,esx=0.02,lsy2lsx=1, esy2esx=1, fsy2fsx=None ,ysize=None , dpi = 120, savedpi=600):
    """
    makes 1x3 square plots
    lsx, esx = label, extra spacing in figure coords in width dir. (fraction of xsize)
    """
    
    nrow, ncol=1, 3 # just coding in generalities in case I like to generalize it.
    fsx = (1. - ncol*lsx - ncol*esx)/ncol # in frame coords
    
    if ysize==None: 
        ysize = nrow*(fsx*xsize) + nrow*(lsx*xsize) + nrow*(esx*xsize)
        fsy2fsxauto=1
    else:
        yysize = nrow*(fsx*xsize) + nrow*(lsx*xsize) + nrow*(esx*xsize)
        fsy2fsxauto = ysize/yysize
    #
    if fsy2fsx==None:
        fsy2fsx=fsy2fsxauto
        
        

    fig = plt.figure(figsize=(xsize,ysize),dpi=dpi)
    aspect = xsize/ysize
    lsy, esy, fsy = lsx*aspect*lsy2lsx, esx*aspect*esy2esx, fsx*aspect*fsy2fsx 
    axes=[]
    #
    for i in range(1,nrow+1): #looped starting from 1 in this case for easier coding
        for j in range(1,ncol+1): #looped starting from 1 in this case for easier coding
            x = j*lsx + (j-1)*esx + (j-1)*fsx
            y = i*lsy + (i-1)*esy + (i-1)*fsy
            ax = fig.add_axes([x,y,fsx,fsy])
            print [x,y,fsx,fsy]
            #ax.plot([1,2,3]) just checking, remove.
            axes.append(ax)
    return axes


def horAxes(ncol=3,xsize=7.,lsx=0.08,esx=0.02,lsy2lsx=1, esy2esx=1, fsy2fsx=None ,ysize=None , dpi = 120, savedpi=600):
    nrow, ncol=1, ncol # just coding in generalities in case I like to generalize it.
    fsx = (1. - ncol*lsx - ncol*esx)/ncol # in frame coords
    
    if ysize==None: 
        ysize = nrow*(fsx*xsize) + nrow*(lsx*xsize) + nrow*(esx*xsize)
        fsy2fsxauto=1
    else:
        yysize = nrow*(fsx*xsize) + nrow*(lsx*xsize) + nrow*(esx*xsize)
        fsy2fsxauto = ysize/yysize
    #
    if fsy2fsx==None:
        fsy2fsx=fsy2fsxauto
        
        

    fig = pylab.figure(figsize=(xsize,ysize),dpi=dpi)
    aspect = xsize/ysize
    lsy, esy, fsy = lsx*aspect*lsy2lsx, esx*aspect*esy2esx, fsx*aspect*fsy2fsx 
    axes=[]
    #
    for i in range(1,nrow+1): #looped starting from 1 in this case for easier coding
        for j in range(1,ncol+1): #looped starting from 1 in this case for easier coding
            x = j*lsx + (j-1)*esx + (j-1)*fsx
            y = i*lsy + (i-1)*esy + (i-1)*fsy
            ax = fig.add_axes([x,y,fsx,fsy])
            print [x,y,fsx,fsy]
            #ax.plot([1,2,3]) just checking, remove.
            axes.append(ax)
    return axes

def mulAxes(ncol=3,nrow=2,skipFrames=[],xsize=7.,lsx=0.08,esx=0.02,lsy2lsx=1, esy2esx=1, fsy2fsx=None ,ysize=None ,extra=[0,0,0,0], dpi = 240, savedpi=600):
    """
    xsize=size of the 
    lsx, esx = label, extra spacing in figure coords in width dir. (fraction of xsize)
    extra = extraL, extraR,extraB, extraT in normalized fig coord
    """
    extraL, extraR,extraB, extraT= array(extra,float)
    
    fsx = (1. - ncol*lsx - ncol*esx-extraL-extraR)/ncol # in frame coords
    
    if ysize==None: 
        ysize = nrow*(fsx*xsize) + nrow*(lsx*xsize) + nrow*(esx*xsize)
        fsy2fsxauto=1
    else:
        yysize = nrow*(fsx*xsize) + nrow*(lsx*xsize) + nrow*(esx*xsize)
        fsy2fsxauto = ysize/yysize
    #
    if fsy2fsx==None:
        fsy2fsx=fsy2fsxauto
        
        
    
    
    fig = pylab.figure(figsize=(xsize , ysize),dpi=dpi)
    # redefine extras normalized for placement work

    aspect = xsize/ysize
    lsy, esy, fsy = lsx*aspect*lsy2lsx, esx*aspect*esy2esx, fsx*aspect*fsy2fsx 
    axes=[]
    #
    count=0
    for i in range(1,nrow+1): #looped starting from 1 in this case for easier coding
        for j in range(1,ncol+1): #looped starting from 1 in this case for easier coding
            if (i,j) in skipFrames: continue
            x = extraL + j*lsx + (j-1)*esx + (j-1)*fsx
            y = extraB + i*lsy + (i-1)*esy + (i-1)*fsy
            ax = fig.add_axes([x,y,fsx,fsy])
            print [x,y,fsx,fsy]
            #ax.plot([1,2,3]) just checking, remove.
            axes.append(ax)
            count +=1
    return axes

def yieldDefaultAxis(**kwds):
    """automates new figure addition and returns the axis that comes with it"""
    fig = plt.figure(**kwds)
    ax= plt.gca()
    return fig ,ax


if __name__=="__main__":
    #tripleAxes(xsize=6.5,lsy2lsx=0.8,ysize=4.)
    ax1,ax2 = horAxes(ncol=2,xsize=6.5,lsy2lsx=0.8,ysize=4.)
    
    pylab.show()
