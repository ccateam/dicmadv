#TODO: Most todo's here are naturally finding out what I did was already a function in
# Numeric scipy or built-in
from scipy import *
arrayType = type(array([1,]))
def isincreasing(vec):
    vec = ravel(vec)
    if not less_equal(diff(vec),0.):
        boo=1
    else:
        boo=0
    return boo
def isnonnegative(arr):
    arr = ravel(arr)
    boo = 1
    for val in arr:
        if val < 0.:
            boo=0
            break
    return boo
def ispositive(arr):
    arr = ravel(arr)
    boo=1
    for val in arr:
        if val <= 0.:
            boo=0
            break
    return boo

def makemonoton(mat,colnum=0,increasing=1):
    '''with recursive removal makes a coloumn of a matrix monotonous, increasing=1
    is for increasing direction =0 is extremely stupid and will help only arrays with scarse
    nonmonotonous points at low jumping values otherwise will knock off too many points. The
    routine does not even know to get rid of which point to preserve maximum number of
    points but does the elimination in a fixed direction. The problem with making something
    sound is time and that very complicated conditions may arise in this seemingly simple
    problem.'''
    if increasing:
        checkfunc = isincreasing
        compfunc = greater
    else :
        checkfunc = isdecreasing
        compfunc = less
    if isarrayD(mat,1):
        mat=vec2colmat(mat)
    elif isarrayD(mat,2):
        pass
    else:
        raise 'makemonoton is intended for arrays of dim 1 and 2.'
    notdone=1
    vec2=mat[:,colnum]
    inilen=len(vec2)
    while notdone:
        cond = compfunc(diff(vec2),0.)
        mat = compress(cond,mat,dimension=0) # does it along the coloumn direction
        vec2=mat[:,colnum]
        if checkfunc(vec2):
            notdone = 0
    print 'Lost %s points'%((inilen-len(vec2)),)
    return mat
    
def mytrim(mat,range,colnum=0):

    '''Improvements will be necessary, expects a monotonous vector for the cond
    coloumn whose defaults is colnum.'''
    a = range[0]
    b = range[1]
    vec = mat[:,colnum]
    if vec[-1]-vec[0] > 0 :
        increasing =1 #assumes monotonous
    elif vec[-1]-vec[0] < 0 :
        increasing = 0
    else :
        raise "Constant vector, don't know how to handle"    
    if a < b and increasing :
        cond = greater_equal(vec,a) * less(vec,b)
    if a > b and not increasing: #means decreasing here
        cond = less_equal(vec,a) * greater(vec,b)    

    condMat = compress(cond,mat,axis=0)
    return condMat
    
def isdecreasing(vec):
    vec = ravel(vec)
    if not greater_equal(diff(vec),0):
        boo=1
    else:
        boo=0
    return boo
def vec2rowmat(vec):
    if isarrayD(vec,1):
        mat= atleast_2d(vec)
    elif isscalar(vec) :
        raise 'vec2rowmat expects a vector.'
    elif isarrayD(vec,2):
        mat=vec #return itself
    return mat
def vec2colmat(vec):
    if isarrayD(vec,1):
        mat= transpose(atleast_2d(vec))
    elif isscalar(vec) :
        raise 'vec2rowmat expects a vector.'
    elif isarrayD(vec,2):
        mat=vec #return itself
    return mat

def dict2matrix(dict,keylist='all'):
    '''converts a dictinary whose items are equal length vector arrays
    into a matrix with columnar merging. If keylist is provided only the keys
    in that list is considered'''
    lengthList=[]
    mat=[]
    if keylist=='all':
        keylist=dict.keys()
    for key in keylist:
        item = dict[key]
        if type(item) != arrayType:
            raise 'Dictionary item correspoding to key %s is not an array!'%(key,)
        #TODO: check 1d
        ll = len(item)
        lengthList.append(ll)
        if ll not in lengthList: #somewhat arcane way to ensure same dimensions
            raise 'Array lengths in the dictionary are not the same.'
        col = reshape(item,(ll,1))
        if mat ==[]:
            mat = c_[col] #initialize
        else:
            mat = c_[mat,col]
    return mat
def subOnce(s,n,r,ignorecase=0):
    '''s is the string, n is the name indicated by $ in the string and r is the
    replacement string. This copies the new Template module under string in 
    function but probably is much poorer by speed and exceptions. Though
    string.replace would probably work fine I made it in re just to extend it
    in the future maybe.'''
    import re
    if ignorecase:
        p = re.compile(r"\$"+n,re.IGNORECASE)
    else:
        p = re.compile(r"\$"+n)
    snew,count = re.subn(p,n,s)
    if count == 0:
        raise 'You have no %s to replace with %s'%("\$"+n,'r')
    elif count !=1 :
        raise 'You have more than 1 (exactly %s) replacements for %s. Did you intend it?'%(count,'\$'+n)
def getStr(fileName):
    '''read opens,gets the entire string in a file and closes the file returning the string'''
    fid=open(fileName,'r')
    str=fid.read()
    fid.close()
    return str
def putStr(fileName, str):
    '''write opens, puts the string and closes the file'''
    fid = open(fileName, 'w')
    fid.write(str)
    fid.close()
def flatten(matrix):
    '''ravels cells as well'''
    if iscell(matrix):
        cellRavel = ac(ravel,1)
        raveledCell = cellRavel(matrix)
        ravelAll=array([])
        for arr in raveledCell:
            ravelAll = r_[ravelAll,arr]
    else:
        ravelAll = ravel(matrix)
    return ravelAll
def vector2matrix(vector,numCols,axis=1):
    '''Note this is a very particular operation but sounds general'''
    #Careful it will flatten it out if you send a matrix instead of a vector.
    vector=reshape(vector,(size(vector),1))
    #consider resizing and transposing instead
    for i in range(numCols):
        if i==0:
            mat=vector
        else :
            mat=c_[mat,vector]
    return mat
def baseLineRow1(mat):
    sh = shape(mat)
    nR, nC =sh[0],sh[1]
    dim = len(shape(mat))
    if dim > 2:
        raise 'This routine is intended for 2-D matrices or arrays!'
    if dim==1:
        mat=mat-mat[0]
    if dim==2:
        tempmat=resize(mat[0,:],(nR,nC))
        mat=mat-tempmat
    return mat
class cell(list):
    '''cell is essentially a list that distributes the common operations
    to its constituents which are arrays.'''
    def __init__(self,matrixList=[]):
        list.__init__(self)
        for mat in matrixList:
            if isarray(mat):
                self.append(mat)
            else:
                raise 'Item(s) in initiating list are not arrays'
    def __add__(self,other):
        sum = self.__class__()
        for selfi,otheri in zip(self, other):
            sum.append(selfi + otheri)
        return sum
    def __sub__(self,other):
        sub= self.__class__()
        for selfi,otheri in zip(self, other):
            sub.append(selfi - otheri)
        return sub
    def __mul__(self,other):
        mul= self.__class__()
        for selfi,otheri in zip(self, other):
            mul.append(selfi * otheri)
        return mul
    def __div__(self,other):
        div= self.__class__()
        for selfi,otheri in zip(self, other):
            div.append(selfi / otheri)
        return div
    def shape(self):
        sh=self.__class__()
        for selfi in self:
            sh.append(shape(selfi))
        return sh
#
cellType = type(cell())
#

def sumsum(arr):
    '''squares and sums all the elements of an array regardless of its dimension
    if cell it will return the sum of all such sums for the matrices in the cell.'''
    def itersum(array):
        sqArray = array * array #elementwise squared
        summed = 0.
        scalar=0 #even if it is scalar it will be taken care of right inside
        summedArray=sqArray
        while not scalar:
            if type(summedArray) == arrayType:
                scalar=0
            else:
                scalar=1
            summedArray = sum(summedArray)
        return summedArray #at this stage it should be a scalar
    summed=itersum(array=arr)
    return summed
def iscell(cel):
    cellType = type(cell())
    if type(cel) == cellType:
        boo=1
    else:
        boo=0
    return boo
def isarray(arr):
    if type(arr) == arrayType:
        boo=1
    else:
        boo=0
    return boo
def isarrayD(arr,dim):
    if isarray(arr):
        actualDim = len(shape(arr))
        if actualDim==dim:
            boo=1
        else:
            boo=0
    else:
        boo=0
    return boo
def isfloat(fl):
    if type(fl)==floatType:
        boo=1
    else:
        boo=0
    return boo
def typeCheck(obj,(fnlist)):
    '''or kind of type checking'''
    boo=0
    for fn in fnlist:
        if fn.__call__(obj):
            boo=1
            break
    return boo
def cellularize(matrixfunction):
    '''takes a function that works on matrices and makes it work on cells,
    returns cells of matrix results. '''
    class cellfunc:
        def __init__(self, matfunc):
            self.matfunc = matfunc
        def __call__(self,cel,*args,**kwargs):
            res = cell()
            for mat in cel:
                try:
                    i = self.matfunc(mat,*args,**kwargs)
                except NameError:
                    raise 'Are you sure there is a function %s that works on matrices?'%(matfunc,)
                res.append(i)
            return res
    func = cellfunc(matfunc = matrixfunction)
    return func
def aoc(func,mainarg,*arg,**kwargs):
    '''stands for array or cell, first tries the input for the array function,
    if it can't find it tries it for the cellularized version. You might thing
    a simple check on the mainarg (whether it is a cell or something might be enough.
    But not so for ones for example where you don't have to send a list.'''
    try:
        res = func(mainarg,*arg,**kwargs)
    except TypeError: #The problem is shape will not even raise anything at times and do something improper
        try :
            funcC = cellularize(func)
            res = funcC(mainarg,*arg,**kwargs)
        except TypeError:
            print 'This function cannot be handled with this input regularly or after cellularizing.'
            raise TypeError
    return res
def ac(func,callCellularize=1,*arg,**kwargs):
    '''stands for array/cell, you have to send in what to do. More robust if you know it.'''
    if callCellularize:
        f = cellularize(func)
    else:
        f=func
    return f

######################################################
# INTERPOLATION
######################################################

class reversibleinterp:
    def __init__(self,interpfunc):
        self.func = interpfunc
    def __call__(self,X1,Y1,X2):
        if isincreasing(X1):
            pass
        elif isdecreasing(X1): #extending the ability to monotonous but decreasing seqs
            X1 = -X1
            X2 = -X2
        return self.func(X1,Y1,X2)
class interp1or2d:
    '''The interpfunc should be X1,Y1,X2 admitting, unfortunately, there is no
    common part in how X1,Y1 is handled to get a class instance or tuple or something'''
    def __init__(self,interpfunc):
        self.func1d = reversibleinterp(interpfunc)
    def __call__(self, X1,Y1,X2):
        type = self.typeInterp(X1,Y1,X2)
        if type=='1d':
            Y2 = self.func1d(X1,Y1,X2)
        elif type=='2d':
            sh1=shape(X1)
            sh2=shape(X2)
            nRows,nCols = sh1[0],sh1[1]
            nRows2,nCols2 = sh2[0],sh2[1]    
            Y2 = zeros((nRows2,0),'d')
            for i in range(nCols):
                row2 = self.func1d(X1[:,i],Y1[:,i],X2[:,i])
                col2 = reshape(row2,(nRows2,1)) #make it a coloumn
                Y2=hstack((Y2,col2))
        return Y2
    def typeInterp(self,X1,Y1,X2):
        if shape(X1) != shape(Y1):
            raise ValueError
        if isarrayD(X1,1) and isarrayD(X2,1) and isarrayD(Y1,1):
            argType = '1d'
        elif isarrayD(X1,2) and isarrayD(X2,2) and isarrayD(Y1,2):
            if size(X1,-1) != size(X2, -1):
                raise ValueError
            argType = '2d'
        else:
            raise ValueError
        return argType
class interpGen:
    '''handles cells as well'''
    def __init__(self,interpfunc):
        self.func1or2d = interp1or2d(interpfunc)
    def __call__(self,X1,Y1,X2):
        type = self.typeInterp(X1,Y1,X2)
        if type =='arr':
            Y2=self.func1or2d(X1,Y1,X2)
        elif type == 'cell':
            
            Y2=cell()
            for x1,y1,x2 in zip(X1,Y1,X2):
                Y2.append(self.func1or2d(x1,y1,x2))
        return Y2
    def typeInterp(self,X1,Y1,X2):
        if isarray(X1) and isarray(X2) and isarray(Y1):
            argType = 'arr'
        elif iscell(X1) and iscell(X2) and iscell(Y1):
            argType = 'cell'
        else:
            raise ValueError
        return argType
def linterp1d(X1,Y1,X2,**args):
    '''scipy's interpolate.interp1d class application as a function limited to
    1d X1 and Y1 and X2'''
    X1=ravel(X1)
    Y1=ravel(Y1)
    X2=ravel(X2)
    f=interpolate.interp1d(X1,Y1,**args)
    return f(X2)
def splinterp1d(X1,Y1,X2,**args):
    '''scipy's splrep and splev application as a function limited to 1d arrays
    with default options for splev (der=0)'''
    X1=ravel(X1)
    Y1=ravel(Y1)
    X2=ravel(X2)
    tck=interpolate.splrep(X1,Y1,**args)
    return interpolate.splev(X2,tck)

if __name__== '__main__':
    #Interpolation checks
    x=arange(6.)
    xm=transpose([x,x,x])
    xcell=cell((xm,xm,x))

    y=pow(x,2)
    ym=transpose([y,y,y])
    ycell=cell((ym,ym,y))
    
    x2=arange(1.5,4.5,.5)
    x2m=transpose([x2,x2,x2])
    x2cell=cell((x2m,x2m,x2))

    #print linterp1d(x,y,x2)
    Linterp1or2d=interp1or2d(linterp1d)
    #print Linterp1or2d(-x,y,-x2) #showing the decreasing monotonous capability
    #print Linterp1or2d(xm,ym,x2m) #showing 2d capability
    #print Linterp1or2d(-xm,ym,-x2m) #showing both
    LinterpGen = interpGen(linterp1d)
    print LinterpGen(x,y,x2)
    print LinterpGen(xm,ym,x2m)
    print LinterpGen(xcell,ycell,x2cell)
    splGen = interpGen(splinterp1d)
    print splGen(x,y,x2)
    print splGen(xm,ym,x2m)
    print splGen(xcell,ycell,x2cell)
