#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 11:21:28 2019

@author: can
"""
import os
import tables, shutil

join = os.path.join

def f(loadNo,expNo,mode='microTop',tablePath=os.environ['DICDBPTH']):
    #
    fid = tables.open_file(tablePath, mode='r')
    imTable = fid.root.exprGroup.exprDataTable    
    rows = [
    x.nrow for x in imTable.iterrows() if
    x['expNo']==expNo
    and x['imagingSet']==mode
    and x['loadPt']==loadNo
    and x['fileName'][-4:]=='.tif'
        ]
    gridNos =[x['gridNo'] for x in imTable.iterrows() if x.nrow in rows] 
    files = [x['fileName'] for x in imTable.iterrows() if x.nrow in rows]
    stagePositions = [x['stagePos'] for x in imTable.iterrows() if x.nrow in rows]
    optics = [x['optics'] for x in imTable.iterrows() if x.nrow in rows]
    fid.close()
    return files,gridNos,stagePositions,optics

def g(expNo,tablePath=os.environ['DICDBPTH']):
    """ query tool for image copy for NAS to local image directory"""
    fid = tables.open_file(tablePath, mode='r')
    imTable = fid.root.exprGroup.exprDataTable    
    rows = [
    x.nrow for x in imTable.iterrows() if
    x['expNo']==expNo
        ]
    files = [x['fileName'] for x in imTable.iterrows() if x.nrow in rows]
    return files

def copyImages_NAS2local(expNo,NASdir=os.environ['DICIMAGEPTH'],
                         localdir=os.environ['DICIMAGEPTH_LOCAL'], **kwds):
    """
    expNo =  experiment number to be copied; it makes sense to copy files by
             experiment and not bloat the local directory. It also makes sense
             to clean it from time to time.
    """
    files =  g(expNo=expNo, **kwds)
    for fil in files:
        src = join(NASdir,fil)
        dest = join(localdir,fil)
        shutil.copy(src,dest)
    
    return 



if __name__=="__main__":
    # copyImages_NAS2local(27)
    f(4, 27, 'macroTop')