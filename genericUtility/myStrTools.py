import os
################################################################################################
def castList(attTypes):
    '''attTypes is a list of string that contains elements like s i or f. Returns a list of operators str float int to cast any other list into correct type from string.'''
    attOps = []
    for item in attTypes:
        if item=='s' or item=='str':
            attOps.append(str)
        elif item=='i' or item=='int':
            attOps.append(int)
        elif item=='f' or item=='float':
            attOps.append(float)
        else:
            raise 'The list contains string that is not associated with any type conversion string.'
    return attOps
################################################################################################
def fileNames(basedir,basename,numList,fillzeros=0,replaceStr='XXX'):
    """fileNames(basedir,basename,numList,fillzeros=0,replaceStr='XXX')
    basedir is the basedir, you may not include the slash(es) at the end
    basename is the name of the file with a replaceStr identifier somewhere to be
    replaced with. numList is the list of numbers to do this for. fillzeros is to precede
    the numbers with some number of zeros to reach a fixed number string length. For example
    fillzeros=4 will result in 5 to be cast into 0005 and 46 into 0046. replaceStr was already
    defined and defaults to XXX."""
    def addzeros(num,fillzeros):
        zeros='0'*fillzeros
        s=str(num)
        numzeros = fillzeros - len(s)
        if numzeros > 0:
            return zeros[:numzeros] + s
        else:
            return s
    fnames=[]
    for num in numList:
        if fillzeros>0:
            numstr = addzeros(num,fillzeros)
        else:
            numstr = str(num)            
        fname = os.path.join(basedir, basename.replace(replaceStr,numstr))
        fnames.append(fname)
    return fnames
#################################################################################################
def getCleanLines(fid,comchar='#',closeFile=True):
    '''gets rid of comment lines and returns list of line strings'''
    rl = fid.readlines()
    #clean off lines that start with #, make a function
    removeList=[]
    removeCount=0
    for i in range(len(rl)):
        if rl[i][0]==comchar:
            removeList.append(i-removeCount)
            removeCount =removeCount+1
    for i in range(len(removeList)):
        del rl[removeList[i]]
    if closeFile:
        fid.close()
    return rl
##################################################################################################\
def processLinesVarType(rl,preliminarySep=None, YNprocess=False):
    '''Takes rl = a list of strings like, e.g returned by readlines. Works on lines of a given format, the first line has the
    variable names, the second line has the type abbreviations these will be cast into. s for string f for float i for integer. If preliminarySep is specified it first
    separates with that separator. Then only the first part is split again with the default.
    This was used to easily separate the multiword comment coloumn from others in peaks.data files. Returns a list of dictionaries with keys
    being variable names of first line and items being cast into correct types. If YNprocess is True strings Y and N are converted into items of True and False.'''
    dList=[]
    attList = rl[0].split()
    attTypes = rl[1].split()
    attOps = castList(attTypes)
    for line in rl[2:]:
        dict={}
        if preliminarySep:
            preliminarySplit = line.split(preliminarySep) 
            actualList = preliminarySplit[0].split()        
            # Now add the rest, probably multiword stuff)
            actualList = actualList + preliminarySplit[1:]
        else:
            actualList = line.split()
        for key,item,op in zip(attList , actualList , attOps):
            dict[key] = op(item)
        if YNprocess:
            for key,val in dict.items():
                dict[key] = YN(val)
        dList.append(dict)
    return dList
##################################################################################################
def YN(x):
    if type(x)==str:
        if x.capitalize()=='Y' or x.capitalize()=='Yes':
            return True
        elif x.capitalize=='N' or x.capitalize()=='No':
            return False
        else:
            return x
    else:
        return x
##################################################################################################
