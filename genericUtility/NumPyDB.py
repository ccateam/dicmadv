import os,sys,cPickle

class NumPyDB:
    def __init__(self, database_name, mode='store'):
        self.filename = database_name
        self.dn = self.filename + '.dat' # NumPy array data
        self.pn = self.filename + '.map' # positions & identifiers
        if mode == 'store':
            # bring files into existence:
            fd = open(self.dn, 'w'); fd.close()
            fm = open(self.pn, 'w'); fm.close()
        elif mode == 'load':
            # check if files are there:
            if not os.path.isfile(self.dn) or \
                   not os.path.isfile(self.pn):
                raise IOError, \
                      "Could not find the files %s and %s" %\
                      (self.dn, self.pn)
            # load mapfile into list of tuples:
            fm = open(self.pn, 'r')
            lines = fm.readlines()
            self.positions = []
            for line in lines:
                # first column contains file positions in the
                # file .dat for direct access, the rest of the
                # line is an identifier
                c = line.split()
                # append tuple (position, identifier):
                self.positions.append((int(c[0]),
                                       ' '.join(c[1:]).strip()))
            fm.close()

    def locate(self, identifier, bestapprox=None): # base class
        """
        Find position in files where data corresponding
        to identifier are stored.
        bestapprox is a user-defined function for computing
        the distance between two identifiers.
        """
        identifier = identifier.strip()
        # first search for an exact identifier match:
        selected_pos = -1
        selected_id = None
        for pos, id in self.positions:
            if id == identifier:
                selected_pos = pos; selected_id = id; break
        if selected_pos == -1: # 'identifier' not found?
            if bestapprox is not None:
                # find the best approximation to 'identifier':
                min_dist = \
                         bestapprox(self.positions[0][1], identifier)
                for pos, id in self.positions:
                    d = bestapprox(id, identifier)
                    if d <= min_dist:
                        selected_pos = pos; selected_id = id
                        min_dist = d
        return selected_pos, selected_id


    def dump(self, a, identifier):
        """Dump NumPy array a with identifier."""
        raise 'dump is not implemented; must be impl. in subclass'
    def load(self, identifier, bestapprox=None):
        """Load NumPy array with identifier or find best approx."""
        raise 'load is not implemented; must be impl. in subclass'


class NumPyDB_cPickle (NumPyDB):
    """Use basic cPickle class."""
    def __init__(self, database_name, mode='store'):
        NumPyDB.__init__(self,database_name, mode)
        return

    def dump(self, a, identifier):
        """Dump NumPy array a with identifier."""
        # fd: datafile, fm: mapfile
        fd = open(self.dn, 'a'); fm = open(self.pn, 'a')
        # fd.tell(): return current position in datafile
        fm.write("%d\t\t %s\n" % (fd.tell(), identifier))
        cPickle.dump(a, fd, 1) # 1: binary storage
        fd.close(); fm.close()


    def overwrite(self,a,identifier):
        """
        if identifier exists and the matrix is same size as before, it overwrites, otherwise dumps
        UNTESTED.
        """
        #fd = open(self.dn, 'a'); fm = open(self.pn, 'a')
        #
        pos, id = self.locate(identifier, bestapprox)
        if pos < 0:
           self.dump(a,identifier)
        else:
            fd = open(self.dn, 'r')
            fd.seek(pos)
            fd.close()
            b = cPickle.load(fd)
            if b.shape==a.shape and b.dtype==a.dtype:
                fd = open(self.dn, 'a')
                fd.seek(pos)
                cPickle.dump(a, fd, 1)
                fd.close()
            else:
                self.dump(a,identifier)
        return
        
        
        

    def load(self, identifier, bestapprox=None):
        """
        Load NumPy array with a given identifier. In case the
        identifier is not found, bestapprox != None means that
        an approximation is sought. The bestapprox argument is
        then taken as a function that can be used for computing
        the distance between two identifiers id1 and id2.
        """
        pos, id = self.locate(identifier, bestapprox)
        if pos < 0: return [None, "not found"]
        fd = open(self.dn, 'r')
        fd.seek(pos)
        a = cPickle.load(fd)
        fd.close()
        return [a, id]


if __name__== "__main__":
    from scipy import *
    k = NumPyDB_cPickle('trash')
    a = arange(25.)
    for i in range(5):
        k.dump(repeat(atleast_2d(a[5*i:5*(i+1)]),5,axis=0),`i`)


    l = NumPyDB_cPickle('trash','load')
    print l.load('4')
    
