#TODO: adding masking and linear/polynomial fitting ability
from scipy import *
import Utility as ut
reload(ut)
class preProcess:
    def __init__(self,X,Y):
        '''admists vector X and matrix Y'''
        X = ut.vec2colmat(X)
        Y=  ut.vec2colmat(Y) #if Y is matrix this function leaves it alone.
        if len(X)==len(Y):
            self.X, self.Y = X,Y
            self.nrows = len(X)
            self.ncols = size(Y,-1)
            self.shapeX,self.shapeY = shape(X), shape(Y)
            self.XY =hstack((X,Y))
        else:
            raise 'Improper input to XYdata, shape(X)=%s shape(Y)=%s'%(shape(X),shape(Y))
        self.lfit=[]
    def checkattribute(self,name):
        """This is a general thing, i'm sure there is a better implementation somewhere."""
        if name in self.__dict__:
            boo=1
        else:
            boo=0
        return boo
    def activeRange(self,start=[],stop=[]):
        '''will take those rows specified for X,Y and XY'''
        #These are ugly; investigate
        if start and not stop:
            self.X = self.X[start:,:]
            self.Y = self.Y[start:,:]
            self.XY = self.XY[start:,:]
        elif stop and not start:
            self.X = self.X[:stop,:]
            self.Y = self.Y[:stop,:]
            self.XY = self.XY[:stop,:]
        elif stop and start:
            self.X = self.X[start:stop,:]
            self.Y = self.Y[start:stop,:]
            self.XY = self.XY[start:stop,:]
        else:
            pass
    def linFit(self,start=[],stop=[]):
        if start and not stop:
            tX = self.X[start:,:]
            tY = self.Y[start:,:]
        elif stop and not start:
            tX = self.X[:stop,:]
            tY = self.Y[:stop,:]
        elif stop and start:
            tX = self.X[start:stop,:]
            tY = self.Y[start:stop,:]
        else:
            tX = self.X
            tY = self.Y
        Amat=hstack((tX,ones((len(tX),1),'d')))
        #
        self.lfit.append(linalg.lstsq(Amat,tY))
    def linFitPlot(self,linFitnum,returnarray=1):
        '''will plot over self.newx'''
        a,b = ravel(self.lfit[linFitnum][0])
        self.switchFigure()
        gplt.hold('on')
        tY = a*self.newx + b
        print self.newx
        print tY
        gplt.plot(self.newx,tY,"title 'Linear fit' with lines")
        gplt.hold('off')
        return hstack((ut.vec2colmat(self.newx),ut.vec2colmat(tY)))
    def figure(self):
        gplt.figure()
        self.figHandle = gplt.current()
    def switchFigure(self):
        '''switch to the figure handle of this pair'''
        gplt.figure(self.figHandle)
    def reverseX(self):
        self.X = -self.X
        self.XY[:,0] = -self.XY[:,0]
    def shiftX(self,value):
        self.X = self.X + value
        self.XY[:,0] = self.XY[:,0] + value
    def monotonX(self,increasing=1):
        self.monXY = ut.makemonoton(self.XY,increasing=increasing)
        self.monX = self.monXY[:,0]
        self.monY = self.monXY[:,1:]
    def newPts(self,freq=0,newpts=[]):
        if newpts:
            self.newpts = newpts
        elif isscalar(freq) and freq!=0:
            self.newpts = range(0,self.nrows,freq)
    def newX(self,newx):
        self.newx=newx       
        #Note averaging filter avgFilter uses pts instead of X values to stay away from
        #problems of nonmonotonous noisy X's
    
        #newX on the other hand is used by spline case
    def avgFilter(self):
        newpts=self.newpts
        print newpts
        print self.nrows
        tempCumSum = cumsum(self.XY,axis=0) #columnar cumsum
        print shape(self.XY)
        cumSum = take(tempCumSum,newpts,axis=0)
        intervalSum = diff(cumSum,axis=0)
        ptsPerInterval = diff(newpts,axis=0)
        #columnize it so that it works with division
        ptsPerInterval = ut.vec2colmat(ptsPerInterval)
        #print tempCumSum
        #print self.newpts
        #print ptsPerInterval
        #print cumSum
        #print intervalSum
        avgXY = intervalSum/ptsPerInterval
        self.avgX = avgXY[:,0]
        self.avgY = avgXY[:,1:]
        self.avgXY = avgXY
    def splrep(self,**args):
        '''This version does not support different arguments being sent to splrep for
        different coloumns which is a perfectly desirable thing...'''
        self.TCK=[]
        if not ut.isincreasing(self.monX):
            raise 'splrep requires a monotonously increasing monX'
        for i in range(self.ncols):
            iY = self.monY[:,i]
            self.TCK.append(interpolate.splrep(ravel(self.monX),iY,**args))
            print shape(ravel(self.X))
            print shape(iY)
    def splev(self,**args):
        '''This version does not support different arguments being sent to splrep for
        different coloumns which is a perfectly desirable thing...'''
        self.splevX =self.newx
        self.splevY = zeros((len(self.newx),self.ncols),'d')
        for i in range(self.ncols):
            self.splevY[:,i] = interpolate.splev(self.newx,self.TCK[i],**args)
        self.splevXY = hstack((ut.vec2colmat(self.splevX),self.splevY))
    def plot(self,mon=0,avg=0,spline=0):
        self.figure()
        gplt.plot(self.X,self.Y,"title 'data' with lines")
        gplt.hold('on')
        #instead of detecting with checkattribute I think this is safer
        #if avg attributes do not exist let it give attributeError
        if mon:
            gplt.plot(self.monX,self.monY,"title 'modified for monotonous' with lines")
        if avg:
            gplt.plot(self.avgX,self.avgY,"title 'interval averaged' with linespoints")
        if spline:
            gplt.plot(self.newx,self.splevY,"title 'spline' with points")
        gplt.hold('off')
    def mixedCond(self,types,xRanges):
        '''types is a list of spline/avg/linearfit/, xRanges is a list of where
        to get these values and concetanate them into a new XY'''
        pass
        #No time to write this decently, this class starts to give a stron sense
        #of being wrongly written so I will correct it later with employing low
        #level classes and unifying properties of operations...
                
            
                
if __name__ == '__main__':
    """Run main loop if launched as an independent Python program."""
    x=arange(0.,pi,pi/20)
    y=array([sin(x),cos(x)])
    y=transpose(y)
    a =preProcess(x,y)
    a.newPts(8)
    a.avgFilter()
    newx=arange(0.,pi,pi/100.)
    a.newX(newx)
    a.splrep(s=0)
    a.splev()
    a.plot(avg=1,spline=1)
