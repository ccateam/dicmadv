#from scipy import ArrayType
class convert:
    """Purpose:
    Convert is a base class where there are interchangable quantitites; you know
    the conversions but you don't want to worry about getting the conversion done
    and where to put the new variable, etc. The class that inherits convert
    defines a vars attribute by overriding Vars method which just set self.vars.
    This HAS to be a tuple, and obviously the elements have to be strings.
    Now if the instance of convert inheriting class knows only one of these
    guys as an attribute then you can just refer to any other provided that
    the conversion function exists. Example say self.vars=('x','y'). If self.x is
    set before then you can refer to self.y without doing anything. It will use
    __getattr__ to make the conversion and return a self.y provided that you
    have the conversion function. For generality, the conversion function name
    has a format it has a 2 between the names of the first and second variables.
    In this case it would be x2y. This function for sake of some convenience later
    to make code more modular at the presence of a lot of interrelated stuff takes one
    argument the value (not name) of the converted stuff itself.
    Any thing
    that you want to do during initiation can be put under an overridden _init.
    Note: It took a long while to work. dir(self) replaced self.__dict__ stuff to
    also show methods. Turned out returning anything with __getattr__ does not
    add the sent name as an attribute to the instance; it just returns a value.
    With __call__, however, instance('x') will be set to self.x with a setting
    statement. Quite many execs and evals, looks ugly and maybe is fragile. Look under
    __call__ for further reasons to conjure it.
    You can constrain the type acceptable attributes typewise by setting acceptableTypes attribute
    in any inheritor.
    Extended to accept operating on differential quantities. Conversion from a differential
    in one var to another requires; what var is (because the derivative of the conversion function
    is not generally constant) plus what the differential is. If _D_x exists in addition to x and
    if y type var is asked for _D_y will be returned. If _D_x does not exist conversion will be
    regular from x to y. To save time as you define self._D_x set self.deltaEval=True.
    """
    def __init__(self): 
        self.Vars()
        self.deltaEval=False
        self.acceptableTypes = 'all'
        if type(self.vars) != tuple:
            raise TypeError, 'self.vars set by Vars method should be a tuple.'
        self._init()
        #self.ct = 0
    def Vars(self):
        self.vars = () #Now I appreciate the tuple use.
        return
    def _init(self):
        pass
    def checkType(self,s):
        pass
    def __getattr__(self,name):
        """

        NOTE: 1) remember if the variable is in self.__dict__ already it
        will never come here. So you don't have to test it or send back the
        self.__dict__(name) if name is in __dict__; since if so it was already
        done.
        2) Does not really set anything. __call__ does it in this version.
        3) Jan 03, extended to work with differential quantities.
        """
        def newval(valueold,varold,varnew,selfrep):
            funcString = varold + '2' + varnew #The conversion function name
            if funcString in selfrep:
                self.checkType(varold)
                #print x
                #set the attribute here
                valuenew = eval('self.' + funcString + '(valueold)') # may want to use apply!!!
            else:
                valuenew = '_NS'
            return valuenew
        
        if name == 'vars': #if this somehow ended up here.
            self.Vars()
        if name in self.vars:
            #form the list to look for conversions
            varList = list(self.vars)
            selfrep = dir(self)
            
            for var in self.vars:
                if var not in selfrep: #self.__dict__ and dir are different. __dict__ is difficult to deal with. For example it does not modify
                    varList.remove(var)

            if varList == []:
                raise 'The conversion failed because there was no set variables that can do this conversion.'    
            # Now varList is a list of already set attributes. 
            # Now try to do the conversion from over the existing attributes. Note the order
            # matters if there are multiple items in varList. It will go with the order priority
            # but under normal circumstances things should be fine
            for var in varList:
                # old and new are for depicting conversion, not great terms really
                valueold = eval ('self.'+var)
                valuenew = newval(valueold,var,name,selfrep)
                if valuenew != '_NS':
                    if self.deltaEval==False: #if conversion of an actual variable is the purpose, we're done.
                        return valuenew
                    else:
                        varworks = var
                        break # conversion can be made with this var; function exists
                else: #when a function is never found for any var.
                    if var == varList[-1]:
                        #if it is last variable, means any conversion failed
                        raise 'The conversion failed since it did not find any functions from set variables %s to new variable %s.'%(varList,name)
                
            if self.deltaEval==True: # now if the objective was the differential
                incrementold = eval('self._D_' + varworks) # then this supposedly exists and we don't go into this function. 
                if self.dmode=='right':
                    valuenew1 = valuenew
                    valueold2 = valueold + incrementold
                    valuenew2 = newval(valueold2,varworks,name,selfrep)
                elif self.dmode=='left':
                    valuenew2 = valuenew
                    valueold1 = valueold - incrementold
                    valuenew1 = newval(valueold1,varworks,name,selfrep)
                elif self.dmode=='center':
                    valueold1 = valueold - incrementold/2.
                    valueold2 = valueold + incrementold/2.
                    valuenew1 = newval(valueold1,varworks,name,selfrep)
                    valuenew2 = newval(valueold2,varworks,name,selfrep)
                else:
                    raise ValueError, 'attribute dmode "%s" is not set or wrong.'%(self.dmode)

                return (valuenew2 - valuenew1)
        else:
            raise AttributeError, 'This object has no attribute %s, also it is not in the self.vars \
            for __getattr__ to work on %s.'%(name,self.vars)
        return
    def __call__(self,name,*args,**kwds):
        '''Call will be used to to return attributes. so instance('r') will try to return instance.r.
        This will emulate instance.r call in the sense that instance.r will first check self.__dict__ for the
        existence of r and then if it cannot find it will call __getattr__. This was necessary since I wanted to
        make the type a variable in one of my codes. If you set instance.cm let us say you can get instance.mm in
        the console but if you try to make mm or cm a variable what do you do? obviously self.__dict__ will
        not work for it will never call __getattr__. if you call getattr right away you eliminate the auto check of
        taking the variable directly if it is in __dict__. You can put another check in __getattr__ with little inefficency
        but that is arcane. Instead I will use call for this purpose also because the coding is shorter and cleaner.'''
        #check if in __dict__
        selfrep = dir(self)
        if name in selfrep:
            if self.deltaEval==False:
                return eval('self.'+name) #alternative to seld.__dict__[name] which looks nicer but my confidence in __dict__ has got low today.
            elif self.deltaEval==True:
                return eval('self._D_'+name)
        else:
            val =  self.__getattr__(name)
            # Here add it to __dict__
            exec 'self.' + name + ' =  val'
            return val
        
    #def x2y(self,x):
        #return x * 2

class convertNoStore(convert):
    '''Does what convert does. However, it does not store any new attributes. It does the calculation each and every time
    from the original type. Advantage is when you have parameters of the inheriting class that take role in the conversion formulas.
    When those are changed; you want them to be reflected automatically. Can be smarter of course but will get too complex.'''
    def __call__(self,name,*args,**kwds):
        '''Call will be used to to return attributes. so instance('r') will try to return instance.r.
        This will emulate instance.r call in the sense that instance.r will first check self.__dict__ for the
        existence of r and then if it cannot find it will call __getattr__. This was necessary since I wanted to
        make the type a variable in one of my codes. If you set instance.cm let us say you can get instance.mm in
        the console but if you try to make mm or cm a variable what do you do? obviously self.__dict__ will
        not work for it will never call __getattr__. if you call getattr right away you eliminate the auto check of
        taking the variable directly if it is in __dict__. You can put another check in __getattr__ with little inefficency
        but that is arcane. Instead I will use call for this purpose also because the coding is shorter and cleaner.'''
        #check if in __dict__
        selfrep = dir(self)
        if name in selfrep:
            if self.deltaEval==False:
                return eval('self.'+name) #alternative to seld.__dict__[name] which looks nicer but my confidence in __dict__ has got low today.
            elif self.deltaEval==True:
                return eval('self._D_'+name)
        else:
            val =  self.__getattr__(name)
        
        # Here we added it to __dict__, now don't
        #exec 'self.' + name + ' =  val'
        return val

class fConvert(convert):
    ''' pretty much convert except that it will check variables from being float or array
    before sending them to conversion. In numerical line of work this is generally the case. So, having
    this modification in a generic location suits.'''
    pass
    ## WITH THE NEW SCIPY TYPES THESE REQUIRE ATTENTION
    ## def checkType(self,name):
##         ''' s in this case is an attribute name string'''
##         try:
##             t = type(self.__dict__[name])
##         except KeyError:
##             raise KeyError, 'In checkType, the variable %s is not in dict so its type cannot be checked.'%(s,)
##         if t == float:
##             pass
##         elif t == ArrayType:
##             pass
##         else:
##             raise TypeError, '%s attribute has to be type float or array. Instead it is %s'%(s,repr(t))
    

class fConvertNoStore(convertNoStore):
    ''' fConvert with no variable storage; only initial guy is stored.'''
    pass
##     def checkType(self,name):
##         ''' s in this case is an attribute name string'''
##         try:
##             t = type(self.__dict__[name])
##         except KeyError:
##             raise KeyError, 'In checkType, the variable %s is not in dict so its type cannot be checked.'%(s,)
##         if t == float:
##             pass
##         elif t == ArrayType:
##             pass
##         else:
##             raise TypeError, '%s attribute has to be type float or array. Instead it is %s'%(s,repr(t))
    
