import scipy
from scipy import linalg
import numpy as np
def planefit(x, y, z,**kwds):
    x = np.ravel(x)
    y = np.ravel(y)
    z = np.ravel(z)
    assert x.size==y.size
    assert x.size==z.size
    n = z.size
    m = n-3
    onearr = np.ones(x.size, float)
    C = np.vstack((x,y,onearr)).transpose()
    L = (np.asmatrix(C).T * np.asmatrix(C)).I
    p,resids,rank,s = np.linalg.lstsq(C,z)
    residuals = np.dot(C,p) -  z
    # resids[0] is sum(residuals*residuals)
    sigmaz = (resids[0]/m)**0.5
    p_err = (np.diag(L) ** 0.5) * sigmaz
    #print p, resids, rank, s
    return p, p_err, sigmaz

#def planecenter(xc,yc,x,y,z,**kwds):
#    """
#    returns the plane fit's value at the supplied center coords xc, yc
#    """
#    p, p_err, sigmaz = planefit(x-xc, y-yc, z)
#    A,B,C = p
#    dA,dB,dC = p_err
#    # the second element returned is the quality of the work
#    def fn(x,y):return A*(x-xc) + B*(y-yc) + C
#    doc = 'A=%s+-%s  , B=%s+-%s   , C = %s+-%s'%(A,dA, B,dB, C,dC)
#    return ( fn(xc,yc), False, fn, doc)

def planecenter2(xc,yc,x,y,z,**kwds):
    """
    xc, yc =  the center coordinates of the fit
    returns the plane fit's value at the supplied center coords xc, yc
    """
    err = {}
    p, p_err, sigmaz = planefit(x-xc, y-yc, z)
    A,B,C = p
    dA,dB,dC = p_err
    err['p_err'] = p_err
    err['sigmaz'] = sigmaz 
    # the second element returned is the quality of the work
    def fn(x,y):return A*(x-xc) + B*(y-yc) + C
    doc = 'A=%s+-%s  , B=%s+-%s  , C = %s+-%s'%(A,dA, B,dB, C,dC)
    return ( fn, p, doc, err )


if __name__ == "__main__" :
    Xin, Yin = np.mgrid[0:51, 0:51]
    a=2.
    b=3.
    c=4.
    Zin = a*Xin+b*Yin+c + np.random.random(Xin.shape)-0.5 # random in range -0.5 to 0.5
    A,B,C = planefit(Xin,Yin,Zin)
    


