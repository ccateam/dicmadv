import copy
# Some of the properties you want in certain classes is very standard.
# Such properties can be inherited from basic classes such as the ones
# listed here.

class diffuse2LowerHierarchy:
    """
    A variable may be defined in the instance of a parent class and the
    instance of a lower hierarchy class that is the parent's attribute.
    This is generally the case if you want to make a rough definition of the
    variables in the parent but want to keep them independent in the attribute
    instance. So, you want to give an initial value for the lower hierarcy from
    the parent, but allow them to be updated later. The function diffuse
    conveniently allows that initial value transfer.
    """
    def diffuseBelow(self,slaveInstance,varList=[],_copy=1):
        """
        slaveInstance is a string of the name of the object that you have
        as the attribute. varList is the name of variables (attributes) that
        exist both in the parent (this object) and below and you want to
        equate them to parent's values.
        _copy = 1 for copy.copy , 2 for copy.deepcopy, 0 for no copy
        """
        cL = [('',''),('copy.copy(',')'),('copy.deepcopy(',')')]
        for var in varList:
            exec 'self.' + slaveInstance + '.' + var + '=' + cL[C][0] + 'self.' + var + cL[C][1]
        return
    def diffuseBelowL(self,slaveInstanceList,varList=[],_copy=1):
        """
        slaveInstanceList is a string of the name of the list/dict object whose
        members/values you want to pass the attributes in varList to.
        varList is the name of variables (attributes) that
        exist both in the parent (this object) and below and you want to
        equate them to parent's values.
        _copy = 1 for copy.copy , 2 for copy.deepcopy, 0 for no copy
        """
        cL = [('',''),('copy.copy(',')'),('copy.deepcopy(',')')]
        exec 'L  = self.'+slaveInstanceList
        C = _copy
        if isinstance(L,list):
            for member in L:
                for var in varList:
                    exec 'member.' + var + '=' + cL[C][0] + 'self.' + var + cL[C][1]
        elif isinstance(L,dict):
            for key,member in L.items():
                for var in varList:
                    exec 'member.' + var + '=' + cL[C][0] + 'self.' + var + cL[C][1]
        return
