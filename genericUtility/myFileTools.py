# These are some tools I will throw in here for EPSC evaluation.
# I will avoid using re most or all of the time.
from scipy import *
import numpy
import os,string
from StringIO import StringIO
def getMatrix(matrixString,uglySolutionList=False,**args):
    '''uglySolution: a list of text that does not respond to python float function; to be replaced with 0s '''
    # Fortran DIC engine throws ********** for numbers
    #ugly ugly solution 
    if uglySolutionList:
        for ugly in uglySolutionList:
            matrixString=matrixString.replace(ugly,'0.00')
    #
    c= StringIO(matrixString)
    matrix = numpy.loadtxt(c)
    return matrix
def getLabelAndMatrix(str,**args):
    '''reads the label line and matrix as well, returns a dictionary where key is the label and
    corresponding coloumn of matrix is the item.'''
    firstReturn = str.find('\n')    
    line1 = str[:firstReturn]
    rest = str[(firstReturn+1):]
    labels = string.split(line1)
    matrix = getMatrix(rest)
    numCol = size(matrix,1)
    if numCol != len(labels):
        raise 'Number of coloumns=%d is not equal to number of labels=%d.'%(numCol,len(labels)) 
    dict={}
    for i in range(len(labels)):
        dict[labels[i]] = matrix[:,i]
    return dict

