import myStrTools as mst  ; reload(mst)
import xlrd
import numpy as np
import os

def intCol(colList):
    """ 
    takes a list of column that is primarily numbers, returns the numbers as well as the mask that describes the rows the numbers are. 
    """ 
    L = len(colList)
    mask = np.zeros(L,dtype=bool)
    numbers = np.zeros(L,dtype=int)
    for i,cand in enumerate(colList):
        try:
            numbers[i] = int(cand)
        except:
            pass
        else:
            mask[i]=1
    return (numbers,mask)

def floatCol(colList):
    L = len(colList)
    mask = np.zeros(L,dtype=bool)
    numbers = np.zeros(L,dtype=float)
    for i,cand in enumerate(colList):
        try:
            numbers[i] = float(cand)
        except:
            pass
        else:
            mask[i]=1
    return (numbers,mask)

class ex:
    def __init__(self,xlsfil, sheetNum=0, colDict={'stressR':6,'microNum':7,'macroNum':8,'extension':9}):
        wb = xlrd.open_workbook(xlsfil)
        self.sh = wb.sheet_by_index(sheetNum)
        self.cd = colDict
        
    def process(self,key,func):
        """
        key is a key of self.cd
        func is intCol or floatCol normally, returns values and mask
        """
        excelColumns = self.sh.col_values
        colVal =  excelColumns( self.cd[key] )
        val, mask = func(colVal)
        return (val,mask)




if __name__=="__main__":
    a = ex("/home/can/EXPERIMENTS/EDM2_s1b/makro/edm2s1b.xls")
    micro, micro_mask = a.process('microNum',intCol)
    macro, macro_mask = a.process('macroNum',intCol)
    stress, stress_mask = a.process('stressR',floatCol)
    stressMic, folderNumMic = stress[micro_mask], micro[micro_mask]
    
