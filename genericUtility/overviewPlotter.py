import os
import os.path as osp
import time

def lpc1(figNums=[],skipPrint=False):
    f = pylabOp(figNums=figNums, numCols=1, title=time.ctime())
    if not skipPrint:
        os.system('lp %s'%(f[:-4]+'.pdf'))
    return

def lpc2(figNums=[],skipPrint=False):
    f = pylabOp(figNums=figNums, numCols=2, title=time.ctime())
    if not skipPrint:
        os.system('lp %s'%(f[:-4]+'.pdf'))
    return

def pylabOp(figNums=[],**kwds):
    import pylab
    base='temptemp'
    k = os.getcwd()
    texfile = os.getcwd()+'/temp.tex'
    fnames=[]
    for num in figNums:
        pylab.figure(num)
        name = base+str(num)+'.eps'
        pylab.savefig(name)
        fnames.append(name)
    ins =overviewPlot(texfile=texfile, fnames=fnames, **kwds)
    ins.run()
    return texfile
    

class overviewPlot:
    
    def __init__(self,texfile,fnames,title='',numCols=2):
        self.fnames=fnames
        self.title=title
        self.texfile=texfile
        self.numCols=numCols
        self.cwd = os.getcwd()
        self.dirname = osp.dirname(self.texfile)
        self.basename = osp.basename(self.texfile)
        templateFile = open('/home/can/MyPy/genericUtility/overviewPlotTemplateS.tex','r')
        self.iniText = templateFile.read()
    def Fnames(self,fnames):
        self.fnames=fnames
    def checkFiles(self):
        for fname in self.fnames:
            if not os.path.exists(fname):
                print 'File %s does not exist. Expunging from the list.'%(fname,)
            self.fnames.remove(fname)
    def prepText(self,putFileName=False):
        self.graphText=''
        if self.numCols==1:
            width = "14cm"
        elif self.numCols ==2:
            width = "7cm"
        else:
            raise ValueError
        for fname in self.fnames:
            if putFileName:
                pgt = "\\includegraphics[width=%s\\textwidth]{%s}\n \newline \\vspace{-0.5cm}\\begin{verbatim}%s\\end{verbatim}\n"%(width,fname,osp.basename(fname))
            else:
                pgt = "\\includegraphics[width=%s]{%s}\n \\newline \n \\vspace{-0.5cm}\n"%(width,fname,)
            
            self.graphText=self.graphText+pgt
    def repText(self):
        tempText = self.iniText
        if self.numCols == 2:
            colText = r'\twocolumn[{\center %s}]'%(self.title,)
            temp2Text = tempText.replace('$COLUMNTEXT',colText)
        elif self.numCols ==1:
            colText = r'%s \newline'%(self.title)
            temp2Text = tempText.replace('$COLUMNTEXT',colText)
        else:
            raise ValueError
        #print 'temp2Text = ' + temp2Text
        self.newtext = temp2Text.replace('ALLFIGURES',self.graphText)
        print self.newtext
    def writeText(self):
        self.fout = open(self.texfile,'w')
        self.fout.write(self.newtext)
        self.fout.close()
    def execText(self):
        os.chdir(self.dirname)
        execString = 'latex '+ self.basename
        os.system(execString)
        try:
            execString = 'dvips '+ self.basename[:-4] + ' -o'
            os.system(execString)
        except:
            raise 'Could not convert to ps.'
        try:
            execString = 'dvipdf '+ self.basename[:-4]
            os.system(execString)
        except:
            raise 'Could not convert to pdf.'
        os.chdir(self.cwd)
    def run(self):
        #self.checkFiles()
        self.prepText()
        self.repText()
        self.writeText()
        self.execText()
        
        
        
