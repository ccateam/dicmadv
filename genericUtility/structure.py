from numpy import isscalar
class structureList(list):
    '''Of course there are no structures but classes in Python, what this list inheriting list is intended for
    is to return lists of class arguments. Since they are only arguments I thought structure was a good term to
    tell what this does. Example: you store in this list (call it a) instances of a class that has an attribute "clam".
    a._clam attribute will return a list of [a[0].clam, a[1].clam, ...etc. Underscore is in case one of the list attributes
    are an attribute of list and __getattr__ is not called at all.

    Most probably fragile. Use only with append and setting items.

    To do: Potential speeding of the for loops in put, take, etc.'''
    def __init__(self,__class):
        self.cl = __class
        list.__init__(self)
        return
        
    def __getattr__(self,name):
        '''__getattr__(self,name) overload to return value list of an attribute of the class instances that are stored.'''
        t1, attrCandidate = name[0],name[1:]
        if t1 == "_" and hasattr(self[0],attrCandidate):
            t=[]
            for ins in self:
                t.append(getattr(ins,attrCandidate))
            return t
        else:
            return list.__getattribute__(self,name)

    def __setattr__(self, name, value):
        ''' __setattr__(self, name, value) overload to set the '''
        t1, attrCandidate = name[0],name[1:]
        
        if t1 == "_" and hasattr(self[0],attrCandidate):
            if (type(value)==list) and (self.__len__() == len(value)):
                for val,ins in zip(value,self):
                    setattr(ins,attrCandidate,val)
            else:
                raise ValueError,"Trying to set to the %s attribute of structureList members but \
                not with a value list of the correct length."%(attrCandidate)
        else:
            list.__setattr__(self,name,value)
        return
                
    ### The following controls are for making sure that the following methods cannot be used to add
    ### members that are not an instance of class.
    def __setitem__(self,i,y):
        '''__setitem__(self,i,y) list itself will check that i is integer; my check is on y.'''
        if isinstance(y, self.cl):
            list.__setitem__(self,i,y)
        else:
            print 'Refusing to set since it is not the appropriate class.'
    def __setslice__(self,i,j,y):
        wrongType = False
        print y
        for ins in y:
            if not isinstance(ins , self.cl):
                wrongType = True
                break
        if wrongType == False:
            list.__setslice__(self,i,j,y)
        else:
            print 'Refusing to set slice since it has members that are not of the arppropriate class.'
    def __getslice__(self,i,j):
        ins = self.__class__(self.cl)
        for k in range(i,j):
            ins.append(self[k])
        return ins

    def sortByIndex(self,newIndices):
        ins = self.__class__(self.cl)
        for ind in newIndices:
            ins.append(self[ind])
        return ins

    def sortByName(self,name,nameList):
        ins = self.__class__(self.cl)
        indices = self.sort1to1(name,nameList)
        return self.sortByIndex(indices)
        
    def append(self,y):
        if isinstance(y, self.cl):
            list.append(self,y)
        else:
            print 'Refusing to append since it is not the appropriate class.'
    # put and take ability like those of array 
    
    def put(self,name,indices,values):
        '''put(self,name,indices,values)'''
        if isscalar(values):
            k=[values]
        else:
            k=values
        for ind,value in zip(indices,k):
            setattr(self[ind],name,value)
        return  

    def take(self,name,indices):
        '''take(self,name,indices)'''
        t = []
        for ind in indices:
            t.append(getattr(self[ind],name))
        return t
    # put a filter function, too, this was necessary for optimization function classes and it was too little a modification
    # to create inheriting classes for. filter functionality is generic enough.
    def filter(self,name,value):
        '''filter(self,name,value) returns the indices of the members whose name argument has value value. It returns a list.'''
        t=[]
        for i,ins in enumerate(self):
            if getattr(ins,name) == value:
              t.append(i)
        return t

    def sort1to1(self,name,nameList):
        '''all of the members in nameList, should be exist in name attributes. In other words there should
        be a one to one correspondence or else you get an error. Returns the indices of the structure list members such that
        these indices correspond to the order members are provided in nameList. Very slow.'''
        #
        t1=[] 
        for n in nameList:
            t = self.filter(name,n)
            if len(t) == 1:
                t =t[0]
            elif t == []:
                raise ValueError, 'The name %s does not exist for any of the %s attributes in the structureList.'%(n,name)
            else:
                raise ValueError, 'The name %s is not unique for %s attribute in the structure list.'%(n,name)
            t1.append(t)
        return t1

    def sort1to1m(self,names,nameLists):
        '''also seeks one to one correspondence but this time with multiple constraints. nameLists should be'''

        def getCommon(list1,list2):
            t = []
            for l in list1:
                if l in list2:
                    t.append(l)
            return t
        
        t1 = []
        #
        for i,n in enumerate(nameLists):
            for j, name in enumerate(names):
                if j==0:
                    t = self.filter(name,n[j])
                else:
                    t = getCommon(self.filter(name,n[j]),t)
                #
            if len(t) == 1:
                t =t[0]
            elif t == []:
                raise ValueError, 'The name %s does not exist for any of the %s attributes in the structureList.'%(n,name)
            else:
                raise ValueError, 'The name %s is not unique for %s attribute in the structure list.'%(n,name)
            t1.append(t)
        
        return t1
    
    
    def correspond(self,name1,name1List,name2,values):
        '''see also sort1to1. correspond(self,name1,name1List,name2,value) pushes this list to a dictionary functionality.
        finds name1 index. sets the name2 value
        to value.name1List,values are lists that are obviously equal length. Unique one to one correspodence is a must. Otherwise
        Sloppily programmed in terms of performance hoping such lists in general are not long.'''
        if isinstance(name1,list):
            indices = self.sort1to1m(name1,name1List)
        else:
            indices = self.sort1to1(name1,name1List)
        self.put(name2,indices,values)

        

class structureDict(dict):

    def __init__(self,__class):
        self._cl = __class
        dict.__init__(self)

    def __getattr__(self,name):
        '''__getattr__(self,name) overload to return the members whose name attribute.'''
        t1, attrCandidate = name[:1],name[1:]
        try:
            TF = hasattr(self.values()[0],attrCandidate)
        except :
            TF = False
        
        if t1 == "_" and TF:
            t=[]
            for key,val in self.items():
                t.append(getattr(val,attrCandidate))
            return (self.keys(),t)
        else:
            return dict.__getattribute__(self,name)
    def __getitem__(self,key):
        if isinstance(key,list) or isinstance(key,tuple):
            t=[]
            for keycand in key:
                try:
                    t.append(self[keycand])
                except KeyError:
                    raise KeyError, 'key %s in keys list %s does not exist in the dict'%(keycand , key)
            return t
        else:
            return dict.__getitem__(self,key)
                    
    def boolCheck(self,key,filter={}):
        tf = True
        for fkey,fval in filter.items():
            if getattr(self[key],fkey) != fval:
                tf = False
                break
        return tf
    
    def filter(self,name,value):
        '''filter(self,name,value) returns the keys of the members whose name argument has value value. It returns a list.'''
        t=[]
        for key,val in self.items():
            if getattr(val,name) == value:
              t.append(key)
        return t

    def filterM(self,names,values,UC=False):
        '''UC stands for uniqueness check.if a unique answer is not returned it cries.'''
        t=[]
        if len(names) != len(values):
            raise ValueError, 'values and names has to be same length in filterM'
        t = self.keys()
        for name,value in zip(names,values):
            for key,val in self.items():
                if getattr(val,name) != value and key in t: t.remove(key)
        if UC:
            if len(t) == 1:
                return t[0]
            else:
                raise ValueError, 'found %s members that matcht the filter.'%(len(t),)
        else:
            return t

class defaultdict(dict):
    def __init__(self,__class):
        self._cl = __class
        dict.__init__(self)

    def __getattr__(self,name):
        '''__getattr__(self,name) overload to return the members whose name attribute.'''
        t1, attrCandidate = name[0],name[1:]
        if t1 == "_m_" and hasattr(self.values()[0],attrCandidate):
            t=[]
            for key,val in self.items():
                t.append(getattr(val,attrCandidate))
            return (self.keys(),t)
        else:
            return dict.__getattr__(self,name)
    def __getitem__(self,key):
        if isinstance(key,list) or isinstance(key,tuple):
            t=[]
            for keycand in key:
                try:
                    t.append(self[keycand])
                except KeyError:
                    raise KeyError, 'key %s in keys list %s does not exist in the dict'%(keycand , key)
            return t
        else:
            return dict.__getitem__(self,key)
    
if __name__=='__main__':
    class c:
        mo=1
        ba=2
        

    o1, o2 = c(),c()
    A = structureDict(c)
    A['o1'] = o1
    A['o2'] = o2
    print A[['o1','o2']]
    print A._mo
    print A.filter('mo',3)
    import pickle
    #b=defaultdict(c)
    #b['c']='lajd'
    #pickle.dump(a,open('delme.p','w'))
    a=structureList(c)
    a.append(o1)
    a.append(o2)
    print a._mo
    pickle.dump(a,open('delme.p','w'))
    
    
