###############################################################
#
# Many times, especially at times you cannot wrap functions into
# python, you just want to parametrize values in the input files
# these functions take. This is a class to make that easier by the
# use of string module's Template class. This class is available
# beyond python version 2.4. lea
#
###############################################################
import os, shutil, sys
from string import Template
import structure
from scipy import array,matrix,io
import StringIO

## Some types should be converted to string beforenad


# Todo: some types may not be appropriate to be substituted with just their repr.
# For that you might consider adding some middle processing based on type during the check.

class inventory:
    ''' the pyre stuff would be really nice here.'''
    def __init__(self, name, typ='default', defaultVal=None):
        """
        type can be 'any' if you don't want any type check.
        Leave defaultVal alone if you think this variable needs to be replaced for sure in each case.
        Otherwise, it will not warn you if it is not replace and just the defaultVal is put in there.
        """
        #
        if typ == 'default':
            if defaultVal==None:
                raise TypeError, "The type was to be extracted from the default Value; yet defaultVal is None. You have to specify the type, here. Type can be 'any' if you don't want any type check."
            typ = type(defaultVal)
        self.typ = typ
        self.name = name
        self.defaultVal = defaultVal
        

class templateObj:
    def __init__(self,file,subList):
        """creates a template object that has a file and all the parameters that need to be substituted
        in it. The subList is a structre list of inventory variables. """
        self.file = file
        self.subDict = structure.structureDict(inventory)
        self.defDict = {}
        self.processtypeList = [type(array([])),type(matrix([]))]
        for sub in subList:
            self.subDict[sub.name] = sub
            self.defDict[sub.name] = sub.defaultVal
        return
    #
    def check(self,subDict):
        for name,value in subDict.items():
            if name not in self.subDict.keys():
                raise ValueError, 'Name not found.'
            if not isinstance(value, self.subDict[name].typ):
                raise TypeError, '%s should be %s'%(name,self.subDict[name].typ)
            if value == None:
                raise ValueError, "A none value is detected in the replacement dictinoary, that is not allowed since none is reserved by user he wants to enforce assigning something."
            if type(value) in self.processtypeList:
                # now at this point this has most probably become a string. But it passed the type test already. So hopefully it is fine.
                subDict[name] = self.processCertainTypes(value)
            
    def processCertainTypes(self,value):
        typ = type(value)
        if typ == type(array([])) or type(matrix([])):
            t =StringIO.StringIO()
            io.write_array(t,value,keep_open=True)
            ret  = t.getvalue()[:-1] # gets rid of the final carriage return
            t.close()
        return ret
    
    def sub(self,newFile,newsubDict):
        #
        td = self.defDict.copy()
        td.update(newsubDict) #update the default dictionary with what is assigned
        self.check(td)
        #
        fin = open(self.file,'r')
        tempstr = fin.read()
        s = Template(tempstr)
        #
        newstr = s.substitute(td)
        #
        fpath = newFile.replace('~/','/home/aydiner/')
        fout  = open(fpath,'w')
        fout.write(newstr)
        fout.close()
        return
    
class subTemplateFunc:
    """This class admits a templateObj which """
    def __init__(self,newFileList,templateObjList):
        """templateObjList is a list of doublets (new) filename and templateObj"""
        self.templateObjList = templateObjList
        self.newFileList = newFileList
        L1 , L2 = len(self.newFileList), len(self.templateObjList)
        self.L = L1
        if  L1 != L2:
            raise ValueError, "newFileList(%s long) and templateObjList(%s long) should be the same length"%(L1,L2)
        
    def checkForFiles(self,subDict):
        """For conveince _file# is reserved as a value to be replaced by the #th member of the
        newFileList. Sometimes hierarchical relation between files is fixed and it would not be cool
        to have modify more than one slot all the time."""
        
        for name,value in subDict.items():
            if isinstance(value,str):
                if value[:5] == '_file':
                    try:
                        ii = int(value[5:])
                    except ValueError:
                        raise ValueError, ' Tried to resolve _file#, the # part is not valid: %s'%(value[5:])
                    try:
                        subDict[name] = self.newFileList[ii-1]
                    except IndexError:
                        raise IndexError, 'newFileList does not have %s index.'%(ii-1)
        return subDict

    def sub(self,subList):
        """subList is a list of dictionaries; for each file for give name value pairs for
        each substitution."""
        if len(subList) != self.L:
            raise ValuError,"Since you have %s substitution templates, you should have that many lists. You should conserve the order and put in an empty dict {}, for the ones that will only use default values."%(self.L)
        for subDict,newFile,templateO in zip(subList,self.newFileList,self.templateObjList):
            subDict = self.checkForFiles(subDict)
            templateO.sub(newFile,subDict)
            
    def run(self,subList):
        raise "inherit me"
                    
                    
