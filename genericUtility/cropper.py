import pylab
import numpy as np
import PIL
import os
from PIL import Image

def crop(imName, cropBox, saveName=None, saveFormat='.tif'):
    """
    cropBox=[topleftx, toplefty, botright, botrights] 
    """
    name,ext=os.path.splitext(imName)
    if saveName==None:
        saveName = name+'_crop'
    saveName += saveFormat
    im = Image.open(imName)
    im2 = im.crop(cropBox)
    im2.save(saveName)
    return
    
                               
                
def cropBySize(imName, topleftcoor, sz=(1280,960), **kwds):
    x,y= topleftcoor
    dx,dy = sz
    cropBox= [ x,y,x+dx,y+dy]
    crop(imName, cropBox,*kwds)
    return




if __name__=='__main__':
    print 'untested yet'
    
