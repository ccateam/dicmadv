import matplotlib.pyplot as plt
import numpy as np


# pcolormesh, contour, contourf, imshow, matshow are the obvious options of 
# surface plotting in matplotlib

gray = plt.cm.gray
blu = plt.cm.RdBu_r

# CASE 1 using extent for defining ranges in an overlay plot

A= np.arange(1200.).reshape(40,30)
B = np.arange(600.).reshape(30,20)
plt.figure()
#Now suppose A is data from (-1.,2.), (5,9.) x (col) and y (row) ranges.
# B has the same metric, its field ranging from (-1.3,1.3) (5.5,8.5)
plt.imshow(A, origin='upper', extent=(-1.,2.,9.,5.),cmap=gray)
# zero is black
# extent = left,right,bot,top
a = (-0.7,1.3,8.5,5.5)
#b = (1.3,-0.7,5.5,8.5)
#c = (-0.7,5.5,1.3,8.5

plt.contourf(B,origin='upper', extent=a,cmap=blu,alpha=0.6)

ax1 = plt.gca()
print ax1.yaxis_inverted() # will return true
#
plt.figure()
plt.contourf(B,origin='upper', extent=a,cmap=blu,alpha=0.6)
ax2=plt.gca()
ax2.set_aspect('equal')
ax2.set_frame_on(False)
# turns out putting the coordinate on the upper corner does not help with 
# y axis running in the reverse direction; image-style
# imshow does that automatically
print ax2.yaxis_inverted() # will return false
ax2.invert_yaxis()
#plt.figure()
#plt.contourf(B,origin='upper', extent=a,cmap=blu,alpha=0.6)
# note using the same origin for both 
plt.show()
