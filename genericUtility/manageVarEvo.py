import time
import pickle
import os

def setequal(list1,list2):
    '''finds out if two lists are equal like a set.'''
    eq = 1
    if len(list1)==len(list2):
        for val in list1:
            if list2.count(val) != 1:
                eq = 0
                break
    else:
        eq = 0
    return eq

def inset(sublist,bigList):
    inn=1
    for val in sublist:
        if val not in bigList:
            inn = 0
    return inn

class manageVarEvo:
    '''just admits a bunch of variables and their values sent to it in a
    dictionary format. The instance then will commit itself to pickling
    the evolution of those variables as more and more dictionaries are
    sent to it.'''
    def __init__(self, pickledFileName=None,flush=False):
        """def __init__(self,pickleFileName=None,flush=False)
        _dict_ret can be dictionary or integer. When it is a dictionary, the program operates
        in the update mode, when it is and integer it is retrieving the record of that number."""
        #
        self.pickledFileName = pickledFileName
        self.flush = flush
        return

    def __call__(self, _dict_ret,info='',*args,**kwds):
        """_dict_ret can be dictionary or integer. When it is a dictionary, the program operates
        in the update mode, when it is and integer it is retrieving the record of that number."""
        if isinstance(_dict_ret,dict):
            self.mode = "update"
            #print "mode is update"
            self.newdict = _dict_ret
        elif isinstance(_dict_ret,int):
            self.mode = "retrieve"
            #print "mode is retrieve"
            self.retNum = _dict_ret
        else:
            raise ValueError, "_dict_ret can be dictionary or integer. When it is a dictionary, the program operates in the update mode, when it is and integer it is retrieving the record of that number."
        self.openFile()
        if self.mode == "update":
            self.check()
            self.update(info=info)
            return
        elif self.mode == "retrieve":
            return self.retrieve(self.retNum,**kwds)
            

    def openFile(self):
        if not self.pickledFileName:
            self.pickledFileName = "./_manVar.p"
        if self.flush == True:
            try:
                os.remove(self.pickledFileName)
                print "\n\n________flushed the record\n\n"
            except OSError:
                print "\n\n________Could not find the file to flush, proceeding_________"
                pass
            self.flush = False # flush should not remain there
                
        try:
            self.old = pickle.load(open(self.pickledFileName))
            self.baseKeys = self.old.baseKeys
            self.valList = self.old.valList
            self.timeList = self.old.timeList
            self.infoList = self.old.infoList
            self.noCheck=False
            self.first = False
        except IOError:
            if self.mode == 'update':
                print '\n\n\n\n_____Starting a new file.\n\n\n\n'
                self.baseKeys = self.newdict.keys() # note baseKeys are only stored at the beginning.
                self.valList = []
                self.timeList = []
                self.infoList = []
                self.noCheck=True
                self.first = True
            elif self.mode == "retrieve":
                raise IOError, "The file could not be opened. Nothing to retrieve."

    def check(self):
        if self.noCheck:
            pass
        else:
            baseKeys = self.baseKeys
            if not setequal(baseKeys,self.newdict.keys()):
                if not inset(self.newdict.keys(),baseKeys):
                    raise ValueError, "Seems you are trying to enter a new set of keys, this container does not allow that. Base keys %s ; your keys %s"%(baseKeys,self.newdict.keys())
                else:
                    print "\n\n\n You are doing a partial update with keys %s; a new record is formed by filling the in the remainder with the old data\n\n\n"%(self.newdict.keys())
        return
    
    def update(self,info):
        newValList = []
        for i,key in enumerate(self.baseKeys):
            try:
                newValList.append(self.newdict[key])
            except KeyError:
                newValList.append(self.old.valList[-1][i])
            # It really uses the previous values as update.
        self.valList.append(newValList)
        self.timeList.append(time.ctime())
        self.infoList.append(info)
        if not self.first:
            del self.old #before pickling to avoid recursive clutter
        pickle.dump(self,open(self.pickledFileName,'w')) # Notice this is overwriting
        return

    def retrieve(self,retNum=None,retDetails=False,retKeys=[]):
        #print retKeys
        if retKeys:
            valList=[]
            for key in retKeys:
                try:
                    valList.append(self.valList[retNum][self.baseKeys.index(key)])
                except ValueError:
                    raise '%s is not in basekeys %s'%(key,self.baseKeys)
        else:
            valList = self.valList[retNum]
            retKeys = self.baseKeys
        retList = valList
        if retDetails:
            retList = [retList] + [retKeys,self.timeList[retNum],self.infoList[retNum],"num total records: "+`len(self.valList)`]
        else:
            pass
        return tuple(retList)

            
            
    
