import numpy as np

def uniqueChecker(mat):
    vals = np.unique(mat)
    if vals.size !=1: 
        raise ValueError,"unique result needed in %s"%(mat,)
    else:
        return vals[0]



def putMatInMat(Mat, subMat, I,J):
    """
    works for 2-D
    inserts a submatrix in a matrix in the I,J index defining the sub matrices
    first element
    """
    a,b = subMat.shape
    Mat[I:I+a, J:J+b] = subMat
    return

def trim_2d(matchoose):
    """
    returns indices for trimming to a nonzero enclosure
    returns (row min, row max, col min, col max)
    """
    m=matchoose
    #along rows
    t = np.where(m.sum(axis=1)!=0)[0]
    indrmin = t.min() 
    indrmax = t.max()
    #along columns
    t = np.where(m.sum(axis=0)!=0)[0]
    indcmin = t.min() 
    indcmax = t.max()
    
    return (indrmin,indrmax,indcmin,indcmax)


if __name__=="__main__":
    A = np.arange(20.).reshape(5,4)
    A[:,-1]=0
    A[-1,:]=0
    print trim_2d(A)
    
