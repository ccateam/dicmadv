from copy import deepcopy,copy
def limitedUpdate(dict1,dict2):
    '''updates dict1 with dict2 but unlike original update does not add new members.'''
    for key, value in dict1.items():
        try:
            dict1[key]=dict2[key]
        except KeyError:
            pass
        
###############################################################################
def filterDictF(dict,f=None,objAtt=None,returnKeys=False,preserveArg = True):
    ''' filters a dictionary based on the items satisfying a true false returning function f. when objAtt is sent it is assumed that the
    the items are instances with attributes of objAtt and those attributes are tested rather then the acutal items. If returnKeys then it
    just returns a list of keys that you can operate on your original dictionary.'''
    #default function    
    def basic(x):
        if x:
            return True
        else:
            return False
    if not f: #if f is not sent in, of course you might want to check its type and stuff later.
        f=basic
    if preserveArg:
        dict2 = copy(dict)
    else:
        dict2=dict
    keyitemL = dict2.items()
    for key,item in keyitemL:
        if objAtt: #Then the item is an object
            testitem = item.__dict__[objAtt]
        else :
            testitem = item
        if not f(testitem):
            del dict2[key]
    if returnKeys:
        return dict2.keys()
    else:
        return dict2
##############################################################################
def filterDictV(dict,value,objAtt=None,returnKeys=False,preserveArg = True ):
    '''a simpler version that just check equality of items or attributes to
    a value'''
    if preserveArg:
        dict2 = copy(dict)
    else:
        dict2=dict
    keyitemL = dict2.items()
    for key,item in keyitemL:
        if objAtt: #Then the item is an object
            testitem = item.__dict__[objAtt]
        else :
            testitem = item
        if not testitem == value:
            del dict2[key]
    if returnKeys:
        return dict2.keys()
    else:
        return dict2
##############################################################################
def setAllTrue(dict,objAtt=None):
    '''sets all items to True or all items.objAtt to True'''
    print len(dict)
    for key,value in dict.iteritems():
        if objAtt:
            value.__dict__[objAtt] = True
            #saddle you have reset the item for the dictionary to see. Otherwise you just work in air.
            #dict[key]=value
        else:
            value = True
    print len(dict)
    return dict
#############################################################################
def setAllFalse(dict,objAtt=None):
    '''sets all items to True or all items.objAtt to True'''
    for value in dict.values():
        if objAtt:
            value.__dict__[objAtt] = False
        else:
            value = False
    return dict
############################################################################
