
import numpy as np
import numpy.linalg
import math

def infStrain(u_i):
    """
    TAKES:
    u_i = list of four derivatives of u, v disp field in 
          the order of  [u_x, u_y, v_x, v_y]
    RETURNS:
    ex,ey,exy,wxy = small strains and in-plane rotation
    
    """
    u_x, u_y, v_x, v_y = u_i
    ex, ey, exy, wxy = u_x, v_y, 0.5*(u_y + v_x) , 0.5*(u_y - v_x)
    return (ex, ey, exy, wxy)

def LagStrain(u_i):
    """
    TAKES:
    u_i = list of four derivatives of u, v disp field in 
          the order of  [u_x, u_y, v_x, v_y]
    RETURNS:
    Ex,Ey,Exy,theta = Lag strains and large rotation
    
    NOTES: implementation copied from Fortran legacy DIC codes. Extended for universal function implementation, namely, it can take any shape matrix input for the displacement derivatives
    Faster than some matrix manipulations. Can be made faster by wrapping or so if need be.
    """
    u_x, u_y, v_x, v_y=u_i
    # provision for a scalar call
    u_x, u_y, v_x, v_y = np.atleast_1d(u_x), np.atleast_1d(u_y), np.atleast_1d(v_x), np.atleast_1d(v_y)
    #
    #provision for the case where an image is compared with itself to yield trivial zeros
    #
    if  ( (u_x==0).all() and  (u_y==0).all() and (v_x==0).all() and (v_y==0).all() ):
        print "\n\n\n\n\nIT APPEARS YOU COMPARED THE SAME IMAGE\n\n\n\n"
        return (np.zeros_like(u_x) , np.zeros_like(u_x) ,np.zeros_like(u_x) ,np.zeros_like(u_x) )
    
    #
    inputShape = list(u_x.shape)
    C = np.zeros(shape= [2,2]+inputShape )
    b = np.zeros(shape= [2,2]+inputShape )
    eig = np.zeros(shape= [2,] + inputShape)
    e=np.zeros(shape= [2,]+inputShape)
    q=np.zeros( shape= [2,2] + inputShape)

    #u_i=[0.,1.,0.,0.]
    #ex,ey,exy--- finds Lagrangian strain
    Ex = 0.5*(2*u_x + u_x**2  + v_x**2 ) 
    Ey = 0.5*(2*v_y + v_y**2  + u_y**2 )
    Exy = 0.5*(u_y + v_x + u_x*u_y + v_x*v_y)
    #Computing Cauchy (C) deformation tensor
    C[0,0,...]=(1.+u_x)**2.+v_x**2.
    C[1,1,...]=(1.+v_y)**2.+u_y**2.
    C[0,1,...]=(1.+v_y)*v_x+(1.+u_x)*u_y
    C[1,0,...]=C[0,1,...]
    #calculating eigenvalues of Cauchy tensor so that squareroot could be taken
    delta=(C[0,0,...]+C[1,1,...])**2.-4.*(C[0,0,...]*C[1,1,...]-C[1,0,...]**2) #delta is the discriminant of second order eigenvalue equation
    #computing strectch values so eigenvalues
    
    eig[0,...] = (C[0,0,...] + C[1,1,...] + np.sqrt(delta))/2
    eig[1,...] = (C[0,0,...] + C[1,1,...] - np.sqrt(delta))/2
    #constituting matrices with eigenvectors
    
    
    for i in range(2):
	b[0,i,...]=1.
	b[1,i,...]=-(C[0,0,...]-eig[i,...])/C[0,1,...]
	anorm=np.sqrt(b[0,i,...]**2+b[1,i,...]**2)
	b[0,i,...]=b[0,i,...]/anorm
	b[1,i,...]=b[1,i,...]/anorm
    
    
    e[0,...] = eig[0,...]**(0.5)
    e[1,...] = eig[1,...]**(0.5)
    x1  =  e[0,...]*b[1,0,...]**2 + e[1,...]*b[1,1,...]**2
    x12 = b[0,0,...]*b[1,0,...]*e[0,...] + b[0,1,...]*b[1,1,...]*e[1,...]
    x2  = e[0,...]*b[0,0,...]**2+e[1,...]*b[0,1,...]**2
    det = x1*x2-x12**2

#       	q is the rotation matrix
    q[0,0,...]=(1.+u_x)*x1-u_y*x12
    q[1,0,...]=-(1.+v_y)*x12+v_x*x1
    q[0,1,...]=-(1.+u_x)*x12+u_y*x2
    q[1,1,...]=(1.+v_y)*x2-v_x*x12
    
    for i in range(2):
        for j in range(2):
             q[i,j,...]=q[i,j,...]/det
       

# 	from structure of q, deduce teta.

    co=q[0,0,...]		  
    si=q[1,0,...]
    wxy =np.arctan2(si,co)
    theta = -1. * wxy # this makes it counterclockwise rotation with y-axis inverted as is the typical case for image CS

    return(Ex,Ey,Exy,theta)
    


def LagStrainSingleOp(u_i):
    """
    TAKES:
    u_i = list of four derivatives of u, v disp field in 
          the order of  [u_x, u_y, v_x, v_y]
    RETURNS:
    Ex,Ey,Exy,theta = Lag strains and large rotation
    
    NOTES: implementation copied from Fortran legacy DIC codes. Faster than some matrix manipulations. Can be made faster by wrapping or so if need be.
    """
    u_x, u_y, v_x, v_y=u_i
    #u_i=[0.,1.,0.,0.]
    #ex,ey,exy--- finds Lagrangian strain
    Ex = 0.5*(2*u_x + u_x**2  + v_x**2 ) 
    Ey = 0.5*(2*v_y + v_y**2  + u_y**2 )
    Exy = 0.5*(u_y + v_x + u_x*u_y + v_x*v_y)
    # all the remaining operations are to calculate large rotation in the plane
    #Computing Cauchy (C) deformation tensor
    C=np.zeros(shape=(2,2))
    C[0,0]=(1.+u_x)**2.+v_x**2.
    C[1,1]=(1.+v_y)**2.+u_y**2.
    C[0,1]=(1.+v_y)*v_x+(1.+u_x)*u_y
    C[1,0]=C[0,1]
    #calculating eigenvalues of Cauchy tensor so that squareroot could be taken
    delta=(C[0,0]+C[1,1])**2.-4.*(C[0,0]*C[1,1]-C[1,0]**2) #delta is the discriminant of second order eigenvalue equation
    #computing strectch values so eigenvalues
    eig=np.zeros(shape=(2,1))
    eig[0]=(C[0,0]+C[1,1]+np.sqrt(delta))/2
    eig[1]=(C[0,0]+C[1,1]-np.sqrt(delta))/2
    #constituting matrices with eigenvectors
    b= np.zeros(shape=(2,2))
    
    for i in range(2):
	b[0,i]=1.
	b[1,i]=-(C[0,0]-eig[i])/C[0,1]
	anorm=np.sqrt(b[0,i]**2+b[1,i]**2)
	b[0,i]=b[0,i]/anorm
	b[1,i]=b[1,i]/anorm
    
    e=np.zeros(shape=(2,1))
    e[0]=eig[0]**(0.5)
    e[1]=eig[1]**(0.5)
    x1=e[0]*b[1,0]**2+e[1]*b[1,1]**2
    x12=b[0,0]*b[1,0]*e[0]+b[0,1]*b[1,1]*e[1]
    x2=e[0]*b[0,0]**2+e[1]*b[0,1]**2
    det=x1*x2-x12**2

#   q is the rotation matrix
    q=np.zeros(shape=(2,2))
    q[0,0]=(1.+u_x)*x1-u_y*x12
    q[1,0]=-(1.+v_y)*x12+v_x*x1
    q[0,1]=-(1.+u_x)*x12+u_y*x2
    q[1,1]=(1.+v_y)*x2-v_x*x12
    
    for i in range(2):
        for j in range(2):
             q[i,j]=q[i,j]/det
       

#   from structure of q, deduce teta.

    co=q[0,0]		  
    si=q[1,0]
    wxy =np.arctan2(si,co)
    theta = -1. * wxy # this makes it counterclockwise rotation with y-axis inverted as is the typical case for image CS

    return (Ex,Ey,Exy,theta)

def eqPlasticStrain(ex,ey,exy,nu):
    E1=((ex+ey)/2)+ (((ex-ey)/2)**2+exy**2)**0.5;
    E2=((ex+ey)/2)- (((ex-ey)/2)**2+exy**2)**0.5;
    E3=-(nu/(1-nu))*(ex+ey);

    #emNomsk=(1/((sqrt(2))*(1+nu)))*sqrt((E1-E2)**2+(E3-E2)**2+(E1-E3)**2)
    em=(1./((2**0.5)*(1+nu))) * ( (E1-E2)**2 + (E3-E2)**2 + (E1-E3)**2 )**0.5
    return em

def maxShearStrain(ex,ey,exy):
    
    emaxsh=np.sqrt((ey-ex)**2+exy**2)
    
    return emaxsh

def pickStrainComponents(u_i, nu=0.5,varType='Lag', retNames=True, retAsDict=False,percentStrn=False):
    """
    TAKES:
    u_i = list of four derivatives of u, v disp field in 
          the order of  [u_x, u_y, v_x, v_y]
    makes the conditional calculations for desired strain components given displacement derivatives
    """
    if varType=='small':
        names = ['ex','ey','exy','wxy', 'emises', 'emaxsh','emean','e1','e2']
        ex, ey, exy, rot = infStrain(u_i)
        if percentStrn:
            ex, ey, exy, rot =  100.*ex, 100.*ey, 100.*exy, 100.*rot
        
        emises = eqPlasticStrain(ex,ey,exy,nu)
        emaxsh = maxShearStrain(ex,ey,exy)
        emean = 0.5*(ex+ey)
        e1 = emean + emaxsh
        e2 = emean - emaxsh
        if retAsDict:
            result = { 'ex':ex, 'ey':ey, 'exy':exy, 'wxy':rot , 'emises':emises, 
                      'emaxsh':emaxsh, 'emean':emean,'e1':e1, 'e2':e2 }
        else:
            result = (ex,ey,exy,rot,emises,emaxsh,emean,e1,e2)
            
    elif varType=='Lag': #
        names = ['Ex','Ey','Exy','Theta', 'Em', 'Emaxsh']
        ex, ey, exy, rot = LagStrain(u_i)
        em = eqPlasticStrain(ex,ey,exy,nu)
        emaxsh = maxShearStrain(ex,ey,exy)
        if percentStrn:
            ex, ey, exy, rot, em = 100.*ex, 100.*ey, 100.*exy, 100.*rot, 100.*em, 100.*emaxsh

        # still reasonable when rotation is large (due to, e.g., sample rotation during experiment)
        # BUT strains are small
        if retAsDict:
            result = { 'Ex':ex, 'Ey':ey, 'Exy':exy, 'Theta':rot , 'Em':em, 'Emaxsh':emaxsh }
        else:
            result = (ex,ey,exy,rot,em,emaxsh)
    else:
        raise ValueError, "Invalid varType %s"%(varType,)
    #
    if retNames:
        return result, names
    else:
        return result

if __name__=="__main__":
    
    #testing with simple shear
    #u_i = [u_x, u_y, v_x, v_y ]
    u_i = [1,1,1,-1]
    #u_i = [-0.5073832E-02 ,0.2456307E-02 ,0.8382247E-03,0.1111459E-01]
    #u_i = [-0.0075145,1.28170000e-03 ,3.79400000e-04,0.0144117]
    Repeat=1000  # testing speed
    u_is = u_i
    for i in range(Repeat):
        exx1,eyy1,exy1,wxy1 = LagStrain(u_i)
        
