######### Cahit Can Aydiner  2017 ##############################################
#
# simpler postprocessing staying away from cell based BOTTOM TO TOP 
# approach. Frames will be processed and treated with masks
# for any region specialization.
# basic data reader postprocess will still be inherited
# 
# Data plotting and data retrieval should be separated
# dataFrame returns data 
# 

import dicTools; reload(dicTools); from dicTools import *
from scipy import *
import scipy
import scipy.stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl
import os, sys, shutil, copy, pickle
import pylab
from PIL import Image, ImageDraw, ImageOps, ImageFont
import pdb
import os.path as osp
import warnings, logging
import numpy as np

import indRun ; reload(indRun)
import strainOp; reload(strainOp)
import maskmaker ;  reload(maskmaker)
import genericUtility.myFileTools as mft ; reload(mft)
import genericUtility.datafit as dft ; reload(dft)
#
import genericUtility.matrixTools as gumt ; reload(gumt)
from genericUtility.matrixTools import putMatInMat,trim_2d

logger = logging.getLogger(__name__)
longColDict = {'X':0,'Y':1,'u':2,'v':3,'ex':4,'ey':5,'exy':6,'dudx':7,'dvdy':8,'dudy':9,'dvdx':10,'Theta':11,'w':12,'status':13, 'iter':14, 'corrcoeff':15}


#########NEW!
import postProcess; reload(postProcess)
from postProcess import postprocess # class
# to do evaluate the following for necessity
from postProcess import openImage, make2d, rowsorter, mean_std, hideTicks
from postProcess import retCorners, gridPt, gridLine, gridText


# class postprocess
#     def __init__(self, rawDatas ,an = None,...) an retrieves im. file names
#     self.rd is the main matrix
#     def _init delegated
#     def pickfromAnalysisObject(self,an)
#     def pickImage(self, imageNum) #returns matrix of an image/load
#     def checkVar(self,var)
#     def pickVar(self,var)  # returns matrix for a variable at all images
#     def pickVarPixels(self,var,pixelList)
#     def pickAvgVar(self,var,make1D=True)
#     def pickAvgVarPixels(self,var,pixelList,make1D=True)
#     def pickVarAtImage(self,var,imageNum,drop='nothing')
#     def checkStatus(self):
#     def showProblem(self, pointSize=3,fontsize=12,drawCon=True,figStart=5,showAll=False,fontPath='/usr/share/fonts/truetype/DejaVuSans.ttf',**kwds):


def centraldiff_der(u,gridx,gridy):
    """
    u = frame (matrix of a spatial variable)
    gridx , gridy = derivative distance (grid spacing)
    find the x and y derivatives for a sent frame
    """
    u_y, u_x = np.gradient(u,gridx,gridy)
    return (u_x, u_y)


class frameData(postprocess):
    """
    frameData is a postprocessing class for one single frame
    shapes the data into its frame and takes care of
    strain processing for the same frame as needed
    """
    def _init(self,imageNum):
        """
        A frame is the data matrix for one load point
        sets self.i=imageNum
        gets position columns; stores 
        self.nx, self.ny number o x/y points in the frame
        self.gridx,self.gridy grid spacings
        self.cm : boolean frame of clean points
        """
        self.i = imageNum
        x = self.pickVar('X')[self.i,:]
        y = self.pickVar('Y')[self.i,:]
        self.nx = len(np.unique(x))
        self.ny = len(np.unique(y))
        # now use the row/column information to get x and y frames
        xframe = self.pickVarGrid('X')
        yframe = self.pickVarGrid('Y')
        # test the assumption that the spacings are uniform
        t = np.diff(xframe)
        if (t - t[0,0]).any():
            raise ValueError
        else:
            self.gridy = t[0,0]
        #
        t=np.diff(yframe,axis=0)
        if (t - t[0,0]).any(): 
            raise ValueError
        else:
            self.gridx = t[0,0]
        #
        self.cleanPoints()
        return
        

    def shapePars(self):
        """
        finds the row column numbers of a regular X-Y grid
        as well as how to sort the remaining variables
        RETURNS nx, ny , sorter
        LIMITATIONS your grid should be regular
        """
        nx,ny = self.nx,self.ny
        x = self.pickVar('X')[self.i,:]
        y = self.pickVar('Y')[self.i,:]
        x,y = [make2d(var, ny,nx) for var in [x,y]]
        sorter = np.argsort(x,1)
        return nx,ny,sorter

    def pickVarGrid(self, var,**kwds):
        try:
            var = self.pickVarAtImage(var,self.i,**kwds)
        except KeyError: # this is for cleanMask and problemMask really
            exec 'var = self.'+var+'['+ str(self.i) + ',:]'
        var=var.copy()
        nx,ny,sorter = self.shapePars()
        var = make2d(var, ny,nx)
        var = rowsorter(var, sorter)
        return var

    def cleanPoints(self):
        """creates the boolean frame of good stateus dic points"""
        self.cm = self.pickVarGrid("cleanMask")
        return
        
    def naned_data(self,var):
        """ sets nan to points whose analysis status is not clean
        essential for only considering converged data for strain analysis"""
        varframe = self.pickVarGrid(var)
        varframe[np.logical_not(self.cm)] = np.nan
        return varframe
    
    def getStrainFrames(self,retNames=True, retAsDict=True, spGradMethod="u_cdiff", **kwds):
        """
        spGradMethod = the method of assigning the four spatial gradients of displacement
                        "u_cdiff"      =      take the central difference of displacements
                        "subset"       =      use subset gradients
        **kwds is passed onto strainOp.pickStrainComponents
        varType='Lag' is default. 
        """
        if spGradMethod == "u_cdiff":
            u = self.naned_data('u')
            v = self.naned_data('v')
            u_x, u_y = centraldiff_der(u,self.gridx,self.gridy)
            v_x, v_y = centraldiff_der(v,self.gridx,self.gridy)
        elif spGradMethod == "subset":
            u_x = self.naned_data("dudx")
            u_y = self.naned_data("dudy")
            v_x = self.naned_data("dvdx")
            v_y = self.naned_data("dvdy")
        else:
            raise ValueError
        #    
        ret = strainOp.pickStrainComponents([u_x, u_y, v_x, v_y] , 
                        retNames=retNames, retAsDict=retAsDict,**kwds)
        return ret


class frameSpatialData(frameData):
    
    def setFullStrainFrame(self, ret=False, **kwds):
        """gets strains as a dictionary also setting it to the instance
        variable self.D; this way the masked data call can be made
        multiple times for different strain variables without computing 
        the same thing over and over
        ret=True returns rather than storing
        """
        if ret:
            return self.getStrainFrames(**kwds)
        else:
            D, names = self.getStrainFrames(**kwds)
            self.D = D
            return
        
    
    def spatialCoords(self, Xc,Yc, optRes,ret=False, flat=False):
        """
        Xc, Yc = corner coordinates in length units
        optRes = in length unit per pixel
        
        sets self.Xs and self.Ys spatial coordinates in length units 
        # replace this function with a theta aware version (as in frameMatrix
        class) if camera is skewed
        flat: returns the results 1-D
        ret=True returns the results and does not store under the object
        """
        self.optRes = optRes
        if flat:
            xpix = self.pickVar('X')
            ypix = self.pickVar('Y')
        else:
            xpix = self.pickVarGrid('X')
            ypix = self.pickVarGrid('Y')
        Xs = Xc + xpix * optRes
        Ys = Yc + ypix * optRes
        Xslim = np.array([Xs.min(), Xs.max()])
        Yslim = [Ys.min(), Ys.max()]
        if ret:
            self.Xslim, self.Yslim = Xslim, Yslim # these are small; can store anyways
            return Xs, Ys, Xslim, Yslim
        else:
            self.Xs, self.Ys, self.Xslim, self.Yslim = Xs, Ys, Xslim, Yslim
            return
    
    def maskMaker(self, **kwds):
        """ creates a mask for pulling spatially masked data
            **kwds are masmaker arguments stringConditionsList , maskValue"""
        mask = np.ones((self.ny,self.nx),dtype='bool')
        self.mask = maskmaker.maskmaker(mask, self.Xs, self.Ys, **kwds)
        self.trimIndices = trim_2d(self.mask)
        return
        
    def pullMaskedStrain(self,name,spat=True,**kwds):
        """
        spat= True  returns 2D submatrix
            =False  returns 1-D array with spatial connections lost 
        assumes the existence of self.mask and self.D ; 
        only confirmed with horizontal/vertical masks
        """
        strnFullFrame= self.D[name]
        if spat==True:
            a,b,c,d = self.trimIndices
            return strnFullFrame[a:b,c:d]
        else:
            return strnFullFrame[self.mask]

class frameSpatialRectData(frameSpatialData):
    """operates with a paddedRectMask instance"""
    
    def spatialCoords(self, Xc,Yc, optRes,ret=False, flat=False):
        """
        Xc, Yc = corner coordinates in length units
        optRes = in length unit per pixel
        
        sets self.Xs and self.Ys spatial coordinates in length units 
        # replace this function with a theta aware version (as in frameMatrix
        class) if camera is skewed
        """
        self.optRes = optRes
        xpixgr = self.pickVarGrid('X')
        ypixgr = self.pickVarGrid('Y')
        Dxpix, Dypix = np.unique(np.diff(xpixgr)), np.unique(np.diff(ypixgr,axis=0))
        if Dxpix.size!=1: raise ValueError,' class is for regular grids'
        if Dypix.size!=1: raise ValueError,' class is for regular grids'
        self.gridspx, self.gridspy = Dxpix[0] * self.optRes , Dypix[0] * self.optRes
        # made the check can still return flat if need be
        if flat:
            xpix = self.pickVar('X')
            ypix = self.pickVar('Y')
        else:
            xpix = xpixgr
            ypix = ypixgr
        Xs = Xc + xpix * optRes
        Ys = Yc + ypix * optRes
        Xslim = np.array([Xs.min(), Xs.max()])
        Yslim = [Ys.min(), Ys.max()]
        if ret:
            self.Xslim, self.Yslim = Xslim, Yslim # these are small; can store anyways
            return Xs, Ys, Xslim, Yslim
        else:
            self.Xs, self.Ys, self.Xslim, self.Yslim = Xs, Ys, Xslim, Yslim
            return
        return

    def maskMaker(self,**kwds):
        """
        kwds contains lims = (xlow, xhi, ylow, yhi) list that is sent to the 
        __init__ of paddedRectMask which admits posMat,gridSpacing,lims
        """       
        self.padMask = maskmaker.paddedRectMask((self.Xs,self.Ys),
                                            (self.gridspx,self.gridspy),**kwds)
        return 

    def pullMaskedStrain(self,name,**kwds):
        strnFullFrame= self.D[name]
        self.padMask.pullData(data=strnFullFrame)
        # and the result is under self.padMask.resMat
        #
        return
    #

   
         
if __name__=="__main__":
    #
    fil = u'/home/can/MDM/MC/exp20/att1microLP11/an95.p'
    ans = pickle.load(open(fil))
    #
    #p = frameData(ans.rawdatamatrices,an=ans.analyses[0], colDict=longColDict, 
    #                imageNum=0)
    #D,names = p.getStrainFrames(varType='small')
    ps = frameSpatialData(ans.rawdatamatrices,an=ans.analyses[0], 
                        colDict=longColDict, imageNum=0)
    ps.setFullStrainFrame(varType='small')
    ps.spatialCoords(Xc=3,Yc=4,optRes=0.19e-3) 
    ps.maskMaker(stringConditionsList=['X<3.2','X>3.3','Y<4.1','Y>4.15'], maskValue=[0,0,0,0])
    exm = ps.pullMaskedStrain('ex')
                        
    
    
    
    
