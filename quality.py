
##########################
#CCA  FEB 2016
#
#comments and descriptions a little non specific. We are at an explarotary stage.
#
###########################

import numpy as np
import scipy
import Image
from scipy import ndimage
import matplotlib.pyplot as plt


interp = ndimage.interpolation

def errImMat(mat1,mat2, errType='simpleL2'):
    """
    scalar measure of the difference between two matrices,
    simpL2: means simple root mean square error
    """
    if errType == 'simpleL2':
         err = (sum(sum((mat1-mat2)**2))/mat1.size)**0.5
    else:
        raise ValueError
    return err
    

def errLandscape(ref,comp,domainSize,x0,y0,keep='u',rng=[-5,5],spacing=0.1,u=0,v=0,interpOrder=1,plotit=False,errType='simpleLS'):  
    """
    compares a subset in image ref to subpixel shifted subsets in image comp. That is why there is interporder in play. It does not take any straining into account. So meant for translation studies or same position error determination.
    Keeps the v (or u) position fixed. rng and spacing define an array of shift points. If v is too far off from the (u,v) minima, the results may not be as desired. 
    Usually, when this is used images are within a pixel shifted with respect to each other. So, v=0 is good.
    ref = original image matrix
    comp = comparison image matrix
    domainSize = square domain size 
    keep  if 'u' means to make a v scan; if 'v' will make a u scan for error
    x0, y0 =  pixel coordinates of top left corner of the subset in the reference image
    rng, spacing = error computation points in u; by convention send a range [-a,a]
    u = central u point
    v = central v point
    """
    mn,mx = rng
    arr = np.arange(mn,mx,spacing)
    indsx, indsy = np.meshgrid(np.arange(domainSize),np.arange(domainSize))
    inds = [indsx+x0,indsy+y0]  
    #this is the region from reference image
    subref = ref[inds]
    subref =subref.astype(float)
    errs=[]

    for i,s_ in enumerate(arr):
        #this is the region taken from the deformed image
        if keep == 'u':
            newinds = [indsx+ x0 + u + s_ , indsy+ y0 + v]
        elif keep == 'v':
            newinds = [indsx+ x0 + u , indsy+ y0 + v + s_]
        #
        newsub= interp.map_coordinates(comp, newinds,order=interpOrder)
        newsub = newsub.astype(float)
        er = errImMat(newsub,subref, errType='simpleL2')
        errs.append(er)
    
    plt.plot(arr,errs,'o-')
    #corr1
    return errs


def addGaussianNoise(imMat,stdev,mean=0.):
    """
    adds Gaussian noise to an image
    imMat = original image matrix
    stdev = standard deviation as a float
    mean = how much mean to add ideally close to zero or zero if you have no reason
    """
    a,b = imMat.shape
    rand=stdev*np.random.randn(a,b)+mean
    randInt = np.round(rand)
    plt.hist(randInt.ravel(),bins=100, color='r')
    newMat = randInt+imMat
    newMat[newMat>255] = 255
    newMat[newMat<0] = 0
    return (newMat,randInt)

def intensityHist(im1):
    """mona:given one image matrix it yields the intensity histogram."""
    
    ref = np.array(im1).astype(float)
   
    plt.hist(ref.ravel(),bins=255)
   
    stdev=np.std(ref)
    mean=np.mean(ref)
    var=np.var(ref)
    
    return (ref,stdev,mean,var)


def diffHist(im1, im2):
    """given two image matrices it yields the difference between the two as an histogram."""
    
    ref = np.array(im1).astype(float)
    comp = np.array(im2).astype(float)

    dif = ref-comp

    plt.hist(dif.ravel(),bins=100)

    stdev=np.std(dif)
    mean=np.mean(dif)
    var=np.var(dif)
    
    return (dif,stdev,mean,var)

def denoise():
    """There is built in openCV stuff e.g cv2.fastNlMeansDenoising() worth checking out. We can also code in FFT based stuff but that might create ripples as well (or I am worried about it). There are three publications we have detected on removing noise before the application

    ndimage.gaussian_filter
    cv2.fastNlMeansDenoising
    """
    pass
def frequencyContent():
    """Check the intensity frequencies in an image. This might layout good or bad patterns  since a good DIC pattern has features int the 2-5 pixel frequency, say for subset sizes of 40x40. If an image does not have features at this frequency, the pattern should not have enough signature. If on top, there is noise in this frequency, things might go bad.

    scipy.fftpack
    """
    
    pass
