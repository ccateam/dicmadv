################CCA 2017 #################
# 
# classes for multi-frame (big) plots
# 2018 MAR
# introduction of commonDataBrute
# commonData tries to find many elegant solutions by avoiding interpolation
# on the original data, but assumes ideal data placement, that the detector
# has no tilt and the Newport positioners make no errors.
# Both are not true and bringing merger coordinates from image analysis (e.g. FIJI
# or frameGrid.refineRel.. if it worked reliablely) shows that images are at least
# shifted with respect to each other. 
# This means if the data is to be put on a big matrix by merging you cannot actually
# physically avoid interpolation (a matrix by its nature will assume that spacings are
# equal)
# Since interpolation cannot be avoided, why not simplify everything about merger that is
# so complicated and also flimsy. 
# Just ORDER a big COORDINATE MATRIX AND get all values from appended values of 
# all images
#############################################################################

#############
import pickle,sys,os
import os.path as osp
from scipy import misc
from scipy import ndimage
import scipy.interpolate
import numpy as np 
import matplotlib.pyplot as plt
import time
import logging
import multiprocessing
from scimath.units.length import micrometer,mm 
logger = logging.getLogger(__name__)
#
import maskmaker ;  reload(maskmaker)
import frameMatrix ;  reload(frameMatrix)
import npostProcess as npp;  reload(npp)
import nplot; reload(nplot)

import genericUtility.matrixTools as gumt ; reload(gumt)
from genericUtility.matrixTools import uniqueChecker

from scipy.interpolate import NearestNDInterpolator,ndgriddata
class myNearestNDInterpolator(NearestNDInterpolator):
    def __call__(self,n_jobs=1, *args):
        """
        overload with n_jobs
        n_jobs = integer value or string "max"

        Parameters
        ----------
        xi : ndarray of float, shape (..., ndim)
            Points where to interpolate data at.

        """
        if n_jobs=="max":
            n_jobs=multiprocessing.cpu_count()-1
        xi = ndgriddata._ndim_coords_from_arrays(args, ndim=self.points.shape[1])
        xi = self._check_call_shape(xi)
        xi = self._scale_x(xi)
        dist, i = self.tree.query(xi,n_jobs=n_jobs) #! changed
        return self.values[i]


class commonData:
    def __init__(self,bigC, frameGridIns,imageDir=os.environ["DICIMAGEPTH"],
                        varType='small'):
        """
        bigC: an array that contains postprocess class of all frames; 
        the postprocess class is supposed to be frameSpatialRectData
        frameGridIns: instance of frameGrid class that exactly corresponds to
        bigC
        varType: 'small' or 'Lag' for strain types 
        """
        self.bigC=bigC
        self.fg = frameGridIns
        self.varType=varType
        # frameGridIns has the image files pulled from the database
        # along with spatial coordinates
        self.setFrameSizeFromImageFile(imageDir) 
        return
    
    def setFrameSizeFromImageFile(self,imageDir):
        """
        sets self.frameSizePix and self.frameSizemm. mm is special
        since machine coordinates of the database are in mm.
        """
        firstImage = self.fg.files[0]
        imarr = misc.imread( osp.join(imageDir,firstImage)  )
        numrow,numcol = imarr.shape
        numx , numy = numcol, numrow
        self.frameSizePix = [numx, numy]
        oRes = self.fg.oResUnit / mm #in mm/pixel
        self.frameSizemm = (numx*oRes, numy*oRes )
        return
    
    
    def setPositionsStrainsOverFrames(self,**kwds):
        """ set spatial coordinates of each frame in the frameSpatialData
        instance; also prep strains"""
        oResmm = self.fg.oResUnit / mm
        for i in range(self.fg.numrow):
            for j in range(self.fg.numcol):
                ii = self.fg.retGrid(i,j) # return the linear counter
                frdata = self.bigC[ii]
                x,y = self.fg.Dx[i,j,:]
                frdata.spatialCoords(x,y,oResmm)
                #
                frdata.setFullStrainFrame(varType=self.varType,**kwds)
                #
        #   
        return
    
    def analyzeDataRegions(self,outerborderTreat='crop',**kwds):
        """outerborderTreat='crop': straighten border by cutting protruding data
                         = 'relaxed': straighten by extanding hence adding nan 
                         regions
           at frame connections there is admitted spatial error: The spacing of the
           data between the last point of one frame and first point of the next
           frame will deviate from overall uniform DIC grid spacing d (10 pixel these days)
           The distance from final data point to limit line (A) in both frames is ideal 
           and will not make such an error if it is A=0.5d. Can't, in general, have that. But,
           can keep it in 0.25d<A<0.75d. The code of this funcition is longer with
           concerns over matrices like phaseErrorx and phaseErrory to guarantee
           that.
        """ 
        numrow,numcol = self.fg.numrow,self.fg.numcol
        self.anlimlowx = np.zeros((numrow, numcol+1)) # a zero column added on the right
        self.anlimhix = np.zeros((numrow,numcol+1)) # a zero column added on the left
        # the zero columns added from right and left will allow matrix averaging
        # for the overlap mean
        self.anlimlowy = np.zeros((numrow+1, numcol)) # a zero row added at the bottom
        self.anlimhiy = np.zeros((numrow+1,numcol)) # a zero row added on the top
        # zero rows added from the bottom and top will allow ...
        for i in range(numrow):
            for j in range(numcol):
                ii = self.fg.retGrid(i,j) # return the linear counter
                frdata = self.bigC[ii]
                self.anlimlowx[i,j] = frdata.Xslim[0] #lowlim 
                self.anlimhix[i,j+1] = frdata.Xslim[1] # j+1 ! hilim
                #
                self.anlimlowy[i,j] = frdata.Yslim[0] #lowlim 
                self.anlimhiy[i+1,j] = frdata.Yslim[1] # i+1 ! hilim
        # will grab gridspacing in mm from the last frame. It should not change
        # over the frames anyway for the current contigous matrix strategy.
        gridspx,gridspy = frdata.gridspx, frdata.gridspy
        nudgex,nudgey = gridspx*0.5 , gridspy*0.5
         
        self.xlim = (self.anlimlowx+self.anlimhix)/2.# this sets overlap centers
        # the average is wrong for left and right borders, recover values
        self.xlim[:,0], self.xlim[:,-1] = self.anlimlowx[:,0], self.anlimhix[:,-1]
        #
        self.ylim = (self.anlimlowy+self.anlimhiy)/2.
        self.ylim[0,:], self.ylim[-1,:] = self.anlimlowy[0,:], self.anlimhiy[-1,:]
        #
         # ensure homogeneity of limits along outer borders
        K = self.ylim[0,:]   #TOP!
        L = self.ylim[-1,:] #BOT
        M = self.xlim[:,0]  #LEFT
        N = self.xlim[:,-1] # right
        if outerborderTreat=='crop':
            self.ylim[0,:] = K.max()
            self.ylim[-1,:]= L.min()
            self.xlim[:,0] = M.max()
            self.xlim[:,-1] = N.min()
        elif outerborderTreat=='relax':
            self.ylim[0,:] = K.min()
            self.ylim[-1,:]= L.max()
            self.xlim[:,0] = M.min()
            self.xlim[:,-1] = N.max()
        else: 
            raise ValueError,"Invalid option for outerborderTreat"
        
        # NOW WORK OVER PHASE ERRORS AMONG DATA
        # THE EFFORT IS TO STAY AS CLOSE AS POSSIBLE TO THE DATA GRID SPACING
        # AT OVERLAP CONNECTIONS. IF DATA SPACING IS 10 PIXELS YOU
        # TRY TO MAKE SURE THE PHYSICAL CONNECTION STAYS BETWEEN 7.5 AND 12.5.
        self.holx = (self.anlimhix-self.anlimlowx)/2.# half overlap
        self.holx[:,0], self.holx[:,-1] =0. , 0.
        #self.holx[:,0], self.holx[:,-1] =self.xlim[:,0] , self.xlim[:,-1] # first and last cols wont have overlap
        # when there is sufficient overlap self.holx should not be negative
        if (self.holx<0).any(): 
            logger.warn("overlap failure in X at %s frame connections"%(self.holx<0).sum())
        #
        fractionx,remx = np.modf(self.holx/gridspx) # positions as a fraction of grid spacing
        fractionx=np.abs(fractionx) # when there is a gap fractionx comes neg.
        # when connection points are too close or too far; it is best to 
        # change the phase such that one stays around the ideal fractionx=0.5
        # to see why 0.5 is ideal sketch an ideal continuatation of two separate grids
        # (this means grid spacing is preserved) and see the average.
        phaseErrorx = np.logical_or( (fractionx<0.25) , (fractionx>0.75) )
        phaseErrorx[:,0] = False # this has nothing to do with outer borders; all about connections
        phaseErrorx[:,-1] = False # right border
        #self.xlimold = self.xlim.copy() # just to debug! erase
        self.xlim[phaseErrorx]+=nudgex # for nudged crossings; holx will not be accurate!
        #
        
        self.holy = (self.anlimhiy-self.anlimlowy)/2.# half overlap
        self.holy[0,:], self.holy[-1,:] = 0 , 0
        #self.holy[0,:], self.holy[-1,:] =self.ylim[0,:] , self.ylim[-1,:] # first and last rows wont have overlap
        # when there is sufficient overlap self.holx should not be negative
        if (self.holy<0).any(): 
            logger.warn("overlap failure in Y at %s frame connections"%(self.holy<0).sum())
        #
        
        fractiony,remy = np.modf(self.holy/gridspy) # positions as a fraction of grid spacing
        fractiony=np.abs(fractiony) #when there is a gap, fractiony too comes neg.
        phaseErrory = np.logical_or( (fractiony<0.25) , (fractiony>0.75) )
        phaseErrory[0,:] = False # top border
        phaseErrory[-1,:] = False # bot border
        self.fractiony,self.remy = fractiony,remy
        #self.phaseErrory = phaseErrory # just to debug! erase
        #self.ylimold = self.ylim.copy() # just to debug! erase
        self.ylim[phaseErrory]+=nudgey
         
       
        return
        
        #
    
    def setMasks(self,eps=1.e-8,**kwds):
        """ set spatial coordinates of each frame in the frameSpatialData
        instance; also prep strains and masks; figures out the total size of the
        contiguous matrices as predicted by paddedmasks that are produced.
        self.numBigRows,numBigCols,SizesRow,SizesCol are set.
        """
        oResmm = self.fg.oResUnit / mm
        numrow,numcol = self.fg.numrow,self.fg.numcol
        self.SizesRow = np.zeros((numrow,numcol),dtype = 'int')
        self.SizesCol = np.zeros((numrow,numcol),dtype = 'int')
        for i in range(numrow):
            for j in range(numcol):
                ii = self.fg.retGrid(i,j) # return the linear counter
                frdata = self.bigC[ii]
                #
                xlimlow  = self.xlim[i,j]-eps
                xlimhi = self.xlim[i,j+1]+eps
                ylimlow = self.ylim[i,j]-eps
                ylimhi = self.ylim[i+1,j]+eps
                #
                frdata.maskMaker(lims=(xlimlow,xlimhi,ylimlow,ylimhi))                 
                self.SizesRow[i,j], self.SizesCol[i,j] = frdata.padMask.regionShape
        # 
        numBigRows = self.SizesRow.sum(axis=0)
        numBigCols = self.SizesCol.sum(axis=1)
        self.numBigRows = uniqueChecker(numBigRows)
        self.numBigCols = uniqueChecker(numBigCols)
        #
        return        
    
    def contiguousCoordMatrix(self,typ = 'undef'):
        """
        returns a mm based X,Y  location matrices that correspond to the big 
        data matrix.
        typ = 'undef' in the undef coordinates; if an overlay is used; overlaid
                      images should be undef configuration
        typ = 'def'   in deformed coordinates X+u, Y+v; does not provide a 
                      regular grid
        """            

        numy,numx = self.numBigRows,self.numBigCols
        Xst,Xend,Yst, Yend = self.xlim.min(),self.xlim.max(),self.ylim.min(),self.ylim.max() # limits in mm
        X = np.linspace(Xst,Xend,numx)
        Y = np.linspace(Yst,Yend,numy)
        gridspx,gridspy = (Xend-Xst)/(numx-1), (Yend-Yst)/(numy-1),
        if typ == 'undef':
            # changed members and return variables to be consistent with
            # commonDataBrute.setOrderedCoords
            self.orderedlinX = X
            self.orderedlinY = Y
            self.orderedCoor = np.meshgrid(X,Y)
            self.orderedList = np.vstack( ( self.orderedCoor[0].ravel(), self.orderedCoor[1].ravel() ) )
            #
            return (Xst,Xend,Yst,Yend), (gridspx,gridspy)
        else:
            raise NotImplementedError
        
                            
    def contiguousDataMatrix(self,strainComponent):
        """
        produces a continuous data matrix based on machine coords
        
        """
        numrow,numcol = self.fg.numrow,self.fg.numcol
        BigMat = np.zeros((self.numBigRows,self.numBigCols))
        placementRow = self.SizesRow.cumsum(axis=0)
        placementCol = self.SizesCol.cumsum(axis=1)
        #
        for i in range(numrow):
            for j in range(numcol):
                # get the placement location
                if i==0: 
                    stRow=0
                else:
                    stRow = placementRow[i-1,j]
                if j==0: 
                    stCol=0
                else:
                    stCol = placementCol[i,j-1]
                #
                fnRow, fnCol = placementRow[i,j], placementCol[i,j]
                #
                # get the data
                ii = self.fg.retGrid(i,j) # return the linear counter
                frdata = self.bigC[ii]
                frdata.pullMaskedStrain(strainComponent)
                BigMat[stRow:fnRow,stCol:fnCol]=frdata.padMask.resMat
        #
        return BigMat

class commonDataBrute(commonData):
    def setPositionsStrainsOverFrames(self,**kwds):
        """ set spatial coordinates of each frame in the frameSpatialData
        instance; also prep strains
        This overloads the commonData version and does not store coordinates or
        values under individual frames.
        It rather forms BIG coor arrays and strain arrays under the names
        self.bigCoor and self.D
        These will be used for interpolation
        """
        oResmm = self.fg.oResUnit / mm
        # pull strain component names
        frdata = self.bigC[self.fg.retGrid(0,0)]
        D, strainNames = frdata.setFullStrainFrame(ret=True,varType=self.varType,
                                                   retNames=True, retAsDict=True,**kwds)
        # empty this D to store the overall array sets
        for key in D.keys(): 
            D[key]=[]
        bigCoor= []
        for i in range(self.fg.numrow):
            for j in range(self.fg.numcol):
                ii = self.fg.retGrid(i,j) # return the linear counter
                frdata = self.bigC[ii]
                x,y = self.fg.Dx[i,j,:]
                Xs, Ys, Xslim, Yslim = frdata.spatialCoords(x,y,oResmm,
                                                            ret=True,flat=True)
                bigCoor.append(np.vstack((Xs,Ys)))#
                Strains = frdata.setFullStrainFrame(ret=True, 
                                                    varType=self.varType, 
                                                    retAsDict=False, 
                                                    retNames=False,**kwds)
                for k,component in enumerate(Strains):
                    D[strainNames[k]].append(component.ravel())
        self.bigCoor=np.hstack(bigCoor)
        for name in strainNames:
            D[name]=np.hstack(D[name])
        self.D = D
        
        return

        
    def setOrderedCoords(self, coorLims=None, spacing=None,eps=1.e-8,**kwds):
        """
        coorLims = (Xst, Xend, Yst, Yend) in mm
        spacing =  (xspacing, yspacing) in mm
        typical to send outerborderTreat='crop' or 'relax' to automatic detection 
        (if specifications are kept None)
        DOES:
        ordered coordinates are the BIG coordinates at which values will be interpolated from
        the data of all frames, order as in ordering pizza in meaning
        stored under self.orderedCoor
        """
        if coorLims==None:
            self.analyzeDataRegions(**kwds) 
            Xst, Xend  = self.xlim.min(), self.xlim.max() 
            Yst, Yend  = self.ylim.min(), self.ylim.max()
        else:
            Xst, Xend, Yst, Yend=coorLims
            # use the complex function analyzeDataRegions to check boundary limits 
        if spacing ==None:
            # grab it from one of the first frame
            frdata =  self.bigC[self.fg.retGrid(0,0)]
            gridspx , gridspy = frdata.gridspx, frdata.gridspy
        else: 
            gridspx , gridspy = spacing
        self.gridspx = gridspx
        self.gridspy = gridspy
        X = np.arange(Xst, Xend+eps, gridspx)
        Y = np.arange(Yst, Yend+eps, gridspy)
        self.orderedlinX = X
        self.orderedlinY = Y
        self.orderedCoor = np.meshgrid(X,Y)
        self.orderedList = np.vstack( ( self.orderedCoor[0].ravel(), self.orderedCoor[1].ravel() ) )
        #
        return (Xst,Xend,Yst,Yend), (gridspx,gridspy)
    
    def detectHoles(self,**queryKwds):
        #from scipy.spatial import cKDTree
        # use an arbitrary strain component we are after the tree at this point
        self.interp=myNearestNDInterpolator(self.bigCoor.T,self.D[self.D.keys()[0]]) 

        distances, i = self.interp.tree.query(self.orderedList.T,**queryKwds)
        shp =  self.orderedCoor[0].shape
        distances = distances.reshape(shp)
        
        Dmax = np.linalg.norm([self.gridspx, self.gridspy])/2. 
        # this is the largest distance a point can fall in the grid
        #
        self.Dmaxfilter = np.zeros_like(distances).astype(bool)
        self.Dmaxfilter[distances>Dmax] = True
        return 
    
    def contiguousDataMatrix(self,strainComponent,putNanToDataGaps=True,
                             helperMat=None,putBack=False,n_jobs=1):
        """
        produces a continuous data matrix based on machine coords
        putBack =  put the Dmaxfiltered points back in if helper does not
                    fill them; a good idea if very small regions are left in a 
                    spatial map. Not a good idea for any other purpose
        """
        self.interp.values= self.D[strainComponent]
        xord, yord = self.orderedCoor
        BigMat = self.interp.__call__(n_jobs,xord,yord)
        if putBack:
            BigMatcp = BigMat.copy()
        if putNanToDataGaps:
            BigMat[self.Dmaxfilter]=np.nan
        else:
            pass
        try :
            helperMat.dtype
            BigMat[self.Dmaxfilter]=helperMat[self.Dmaxfilter]
            if putBack:
                noHelp = np.isnan(helperMat)
                BigMat[noHelp] = BigMatcp[noHelp]
        except AttributeError:
            pass # no help op
            
        #
        return BigMat


class commonDataBruteRelaxed(commonDataBrute):
    """
    follows commonDataBrute type processing however the postprocess frames are 
    not enforced to be all the frames in the imaging set
    """
    def __init__(self,bigC_dict, frameGridIns,
                 imageDir='/home/can/imageCentral/',varType='small'):
        """
        bigC_dict: a dictionary whose keys are frameNos and 
        values are postprocess instances of the corresponding frames; the
        dictionary does not have to be comprehensive
        the postprocess class is supposed to be frameSpatialRectData
        frameGridIns: instance of frameGrid class that exactly corresponds to
        the overall frame grid at this load 
        varType: 'small' or 'Lag' for strain types 
        Example: 
            
            if the image scane is 6x16 frameGradeIns will belong to that
        frame grid with all the coordinate relations. Included frameNos can be 
        just several frames out of the 96 like 19, 26 and 45
        bigCDict = {19:...,26:..., 45:... }
        """
        self.bigC_dict=bigC_dict
        self.frameNos = np.sort(self.bigC_dict.keys()).tolist() 
        # sorting in numerical order is not necessary but just aesthetic
        self.fg = frameGridIns
        self.varType=varType
        
        # frameGridIns has the image files pulled from the database
        # along with spatial coordinates
        self.setFrameSizeFromImageFile(imageDir) 
        return
    def setPositionsStrainsOverFrames(self,**kwds):
        """
        """
        oResmm = self.fg.oResUnit / mm
        # pull strain component names
        frdata = self.bigC_dict[self.frameNos[0]]
        D, strainNames = frdata.setFullStrainFrame(ret=True,varType=self.varType,
                                                   retNames=True, retAsDict=True)
        # empty this D to store the overall array sets
        for key in D.keys(): 
            D[key]=[]
        bigCoor= []
        for ii in self.frameNos:
            frdata = self.bigC_dict[ii]
            i,j = self.fg.retIJ(ii)
            x,y = self.fg.Dx[i,j,:] # these are metric corner coords
            Xs, Ys, Xslim, Yslim = frdata.spatialCoords(x,y,oResmm,
                                                        ret=True,flat=True)
            bigCoor.append(np.vstack((Xs,Ys)))#
            Strains = frdata.setFullStrainFrame(ret=True, 
                                                varType=self.varType, 
                                                retAsDict=False, 
                                                retNames=False)
            for k,component in enumerate(Strains):
                D[strainNames[k]].append(component.ravel())
        self.bigCoor=np.hstack(bigCoor)
        for name in strainNames:
            D[name]=np.hstack(D[name])
        self.D = D
        
        return
    def setOrderedCoords(self, coorLims, spacing,eps=1.e-8,**kwds):
        """
        coorLims = (Xst, Xend, Yst, Yend) in mm
        spacing =  (xspacing, yspacing) in mm
        typical to send outerborderTreat='crop' or 'relax' to automatic detection 
        (if specifications are kept None)
        DOES:
        SAME as in parent class but enforces coorLims and spacing
        """
        Xst, Xend, Yst, Yend=coorLims
        gridspx , gridspy = spacing
        self.gridspx = gridspx
        self.gridspy = gridspy
        X = np.arange(Xst, Xend+eps, gridspx)
        Y = np.arange(Yst, Yend+eps, gridspy)
        self.orderedlinX = X
        self.orderedlinY = Y
        self.orderedCoor = np.meshgrid(X,Y)
        self.orderedList = np.vstack( ( self.orderedCoor[0].ravel(), self.orderedCoor[1].ravel() ) )
        #
        return
        

class commonImage(commonData):
    def __init__(self,frameGridIns,factor =0.1, imageDir='/home/can/imageCentral/'):
        """
        frameGridIns: instance of frameGrid class 
        factor: reduction factor of images; a 500 megapixel final image is not
                practical
        #
        commonImage is an image merger which has similar but simpler 
        operation to commonData. So commonImage inherits commonData though it
        only uses one function without overwriting.
        The only variable is intensity and there is no
        need for the postprocess objects. The masks that will be produced 
        are not common to data masks of DIC grid. So, I separated
        it to this class.
        """
        self.imageDir = imageDir
        self.fg = frameGridIns
        self.factor=factor
        self.files =  [osp.join(self.imageDir,fil) for fil in self.fg.files]
        # self.fg.files has all the files
        self.setFrameSizeFromImageFile() 
        numx, numy = self.frameSizePix
        oRes = self.fg.oResUnit / mm #in mm/pixel
        
        return  
    def setFrameSizeFromImageFile(self):
        """
        sets self.frameSizePix and self.frameSizemm. mm is special
        since machine coordinates of the database are in mm.
        """
        firstImage = self.files[0]
        imarrOri = misc.imread( osp.join(self.imageDir,firstImage)  )
        imarr = ndimage.interpolation.zoom(imarrOri,self.factor)
        numyOri,numxOri = imarrOri.shape
        numy , numx = imarr.shape
        self.frameSizePix = [numx, numy]
        oResOri = self.fg.oResUnit / mm #in mm/pixel,original
        self.frameSizemm = (numxOri*oResOri, numyOri*oResOri) # get mm sizes from ori
        # with interpolation metric will in general be slightly distorted
        oResx , oResy = self.frameSizemm[0]/numx , self.frameSizemm[1]/numy
        self.localCoords = np.meshgrid(np.arange(numx)*oResx,np.arange(numy)*oResy)
        self.oResxmm,self.oResymm = oResx,oResy
        return

    def analyzeDataRegions(self,outerborderTreat='crop',**kwds):
        """outerborderTreat='crop': straighten border by cutting protruding data
                         = 'relaxed': straighten by extanding hence adding nan 
                         regions
         Since image frames are uniform in size; avoiding certain complications in
         the analyzeDataRegions of commonData class.
        """ 
        eps = 1.e-8 # watch!
        numrow,numcol = self.fg.numrow,self.fg.numcol
        self.anlimlowx = np.zeros((numrow, numcol+1)) # a zero column left on the right
        self.anlimhix = np.zeros((numrow,numcol+1)) # a zero column left on the left
        # the zero columns added from right and left will allow matrix averaging
        # for the overlap mean
        self.anlimlowy = np.zeros((numrow+1, numcol)) # a zero row left at the bottom
        self.anlimhiy = np.zeros((numrow+1,numcol)) # a zero row left on the top
        # zero rows added from the bottom and top will allow ...
        xs,ys =  self.frameSizemm
        for i in range(numrow):
            for j in range(numcol):
                xc,yc = self.fg.Dx[i,j,:]
                #
                self.anlimlowx[i,j] = xc #lowlim 
                self.anlimhix[i,j+1] = xc + xs #hilim
                #
                self.anlimlowy[i,j] = yc
                self.anlimhiy[i+1,j] = yc + ys
        
        self.xlim = (self.anlimlowx+self.anlimhix)/2.
        self.xlim[:,0], self.xlim[:,-1] = self.anlimlowx[:,0]-eps, self.anlimhix[:,-1]+eps
        self.holx = (self.anlimhix-self.anlimlowx)/2.# half overlap
        self.holx[:,0], self.holx[:,-1] =0. , 0. # first and last cols wont have overlap
        # when there is sufficient overlap self.holx should not be negative
        if (self.holx<0).any(): 
            logger.warn("overlap failure in X at %s frame connections"%(self.holx<0).sum())
        #
        self.ylim = (self.anlimlowy+self.anlimhiy)/2.
        self.ylim[0,:], self.ylim[-1,:] = self.anlimlowy[0,:]-eps, self.anlimhiy[-1,:]+eps
        self.holy = (self.anlimhiy-self.anlimlowy)/2.# half overlap
        self.holy[0,:], self.holy[-1,:] =0. , 0. # first and last rows wont have overlap
        # when there is sufficient overlap self.holx should not be negative
        if (self.holy<0).any(): 
            logger.warn("overlap failure in Y at %s frame connections"%(self.holy<0).sum())
        # check homogeneity of limits along outer borders
        K = self.ylim[0,:]   #TOP!
        L = self.ylim[-1,:] #BOT
        M = self.xlim[:,0]  #LEFT
        N = self.xlim[:,-1] # RIGHT
        #
        if outerborderTreat=='crop':
            self.ylim[0,:] = K.max()
            self.ylim[-1,:]= L.min()
            self.xlim[:,0] = M.max()
            self.xlim[:,-1] = N.min()
        elif outerborderTreat=='relax':
            self.ylim[0,:] = K.min()
            self.ylim[-1,:]= L.max()
            self.xlim[:,0] = M.min()
            self.xlim[:,-1] = N.max()
        else: 
            raise ValueError,"Invalid option for outerborderTreat"
        #
        return  
                    
    def setMasks(self,**kwds):
        """ set spatial coordinates of each frame in the frameSpatialData
        instance; also prep strains and masks; figures out the total size of the
        contiguous matrices as predicted by paddedmasks that are produced.
        self.numBigRows,numBigCols,SizesRow,SizesCol are set.
        """
        numrow,numcol = self.fg.numrow,self.fg.numcol
        self.SizesRow = np.zeros((numrow,numcol),dtype = 'int')
        self.SizesCol = np.zeros((numrow,numcol),dtype = 'int')
        xloc,yloc = self.localCoords # local coord matrices
        self.masks=[None,]*(numrow*numcol)
        for i in range(numrow):
            for j in range(numcol):
                xc,yc = self.fg.Dx[i,j,:]
                #corner coordinates
                xlimlow  = self.xlim[i,j]
                xlimhi = self.xlim[i,j+1]
                ylimlow = self.ylim[i,j]
                ylimhi = self.ylim[i+1,j]
                #
                msk = maskmaker.paddedRectMask((xc+xloc,yc+yloc),
                                            (self.oResxmm,self.oResymm), 
                                        lims=(xlimlow,xlimhi,ylimlow,ylimhi))                 
                #
                self.SizesRow[i,j], self.SizesCol[i,j] = msk.regionShape
                ii = self.fg.retGrid(i,j)
                self.masks[ii] = msk # just a matter of consistency
                                    # every linear list/array has been stored with 
                                    # ii reverse order
        #
        numBigRows = self.SizesRow.sum(axis=0)
        numBigCols = self.SizesCol.sum(axis=1)
        self.numBigRows = uniqueChecker(numBigRows)
        self.numBigCols = uniqueChecker(numBigCols)
        #
        return        

    def contiguousImageMatrix(self):
        """
        produces a stitched image file based on machine coords
        """
        numrow,numcol = self.fg.numrow,self.fg.numcol
        BigMat = np.zeros((self.numBigRows,self.numBigCols))
        placementRow = self.SizesRow.cumsum(axis=0)
        placementCol = self.SizesCol.cumsum(axis=1)
        #
        for i in range(numrow):
            for j in range(numcol):
                # get the placement location
                if i==0: 
                    stRow=0
                else:
                    stRow = placementRow[i-1,j]
                if j==0: 
                    stCol=0
                else:
                    stCol = placementCol[i,j-1]
                #
                fnRow, fnCol = placementRow[i,j], placementCol[i,j]
                #
                # get the data
                ii = self.fg.retGrid(i,j) # return the linear counter
                I = misc.imread(self.files[ii])
                Ired = ndimage.interpolation.zoom(I,zoom=self.factor)
                self.masks[ii].pullData(Ired)
                BigMat[stRow:fnRow,stCol:fnCol]=self.masks[ii].resMat
        # left,right,bot,top ordering for origin=upper and extent=...
        return BigMat

#
#        
def bigcMaker(files,npy=False,loadNum=0):
    """npy=True is much faster if you first process the files with npyMaker"""
    st = time.time()
    bigc=[]
    for fil in files:
        print fil
        if npy:
            fil = osp.splitext(fil)[0] + '.npy'
            rawdata = np.load(fil)
            c = npp.frameSpatialRectData(rawdata,colDict = npp.longColDict,imageNum=loadNum)
        else:
            an = pickle.load(open(fil))
            c = npp.frameSpatialRectData(an.rawdatamatrices, colDict = npp.longColDict, 
                                an=an.analyses[0],imageNum=loadNum)
        bigc.append(c)
    print time.time()-st ,  'seconds'
    return bigc

def bigcDictMaker(files, npy=False,loadNum=0):
    st = time.time()
    bigc_dict={}
    for fil in files:
        print fil
        pth,root = osp.split(fil)
        # a cheap way to get the frame number out of the image
        frameNo = int(filter(str.isdigit, root))
            
        if npy:
            fil = osp.splitext(fil)[0] + '.npy'
            rawdata = np.load(fil)
            c = npp.frameSpatialRectData(rawdata,colDict = npp.longColDict,imageNum=loadNum)
        else:
            an = pickle.load(open(fil))
            c = npp.frameSpatialRectData(an.rawdatamatrices, colDict = npp.longColDict, 
                                an=an.analyses[0],imageNum=loadNum)
        bigc_dict[frameNo]=c
    print time.time()-st ,  'seconds'
    return bigc_dict

def processStatus(pobj):
    errCount = np.zeros(7 , dtype=np.int)
    st = pobj.pickVar("status")
    # the first index is left unused since status 0 is healthy points 
    # no need to evaluate here
    for j in [1,2,3,4,5]:
        errCount[j] = (st==j).sum()
    errCount[6] = errCount[1:6].sum()
    return errCount
    

def qualitySummary(bigc):
    if type(bigc)==type([]):
        sz = len(bigc)
        errors = np.zeros((sz,7),dtype=np.int)
        for i,pobj in enumerate(bigc):
            st = pobj.pickVar("status")
            errors[i,0]=i # the first col is just analysis number
            errCount = processStatus(pobj)
            errors[i,1:] = errCount[1:]
        _sort = np.argsort(errors[:,-1])
        sorted_errors = errors[_sort,:]
    return errors,sorted_errors
                
def bigcMesher(bigcSets):
    """
    uses multiple bigcSets and decides which frame to use by error
    """
    numSets = len(bigcSets)
    numFrames = len(bigcSets[0])
    errs = np.zeros((numFrames,7,numSets))
    meshedbigc=[]
    for i,bigc in enumerate(bigcSets):
        errs[...,i],_ = qualitySummary(bigc)
    for j in range(numFrames):
        imin = errs[j,6,:].argmin()
        meshedbigc.append(bigcSets[imin][j])
    return meshedbigc            
            
    
def npyMaker(files):
    """takes the names of an files and turns their rawdatamatrices into npy files"""
    for fil in files:
        print fil
        filbase = osp.splitext(fil)[0]
        an = pickle.load(open(fil))
        np.save(filbase,an.rawdatamatrices)
    #
    return 
        
    

def sizesUnder(obj):
    L =  dir(obj)
    S = []
    for mem in L:   
        exec "S.append(sys.getsizeof(obj."+mem+"))"
    return L,S
         
    
if __name__=="__main__":
    
    expNo=23
    loadNo=0
    mode = 'microTop'
    
    print "# create and process fgrid instance"
    fgrid = frameMatrix.frameGrid(expNo, loadNo,mode, pthFG= "/home/can/meta/experiments/frameGridDefs.py")
    fgrid.dataBaseOp(frameMatrix.f)
    fgrid.setMPositions(0.0)
    fgrid.setoptRes(pathOptics="/home/can/meta/optics/configurations.py")
    
    print "# create bigc object"
    filenums = range(0,96)
    folder= "/home/can/MDM/MC/exp23/attnew/microLP24/"
    #
    fileOrderStr = [str(i) for i in filenums]
    sizeStr = [len(k) for k in fileOrderStr]
    sub = ''
    numStr = [sub[:(3-sizeStr[i])]+fileOrderStr[i] for i in range(len(fileOrderStr))]
    gridptfiles = [folder+'an'+ k +'.p' for k in numStr]
    #
    bigc = bigcMaker(gridptfiles,npy=True) # this requires npyMaker to be run beforehand
    
    print "# create the commonPlot object"
    cd = commonData(bigC=bigc, frameGridIns=fgrid,  imageDir='/home/can/imageCentral/')
    #cp.setDemandRegionLimits()
    cd.setPositionsStrainsOverFrames()
    cd.analyzeDataRegions()
    cd.setMasks()
    # get a strain component for plotting checks
    eybig = cd.contiguousDataMatrix('ey')
    exbig = cd.contiguousDataMatrix('ex')
    exybig = cd.contiguousDataMatrix('exy')
    wxybig = cd.contiguousDataMatrix('wxy')
    bigX,bigY = cd.contiguousCoordMatrix(typ = 'undef')
    
    # create the commonImage object
    ci = commonImage(fgrid,factor=0.2, imageDir='/home/can/imageCentral/')
    ci.analyzeDataRegions();ci.setMasks()
    #
    A = ci.contiguousImageMatrix()
    ###
    
    #### 1st figure; merged rot
    plt.figure()
    #extent = (cd.xlim.min(),cd.xlim.max(),cd.ylim.max(),cd.ylim.min())
    extent = None
    #   [x0,x1,y0,y1]
    _contourfpKwds = dict(data=eybig,x=bigX,y=bigY,origin='upper',
                        extent=extent,alpha=1,extend='both')
    leveling={'timesStd':2,'numLevels':8,'symm':True,'fixedrange':[-0.02,0.02]}
    nplot.dataplot(leveling,**_contourfpKwds)           
    #or as needed do a vertical flip
    #nplot.plot(np.flipud(wxybig))
    
    ### 2 nd figure; merged images
    plt.figure()
    coords = (ci.xlim.min(),ci.xlim.max(),ci.ylim.max(),ci.ylim.min())
    _imshowpKwds = dict(imdata=A,coords=coords,origin="upper")
    nplot._imshowp(**_imshowpKwds)
    #
    
    ### 3rd figure; overlay
    #for the overlayPlot modify alpha
    plt.figure()
    _contourfpKwds['alpha']=0.5
    
    nplot.overlayplot(_imshowpKwds,leveling,_contourfpKwds)
