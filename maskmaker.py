import numpy as np
import matplotlib.pyplot as plt
import genericUtility
from genericUtility.matrixTools import putMatInMat,trim_2d
import logging
logger = logging.getLogger(__name__)

def maskmaker(a,X,Y,stringConditionsList=[],maskValue=[]):
    """
    a= 2-D matrix initial mask of booleans
    X,Y = same size as A defines x and y coords (typically produced by meshgrid)
    stringConditionsList = ["X-3-Y>0","X>5"]
    maskValue= [1, 0]
    """
    a=a.copy()
    for i,condition in enumerate(stringConditionsList):
        exec 'a['+condition+']='+str(maskValue[i])
    return a

def lineInEqFromIntercept(x0,y0,x1,y1,op='='):
    a = y1-y0
    b = x0-x1
    c = x1*y0-x0*y1
    return "%s*X + %s*Y + %s %s 0"%(a,b,c,op)

class paddedRectMask:
    def __init__(self, posMat, gridSpacing, lims ):
        """
        posMat = (X, Y) regular matrices defining the field
        gridSpacing = (dX, dY) scalars of grid spacing in the units of X and Y
        lims = (xlow, xhi, ylow, yhi)
        returns regular data in the demanded zone even when data does not exist
        It will provide column and row pads of nans from all four borders if data 
        does not exist and the padded region dimensions will be to scale. 
        """
        X,Y = posMat
        self.gridShape = X.shape                                                                                 
                                   
        gridRow,gridCol = self.gridShape
        #
        dX,dY =gridSpacing
        xlow, xhi, ylow, yhi =lims
        # create mask
        mask = np.ones(X.shape, dtype='bool')
        conds = ['X<%s'%(xlow), 'X>%s'%(xhi),'Y<%s'%(ylow),'Y>%s'%(yhi)]
        for i,cond in enumerate(conds):
            exec 'mask['+cond+']=0'
        self.trimIndices = trim_2d(mask) # pulling with logical mask does not
        
                                        # not preserve 2-D structure; hence...
                                        
        a,b,c,d = self.trimIndices
        
        dxeast = X[0,c]-xlow
        dxwest = xhi - X[0,d]
        logger.debug("dxeast %s dxwest %s"%(dxeast/dX,dxwest/dX))
        # trimmed sizes
        trimRow,trimCol = b+1 - a, d+1 - c                                                                                   
                                   
        # Now there is spatial trouble if the condition passes outside the 
        # data region. To respect spatial positions, one might nan-pad the any
        # return matrix as needed for the missing positions
        # The rest is for that
        Xmin , Xmax = X.min() , X.max()
        Ymin , Ymax = Y.min() , Y.max()
        #logger.debug("data lims %s %s %s %s "%(Xmin,Xmax,Ymin,Ymax))
        #logger.debug("req. lims %s %s %s %s "%(xlow, xhi, ylow, yhi))
        self.pads = np.array( [ (Xmin-xlow)/dX, (xhi-Xmax)/dX, 
                    (Ymin-ylow)/dY, (yhi-Ymax)/dY], dtype='int').flatten()
        # if any of these numbers are less than 0, it will crop from inside the
        # data and there is no need for any pads
        self.pads[self.pads<0]=0
        # self.pads.any() will be the check for padded op
        if self.pads.any():
            self.colAdd = self.pads[0]+self.pads[1] # E and W pads total
            self.rowAdd = self.pads[2]+self.pads[3] # T and B pads total
            # self.resMat is set to nan for easy pad addition 
            self.regionShape = (trimRow+self.rowAdd,trimCol + self.colAdd)
            self.resMat = np.zeros(self.regionShape)
            self.resMat[...] = np.nan
            self.insertI,self.insertJ=self.pads[2],self.pads[0] # Top and East
            # The inversion is the usual inversion of X <=> col and Y<=> row
        else:
            self.regionShape = (trimRow,trimCol)
                        
    def pullData(self, data):
        """
        data =  data matrix same size as X and Y
        """
        if data.shape!= self.gridShape: raise ValueError
        a,b,c,d = self.trimIndices
        masked = data[a:b+1,c:d+1] # +1 since those cols/rows are to be included
        if self.pads.any():
            putMatInMat(self.resMat, masked, self.insertI,self.insertJ)
        else: # if data covers the spatial range requested
            self.resMat = masked
        return # the user will get the result from self.resMat
            
        
        
        
def rectangularChopMaskMaker(a,X,Y,xlow,xhi,ylow,yhi,
                                maskValue=[0,0,0,0]):
    """
    precisely takes 4 conditions of 'X<', 'X>','Y<','Y>'
        When the condition misses the data set, it warns back that it 
        could not crop and by what distance.
    """
    
    Xmin , Xmax = X.min() , X.max()
    Ymin , Ymax = Y.min() , Y.max()
    #
    cropSuccess=np.ones(4, 'bool')
    failDistance=np.zeros(4)
    #EAST
    if xlow<Xmin: # the check will not crop; no need to impose
        cropSuccess[0]=False
        failDistance[0] = Xmin-xlow
    else: # now it crops; it is good.
        cond = 'X<%s'%(xlow)
        exec 'a['+cond+']='+str(maskValue[0])
    #WEST
    if xhi>Xmin: # the check will not crop; no need to impose
        cropSuccess[1]=False
        failDistance[1] = xhi-Xmin
    else: # now it crops; it is good.
        cond = 'X>%s'%(xhi)
        exec 'a['+cond+']='+str(maskValue[1])
    #NORTH
    if ylow<Ymin: # the check will not crop; no need to impose
        cropSuccess[2]=False
        failDistance[2] = Ymin-ylow
    else: # now it crops; it is good.
        cond = 'Y<%s'%(ylow)
        exec 'a['+cond+']='+str(maskValue[2])
    #SOUTH
    if yhi>Ymax: # the check will not crop; no need to impose
        cropSuccess[3]=False
        failDistance[3] = yhi-Ymax
    else: # now it crops; it is good.
        cond = 'Y>%s'%(yhi)
        exec 'a['+cond+']='+str(maskValue[3])
    success = cropSuccess.all()
    return (a,success,cropSuccess,failDistance)
            
    
if __name__=="__main__":
    if 0:
        a=np.zeros((100,100))
        #define a 10cmx10cm box
        x = np.arange(100.)*0.1
        y = np.arange(100.)*0.1
        X,Y = np.meshgrid(x,y)
        stringConditionsList = ["X+Y+23.411556+107.639494<0","X+Y+24.225051+107.639494>0"]
        maskValue= [1, 0]
        M = maskmaker(a,X,Y,stringConditionsList=stringConditionsList,maskValue=maskValue)
        plt.imshow(M) 
        # of course the mask is not for showing but for pulling elements from another
        #array,e.g. ey[M]
    if 1:
        a=np.ones((100,100))
        x = np.arange(100.)*0.1
        y = np.arange(100.)*0.1
        X,Y = np.meshgrid(x,y)
        M2,success,cropSuccess,failDistance = rectangularChopMaskMaker(a,
                                                    X,Y,2.,12.,3.,5.,
                                                    maskValue=[0,0,0,0])
        print success,cropSuccess,failDistance
    
