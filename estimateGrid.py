#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 12:04:09 2018

@author: can
"""
import numpy as np
import scipy.ndimage as ndim


def subsum(bool1,bool2):
    """yields the size of true elements of bool1 different from that 
    of bool2; this is a difference score
    output > 0 when bool1 is a superset of bool2
    output = 0 when bool1 is a subset of bool2
    OR
    output = the number of bool1 points outside bool2
    """
    temp = bool1.copy()
    temp[bool2]=False
    return temp.sum()

def shave(inbool, mode):
    """mode=0,1,2,3 corresponds to top,down,left and right shaves."""
    topshave = np.array([[0,1,0],[0,1,0],[0,0,0]])
    downshave =np.array([[0,0,0],[0,1,0],[0,1,0]])
    leftshave = np.array([[0,0,0],[1,1,0],[0,0,0]])
    rightshave = np.array([[0,0,0],[0,1,1],[0,0,0]])
    shaves = [topshave,downshave,leftshave,rightshave]
    outbool = ndim.binary_erosion(inbool , shaves[mode])
    return outbool

def shaveWcheck(bool1, bool2, mode):
    """
    mode=0,1,2,3 corresponds to top,down,left and right shaves
    DOES:
    shaves a bool1 1 pixel from a desired face that 
    is given by the mode, only if it is necessary, i.e. if the action
    removes bool1 points that are outside bool2. 
    RETURNS:
        region binary  map to use (bool matrix), shaved/unshaved (bool) 
        unshaved bool signals nor further op needed on this face
    """
    S1 = subsum(bool1,bool2)
    shaved_bool1 =  shave(bool1,mode)
    S2 = subsum(shaved_bool1,bool2)
    diff = S2-S1 
    # diff is the number of bool1 points outside bool2
    # removed by this particular shave
    # Why not just check S2==0? You in general have more outside-bool2
    # points on other faces. With S1 you get a base number for all and
    # then check if you improve on that by shaving on a particular face
    if diff==0:
        return bool1, False
    else:
        return shaved_bool1, True

def shaveConsecutivelyAllSides(bool1,bool2):
    """
    This shaves bool1 from all sides in turn until the resultant rectangular
    region falls inside bool2. In a single step, it will not shave at all 
    if it does not remove points outside bool2. The consecutive shave op.
    on top, down, left, right faces in turn (by 1 pixel) is deemed to be 
    more prudent to keep the maximum rectangular x-y grid that falls inside 
    the region.
    """
    boolop=bool1.copy()
    needToShave = np.ones(4,bool)
    while needToShave.any():
        for mode in range(4):
            boolop, needToShave[mode]= shaveWcheck(boolop,bool2,mode)
    #
    return boolop   

def prepRectGridLims(inbool, margin=0, shift=(0,0), delta=(10,10), phase=(0,0)):
    """
    inbool = arb. shaped single piece binary matrix that contains the region
    margin = the number of pixels that will be eroded first
    shift = final shift that will be imposed on the grids 
    delta = grid spacing in pixels
    phase = gridding phase
    DOES:
    prepares an x-y rectangular grid that falls inside a region defined by a
    binary matrix inbool. Tries to give the maximum grid that falls in the 
    by first defining a rectangle that is a superset of the region and then
    judiciously shaving it from all sides.
    """
    Ny,Nx =  inbool.shape
    phase_x,phase_y = phase
    delta_x,delta_y = delta
    shift_x, shift_y = shift
    shift_x = int(shift_x)
    shift_y = int(shift_y)
    eroded = inbool.copy()
    for i in range(margin):
        eroded = ndim.binary_erosion(eroded)
    gridPtsBool = eroded[phase_y::delta_y,phase_x::delta_x] #in/out info at grid points
    xarr = np.arange(phase_x,Nx,delta_x)
    yarr = np.arange(phase_y,Ny,delta_y)
    X, Y = np.meshgrid(xarr,yarr)
    # Create a containing rectangle for  
    meshBool = np.ones_like(gridPtsBool)
    xc_min,xc_max = X[gridPtsBool].min() , X[gridPtsBool].max()
    yc_min,yc_max = Y[gridPtsBool].min() , Y[gridPtsBool].max()
    meshBool[X<xc_min]=False
    meshBool[X>xc_max]=False
    meshBool[Y<yc_min]=False
    meshBool[Y>yc_max]=False
    inRectBool = shaveConsecutivelyAllSides(meshBool,gridPtsBool)
    # Now once inRectBool is determined; its limits are sent to the user
    # since he might prefer subgridding and other options for DIC grids.
    left , right = X[inRectBool].min() , X[inRectBool].max()
    top, bottom = Y[inRectBool].min() , Y[inRectBool].max()
    return (left+shift_x , right+shift_x , top+shift_y , bottom+shift_y)