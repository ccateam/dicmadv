# To do: Output receiving and processing classes elsewhere
import os,sys
import FileGen as fg
reload(fg)
import logging
logger = logging.getLogger(__name__)

def execdic(datName):
    logger.info('Submitting %s for analysis.'%(datName,))
    if sys.platform[:3]=='win':
        execStr = 'windic26.exe ' + datName[:-4]
                                                  
    elif sys.platform[:5]=='linux':
        execStr = 'dic_ori ' + datName
    else:
        raise ValueError
    ret = os.system(execStr)

    return

class runDIC:
    def __init__(self,ppFile,npFile,runDir,extraVar=True):
        self.ppFile, self.npFile,self.runDir = ppFile, npFile, runDir
        self.pp = fg.physicalPar(ppFile)
        self.pp.inpfile = npFile
        self.np = fg.numericalPar(npFile,extraVar=extraVar)
    def createppFile(self):
        '''Creating both files. Naturally, the additions have to be attribute
        assignments should have been done at this level.'''
        os.chdir(self.runDir)
        self.pp.createFile()
    def createnpFile(self):
        os.chdir(self.runDir)
        self.np.createFile()
    def __repr__(self,**kwds):
        ret = 'PHYSICAL PARS\n'
        ret += self.pp.__repr__(**kwds)
        ret += 'NUMERICAL PARS\n'
        ret += self.np.__repr__(**kwds)
        return ret 

    def run(self):
        '''Goes to runDir'''
        os.chdir(self.runDir)
        execdic(self.ppFile)
        return

        
        
