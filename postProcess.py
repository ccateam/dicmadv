######### Cahit Can Aydiner  2017 ##############################################
# This is a postprocessing module that intends to plot and present DIC results
# that are in the analysis instance format.
#   
# Classes:
#  sub           defined by a rect. "zone" and a "cell" defining its shape 
#  subuv(sub)    also receives displacements and does strain stuff using them
#  postprocess   basic processor of rawdata, e.g.brings u arrays for a load
#  regularGridPostprocess(postprocess) 
#      shapes the data into plottable frames; pickVarGrid is the necessary
#      functionality to get list data and put it in a matrix
#       of spatial positions
#                      
#  postProcessMulti   depracated (never used)
#  regionPlot         also seems deprecated only regu..smooth calls it.
# 
#  The structure builds on the possibility that one does not have to do strain
#  analysis over the data grid but over a possibly coarser cell grid. So, one
#  can get strains averaged over a circular cell grid. Its plotting functions
#  work over a "sgrid" cell grid. 
#
#  In time, definitions of these cell grids became basically too arcane. Cells have 
#  the job of pulling out a region. However, enforcing a cell grid simply did not 
#  fit our practice. Practice had a lot of only plotting the full frame using 
#  differentiation over the data grid. In addition,
#  positioning variables started to come from physical machine coordinates and
#  translating those to cell sizes (which are not in pixels but in data matrix
#  length scales) is again backwards. I WILL SWITCH TO A TOP DOWN APPROACH WHERE 
#  DATA WILL BE PROCESSED OVER ENTIRE FRAMES AND MASKED TO SMALLER ZONES AS
#  NECESSARY. postprocess class remains valid for further efforts. 
#  I think all else has to be simpified in a new module

import dicTools; reload(dicTools); from dicTools import *
from scipy import *
import scipy
import scipy.stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl
import os, sys, shutil, copy, pickle
import indRun ; reload(indRun)
import strainOp; reload(strainOp)
import genericUtility.myFileTools as mft ; reload(mft)
import genericUtility.datafit as dft ; reload(dft)
import numpy as np
import pylab
from PIL import Image, ImageDraw, ImageOps, ImageFont
import pdb
import os.path as osp
import warnings, logging
logger = logging.getLogger(__name__)

def openByExt(nm, frameNo=0):
    """
    TAKES:
    nm : path
    frameNo = only important if it is a multiple image file
    RETURNS:
    an image matrix (for the multiple image files, returns one at the prescribed frameNo)
    """
    extension = osp.splitext(nm)[-1]
    if extension in ['.tif','.tiff']:
        ret = scipy.misc.imread(nm)
    elif extension=='.npy':
        ret = np.load(nm)
        if len(ret.shape)==3:
            ret = ret[...,frameNo]
    # lakdjfla
    return ret


def openImage(imnm,frameNo=0,retPILim=False):
    """
    frameNo = only important if it is a multiple image file
    if the image is not in the local path it will try to locate it under the standard image directory on the network. The mounted directory for that path is put under an environment variable DICIMAGEPTH typically."""
    
    if osp.exists(imnm):
        im = openByExt(imnm, frameNo=frameNo)
        logger.info('Found image %s in the local directory.'%(imnm,))
    else:
        logging.warn('Image %s not found in the local directory. Grabbing from NAS.'%(imnm,))
        basename = osp.basename(imnm)
        imageDir = os.environ['DICIMAGEPTH']
        newnm = osp.join(imageDir,  basename)
        if osp.exists(newnm):
            im = openByExt(newnm, frameNo=frameNo)
        else:
            raise IOError, "couldn't find %s or %s"%(imnm, newnm)
    logger.debug('SHAPE is %s %s '%(im.shape[0], im.shape[1]))
    logger.debug( '\n image name is %s'%(imnm))
    if retPILim: 
        im = scipy.misc.toimage(im)
    return im
        

def make2d(A,numrow,numcol):
    return np.atleast_2d(A).reshape(numrow,numcol)

def rowsorter(A, sorter):
    numrow,numcol=A.shape
    for row in range(numrow):
        A[row,:] = A[row,:][sorter[row,:]]
    return A

def mean_std(f,sanityRange=None,**kwds):
    tsize = f.size
    msk = np.logical_not(np.isnan(f))
    ptsLostNan = tsize-np.sum(msk)
    logger.info('lost %s points to nan out of %s'%(ptsLostNan,tsize))
    if sanityRange:
        mn,mx=sanityRange
        msk2 = np.logical_and((f>mn),(f<mx))
        msk = np.logical_and(msk,msk2)
        ptsLost = (tsize-np.sum(msk2))-ptsLostNan
        logger.info('lost %s points to sanity range out of %s'%(ptsLost,tsize))
    else:
        ptsLost = ptsLostNan
    ff = f[msk]
    return (np.average(ff),np.median(ff),np.std(ff),float(ptsLost)/tsize)

def hideTicks():
    """
    this does not hide ticks but ticklabels only
    ax.set_xticks([]) looks more practical
    """
    ax = plt.gca()
    axx,axy = ax.get_xticklabels() , ax.get_yticklabels() 
    plt.setp(axx,visible=False)
    plt.setp(axy,visible=False)
    return

def cellMaker(shape, _typ = 'reg',**kwds):
    """
    shape (3,3) _typ reg makes regular grid
    """
    sx,sy = shape
    Ones = asarray(np.ones((sx,sy)),'bool')
    Zeros = asarray(np.zeros((sx,sy)),'bool')
    if _typ == 'reg':
        return Ones
    elif _typ == 'perimeter':
        nl =  kwds['numloops']
        msk = Zeros.copy()
        msk[:nl,:]=True ; msk[-nl:]=True ; msk[:,:nl]=True ; msk[:,-nl:]=True 
        return msk
    elif _typ == 'circle':
        msk = Ones.copy()
        assert sx==sy
        cenInd = sx/2
        for i in range(sx):
            for j in range(sy):
                rad2 = (i-cenInd)**2+(j-cenInd)**2
                if rad2 > cenInd**2:
                    msk[i,j]=False        
        return msk


def periodicGrid(freqx=2, freqy=2, imSize=(2452,2054)):
    """
    returns a grid such that the distance to the grid point in the neighboring
    image that is right next to the current image remains the same as the grid
    spacing in this image.
    """
    Lx , Ly = imSize
    distx = Lx/freqx
    disty = Ly/freqy
    x0=distx/2
    y0=disty/2
    coors=[]
    for j in range(freqy):
        for i in range(freqx):
            coor= (x0 + i*distx , y0 + j*disty)
            coors.append(coor)
    return coors


def retCorners(xy,dxy):
    '''xy and dxy are doublet list or tuples signifying coords and delta
    coords'''
    return (tuple(array(xy)-array(dxy)), tuple(array(xy)+array(dxy)))


def gridPt(im, coords, pointSize, color='cyan'):
    draw = ImageDraw.Draw(im)
    #draw points
    coords=array(coords)
    N = coords.shape[0]
    for i in range(N):
        c = coords[i,:]
        dc = [pointSize,pointSize]
        x0,x1 = retCorners(c,dc)
        draw.ellipse([x0,x1],fill=color)
        if i==0: c0 = c
        if i==N-1: cN = c
    return (draw,c0,cN)

def gridLine(im, coords1, coords2,color='green'):
    draw = ImageDraw.Draw(im)
    coords1 = array(coords1)
    coords2 = array(coords2) 
    N = coords1.shape[0]
    for i in range(N):
        c1 = tuple(coords1[i,:])
        c2 = tuple(coords2[i,:])
        draw.line([c1,c2],fill=color)
    return draw

def gridText(im,coords,textList,font,color='red'):
    draw = ImageDraw.Draw(im)
    #draw points
    coords=array(coords)
    N = coords.shape[0]
    for i in range(N):
        c = coords[i,:]
        draw.text(c,textList[i] ,color,font=font)
    return draw

class sub:
    def __init__(self,I,J, x,y,var,cm,cell):
        """
        I,J = indices of sub center in the macro matrices
        x, y, var, cm = macro matrices that stand for x, y, variable to be processed and cleanmask
        Cell is a boolean matrix of 1s and 0s in the shape of the subzone. It is a mask that will 
        pull data points of a location in the cell shape for being fit or analyzed.
        """
        self.I = I
        self.J = J
        self.xall = x
        self.yall = y
        self._setvar(var)
        self.cmall = cm
        self.xcen,self.ycen = self.xall[self.I,self.J] , self.yall[self.I,self.J]
        self.defineMasks(cell)
        self.pullAllFromZone()
        return

    def _setvar(self,var):
        self.varall = var
        return
    
    def defineMasks(self,cell):
        """
        zone: the rectangular area that contains the cell
        cell: the region defined by 1s inside a zone.
        """
        self.cell = cell
        self.zone = cell.copy()
        # make zone the mask with all ones, same size as the cell. Physically, see docstring
        self.zone[:,:] = True
        self.shape = self.cell.shape
        row , col = self.I , self.J
        # create a boolean matrix same shape as the DIC grid (macro data) (e.g. 210x207 etc.)
        self.cellFromAll = zeros_like(self.cmall)
        # over the zero background of self.cellFromAll, you imprint self.cell to create 
        # a mapping to the entire frame data
        ##### BUG? cell2all no such func
        self.zone2all(self.cellFromAll,self.cell)
        
        # create a boolean matrix same shape as the DIC grid (macro data) (e.g. 210x207 etc.)
        self.zoneFromAll = zeros_like(self.cmall)
        # over the zero background of self.zoneFromAll, you imprint self.zone to create 
        # a mapping to the entire frame data
        self.zone2all(self.zoneFromAll,self.zone)
        #
        self.numCellPts = (np.nonzero(cell.ravel())[0]).size
        return
    
    def zone2all(self,allmat, zonemat):
        cx,cy = self.shape
        cx2 = cx/2 #integer division
        cy2 = cy/2
        row,col=self.I, self.J
        if row-cx2 < 0 : raise ValueError, "The cell you have requested does not fit to the grid you have."
        if col-cy2 < 0 : raise ValueError, "The cell you have requested does not fit to the grid you have."
        allmat[ (row-cx2) : (row+cx2+1) ,  (col-cy2) : (col+cy2+1) ] = zonemat
        return
    
    def set_cmall(self):
        """in case self.cmzone is changed by the evaluation (outlier detection, the result can be mapped back"""
        self.zone2all(self.cmall,self.cmzone)
        return

    def pullZone(self,f):
        """
        from the overall matrix of variable f, the data in the prescribed zone is pulled and then put back as
        a matrix same shape as self.zone.
        """
        return f[self.zoneFromAll].reshape(self.shape)

    def pullAllFromZone(self):
        self.xzone = self.pullZone(self.xall)
        self.yzone = self.pullZone(self.yall)
        self.varzone = self.pullZone(self.varall)
        self.cmzone = self.pullZone(self.cmall)
        return
    
    def analysisMask(self,**kwds):
        """can put in options as need be"""
        cellANDcm = np.logical_and(self.cell,self.cmzone)
        self.anMask = cellANDcm
        self.memx,self.memy = np.nonzero(self.anMask) # these will serve as memory for the index of sent points
        return 

    def analyze(self,func,**kwds):
        """
        func works on big picture coordinates
        """
        self.analysisMask(**kwds)
        x = self.xzone[self.anMask]
        y = self.yzone[self.anMask]
        var = self.varzone[self.anMask]
        fnxy, pars, doc, err = func( self.xcen , self.ycen , x , y ,var )
        self.pars = pars
        self.fnxy=fnxy
        self.fndoc =doc
        self.okpercent = float( nonzero(self.anMask)[0].size ) / float( nonzero(self.cell)[0].size ) * 100.
        self.err = err
        self.errFilter(**kwds)
        return

    def errFilter(self,probFilter={'okpercent':0, 'sigmaz':100},**kwds):
        """
        DOES: sets self.prob
        The defaults of probFilter are such that they don't place any limit
        """
        filters=[]
        if self.okpercent < probFilter['okpercent']: 
            print 'Cell did not meet the condition 0 percent: %s < %s'%(self.okpercent,probFilter['okpercent'] )  
            filters.append(True)
        if self.err['sigmaz']>probFilter['sigmaz']:
            print 'Cell did not meet the normalized error between data and fit: %s>%s'%(self.err['sigmaz'],probFilter['sigmaz']) 
        filters=array(filters,bool)
        if filters.any():
            self.prob=True
        else:
            self.prob=False
        return
    
    def retCenValue(self):
        return self.fnxy(self.xcen,self.ycen)

    def plotfit(self,prnt=False,c='b',**kwds):
        x = self.xzone[self.anMask]
        y = self.yzone[self.anMask]
        var = self.varzone[self.anMask]
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.hold(True)
        scat = ax.scatter(x,y,var,c=c)
        if prnt: print x, y, var
        fit = self.fnxy(self.xzone,self.yzone)
        surf = ax.plot_wireframe(self.xzone,self.yzone,fit)
        ax.set_xlabel('x [pixels]')
        ax.set_ylabel('y [pixels]')        
        ax.set_title(self.fndoc)
        ax.hold(False)
        return

class subuv(sub):
    def _setvar(self,var):
        """ need to send var as a doublet of u and v"""
        self.uall, self.vall = var
        return

    def pullAllFromZone(self,**kwds):
        # x values in the zone
        self.xzone = self.pullZone(self.xall)
        self.yzone = self.pullZone(self.yall)
        # u values in the zone
        self.uzone = self.pullZone(self.uall)
        self.vzone = self.pullZone(self.vall)
        # clean mask values in the zone
        self.cmzone = self.pullZone(self.cmall)
        # added this call here to avoid repitition
        self.analysisMask(**kwds)
        return
    
    def naned_uvcopy(self):
        u = self.uzone.copy()
        v = self.vzone.copy()
        u[np.logical_not(self.anMask)] = nan
        v[np.logical_not(self.anMask)] = nan
        return (u,v)
    
    def detect_gridSpacing(self):
        """ finds the DIC grid spacing. It typically is uniform and this class assumes that."""
        x = self.xzone.copy()
        logger.debug('x in detect_gridSpacing =  %s'%(x,))
        y = self.yzone.copy()
        gridy, gridx = np.diff(x)[0,0] , np.diff(y,axis=0)[0,0]
        # test the assumption that the spacings are uniform
        # if false set a nan to the returned quantity
        if (np.diff(x) - gridy).any(): gridy=np.nan
        if (np.diff(y,axis=0) - gridx).any(): gridx=np.nan
        return (gridx,gridy)

    def centraldiff_derivatives(self):
        """
        finds the derivatives for u, v
        """
        gridx, gridy = self.detect_gridSpacing()
        # should recall x runs down over the image over rows of the matrix
        if np.isnan(gridx): raise ValueError, 'nonuniform grid'
        if np.isnan(gridy): raise ValueError, 'nonuniform grid'
        u,v = self.naned_uvcopy() 
        u_y, u_x = np.gradient(u,gridx,gridy)
        v_y, v_x = np.gradient(v,gridx,gridy)
        return (u_x, u_y, v_x, v_y)

    #added by NS to calculate second neighbour central difference approximation
    
    def secNeighbor_derivatives(self):
        """
        finds the derivatives for u, v
        """
        
        def secDerivative(arr,gridx,gridy):
            xsz, ysz= np.shape(arr)
            y_A=(arr[1,:]-arr[0,:])/gridy
            y_B=(arr[2,:]-arr[0,:])/(2*gridy)
            y_C=(arr[2:xsz-2,:]-arr[0:xsz-4,:])/(4*gridy)
            y_D=(arr[xsz-3,:]-arr[xsz-1,:])/(2*gridy)
            y_E=(arr[xsz-2,:]-arr[xsz-1,:])/(gridy)
            arr_y=np.vstack([y_A,y_B,y_C,y_D,y_E])

            x_a=(arr[:,1]-arr[:,0])/gridx
            x_A=x_a.reshape(xsz,1)
            x_b=(arr[:,2]-arr[:,0])/(2*gridx)
            x_B=x_b.reshape(xsz,1)
            x_C=(arr[:,2:ysz-2]-arr[:,0:ysz-4])/(4*gridx)
            x_d=(arr[:,ysz-3]-arr[:,ysz-1])/(2*gridx)
            x_D=x_d.reshape(xsz,1)
            x_e=(arr[:,ysz-2]-arr[:,ysz-1])/(gridx)
            x_E=x_e.reshape(xsz,1)
            arr_x=np.hstack([x_A,x_B,x_C,x_D,x_E])
            return arr_y, arr_x
        
        gridx, gridy = self.detect_gridSpacing()
        # should recall x runs down over the image over rows of the matrix
        if np.isnan(gridx): raise ValueError, 'nonuniform grid'
        if np.isnan(gridy): raise ValueError, 'nonuniform grid'
        u,v = self.naned_uvcopy() 
        u_y, u_x = secDerivative(u,gridx,gridy)
        v_y, v_x = secDerivative(v,gridx,gridy)
        return (u_x, u_y, v_x, v_y)
        #
    
    def crop(self,x, y, undefFil,defFil,defPlotMode='def',cropAllowance=100):
        # prepare ROI
        minx, maxx, miny, maxy =x.min(), x.max(), y.min(), y.max()
        minx -= cropAllowance
        maxx += cropAllowance
        miny -= cropAllowance
        maxy += cropAllowance
        # crop undef 
        im = np.array(openImage(undefFil))
        im2 =im[miny:maxy,minx:maxx]
        if defPlotMode!=None:
            im3 = np.array(openImage(defFil))
            im4 = im3[miny:maxy , minx:maxx]
        else:
            im4=None
        xx = x.copy()
        yy = y.copy()
        # x y in new CS of im2/im4 top left
        xx -= minx
        yy -= miny
        return (im2, im4, xx, yy)
    
    def measure_strains(self,measure='mean',**kwds):
        """returns a scalar measure for the strain fields"""
        u_x, u_y, v_x, v_y = self.centraldiff_derivatives()
        ex, ey, exy, wxy = u_x, v_y, 0.5*(u_y + v_x) , 0.5*(u_y - v_x)
        ret=[]
        #

        #pdb.set_trace()
        for i,f in enumerate([ex,ey,exy,wxy]):
            av,mn,_std,invalid = mean_std(f,**kwds)
            if measure=='mean':
                ret.append(av)
            elif measure=='median':
                ret.append(mn)
            elif measure=='std':
                ret.append(_std)
            elif measure=='mode':
                msk = np.logical_not(np.isnan(f))
                ret.append(scipy.stats.mode(f[msk])[0])
            elif measure=='invalid':
                ret.append(invalid)
            else:
                raise ValueError
        return ret
    #
    
    def plot_derivatives(self,undefFil,defFil,undefPlot=True,defPlotMode='def',
                            cropAllowance=100, figNum=30,leveling={'timesStd':2,'numLevels':8,'symm':True},
                            plotvar='all',varType='small',colorbar=True,
                            contourKwds={'alpha':0.6,'extend':'both','cmap':cm.jet},
                            maskRange=None,percentStrn=False,nu=0.5, **kwds):
        """
        varType = 'small' works with inf. strain and rot
                  'Lag'   works with Lagrangian strain and large rotation (Lag since I want to leave room for Eulerian)
        leveling= a dictionary that adjusts the limits and levels of the contour plot; keys = timesStd (float) or fixedRange (doublet) , symm (bool), numLevels (int)
        if plotvar=='all': plots all ex, ey, exy, wxy in a 2x2 plot array
               =='ex' or ... plots just this one.
        """

        def levelMaker(f, leveling):
            av,mn,_std,inval= mean_std(f)
            try:
                mincon,maxcon = leveling['fixedrange']
            except KeyError:
                rng = _std*leveling['timesStd']
                mincon,maxcon = av-rng, av + rng
                if leveling['symm']:
                    rng = max(abs(mincon),abs(maxcon))
                    mincon,maxcon = -rng, rng
            
            levels = np.linspace(mincon,maxcon, leveling['numLevels']+1)
            _norm = mpl.colors.Normalize(vmin=mincon, vmax=maxcon)
            # I could not make use of _norm really.
            levelsD = {'levels':levels}
            print levelsD
            return levelsD, av, mn, _std

        def defplot(x,y,f,defPlotMode,contourKwds):
            if contourKwds['alpha']=='nofill':
                fn=plt.contour
                del contourKwds['alpha']
            else:
                fn=plt.contourf
            if defPlotMode=='undef':
               fn(x, y, f, **contourKwds)
            elif defPlotMode=='tripcolor':
	      
		fn=plt.tripcolor 
		triangles=mpl.tri.Triangulation(x.flatten(),y.flatten())
		fn(x.flatten(), y.flatten(), f.flatten(), edgecolors='k', alpha=1,  cmap=cm.hsv_r)
            elif defPlotMode=='def':
                fn(x+uu, y+vv, f,**contourKwds)
            elif defPlotMode=='defmedian':
                fn(x+np.median(uu), y+np.median(vv), f, **contourKwds)
            else:
                raise ValueError
            return

        def overlay(x,y,var,im,defPlotMode,contourKwds,colorbar=True):
            plt.hold(True)
            plt.imshow(np.array(im),aspect='equal',cmap=cm.gray) 
            defplot(x,y,var,defPlotMode,contourKwds)
            if colorbar: plt.colorbar()
            #hideTicks()
            ax=plt.gca()
            ax.axes.set_xticks([])
            ax.axes.set_yticks([])
            plt.hold(False)
        
        def masker(f,maskRange):
            if maskRange==None: return f
            mn,mx= maskRange
            g=f.copy()
            g[np.logical_and(g>mn,g<mx)]=nan
            return g

        x = self.xzone.copy()
        y = self.yzone.copy()
        u_x, u_y, v_x, v_y = self.centraldiff_derivatives()
        
        D, names = strainOp.pickStrainComponents([u_x, u_y, v_x, v_y] , nu=nu,varType=varType, retNames=True, retAsDict=True,percentStrn=percentStrn)
        
        #
        im2, im4, x, y = self.crop(x, y, undefFil,defFil,defPlotMode=defPlotMode,cropAllowance=cropAllowance)
        # im2 relevant part for undef image; im4 relevant part of def image
        if undefPlot:
            fig = plt.figure(figNum)
            figNum+=1
            if plotvar=='all':
                for i,n in enumerate(names):
                    f = D[n]
                    levelsD, av, mn, _std = levelMaker(f, leveling)
                    contourKwds.update(levelsD)
                    ax = fig.add_subplot(3,2,i+1)
                    ax.set_title(n+' median %s av %s'%(mn,av))
                    overlay(x,y,f,im2,'undef',contourKwds,colorbar=colorbar)
            else:
                f = D[plotvar]
                levelsD, av, mn, _std = levelMaker(f, leveling)
                contourKwds.update(levelsD)
                overlay(x,y,f,im2,'undef',contourKwds,colorbar=colorbar)
        
        if defPlotMode!=None:
            uu, vv= self.naned_uvcopy()
            fig = plt.figure(figNum)
            figNum+=1
            if plotvar=='all':
                for i,n in enumerate(names):
                    f = D[n]
                    levelsD, av, mn, _std = levelMaker(f, leveling)
                    contourKwds.update(levelsD)
                    ax = fig.add_subplot(3,2,i+1)
                    ax.set_title(n+' median %s av %s'%(mn,av))
                    overlay(x,y,masker(f,maskRange),im4,defPlotMode,contourKwds,colorbar=colorbar)
            else:
                f = D[plotvar]
                levelsD, av, mn, _std = levelMaker(f, leveling)
                contourKwds.update(levelsD)
                print contourKwds
                overlay(x,y,masker(f,maskRange),im4,defPlotMode,contourKwds,colorbar=colorbar)
        #
        return figNum

    def plot_derivativeHist(self,figNum=50,plotvar='all',histProps={'timesStd':4,'symm':False},showInfo = True,pickler=None,nu=0.5,varType='small',**kwds):
        """
        see doc of np.histogram, most relevant are bins and range
        putInfo: if true: will write mean and std
        showInfo: shows a text of information on the histogram
        histProps: a dictionary that guides histogram range and bins
                   similar to leveling in plot_derivatives
                   keywords: fixedrange, timesStd, symm, numLevels (bins)
                   normalizetoFreq: Padilla-style bin count divided by total count
                   overrides: a dictionary that is directly sent, if 'normed' is True, you will get probability density style overrideing normalizeToFreq
        pickler: name of a pickling file if you want to play with the histogram outside; particularly used for creating publication figures
                   
        """
    
        def histProcessor(f, histProps):
            #initialize kwds for plt.hist
            histKwds={}
            #
            msk = np.logical_not(np.isnan(f))
            ff = f[msk]
            if histProps['percent']==True:
                ff=ff*100.
            av,_std= np.average(ff),np.std(ff)
            dum = ones_like(msk)
            totalPoints = sum(dum.astype(float))
            dataComplete = sum(msk.astype(float))/totalPoints
            #
            if histProps['normalizetoFreq']:
                histKwds['weights'] = ones_like(ff)/totalPoints
            else:
                histKwds['weights'] = ones_like(ff)
            #
            
            try:
                mincon , maxcon = histProps['fixedrange']
            except KeyError:
                rng = _std*histProps['timesStd']
                mincon,maxcon = av-rng, av + rng
                if histProps['symm']:
                    rng = max(abs(mincon),abs(maxcon))
                    mincon,maxcon = -rng, rng
            histKwds['bins']=histProps['numLevels']
            histKwds['range'] = (mincon, maxcon)
            histTxt = 'av %s\nstd%s\ncp%s'%(av,_std,round(dataComplete*100,1))
            D=histProps['overrides']
            for key, value in D.items():
                histKwds[key]=value
            return ff,histKwds,histTxt
        #
        x = self.xzone.copy()
        y = self.yzone.copy()
        u_x, u_y, v_x, v_y = self.centraldiff_derivatives()
        #
        
        D, names = strainOp.pickStrainComponents([u_x, u_y, v_x, v_y] , nu=nu,varType=varType, retNames=True, retAsDict=True,percentStrn=False)
        
        # dump for error evaluation purposes
        u,v = self.naned_uvcopy()
        E = D.copy()
        E.update( {'u':u,'v':v} )
        pickle.dump(E , open('straindataForFrame.p','w'))
        # 
        labels=names
        #
        fig = plt.figure(figNum)
        figNum+=1
        if plotvar=='all':
            #pdb.set_trace()
            for i,n in enumerate(names):
                ax = fig.add_subplot(3,2,i+1)
                f = D[n]
                ff,histKwds,histTxt = histProcessor(f,histProps)
                plt.hist(ff,**histKwds)
                if showInfo: ax.text(0.5,0.5,histTxt,transform=ax.transAxes)
                plt.xlabel(labels[i],fontsize=14)
                
        else:
            f = D[plotvar]
            ff,histKwds,histTxt = histProcessor(f,histProps)
            #pdb.set_trace()
            if pickler:
                obj = {'ff':ff,'histKwds':histKwds,'histTxt':histTxt}
                pickle.dump(obj,open(pickler,'w'))
            plt.hist(ff,**histKwds)
            ax=plt.gca()
            if showInfo: ax.text(0.5,0.5,histTxt,transform=ax.transAxes)
        return figNum

    def plotuvOnImage(self,undefFil,defFil,drop='nothing',defPlotMode='def',cropAllowance=100, figNum=20, quiverKwds = {'angles':'xy','scale_units':'xy','scale':0.1,'color':'r'},**kwds):
        """
        plots displacement vector plots on cells
        """
        #,defPlotMode='def',cropAllowance=100, figNum=20, quiverKwds = {'angles':'xy','scale':245,'color':'r'}, **kwds):
        # prepare x,y,u,v
        x = self.xzone.copy()
        y = self.yzone.copy()
        u,v = self.naned_uvcopy()
        uu,vv = self.naned_uvcopy() # leave this untouched later
        print 'x=',x
        print 'y=',y
        print 'uu=',uu
        print 'vv=',vv
        if drop=='nothing':
            pass
        elif drop=='mean':
            u -= np.mean(self.uzone[self.anMask])
            v -= np.mean(self.vzone[self.anMask])
        elif drop=='median':
            u -= np.median(self.uzone[self.anMask])
            v -= np.median(self.vzone[self.anMask])
        else:
            raise ValueError
        #
        print 'u=',u
        print 'v=',v
        im2, im4, x, y = self.crop(x, y, undefFil,defFil,defPlotMode=defPlotMode,cropAllowance=cropAllowance)
        #
        fig = plt.figure(figNum)
        plt.hold(True)
        plt.imshow(np.array(im2),aspect='equal',cmap=cm.gray) 
        # use np.array(im) rather than im to avoid upside down plotting
        q = plt.quiver(x,y,u,v,**quiverKwds)
        p = plt.quiverkey(q,-10,-10,10,"10 pixel",coordinates='data',color='r')
        hideTicks()
        plt.hold(False)
        if defPlotMode!=None:
            fig = plt.figure(figNum+1)
            plt.hold(True)
            plt.imshow(np.array(im4),aspect='equal',cmap=cm.gray) 
            if defPlotMode=='undef':
                q = plt.quiver(x,y,u,v,**quiverKwds)
            elif defPlotMode=='def':
                q = plt.quiver(x+uu,y+vv,u,v,**quiverKwds)
            elif defPlotMode=='defmedian':
                q = plt.quiver(x+np.median(uu),y+np.median(vv),u,v,**quiverKwds)
            else:
                raise ValueError
            p = plt.quiverkey(q,-10,-10,10,"10 pixel",coordinates='data',color='r')
            hideTicks()
        return
    

class postprocess:
    def __init__(self, rawDatas ,an = None, seq=False,
                colDict = {'X':0,'Y':1,'u':2,'v':3,'ex':4,'ey':5,
                'exy':6,'dudx':7,'dvdy':8, 'dudy':9,'dvdx':10
                ,'Theta':11,'w':12,'status':13, 'iter':14, 'corrcoeff':15},
                **kwds):
        '''__init__(self, rawDatas,seq=False):
        rawDatas is just a 3 dimensional matrix. seq allows cumulative summing things over
        the load point (image) direction. That is necessary when the analysis is
        sequential, i.e., each guy follows the other.'''
        if an:
            # this only gathers the names of the image def/undef files to a
            # a member dictionary called self.an
            self.pickfromAnalysisObject(an)
        self.seq = seq
        typ=`type(rawDatas)`
        if typ == "<type 'str'>":
            try:
                import pickle
                self.rd = pickle.load(rawDatas)
            except IOError:
                raise IOError, 'The file %s is not found.'%(rawDatas)
        elif typ == "<type 'array'>" or typ=="<type 'numpy.ndarray'>":
            self.rd = rawDatas
        else:
            raise 'the type specified for rawDatas input to postprocess. %s, does not work.'%(typ,)
        #
        if self.seq == True:
            print 'Every variable is cumulative summed over load points with \
            the exception of X, Y and status.'
            self.rd[:,:,2:-1] = cumsum(self.rd[:,:,2:-1],axis=0)
        self.numImages = size(self.rd,0)
        self.numPixels = size(self.rd,1)
        self.numVar = size(self.rd,2)
        if self.numVar != len(colDict.keys()):
            raise ValueError, 'numVar is %s, number of colDict keys are %s'%(self.numVar,len(colDict.keys()))
        self.colDict=colDict
        # added 2012 immediately creates a cleanMask for points at which the analysis worked
	self.problemMask = np.asarray(self.pickVar('status') ,np.bool)
        self.cleanMask = np.logical_not(self.problemMask)
        self._init(**kwds)
	# self.cleanMask.all() will tell you already if the analysis is clean or not.
	return
    
    def pickfromAnalysisObject(self,an):
        self.an = {}
        self.an['defFiles'] = an.defFiles
        self.an['undefFiles'] = an.undefFiles
        return
    def _init(self,**kwds):
        pass
    def pickImage(self, imageNum):
        return self.rd[imageNum,:,:]
    
    def checkVar(self,var):
        try:
            ind = self.colDict[var]
        except KeyError:
            print 'valid keys:'
            print self.colDict.keys()
            raise KeyError,'Invalid key %s'%(var,)
        return ind

    def pickVar(self,var):
        ind = self.checkVar(var)
        return self.rd[:,:,ind]

    def pickVarPixels(self,var,pixelList):
        #shows the appropriate behaviour for seq. analysis through pickVar
        mat = self.pickVar()
        return take(mat,pixelList,axis=-1)

    def pickAvgVar(self,var,make1D=True):
        mat = self.pickVar(var)
        if make1D:
            return ravel(average(mat))
        else:
            return average(mat)

    def pickAvgVarPixels(self,var,pixelList,make1D=True):
        mat = self.pickVar(var,pixelList)
        if make1D:
            return ravel(average(mat))
        else:
            return average(mat)
    def pickVarAtImage(self,var,imageNum,drop='nothing'):
        ind = self.checkVar(var)
        k = self.rd[imageNum,:,ind]
        if drop=='nothing':
            pass
        elif drop == 'mean':
            k = k - k.mean()
        elif drop == 'median':
            k = k - np.median(k)
        else:
            raise ValueError
        return k
    
    def checkStatus(self):
        retstr='''(0) OK\n(1) Divergence\n(2) singular Hessian matrix\n(3) reach maximum iteration allowed\n(4) computation problem in funcValXXX\n(5) subset out of bounds'''
        self.statArray = zeros(self.numImages)
        allClean=True
        for i in range(self.numImages):
            retstr = retstr + '\n' + 'For run %s :'%(i+1,)
            statusArray = self.pickVarAtImage('status',i)
            for j in range(6):
                temp = statusArray[statusArray==j]
                h = temp.size
                retstr = retstr + '\n %s grid points in status %s'%(h,j)
                if j != 0:
                    if h != 0:
                        retstr = retstr + '  WARNING!!!'
                        self.statArray[i] = 1
                        allClean = False
        if allClean:
            cleanStr = 'CLEAN MINIMIZATION'
            retstr = cleanStr
        else:
            cleanStr = 'PROBLEMS IN MINIMIZATION\n'
            retstr= cleanStr + retstr
        return retstr

    def showProblem(self, pointSize=3,fontsize=12,drawCon=True,figStart=5,showAll=False,fontPath='/usr/share/fonts/truetype/DejaVuSans.ttf',**kwds):
	"""
	ppObj is the postprocess object
	ppObj.cleanMask and ppObj.problemMask ppObj.pickVar are the important stuff
	"""
        defFiles, undefFiles = self.an['defFiles'],self.an['undefFiles']
	x = self.pickVar('X')
	y =self.pickVar('Y')
        u = self.pickVar('u')
        v =self.pickVar('v')
        
        status = self.pickVar('status')
        xstar = x+u
        ystar = y+v
	cl = self.cleanMask
        prob = self.problemMask
        #
        font = ImageFont.truetype(fontPath,fontsize)
        #
        numLoads = x.shape[0]
        for ld in range(numLoads):
            xld, yld, clld, probld,statusld,xstarld, ystarld  = x[ld,:], y[ld,:] ,cl[ld,:], prob[ld,:],status[ld,:] , xstar[ld,:], ystar[ld,:]
            #
            if clld.all():
                print 'No problem in load %s'%(ld,)
                print clld
                if showAll:
                    pass
                else:
                    continue # move on
            pylab.figure(figStart+ld)
            pylab.subplot(1,2,1)
            imFile = undefFiles[ld]
            im=openImage(imFile, retPILim=True)
            im2= im.convert("RGB")
            coordsU = np.vstack((xld[clld],yld[clld])).T
            draw,c0,cN = gridPt(im2, coordsU, pointSize, color='cyan')
            coordsD = np.vstack((xstarld[clld],ystarld[clld])).T
            draw,c0,cN = gridPt(im2, coordsD, pointSize, color='yellow')
            coordsP = np.vstack((xld[probld],yld[probld])).T
	    if drawCon: draw = gridLine(im2, coordsU, coordsD,color='green')
            if not clld.all():
                draw,c0,cN = gridPt(im2, coordsP, pointSize, color='red')
                textList = [str(int(num)) for num in statusld[probld] ]
                draw = gridText(im2,coordsP,textList,font,color='red')
            #
            pylab.imshow(im2,aspect='equal',origin='lower')
            pylab.subplot(1,2,2)
            imFile = defFiles[ld]
            im=openImage(imFile, retPILim=True)
            im2= im.convert("RGB")
            draw,c0,cN = gridPt(im2, coordsU, pointSize, color='cyan')
            draw,c0,cN = gridPt(im2, coordsD, pointSize, color='yellow')
            if drawCon: draw = gridLine(im2, coordsU, coordsD,color='green')
            if not clld.all():
                draw,c0,cN = gridPt(im2, coordsP, pointSize, color='red')
                textList = [str(int(num)) for num in statusld[probld] ]
                draw = gridText(im2,coordsP,textList,font,color='red')
            pylab.imshow(im2,aspect='equal',origin='lower')
            pylab.show()
            #
        return


#################################### higher level functions #############

class regularGridPostprocess(postprocess):
    """
    ASSUMES same regular (rectangular) grid on all member comparisons
    """
    def _init(self,**kwds):
        imageNum=0 #pick any
        x = self.pickVar('X')[imageNum,:]
        y = self.pickVar('Y')[imageNum,:]
        self.nx = len(np.unique(x))
        self.ny = len(np.unique(y))
        return

    def shapePars(self,imageNum):
        """
        finds the row column numbers of a regular X-Y grid
        as well as how to sort the remaining variables
        RETURNS nx, ny , sorter
        LIMITATIONS your grid should be regular
        """
        nx,ny = self.nx,self.ny
        x = self.pickVar('X')[imageNum,:]
        y = self.pickVar('Y')[imageNum,:]
        x,y = [make2d(var, ny,nx) for var in [x,y]]
        sorter = np.argsort(x,1)
        return nx,ny,sorter

    def pickVarGrid(self, var, imageNum,**kwds):
        try:
            var = self.pickVarAtImage(var,imageNum,**kwds)
        except KeyError: # this is for cleanMask and problemMask really
            exec 'var = self.'+var+'['+ str(imageNum) + ',:]'
        var=var.copy()
        nx,ny,sorter = self.shapePars(imageNum)
        var = make2d(var, ny,nx)
        var = rowsorter(var, sorter)
        return var

    def neighborhoodmask(self,row,col,cell):
        nx,ny=self.nx, self.ny
        cx,cy = cell.shape
        assert cx%2==1 #these should be odd for the cell to be centered about desired point
        assert cy%2==1
        cx2 = cx/2 #integer division
        cy2 = cy/2
        msk = zeros((ny,nx),'bool')
        msk[ (row-cx2) : (row+cx2+1) ,  (col-cy2) : (col+cy2+1) ] = cell
        return msk
    

    def setCellGrid(self,x,y,pointCoords='auto',freqx=None,freqy=None,cell=None,**kwds):
        """
        called by cellOperations
        DOES: 
        if cell != None, a legitimate cell is provided, sets self.sgrid which is a mask whose true portion
                         determines the smoothing grid
        if cell = None, pointCoords need to be sent and the routine finds the maximum cell for a point
        pointCoords = 'auto' just makes a self.sgrid using freqx, freqy
        freqx, freqy : the frequency of taking self.sgrid points out of the original grid 
                       if None will be set to half the cell coverage region
        pointCoords =  a list of coordinates to be specifically set to self.sGrid. Since these are
                       pixel coordinates nearest point is found
        """
        def locate(coord,x,y):
            xp,yp = coord
            d = ((x-xp)**2. + (y-yp)**2.)**0.5
            Is , Js  = where(d==d.min())
            if len(Is)!=1:
                print "more than one equal distance point, picking the first one" 
            I , J  = Is[0], Js[0]
            if d[I,J]!=0:
                print "could not find exact match for point %s, %s; picking %s, %s as nearest."%(xp,yp,x[I,J],y[I,J])
            
            return (I,J)
        #pdb.set_trace()
        imin, jmin = 0, 0
        imax, jmax = x.shape
        imax = imax - 1
        jmax = jmax - 1
        nx, ny = self.nx, self.ny
        msk = zeros((ny,nx))
        newCellSetting = None
        if cell != None:
            cx,cy = cell.shape
            assert cx%2==1 #these should be odd for the cell to be centered about desired point
            assert cy%2==1
            cx2 = cx/2 #integer division
            cy2 = cy/2
            if pointCoords=='auto':
                if freqx==None : freqx = cx2
                if freqy==None : freqy = cy2
                msk[::freqx,::freqy] = 1
                # kill the side corridors
                msk[:cx2,:]=0 ; msk[-cx2:]=0 ; msk[:,:cy2]=0 ; msk[:,-cy2:]=0 
            else:
                for coord in pointCoords:
                    I,J= locate(coord,x,y)
                    max_ishape = 2 * min(abs(I-imin), abs(imax-I))  + 1
                    max_jshape = 2 * min(abs(J-jmin), abs(jmax-J))  + 1
                    msk[I,J] = 1
                    sh =[cx,cy]
                    flag=0
                    if (max_ishape < cx) :
                        sh[0]=max_ishape
                        flag=1
                        print 'correcting cell x size from %s to %s'%(cx, max_ishape)
                    if (max_jshape < cy) :
                        sh[1]=max_jshape
                        flag=1
                        print 'correcting cell y size from %s to %s'%(cy, max_jshape)
                    if flag:
                        newCellSetting={'_typ':'reg'}
                        newCellSetting['shape'] = tuple(sh)
                    
        elif cell == None:
            if pointCoords == 'auto': raise ValueError, 'If cell is not specified point coordinates should be'
            newCellSetting={'_typ':'reg'}
            ishape , jshape = [] , []
            
            for coord in pointCoords:
                I,J= locate(coord,x,y)
                auto_ishape = min(abs(I-imin), abs(imax-I))
                auto_jshape = min(abs(J-jmin), abs(jmax-J))
                msk[I,J] = 1
                ishape.append(auto_ishape)
                jshape.append(auto_jshape)
            sh = (int(2*min(ishape)+1), int(2*min(jshape)+1))
            newCellSetting['shape'] = sh
        #
        self.sgrid = msk
        self.sI, self.sJ = np.nonzero(self.sgrid)
        self.sny, self.snx = np.unique(self.sI).size, np.unique(self.sJ).size
        print newCellSetting
        return newCellSetting
    
    def cellOperations(self,imageNum,cellSetting,**kwds):
        """
        called by e.g. plot_derivatives; makes the cell (limited region of the entire grid) to process for, e.g., overlay plots
        cellSetting = None or 'all' or particular setting. 
                      'all' is when you want the entire frame data basically a cell of everything. Useful to fool functions
                       written for cells when you want them to work for the entire data.
        """
        x = self.pickVarGrid('X',imageNum)
        y = self.pickVarGrid('Y',imageNum)
        if cellSetting == 'all':
            self.sgrid = zeros((self.ny,self.nx)) # self.ny and self.nx are the entire grid shape
            cell = cellMaker(shape = (self.ny,self.nx) , _typ='reg')
            self.sI, self.sJ = array([self.ny/2,]),array([self.nx/2,]) # use the center of the entire grid and put a cell of entire grid on it: YIELDS a cell of entire grid
            self.sny, self.snx = 1 , 1 
            #
        elif cellSetting != None: 
            cell = cellMaker(**cellSetting)
            cellSetting = self.setCellGrid(x,y,cell=cell,**kwds)
            # reforming the cell if cellSetting returned corrected
            if cellSetting !=None: cell = cellMaker(**cellSetting)
        else:
            cellSetting = self.setCellGrid(x,y,cell=None,**kwds)
            cell = cellMaker(**cellSetting)
        return (cell,cellSetting)
    
    def showDispOnImage(self,imageNum, drop = 'nothing',  figStart=10,quiverKwds = {'angles':'xy','scale_units':'xy','scale':0.1,'color':'r'},defPlotMode='def',**kwds):
	"""
        imageNum : the number of the comparison in the analysis object
        drop     : if 'mean' or 'median' drops the mean or median of the displacement from the displacement field; for visibility
        quiverKwds: keywords taken by the vector plot
        defPlotMode = None for no plot 'def','undef', 'defmedian' for plot options
        DOES: plots the displacement vectors on the undeformed and if desired on the deformed image. It works on the entire image.
	"""
        defFiles, undefFiles = self.an['defFiles'],self.an['undefFiles']
        ld = imageNum
        x = self.pickVarGrid('X',imageNum)
        y = self.pickVarGrid('Y',imageNum)
        uu = self.pickVarGrid('u',imageNum,drop='nothing')
        vv = self.pickVarGrid('v',imageNum,drop='nothing')
        u = self.pickVarGrid('u',imageNum,drop=drop)
        v = self.pickVarGrid('v',imageNum,drop=drop)
        _cm  = self.pickVarGrid('cleanMask',imageNum)
        _pm = self.pickVarGrid('problemMask',imageNum)
        # set NAN to invalid points
        u[_pm] = np.nan
        v[_pm] = np.nan
        #
        fig = plt.figure(num=figStart,figsize=(10,8))
        plt.hold(True)
        fig.add_axes([0.05,0.05,0.9,0.9])
        imFile = undefFiles[ld]
        im=openImage(imFile)
        #plt.imshow(im,aspect='equal',origin='lower',cmap=cm.gray)
        plt.imshow(np.array(im),aspect='equal',cmap=cm.gray) 
        # use np.array(im) rather than im to avoid upside down plotting
        
        q = plt.quiver(x,y,u,v,**quiverKwds)
        p = plt.quiverkey(q,100,2000,10,"10 pixel",coordinates='data',color='r')
        hideTicks()
         #xl = plt.xlabel("x (km)")
        #yl = plt.ylabel("y (km)")
        # plt.show()
        if defPlotMode!=None:
            fig = plt.figure(figStart+1)
            fig.add_axes([0.05,0.05,0.9,0.9])
            plt.hold(True)
            imFile = defFiles[ld]
            im=openImage(imFile)
            plt.imshow(np.array(im),aspect='equal',cmap=cm.gray) 
            if defPlotMode=='undef':
                q = plt.quiver(x,y,u,v,**quiverKwds)
            elif defPlotMode=='def':
                q = plt.quiver(x+uu,y+vv,u,v,**quiverKwds)
            elif defPlotMode=='defmedian':
                q = plt.quiver(x+np.median(uu),y+np.median(vv),u,v,**quiverKwds)
            p = plt.quiverkey(q,100,2000,10,"10 pixel",coordinates='data',color='r')
            hideTicks()
        return
    
    def showImage(self,imageNum,figStart=10,**kwds):
	"""
	only shows undeformed images, might tag for deprecating
	"""
        defFiles, undefFiles = self.an['defFiles'] , self.an['undefFiles']
        ld = imageNum
        #
        fig = plt.figure(num=figStart,figsize=(10,8))
        plt.hold(True)
        ax = fig.add_axes([0.05,0.05,0.9,0.9])
        ax.set_frame_on(False)
        imFile = undefFiles[ld]
        im=openImage(imFile)
        #plt.imshow(im,aspect='equal',origin='lower',cmap=cm.gray)
        plt.imshow(np.array(im),aspect='equal',cmap=cm.gray) 
        # use np.array(im) rather than im to avoid upside down plotting
        ax.axes.set_xticks([])
        ax.axes.set_yticks([])
        #hideTicks()
        #tckx = ax.axes.get_xticklines()
        #tckx.set_visible(False)
        #tcky = ax.axes.get_yticklines()
        #tcky.set_visible(False)
        #
        return
    
    
        

    def plotuvOnImage(self,imageNum,cellSetting=None,varKwds={},figStart=1,**kwds):
        """
        DOES: makes displacement vector plots on defined cells
        """
        defFil, undefFil = self.an['defFiles'][imageNum],self.an['undefFiles'][imageNum]
        x = self.pickVarGrid('X',imageNum)
        y = self.pickVarGrid('Y',imageNum)
        cell,cellSetting = self.cellOperations(imageNum,cellSetting,**kwds)
        #
        var = [self.pickVarGrid('u',imageNum,**varKwds) , self.pickVarGrid('v',imageNum,**varKwds)]
        cm  = self.pickVarGrid('cleanMask',imageNum)
        for cnt in range(self.sI.size):
            figNum = figStart +2*cnt
            I,J = self.sI[cnt],self.sJ[cnt]
            sb = subuv(I,J, x,y,var,cm,cell)
            sb.plotuvOnImage(undefFil,defFil,figNum=figNum,**kwds)
            
        return
    
    
    def measure_strains(self,imageNum,cellSetting=None,varKwds={},retcmzone=False,**kwds):
        """
        loops over all cell points runs measure_strains of the subregion, sb.measure_strains for each sub region 
        """
        defFil, undefFil = self.an['defFiles'][imageNum],self.an['undefFiles'][imageNum]
        #pdb.set_trace()
        x = self.pickVarGrid('X',imageNum)
        y = self.pickVarGrid('Y',imageNum)
        cell,cellSetting = self.cellOperations(imageNum,cellSetting,**kwds)
        #
        var = [self.pickVarGrid('u',imageNum,**varKwds) , self.pickVarGrid('v',imageNum,**varKwds)]
        cm  = self.pickVarGrid('cleanMask',imageNum)
        
        ex,ey,exy,wxy,invalidRat = zeros((self.sI.size),float),zeros((self.sI.size),float),zeros((self.sI.size),float),zeros((self.sI.size),float),zeros((self.sI.size),float)
        for cnt in range(self.sI.size):
            I,J = self.sI[cnt],self.sJ[cnt]
            sb = subuv(I,J, x,y,var,cm,cell)
            ex[cnt] , ey[cnt] , exy[cnt] , wxy[cnt]  = sb.measure_strains(**kwds)
            if retcmzone:
                invalidRat[cnt], t2, t3, t4 = sb.measure_strains(measure='invalid')
        
        def resh(a): 
            if a.size==self.sny*self.snx:
                return a.reshape(self.sny,self.snx)
            elif a.size==self.sny*self.snx*3:
                return a.reshape(self.sny,self.snx,3)
            else:
                raise ValueError
        if retcmzone:
            ret = [ resh(_par) for _par in [ex,ey,exy,wxy,invalidRat] ]
        else:
            ret = [ resh(_par) for _par in [ex,ey,exy,wxy] ]
        return ret
        
    
    def plot_derivatives(self,imageNum,cellSetting=None,varKwds={},figStart=1,hist=False,**kwds):
        defFil, undefFil = self.an['defFiles'][imageNum],self.an['undefFiles'][imageNum]
        x = self.pickVarGrid('X',imageNum)
        y = self.pickVarGrid('Y',imageNum)
        #pdb.set_trace()
        cell,cellSetting = self.cellOperations(imageNum,cellSetting,**kwds)      
        #
        var = [self.pickVarGrid('u',imageNum,**varKwds) , self.pickVarGrid('v',imageNum,**varKwds)]
        cm  = self.pickVarGrid('cleanMask',imageNum)
        figNum=figStart
        for cnt in range(self.sI.size):
            I,J = self.sI[cnt],self.sJ[cnt]
            sb = subuv(I,J, x,y,var,cm,cell)
            figNum=sb.plot_derivatives(undefFil,defFil,figNum=figNum,**kwds)
            if hist==True:
                figNum=sb.plot_derivativeHist(figNum=figNum,**kwds)
        return


    def smooth(self,var, imageNum, func=dft.planecenter2,varKwds={}, cellSetting=None, plotpatch=True,plotfits=False,plotNums='all',**kwds):
        """
        cellSetting = {'shape':(3,3),'_typ':'reg'}, e.g.
                      None -- in this case a maximum region about the supplied coordinate is considered
        if plotNums is specied as a list of integers instead of default 'all'
        only indexes in that list will be used for plotting
        """
        
        x = self.pickVarGrid('X',imageNum)
        y = self.pickVarGrid('Y',imageNum)
        cell,cellSetting = self.cellOperations(imageNum,cellSetting,**kwds)
        #
        var = self.pickVarGrid(var,imageNum,**varKwds)
        cm  = self.pickVarGrid('cleanMask',imageNum)
        #
        varsmooth = zeros((self.sI.size),float)
        parsmooth = zeros((self.sI.size , 3),float)
        par_errsmooth = zeros((self.sI.size , 3),float)
        xsmooth = zeros(self.sI.size,float)
        ysmooth = zeros(self.sI.size,float)
        pmask = zeros(self.sI.size,bool)
        okpercent = zeros(self.sI.size,float)
        sigmaz = zeros(self.sI.size,float)
        if plotNums=='all': plotNums = range(self.sI.size)
        for cnt in range(self.sI.size):
            I,J = self.sI[cnt],self.sJ[cnt]
            xsmooth[cnt] = x[I,J]
            ysmooth[cnt] = y[I,J]
            sb = sub(I,J, x,y,var,cm,cell)
            sb.analyze(func,**kwds)
            pmask[cnt] = sb.prob
            okpercent[cnt] = sb.okpercent
            varsmooth[cnt] = sb.retCenValue()
            parsmooth[cnt,:] = sb.pars
            par_errsmooth[cnt,:] = sb.err['p_err']
            sigmaz[cnt] = sb.err['sigmaz']
            if plotfits:
                if cnt in plotNums:
                    sb.plotfit(**kwds)
                #print var[0]
                #
                pass
        def resh(a): 
            if a.size==self.sny*self.snx:
                return a.reshape(self.sny,self.snx)
            elif a.size==self.sny*self.snx*3:
                return a.reshape(self.sny,self.snx,3)
            else:
                raise ValueError
        ret = [ resh(_par) for _par in [xsmooth, ysmooth, varsmooth,pmask, parsmooth, okpercent, par_errsmooth, sigmaz] ]
        return ret
            
    def uv_smooth(self, imageNum, _plot=True, **kwds):
        x, y , u, pm, par_u, okp, par_uerr, sigma_u = self.smooth('u', imageNum, **kwds)
        x, y , v, pm, par_v, okp, par_verr, sigma_v = self.smooth('v', imageNum, c='r',**kwds)
        ex = par_u[...,0]
        ey = par_v[...,1]
        exy = 0.5*(par_u[...,1] + par_v[...,0])
        wxy = 0.5*(par_u[...,1] - par_v[...,0])
        if _plot:
            uplot = regionPlot(x,y,u,pm) ; uplot.surf() ; labels('u [pixels]')
            vplot = regionPlot(x,y,v,pm) ; vplot.surf() ; labels('v [pixels]')
            explot = regionPlot(x,y,ex,pm) ; explot.surf() ; labels('ex')
            eyplot = regionPlot(x,y,ey,pm) ; eyplot.surf() ; labels('ey')
            exyplot = regionPlot(x,y,exy,pm) ; exyplot.surf() ; labels('exy')
            wxyplot = regionPlot(x,y,wxy,pm) ; wxyplot.surf() ; labels('wxy')
        return (ex,ey,exy,wxy,okp, sigma_u, sigma_v)
        

def labels(zlabel, xlabel = 'x [pixels]',  ylabel= 'y [pixels]'):
    ax=plt.gca()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)  
    ax.set_zlabel(zlabel)
    return


def get_okpercent(c, imageNum):
    return nonzero(c.cleanMask[imageNum,...])[0].size/float(c.numPixels)*100.

class postProcessMulti:
    def __init__(self, pFiles , gridCoords):
        self.gridCoordsX, self.gridCoordsY= gridCoords
        self.pFiles=pFiles
        self.gridShape = self.gridCoordsX.shape
        self.load()
        self.numImages = self.cs[0].numImages
        return
    
    def load(self):
        """ this is very time consuming if the files are very detailed."""
        self.cs=[]
        for fil in self.pFiles:
            print fil
            an = pickle.load(open(fil))
            rawDataColDict = {'X':0,'Y':1,'u':2,'v':3,'ex':4,'ey':5,'exy':6,'dudx':7,'dvdy':8,'dudy':9,'dvdx':10,'Theta':11,'w':12,'status':13, 'iter':14, 'corrcoeff':15}
            c = regularGridPostprocess(an.rawdatamatrices, colDict = rawDataColDict)
            self.cs.append(c)
        return
    
    def workOnAll(self,funcName,**kwds):
        rets = []
        for c in self.cs:
            ret = funcName(c,**kwds)
            rets.append(ret)
        return rets
    
    def get_okPercent(self,  plot=False):
        okPercents = []
        for imageNum in range(self.numImages):
            ret  = self.workOnAll(get_okpercent,imageNum=imageNum)
            okPercent = array(ret).reshape(self.gridShape)
            fig = plt.figure()
            ax = fig.gca(projection='3d')
            if plot:
                x, y = self.gridCoordsX, self.gridCoordsY
                ax.plot_surface(x,y, self.okPercent , rstride=1, cstride=1, cmap=cm.gray,
                            linewidth=0, antialiased=False)
                ax.set_xlabel('x [mm]')
                ax.set_ylabel('y [mm]')
            okPercents.append(okPercent)
        self.okPercents= array(okPercents)
        return
    

class regionPlot:
    """ the context of region plot is plotting F(x,y) in different ways by smoothing or taking its derivatives and so forth."""
    def __init__(self,x,y,F,badPtsMask):
        """
        x, y, F a decent grid definition such that, say contour(x,y,F) works 
        badPtsmask
        """
        self.x = x
        self.y = y
        self.F = F
        self.plot = F
        self.bad = badPtsMask
        self.good = np.logical_not(badPtsMask)
        return 
    
    def prop(self,maskBad=True):
        if maskBad :
            pl = self.plot[self.good]
        else:
            pl = self.plot
        return (pl.min(), pl.max(), pl.mean(), np.median(pl), pl.std())
        
    def contourf(self,maskBad=True):
        pl = self.plot.copy()
        if maskBad: pl[self.bad] = np.nan
        plt.figure()
        plt.contourf(self.x,self.y,pl)
        plt.colorbar()
        return
    
    def surf(self,maskBad=True,ax=None):
        pl = self.plot.copy()
        if maskBad: pl[self.bad] = np.nan
        if ax==None:
            fig = plt.figure()
            ax = fig.gca(projection='3d')
        else:
            ax.hold(True)
        surf = ax.plot_surface(self.x,self.y,pl)
        return ax
    
    def scatter(self,maskBad=True,ax=None):
        pl = self.plot.copy()
        if maskBad: pl[self.bad] = np.nan
        if ax==None:
            fig = plt.figure()
            ax = fig.gca(projection='3d')
        else:
            ax.hold(True)
        surf = ax.scatter(self.x,self.y,pl)
        return ax
    
    def wireframe(self,maskBad=True,ax=None):
        pl = self.plot.copy()
        if maskBad: pl[self.bad] = np.nan
        if ax==None:
            fig = plt.figure()
            ax = fig.gca(projection='3d')
        else:
            ax.hold(True)
        surf = ax.plot_wireframe(self.x,self.y,pl)
        return ax

    def pcolor(self,maskBad=True):
        pl = self.plot.copy()
        if maskBad: pl[self.bad] = np.nan
        plt.figure()
        plt.pcolor(self.x,self.y,pl)
        plt.colorbar()
        #plt.show()
        return
        
    def fitSpline(self,maskBad=True,**kwds):
        if maskBad:
            tck = interpolate.bisplrep(self.x[self.good],self.y[self.good],self.F[self.good],**kwds)
        else:
            tck = interpolate.bisplrep(self.x,self.y,self.F,**kwds)
        self.plot = interpolate.bisplev(self.x[:,0],self.y[0,:],tck)
        pass

    def dropAv(self):
        pass
