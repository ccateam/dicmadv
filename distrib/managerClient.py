from multiprocessing.managers import BaseManager
class QueueManager(BaseManager): pass
QueueManager.register('get_queue')
m = QueueManager(address=('navier.me.boun.edu.tr', 50000), authkey='karamazof')
m.connect()
queue = m.get_queue()
print queue.get()
