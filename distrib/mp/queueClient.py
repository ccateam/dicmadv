
from multiprocessing.managers import BaseManager



def queueNext(**managerKwds):
    class QueueManager(BaseManager): pass
    QueueManager.register('get_queue')
    m = QueueManager(**managerKwds)
    m.connect()
    queue = m.get_queue()
    if queue.empty():
        return False
    else:
        return queue.get()

if __name__=="__main__":
    print queueNext(address=('navier.me.boun.edu.tr', 50000), authkey='karamazof')
