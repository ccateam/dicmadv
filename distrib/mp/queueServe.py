
from multiprocessing.managers import BaseManager
from numpy import arange
import Queue

def serveQueue(fillList,**managerKwds):
    queue = Queue.Queue()
    for mem in fillList:
        queue.put(mem)
    class QueueManager(BaseManager): pass
    QueueManager.register('get_queue', callable=lambda:queue)
    m = QueueManager(**managerKwds)
    s = m.get_server()
    s.serve_forever()
    return

def fillQueue(fillList,**managerKwds):
    class QueueManager(BaseManager): pass
    QueueManager.register('get_queue')
    m = QueueManager(**managerKwds)
    m.connect()
    queue = m.get_queue()
    for mem in fillList:
        queue.put(mem)
    return 

def serveOrFill(fillList,**managerKwds):
    import socket
    try:
       serveQueue(fillList,**managerKwds)
    except socket.error:
        fillQueue(fillList,**managerKwds)
    return 


if __name__=="__main__":
    #serveQueue(arange(1.,5.)*1.e7, address=('', 50000), authkey='karamazof')
    #fillQueue(arange(5.,9.)*1.e7, address=('', 50000), authkey='karamazof')
    # make its default serving an empty queue
    serveOrFill([], address=('', 50000), authkey='karamazof')
