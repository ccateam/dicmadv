import multiprocessing as mp
import os,time
#mp.Lock
#mp.Queue
#mp.Process
#mp.Pipe

def f(n):
    sum=0.
    for i in range(n):
        sum+=n*n
    #print sum
    return sum

def fq(qlocal,qout):
    sum=0.
    n  = qlocal.get()
    print n
    for i in range(n):
        sum+=n*n
    #print sum
    qout.put(n)
    return sum


def fq_init(qin,qleft,qout):
    """http://stackoverflow.com/questions/3827065/can-i-use-a-multiprocessing-queue-in-a-function-called-by-pool-imap"""
    fq.qin = qin
    fq.qleft = qleft
    fq.qout = qout
    return
    
def cback(x):
    print "Done."
    return

if __name__=="__main__":
    mode = 0 
    if mode==0:
        qin = mp.Queue()
        qleft = mp.Queue()
        qout = mp.Queue()
        n=10000000
        NumWork=10
        for i in range(NumWork): 
            qin.put((i+3)*n)
            qleft.put((i+3)*n)
        results=[]
        for i in range(5) :
            p = mp.Process(target=fq, args = (qin,qout))
            p.start() 
            print "working on %s"%(n*i)
            #p.join()
        p.join()  
