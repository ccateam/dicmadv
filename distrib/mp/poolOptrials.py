import multiprocessing as mp
import os,time
#mp.Lock
#mp.Queue
#mp.Process
#mp.Pipe

def f(n):
    sum=0.
    for i in range(n):
        sum+=n*n
    #print sum
    return sum

def fq():
    sum=0.
    n  = fq.qin.get()
    print n
    for i in range(n):
        sum+=n*n
    #print sum
    fq.qleft.get()
    fq.qout.put("Done")
    return sum


def fq_init(qin,qleft,qout):
    """http://stackoverflow.com/questions/3827065/can-i-use-a-multiprocessing-queue-in-a-function-called-by-pool-imap"""
    fq.qin = qin
    fq.qleft = qleft
    fq.qout = qout
    return
    
def cback(x):
    print "Done."
    return

if __name__=="__main__":
    mode=3
    if mode==0:
        t = mp.Pool(5)
        n=10000000
        k = t.map(f,[n*i for i in range(10)]   )
        t.close()
        print "B"
        print k
    elif mode==1:
        t =  mp.Pool(5)
        n=10000000
        NumWork=10
        L = range(NumWork)
        results = []
        for i in L :
            #t.apply(f, [N,])
            # apply will practically will not be a parallel run
            # it will wait until f is evaluated so, you will only 
            # see a single cpu active
            result = t.apply_async(f, args = [n*i,])
            #result.get() 
            #result.wait()
            #both will have a similar effect and block the process 
            #until it is finished. This will be an apply execution 
            #rather than a apply_async op.
            results.append(result)
            print "working on %s"%(n*i)
        print "A"
        t.close()
        #t.join()
        # with this it will wait until all the process in the pool are done. So,
        # the rest will start getting executed only after all computation is done.
        print "B"
        for i in range(10):
            #return result readiness for 10s every 1s
            time.sleep(1.5)
            print("round %s"%(i))
            for res in results:
                print res.ready()
                
    elif mode==2:
        t = mp.Pool(5)
        n=10000000
        k = t.map_async(f,[n*i for i in range(10)]   )
        t.close()
        print "B"
        for i in range(10):
            #return result readiness for 10s every 1s
            time.sleep(1.5)
            print k.ready()
        
        t.join()
    elif mode==3:
        qin = mp.Queue()
        qleft = mp.Queue()
        qout = mp.Queue()
        t =  mp.Pool(5,fq_init,[qin,qleft,qout])
        n=10000000
        NumWork=10
        for i in range(NumWork): 
            qin.put(i*n)
            qleft.put(i*n)
        results=[]
        for i in range(10) :
            result = t.apply_async(fq, args = ())
            results.append(result)
            print "working on %s"%(n*i)
        
        t.close()
        #t.join()
       
        for i in range(20):
            #return result readiness for 10s every 1s
            time.sleep(1.5)
            print("round %s"%(i))
            print 'Queue in size is', qin.qsize()
            print 'Queue left size is', qleft.qsize()
            print 'Queue out size is', qout.qsize()