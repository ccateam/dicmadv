import shutil
import os
import sys
import os.path as osp
from scipy import *
from scipy import stats

#### 
#### 
def linregress(x,y,confint=0.95):
    """since I wanted error bars on my slope (which will become the strain) and
    regular linear regression did not provide those, I went programming my own"""
    x=ravel(x)
    y=ravel(y)
    if len(x)!=len(y):
        raise ValueError, "unequal lengths in linear regression"
    n = len(x)
    c = stats.t.isf((1-confint)/2.,n-2)
    invn = 1.0/n
    X = sum(dot(x,x))
    Y = sum(dot(y,y))
    XY = sum(dot(x,y))
    sx = sum(x)
    sy = sum(y)
    s1sqn_1 = X - invn *sx*sx
    sl = (XY - invn*sx*sy)/s1sqn_1
    inte = (sy - sl*sx)* invn
    s2sqn_1 = Y - invn*sy*sy
    resid = (s2sqn_1-sl*sl*s1sqn_1)
    sdata = sqrt(resid/(n-2))
    errsl = c*sdata/sqrt(s1sqn_1)
    return sl,inte,errsl,resid,sdata
    
    
def linregress_2d(xmat,ymat,axis='row'):
    '''def linregress_2d(xmat,ymat,axis='row') Does row to row or col to col
    scipy.stats.linregress for 2 2D matrices of the same size. Returns a tuple
    of slope list, intercept list, r list, ttp list, stderr list.'''
    if shape(xmat) != shape(ymat):
        raise ValueError, 'The sizes of the two matrices supplied to linregress_2d should match.'
    if not(axis == 'row' or axis == 'col'):
        raise ValueError,'axis can have two values: row or col'
    #Get col implementation simply by transposing these matrices
    if axis=='col':
        xmat , ymat = transpose(xmat),transpose(ymat)
    #Implement as if row by row:
    slope, intercept, errsl, resid, sdata = [],[],[],[],[]
    for i in range(size(xmat,0)):
        x , y = xmat[i,:], ymat[i,:]
        sl, inte, e, r, sd = linregress(x,y)
        slope.append(sl) , intercept.append(inte) , errsl.append(e)
        resid.append(r), sdata.append(sd)
    return (slope, intercept, errsl, resid, sdata)


def gridMaker(x0,y0,delX,delY,numX,numY,format='leftAndRight'):
    '''The way you define your format here matters mostly if
    you have icoflag=1, so that initial values are inherited from
    the last point. Say you do uniaxial loading and the load
    axis is Y and transverse axis X. Then you'd probably want to
    go left and right on X since ideally X variation is zero. Watch
    out for sorting in postprocessing as you want a routine grid there.
    '''
    xf = x0 + delX*(numX)
    yf = y0 + delY*(numY)
    gr = mgrid[x0:xf:delX,y0:yf:delY]
    X , Y = gr[0], gr[1]
    #produce row-like doublet
    doublets=[]
    if format=='row':
        for i in range(numY): 
            Xcol,Ycol = X[:,i],Y[:,i]
            doublets = doublets + list(zip(Xcol,Ycol))
    elif format=='col':
        for i in range(numX):
            Xrow,Yrow = X[i,:],Y[i,:]
            doublets = doublets + list(zip(Xrow,Yrow))
    elif format=='leftAndRight':
        for i in range(numY): 
            Xcol,Ycol = X[:,i],Y[:,i]
            L = list(zip(Xcol,Ycol))
            if i%2==1:
                L.reverse()
            doublets=doublets + L
    elif format=='upAndDown':
        for i in range(numX):
            Xrow,Yrow = X[i,:],Y[i,:]
            L = list(zip(Xrow,Yrow))
            if i%2==1:
                L.reverse()
            doublets=doublets + L
    return doublets
#
def listToString(myList,sep=' '):
    retstr=''
    for i,item in enumerate(myList):
        #print i, item
        if type(item)==str:
            pass
        else:
            item=`item`
        if i==0:
            retstr=retstr + item
        else:
            retstr=retstr + sep + item
    return retstr
#
def genStrMaker(var, listSep=' '):
    if str(type(var))=="<type 'array'>":
        var=list(var)
    if type(var)==list or type(var)==tuple:
        retstr=listToString(var,sep=listSep)
    elif type(var)==str:
        retstr=var
    else:
        retstr=`var`
    if type(retstr)!=str:
        raise 'not returning string for var %s'%(var,)
    return retstr
#
def varToFileStr(varSepList):
    '''admits a list of two or three element lists. The second element is the
    separator between the consequtive variables. The third element is if
    the variable is a list and wants the separator for the list itself.'''
    retstr=''
    listSep='' #just dummy in case it is not redefined.
    for varSep in varSepList:
        var, sep = varSep[0], varSep[1]
        if type(var)==list or type(var)==tuple:
            listSep = varSep[2]
        retstr =retstr + genStrMaker(var,listSep=listSep)+sep
    return retstr
#
def removeL(L,remL):
    for l in remL:
        try:
            L.remove(l)
        except ValueError:
            raise ValueError, '%s does not exist in list %s'%(l,L)
    return
        
