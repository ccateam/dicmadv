#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 15:05:09 2019

@author: can
"""
import numpy as np
import matlab.engine
eng=matlab.engine.start_matlab()

A = np.linspace(0,1,num=101)
x,y = np.meshgrid(A,A)

z0=np.exp(x+y)

znan=z0.copy()
znan[19:50,39:70]=np.nan
znan[29:90,4:10]=np.nan
znan[69:75,39:90]=np.nan

znand = matlab.double(znan.tolist())
z4 = eng.inpaint_nans(znand,4)
z0 = eng.inpaint_nans(znand,0)
ddz = np.array(z4) - np.array(z0)