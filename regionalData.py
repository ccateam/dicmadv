############ CCA 2017 ##################
#
# contains convenience functions of specializing on the data
# of a region; in contour and histogram terms
#
########################################


import pickle,sys
import os.path as osp
from scipy import misc
from scipy import ndimage
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.path import Path
import time
import logging
logger = logging.getLogger(__name__)
# DIC package modules
import maskmaker ;  reload(maskmaker)
import frameMatrix ;  reload(frameMatrix)
import npostProcess as npp;  reload(npp)
import frameMatrixPlot as fmp; reload(fmp)
import nplot; reload(nplot)
import click ; reload(click)
import genericUtility.matrixTools as gumt ; reload(gumt)
trim_2d = gumt.trim_2d
import genericUtility.matplotlibstyles as gms;reload(gms)

def getBorder(mat):
    """ applies edge detection to get the border of a True False mask that defines
        a region
    """
    sx = ndimage.sobel(mat, axis=0, mode='constant')
    sy = ndimage.sobel(mat, axis=1, mode='constant')
    sob = np.hypot(sx, sy)
    sob[sob!=0]=1
    sob[sob==0]=np.nan
    return sob

def cleanNan(mat):
    """ cleans nan values from a matrix, the sent back array is 1d"""
    return mat[np.logical_not(np.isnan(mat))]

class regionData:
    def __init__(self,X,Y,dataDict,limList=[]):
        """
        X, Y = X and Y matrices of a region
        dataDict = a dictionary of variable as key and data matrix as value
        {'ex':exmatrix,'wxy', wxymatrix, ...}
        limList = list of [(andOr, variable, minVal, maxVal, notCond), andOr
        defines how it will combine with the previous condition
                e.g. [('','wxy',-0.02, 0.05, False),('or','ey',-0.01, 0.05, True),]
        The first andOr can be left empty for there is logical combination to process there.
        """
        self.X = X
        self.Y = Y
        self.dataDict = dataDict
        self.limList = limList
        self.dataLimOn=False
        self.overlay=False
        return
    
    def resetAndActivateLim(self,limList=[],**kwds):
        """
        TAKES : limList
        DEOS: in usage setting self.limList and later having to run setDataMask creates
        a mental barrier for the user. In this function, the user will directly 
        provide the full new limList [what is provided will override old limList]
        and according self.dataMask will be formed immediately. 
        """
        self.limList = limList
        self.setDataMask()
        return
        
    
    def setDataMask(self):
        """"implement the data mask based on self.limList. You can set externally. 
        Do not have to set it during __init__"""
        #
        
        for i,subList in enumerate(self.limList):
            op,key,mn,mx,leaveAsIs = subList
            tmp = np.logical_and(self.dataDict[key]>mn, self.dataDict[key]<mx)
            if leaveAsIs:
                pass
            else:  # take the negative of the mask
                tmp = np.logical_not(tmp)
            if i==0:
                #op is dummy
                self.dataMask=tmp
            else:
                if op=="and":
                    op = np.logical_and
                elif op=="or":
                    op = np.logical_or
                else:
                    raise ValueError
                self.dataMask = op(self.dataMask,tmp)
            #
        return
    
    def inputMicroStructure(self,im,imcoords):
        """ optional image matrix inclusion for overlay plots. 
        Bigger matrices so can be expensive."""
        self.im = im
        ny,nx = self.im.shape
        xmin,xmax,ymin,ymax=imcoords
        xarr =  np.linspace(xmin,xmax,nx)
        yarr =  np.linspace(ymin,ymax,ny)
        self.imX,self.imY = np.meshgrid(xarr,yarr) 
        return
    
        
    
    def getBox(self,*args,**kwds):
        """
        always sets self.boxMask. sets self.imboxMask if self.overlay is True
        x0,y0,x1,y1 to be sent
        """
        self.getDataBox(*args,**kwds)
        if self.overlay:
            self.getImageBox(*args,**kwds)
        return
    
    def setFullBox(self):
        """
        DOES: sets box to entire data region
        MOTIVATION: when we want to do data filter work on the entire map; getting
        the box to comprise everything is needed
        """ 
        eps=1.e-8 # deal with inequalities to cover all data
        x0,y0,x1,y1 = np.min(self.X)-eps,  np.min(self.Y)-eps, np.max(self.X)+eps, np.max(self.Y)+eps
        self.getBox(x0,y0,x1,y1)
        return
        
    def getDataBox(self, x0,y0,x1,y1):
        """
        x0,y0 is the low limit
        x1,y1 both are higher numbers
        """
        alltrue = np.ones_like(self.X).astype('bool')
        cond1 = "X<%s"%(min(x0,x1), )
        cond2 = "X>%s"%(max(x0,x1),)
        cond3 = "Y<%s"%(min(y0,y1),)
        cond4 = "Y>%s"%(max(y0,y1),)
        self.boxMask = maskmaker.maskmaker(alltrue,self.X,self.Y,
                               stringConditionsList=[cond1,cond2,cond3,cond4],
                               maskValue=[False,]*4)
        self.trim_2d = gumt.trim_2d(self.boxMask)
        a,b,c,d = self.trim_2d
        self.boxX = self.X[a:b+1,c:d+1]
        self.boxY = self.Y[a:b+1,c:d+1]
        return
        
    def getImageBox(self,x0,y0,x1,y1):
        alltrue = np.ones_like(self.imX).astype('bool')
        cond1 = "X<%s"%(min(x0,x1), )
        cond2 = "X>%s"%(max(x0,x1),)
        cond3 = "Y<%s"%(min(y0,y1),)
        cond4 = "Y>%s"%(max(y0,y1),)
        # may discard
        self.imboxMask = maskmaker.maskmaker(alltrue,self.imX,self.imY,
                               stringConditionsList=[cond1,cond2,cond3,cond4],
                               maskValue=[False,]*4)
        self.imtrim_2d = gumt.trim_2d(self.imboxMask)
        a,b,c,d = self.imtrim_2d
        imboxX = self.imX[a:b+1,c:d+1]
        imboxY = self.imY[a:b+1,c:d+1]
        self.imboxcoords = (imboxX.min(),imboxX.max(),imboxY.min(),imboxY.max())
        self.imboxData = self.im[a:b+1,c:d+1]
        return
        
    def getActiveRegion(self):
        """
        unifies region grabs. 
        if dataLimOn=True it combines regionMask and dataMask and sets self.activeMask
        else it sets self.activeMask to self.regionMask
        """
        try:
            self.regionMask
        except AttributeError:
            self.regionMask = self.boxMask.copy()
        #
        if self.dataLimOn==True:
            self.activeMask = np.logical_and(self.dataMask,self.regionMask)
        else:
            self.activeMask = self.regionMask
        return
    
    def getDataRegion(self,regionConds,maskValues,maskInvert=False):
        """
        regionConds =  a list of string inequality conditions in X and Y
        maskValue =  a list of True or False that correspond to the list of regionConds
        """
        alltrue = np.ones_like(self.X).astype('bool')
        
        regionMask = maskmaker.maskmaker(alltrue, self.X,self.Y, 
                        stringConditionsList=regionConds, maskValue=maskValues)
        if maskInvert: 
            regionMask = np.logical_not(regionMask)
        # now bring boxMask in play
        self.regionMask = np.logical_and(self.boxMask,regionMask)
        #    
        return

    
    def getDataRegionLines(self,twoPointList,ops,maskValues,**kwds):
        """
        interceptList= a list of four value tuples that contain two x,y
                        point coords of a line e.g.
                        twoPointList = [(x0,y0,x1,y1), (x2,y2,x3,y3)]
        ops = a list of inequality operands that define the region
        maskValues = a list of True or False that correspond to the list of regionConds
        """
        self.regionConds=[]
        for twoPoints,op in zip(twoPointList,ops):
            x0,y0,x1,y1 = twoPoints
            self.regionConds.append(maskmaker.lineInEqFromIntercept(x0,y0,x1,y1,op))
            
       
        self.getDataRegion(self.regionConds,maskValues,**kwds)
        return
        
        
        return
    
    def getDataRegionPoints(self,fignum=None,ax=None):
        if ax:
            pass
        else:
            if fignum:
                plt.figure(fignum)
            ax=plt.gca()
        #
        line,= ax.plot(self.boxX[0][0], self.boxY[0][0])
        linebuilder = click.LineBuilder(line)
        
        return linebuilder
        
    
    def getDataRegionFromClickPoints(self,linebuilder):
        twoPointsList=[]
        for i in range(2,len(linebuilder.xs)):
       
            x0,x1=linebuilder.xs[i-1:i+1]
            y0,y1=linebuilder.ys[i-1:i+1]
            twoPointsList.append([x0,y0,x1,y1])
        #
        twoPointsList.append([twoPointsList[len(twoPointsList)-1][2],twoPointsList[len(twoPointsList)-1][3],twoPointsList[0][0],twoPointsList[0][1]])
        
        if np.cross([twoPointsList[0][2]-twoPointsList[0][0],twoPointsList[0][3]-twoPointsList[0][1]],[twoPointsList[1][2]-twoPointsList[1][0],twoPointsList[1][3]-twoPointsList[1][1]])>0:
            op=['>']
        if np.cross([twoPointsList[0][2]-twoPointsList[0][0],twoPointsList[0][3]-twoPointsList[0][1]],[twoPointsList[1][2]-twoPointsList[1][0],twoPointsList[1][3]-twoPointsList[1][1]])<0:
            op=['<']
        
        ops=op*len(twoPointsList)
        maskValues=[False]*len(twoPointsList)
        self.getDataRegionLines(twoPointsList,ops,maskValues)
        #self.getDataRegionLines(twoPointsList,ops,maskValues,maskInvert=True)
        # len of two point list => prepare ops and maskValues ['>',]* len etc.
        # send to    getDataRegionLines
    
        return     
    
    def getDataBoxFromPlot(self,figNum=None):
        """
        get box selection (~.boxMask) from a figure whose data range is selected
        by resizing.
        """
        if figNum: 
            plt.figure(figNum)
        #
        ax=plt.gca()
        x0,x1 = ax.get_xlim()
        logger.info("got x0=%s,x1=%s"%(x0,x1))
        y0,y1 = ax.get_ylim()
        logger.info("got y0=%s,y1=%s"%(y0,y1))
        #
        self.getBox(x0,y0,x1,y1)
        return
    
    def plotBox(self,var,leveling,_contourfpKwds,ax=None,background=None,**kwds):
        """ background = send a color for background if you want to change background color
        , useful for,e.g. makin NaN point stand out, by sending 'gray'"""
        data=self.dataDict[var]
        a,b,c,d = self.trim_2d
        # dictionary is mutable; leave the original dictionary alone
        _contourfpKwds_use = _contourfpKwds.copy()
        _contourfpKwds_use['x'] = self.boxX
        _contourfpKwds_use['y'] = self.boxY
        boxData = data[a:b+1,c:d+1]
        _contourfpKwds_use['data']=boxData
            
        
        if ax==None:
            fig,ax = gms.yieldDefaultAxis()
        
        ax.set_title(var)
        if self.overlay==True:
            _imshowpKwds=dict(imdata=self.imboxData,coords=self.imboxcoords,
                                origin="lower") # lower is weird but...r
            nplot.overlayplot(_imshowpKwds,leveling,_contourfpKwds_use,ax=ax) 
        elif self.overlay==False:
            nplot.dataplot(leveling,ax=ax,**_contourfpKwds_use) 
        else: 
            raise ValueError
        
        if background:
            try:
                ax.set_axis_bgcolor(background)
            except AttributeError:
                ax.set_facecolor(background)

        return
    
    def showActiveMask(self,fignum=None,ax=None):
        if ax:
            pass
        else:
            if fignum:
                plt.figure(fignum)
            ax=plt.gca()
        self.getActiveRegion()
        ax.imshow(self.activeMask, cmap='binary')
        return
        
    
    def plotRegion(self,var,leveling,_contourfpKwds,_imshowpKwds=None,ax=None,background=None,
        regionBorderStyle=dict(colors='y',linewidths=0.8,linestyles='dotted',alpha=1),**kwds):
        """ background = send a color for background if you want to change background color
        , useful for,e.g. makin NaN point stand out, by sending 'gray'"""
        if ax==None:
            fig,ax = gms.yieldDefaultAxis()
        self.plotBox(var,leveling,_contourfpKwds,_imshowpKwds = _imshowpKwds,
                        ax=ax,background=background)
        a,b,c,d = self.trim_2d
        #regmask = getBorder(self.regionMask)
        #reg = regmask[a:b+1,c:d+1]
        self.getActiveRegion()
        reg = self.activeMask[a:b+1,c:d+1]
        ax.contour(self.boxX,self.boxY,reg,**regionBorderStyle) 
        #ax.contourf(self.boxX,self.boxY,reg)
        #ax.colorbar()
        ax.set_aspect('equal')
        #ax.invert_yaxis()
        
        return
    
    def histData(self,var,where,**kwds):
        """
        where = 'box' does the work on box
              = 'region' works on region
        histProc =  a dictionary of keywords to be sent to
                   nplot.histProcessor
        """
        data=self.dataDict[var]
        if where == 'box':
            dat =data[self.boxMask]
        elif where == 'region':
            self.getActiveRegion()
            dat = data[self.activeMask]
        else: 
            raise ValueError
        # you might want to keep track of lost point percent
        procData, histKwds,histTxt = nplot.histProcessor(dat,**kwds)
        
        return procData, histKwds,histTxt

    def plotHistPar(self,par,where,ax=None,**kwds):
        """
        par =  par name as defined as a key in self.dataDict, 
                 conventionally eyy, wxy, etc.
        where = 'box' does the work on box
              = 'region' works on region

        """
        procData, histKwds,histTxt  = self.histData(par,where=where,**kwds)
        print 'stats for par %s: %s\n'%(par,histTxt)
        if ax==None:
            fig,ax = gms.yieldDefaultAxis()
        ax.plot()
        ax.set_title(par)
        ax.hist(procData,**histKwds)
        return
    
    def plotHistPars(self,ax,pars,where, axis='both',labelsize=22 ,which='major',length=6, width=2, colors='k',direction='in',legSize=22,borderSz=4.0, hStyles = [{'color':'k','linewidth':4.},{'color':'r','linewidth':4.},{'color':'b','linewidth':4.},{'color':'g','linewidth':4.},{'color':'m','linewidth':4.},],histDefaults={'histtype':'step'},legTxt=[r'$\varepsilon_{yy} $',r'$\varepsilon_{xx}$' ],**kwds):
        """
        pars = list of par name as defined as a key in self.dataDict, 
                 conventionally eyy, wxy, etc.
        hStyles = styles of the histogram plotting, give it as a list of dictionaries
        plots them on the same figure and puts the names in the label
        where = 'box' does the work on box
              = 'region' works on region
        """
        if ax==None:
            fig,ax = gms.yieldDefaultAxis()
        ax.plot()
        ax.hold(True)
        kwds.update(histDefaults)
        for i,par in enumerate(pars):
            style = hStyles[i]
            kwds.update(style)
            procData, histKwds,histTxt= self.histData(par,where=where,**kwds)
            print 'stats for par %s: %s\n'%(par,histTxt)
           
           
            
            ax.hist(procData,label=par,**histKwds)
        ax.grid()
        ax.tick_params(axis=axis,labelsize=labelsize ,which=which,length=length, width=width, colors=colors,direction=direction)
        #ax.tick_params(axis='both',labelsize=22 ,which='major',length=6, width=2, colors='k',direction='in')
        ax.legend(legTxt,prop={'size':legSize})
        #ax.legend()

        ax.set_ylabel(r'$f$ [%]',fontsize=38,rotation=90)
        ax.set_xlabel(r'${\varepsilon}$ [%]',fontsize=32)

        ax.spines['top'].set_linewidth(borderSz)
        ax.spines['right'].set_linewidth(borderSz)
        ax.spines['bottom'].set_linewidth(borderSz)
        ax.spines['left'].set_linewidth(borderSz)
        return
            
    def plotHistAndCorrArea(self,where,_histPars,_areaPars):
        """
        _histPars = dictionary of variables sent to histogram; pars, histProc are necessary!
        _areaPars = dictionary of variables sent to region plots; var,leveling,_contourfpKwds are necessary!
        stands for plot histogram and cdictionary of variables sent to histogram; pars is necessary!orresponding region; we should indeed
        store our histograms with these not to lose track of where they belong to. 
        """
        f, (axh, axarea) = plt.subplots(1, 2)
        _histPars['ax']=axh
        _histPars['where']=where
        _areaPars['ax']=axarea

        self.plotHistPars(**_histPars)
        if where =='box':
            self.plotBox(**_areaPars)
        elif where =='region':        
            self.plotRegion(**_areaPars) 
        return
        
        #self.plotBox('wxy',leveling,_contourfpKwds,ax1,background='gray')
        #self.plotHistPars(ax2,['eyy','exx'],where='box',bins=2000,normed=1)
        #return
        
    def plotHistAndCorrAreaM(self,where,_histPars,_areaPars,areaVars=['eyy','wxy','exx']):
        num_areaVars = len(areaVars)
        
        #axis work
        areaAxes =[]
        f=plt.figure(figsize=(4,8))

        gs0=gridspec.GridSpec(2, 1)
        axh = plt.Subplot(f, gs0[0,0])
        f.add_subplot(axh)
        gs01 = gridspec.GridSpecFromSubplotSpec(1, num_areaVars, 
                                    subplot_spec=gs0[1,0],wspace=0,hspace=0)
        for i in range(num_areaVars):
            ax=plt.Subplot(f, gs01[0,i])
            areaAxes.append(ax)
        for i in range(num_areaVars):
            f.add_subplot(areaAxes[i])
        #
        _histPars['ax']=axh
        _histPars['where']=where
        self.plotHistPars(**_histPars)
        #
        for i in range(num_areaVars):
            _areaPars['ax']=areaAxes[i]
            _areaPars['var']=areaVars[i]
            if where =='box':
                self.plotBox(**_areaPars)
            elif where =='region':        
                self.plotRegion(**_areaPars) 
        #
        for i in range(num_areaVars):
            areaAxes[i].axes.set_xticks([])
            areaAxes[i].axes.set_yticks([])
        return

class regionDataPath(regionData):
    """using matplotlib.path.Path objects makes sense since inequality based
    borders fail over non-convex borders"""
    

        
    def getDataRegionPoints(self,fignum=None,ax=None,resetRegion=True):
        """
        Usage: use mouse clicks to draw the path; press any key on keyboard to 
        transfer the path. 
        RESE
        In this version, using a more advanced interaction class; this single
        function takes care of region selection all together. No need to call
        getDataRegionFromClickPoints afterwards (actually that would raise 
        an error.)
        """
        if ax:
            pass
        else:
            if fignum:
                plt.figure(fignum)
            ax=plt.gca()
        #
        #line,= ax.plot(self.boxX[0][0], self.boxY[0][0])
        if resetRegion: 
            print 'RESETTING regionMask'
            self.regionMask=self.boxMask.copy()
        
        try:
            self.regionMask
        except AttributeError:
            print 'did not find initial regionMask setting it to boxMask'
            self.regionMask=self.boxMask.copy()

        self.paths,self.pathOps=[],[]
        linebuilder = PathBuilder(ax,self.X,self.Y,self.boxMask,
                                     self.regionMask,self.paths,self.pathOps)
        # implement the data mask
        #
        
        return
    

class PathBuilder:
    def __init__(self, ax,X,Y,boxMask,regionMask,paths,pathOps):
        line, = ax.plot(X[boxMask][0], Y[boxMask][0])
        #line, = ax.plot([0], [0])
        self.line = line
        self.xs = []
        self.ys = []
        self.xys = []
        self.cid = line.figure.canvas.mpl_connect('button_press_event', self)
        self.fig = plt.gcf()
        self.cidm = self.fig.canvas.mpl_connect('key_press_event',self) 
        self.X,self.Y =X,Y
        self.XY = np.vstack((self.X.ravel(), self.Y.ravel())).T
        self.boxMask=boxMask
        self.regionMask=regionMask
        self.paths=paths
        self.pathOps=pathOps
        return
    
    def disconnect(self):
        self.fig.canvas.mpl_disconnect(self.cidm)
        self.line.figure.canvas.mpl_disconnect(self.cid)
        return
    
    def __call__(self, event):
        print event.name
        if event.name=='button_press_event':
            print 'click', event
            if event.inaxes!=self.line.axes: return
            self.xs.append(event.xdata)
            self.ys.append(event.ydata)
            self.xys.append([event.xdata,event.ydata])
            self.line.set_data(self.xs[:], self.ys[:])
            self.line.set_marker('o')
            self.line.figure.canvas.draw()
        elif event.name=='key_press_event':
            if event.key=='i':
                print 'ignoring and restarting'
                # resetting for next path
                self.xs, self.ys,self.xys= [],[],[]
            elif event.key=='a':
                print 'adding path %s; key pressed %s'%(len(self.paths),event.key)
                pth = Path(np.array(self.xys))
                self.paths.append(pth)
                self.pathOps.append('a')
                # resetting for next path
                self.xs, self.ys,self.xys= [],[],[]
            elif event.key=='e':
                print 'excluding path %s; key pressed %s'%(len(self.paths),event.key)
                pth = Path(np.array(self.xys))
                self.paths.append(pth)
                self.pathOps.append('e')
                # resetting for next path
                self.xs, self.ys,self.xys= [],[],[]
            elif event.key=='c':
                print 'combining paths and creating region; key pressed %s'%(event.key)
                combmask = np.zeros(self.X.shape, 'bool')
                for i,pth in enumerate(self.paths):
                    op = self.pathOps[i]
                    msk = pth.contains_points(self.XY)
                    msk = msk.reshape(self.X.shape)
                    if op=='a':
                        combmask =np.logical_or(combmask,msk)
                    elif op=='e':
                        excludemask = np.logical_or(excludemask,msk)
                        
                    else:
                        raise ValueError
                tmp = self.regionMask.copy()
                rgm= np.logical_and(tmp,combmask)
                self.regionMask[...]=rgm
                self.disconnect()
            elif event.key=='d':
                print 'dropping paths and creating region; key pressed %s'%(event.key)
                excludemask = np.zeros(self.X.shape, 'bool')
                for i,pth in enumerate(self.paths):
                    op = self.pathOps[i]
                    msk = pth.contains_points(self.XY)
                    msk = msk.reshape(self.X.shape)
                    if op=='a':
                        pass
                    elif op=='e':
                        excludemask = np.logical_or(excludemask,msk)
                        
                    else:
                        raise ValueError
                tmp = self.regionMask.copy()        
                rgm= np.logical_and(tmp,np.logical_not(excludemask))
                self.regionMask[...]=rgm
                self.disconnect()
            else:
                print 'no function assocaited with key pressed %s'%(event.key)
        else:
            raise ValueError
        return


class newRD(regionDataPath):
    """example to inherit and develop for your specific needs"""
    pass
                        
if __name__=="__main__":                
    
    ################# frameMatrixPlot operations #########################
    ################### content from frameMatrixPlot unit test area ####
    expNo=20
    loadNo=0
    mode = 'microTop'
    
    print "# create and process fgrid instance"
    fgrid = frameMatrix.frameGrid(expNo, loadNo,mode, pthFG= "/home/can/meta/experiments/frameGridDefs.py")
    fgrid.dataBaseOp(frameMatrix.f)
    fgrid.setMPositions(0.0)
    fgrid.setoptRes(pathOptics="/home/can/meta/optics/configurations.py")
    
    print "# create bigc object"
    filenums = range(0,96)
    folder= "/home/can/MDM/MC/exp20/attnew/microLP11/"
    #
    fileOrderStr = [str(i) for i in filenums]
    sizeStr = [len(k) for k in fileOrderStr]
    sub = ''
    numStr = [sub[:(3-sizeStr[i])]+fileOrderStr[i] for i in range(len(fileOrderStr))]
    gridptfiles = [folder+'an'+ k +'.p' for k in numStr]
    #
    bigc = fmp.bigcMaker(gridptfiles,npy=True) # this requires npyMaker to be run beforehand
    
    print "# create the commonPlot object"
    cd = fmp.commonData(bigC=bigc, frameGridIns=fgrid,  imageDir='/home/can/imageCentral/')
    #cp.setDemandRegionLimits()
    cd.setPositionsStrainsOverFrames()
    cd.analyzeDataRegions()
    cd.setMasks()
    # get a strain component for plotting checks
    eybig = cd.contiguousDataMatrix('ey')
    exbig = cd.contiguousDataMatrix('ex')
    exybig = cd.contiguousDataMatrix('exy')
    wxybig = cd.contiguousDataMatrix('wxy')
    
    
    bigX,bigY = cd.contiguousCoordMatrix(typ = 'undef')
        
    #plt.figure()
    #extent = (cd.xlim.min(),cd.xlim.max(),cd.ylim.max(),cd.ylim.min())
    extent = None
    #   [x0,x1,y0,y1]
    _contourfpKwds = dict(data=eybig,x=bigX,y=bigY,origin='upper',
                        extent=extent,alpha=1,extend='both')
    leveling={'timesStd':2,'numLevels':8,'symm':True,'fixedrange':[-0.02,0.02]}
    nplot.dataplot(leveling,**_contourfpKwds)   

      
    #############################frameMatrixPlot ops over #################
    ######################################################################
    ######################################################################
    ################## regionalData work here ##################################
    
    #cls = regionData # select region
    cls = regionDataPath
    
    rdm = cls(bigX,bigY,{'wxy':wxybig,'eyy':eybig,'exx':exbig,'exy':exybig})
    # box work
    x0=1.05 ; x1=1.4
    y0=1.65 ; y1=2.5
    #
    rdm.getDataBox(x0,y0,x1,y1)
    
    #f, (ax1, ax2, ax3) = plt.subplots(3, 1)
    #rdm.plotHistAndCorrRegion(ax1,ax2)
    f,ax=plt.subplots()
    
    rdm.plotBox('eyy',leveling,_contourfpKwds,background='gray')
    
    #rdm.plotBox('exx',leveling,_contourfpKwds,background='gray')
    
    #rdm.plotHistPars(ax,['eyy','exx'],where='box',bins=2000,normed=1)
    
    ##
    # region work
    # inside the lines
    
    #linebuilder=rdm.getDataRegionPoints(ax=ax1)
    
    '''
    A = [1.07671687156,2.49209053405]
    B = [1.39600155859,2.49620323004]
    C = [1.39780351544,1.65210420847]
    D = [1.07069211617,1.6502885772]
    twoPointList = [A+B,C+D]
    '''
    _histPars = dict(pars = ['eyy','exx'],percent=True,fixedrange=[-10,10],bins=400)
    _areaPars = dict(leveling=leveling,_contourfpKwds=_contourfpKwds,var='eyy')
    rdm.plotHistAndCorrArea(where='box',_histPars=_histPars,_areaPars=_areaPars)
    rdm.plotHistAndCorrAreaM(where='box',_histPars=_histPars,_areaPars=_areaPars,areaVars=['eyy','wxy','exx'])
    #twoPointList=rdm.getDataRegionFromClickPoints(linebuilder)
    

    #rdm.plotHistPars(['eyy','exx'],where='region',bins=2000,normed=1)
    
    #rdm.plotRegion('wxy',leveling,_contourfpKwds,background=None)
    #

    #outside the same lines
    #
    #ops = ['>','>']
    #rdm.plotHistPars(['eyy','exx'],where='region',bins=2000,normed=1)
    #rdm.plotRegion('wxy',leveling,_contourfpKwds,background=None)
