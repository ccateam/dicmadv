import os, pickle, shutil
import tables
import numpy as np
import matplotlib.pyplot as plt
import skimage.io
import pickle

import skimage.transform as sktr
#import cv2 # used for very large files doesn't seem to be an issue anymore for
            #skimage.io if one  nullifies image limit
from PIL import Image # to nullify large file limit
from scimath.units.length import mm

import imp
import commonCSop
reload(commonCSop)
import boundaryDet as bdet; reload(bdet)


opim = skimage.io.imread
join = os.path.join
import logging
logger = logging.getLogger(__name__)

def limitProvider(numrow,numcol,xs, ys, Xloc, Yloc, mode="lefttop"):
    """
    numrow, numcol = frame matrix numrow, numcol
    Xloc, Yloc = large location top left corner matrix
    xs,ys= frame size
    """
    anlimlowx, anlimhix = np.zeros((numrow, numcol+1),dtype=int),np.zeros((numrow, numcol+1),dtype=int)          
    anlimlowy, anlimhiy = np.zeros((numrow+1, numcol),dtype=int),np.zeros((numrow+1, numcol),dtype=int)
    for i in range(numrow):
        for j in range(numcol):
            xc,yc = Xloc[i,j], Yloc[i,j]
            #
            anlimlowx[i,j] = xc #lowlim 
            anlimhix[i,j+1] = xc + xs #hilim
            #
            anlimlowy[i,j] = yc
            anlimhiy[i+1,j] = yc + ys
    if mode=="mid":
        xlim = (anlimlowx + anlimhix)/2
        ylim = (anlimlowy + anlimhiy)/2
    elif mode=="lefttop":
        xlim , ylim = anlimlowx , anlimlowy
    else:
        raise ValueError
    xlim[:,0], xlim[:,-1] = anlimlowx[:,0], anlimhix[:,-1]
    ylim[0,:], ylim[-1,:] = anlimlowy[0,:], anlimhiy[-1,:]
    #
    lowIndividualx = xlim[:,:-1] - anlimlowx[:,:-1]
    lowIndividualy = ylim[:-1,:] - anlimlowy[:-1,:]
    hiIndividualx = xlim[:,1:] - anlimhix[:,1:] + xs
    hiIndividualy = ylim[1:,:] - anlimhiy[1:,:] + ys
    return xlim, lowIndividualx, hiIndividualx, ylim, lowIndividualy, hiIndividualy
        
    

def get_imshow_Inputs(fgridIns,res,mod=False):
    """ 
    red = an integer for resolution reduction
    mod = True if you want to work on the routine file; False if not.
    #
    The things that a microstructure overlay needs with imshow are
        0) data
        1) extent coords (xmin, xmax, ymin, ymax)
        2)
    """
    if mod:
        data = skimage.io.imread(fgridIns.imageNames[(res,mod)])
    else:
        data = skimage.io.imread(fgridIns.imageNames[res])

        
    numPixY, numPixX = data.shape
    from scimath.units.length import mm
    mmPerPix = fgridIns.oResUnit/mm * res
    coords = np.array([0.,numPixX,0.,numPixY])
    # take care of corner shift
    xshift,yshift = fgridIns.topPixMerge/res
    # minus since the strain coords are at top left corner of the image
    coords[:2] -= xshift 
    coords[2:] -= yshift
    #turn it to mm
    coords *= mmPerPix 
    #
    return (data,coords)

def retNeighborIndex(I,J,direc, numcol, numrow, circle=1):
    """ I row index not x coord 
        J column index
        North is previous row same column
    """
    if direc=='N':
        i, j  = I-1*circle, J
    elif direc=='S':
        i, j  = I+1*circle, J
    elif direc=='E':
        i, j  = I, J+1*circle
    elif direc=='W':
        i, j  = I, J-1*circle
    #
    if i<0 or j<0 or i>=numrow or j>=numcol:
        return None
    else:
        return (i,j)

def row4fileName(fileName,tablePath=os.environ['DICDBPTH'],data=True):
    fid = tables.open_file(tablePath, mode='r')
    imTable = fid.root.exprGroup.exprDataTable    
    rows = [x.nrow for x in imTable.iterrows() if x['fileName']==fileName]
    if len(rows)==0:
        raise ValueError, "no result found for fileName %s"%(fileName) 
    elif len(rows)>1:
        raise ValueError, "no unique result; found rows are %s"%(rows,)
    if data:
        return (rows[0],imTable.read(rows[0],rows[0]+1))
    else:
        return rows[0]

def stagePos4fileName(fileName,tablePath=os.environ['DICDBPTH']):
    fid = tables.open_file(tablePath, mode='r')
    imTable = fid.root.exprGroup.exprDataTable    
    rows = [x.nrow for x in imTable.iterrows() if x['fileName']==fileName]
    stagePos= [x['stagePos'] for x in imTable.iterrows() if x.nrow in rows][0]
    return stagePos
    
        

def f(loadNo,expNo,mode='microTop',tablePath=os.environ['DICDBPTH']):
    #
    fid = tables.open_file(tablePath, mode='r')
    imTable = fid.root.exprGroup.exprDataTable    
    rows = [
    x.nrow for x in imTable.iterrows() if
    x['expNo']==expNo
    and x['imagingSet']==mode
    and x['loadPt']==loadNo
    and x['fileName'][-4:]=='.tif'
        ]
    gridNos =[x['gridNo'] for x in imTable.iterrows() if x.nrow in rows] 
    files = [x['fileName'] for x in imTable.iterrows() if x.nrow in rows]
    stagePositions = [x['stagePos'] for x in imTable.iterrows() if x.nrow in rows]
    optics = [x['optics'] for x in imTable.iterrows() if x.nrow in rows]
    fid.close()
    return files,gridNos,stagePositions,optics

class pairUp:
    def __init__(self,expNo, loadRef, loadDef,mode,databaseFunc,
                 pthFG= "/home/can/meta/experiments/frameGridDefs.py"):
        self.Ref = frameGrid(expNo, loadRef, mode, pthFG = pthFG)
        self.Def = frameGrid(expNo, loadDef, mode, pthFG = pthFG)
        self.Ref.dataBaseOp(databaseFunc)
        self.Def.dataBaseOp(databaseFunc)
        self.Ref.strMatrixFiles()
        self.Def.strMatrixFiles()
        self.Refm = self.Ref.filNameMatrix
        self.Defm = self.Def.filNameMatrix
        assert self.Refm.shape == self.Defm.shape
        return
    
    def retRegion(self, ylim="all" , xlim="all"):
        """
        ylim =  doublet of row start row end in the frame matrix
                
        xlim = same for columns...
        """
        if ylim == "all":
            r , d = self.Refm , self.Defm
        else:
            rows,rowf = ylim
            cols,colf = xlim
            r , d = self.Refm[rows:rowf+1 , cols:colf+1] , self.Defm[rows:rowf+1,cols:colf+1]
        return r,d
    
    def prepFolder(self, localDir,reftag="ref",deftag="def",
                   imageDir=os.environ["DICIMAGEPTH"],**kwds ):
        if not os.path.exists(localDir):
            os.mkdir(localDir)
        r,d = self.retRegion(**kwds)
        numrow,numcol = r.shape
        for i in range(numrow):
            for j in range(numcol):
                rsrc = join(imageDir,r[i][j])
                dsrc = join(imageDir,d[i][j])
                rdest = join(localDir,reftag+"%s_%s.tif"%(i,j))
                ddest = join(localDir,deftag+"%s_%s.tif"%(i,j))
                shutil.copy(rsrc,rdest)
                shutil.copy(dsrc,ddest)
        return
                
        
            
            
    

class frameGrid:
    """
    This class assumes that the grid matrix should be constructed in accordance with the
    current arrangment of the setup; where the camera is hanged opposite to the user and
    x-y machine coordinates are as they are.
    """
    def __init__(self, expNo,loadNo, mode,pthFG= "/home/can/meta/experiments/frameGridDefs.py"):
        fg = imp.load_source('fgs',pthFG)
        from fgs import frameGrids
        self.expNo=expNo
        self.loadNo = loadNo
        self.mode = mode
        self.default_identifer = "exp%s_load%s_%s"%(self.expNo,self.loadNo,self.mode)
        self.numcol, self.numrow = frameGrids[expNo][mode]
        self.gs = self.numcol * self.numrow # gs for gridsize
        return
    
    def dataBaseOp(self,f):
        _res = f(loadNo=self.loadNo,expNo=self.expNo,mode=self.mode)  
        self.files, self.gridNos, self.stagePositions,optics = _res
        if len(set(optics))==1: # need a uniqueness test over all the frames for used optics
            self.optics=set(optics).pop()
        return
    
    def strMatrixFiles(self):
        """
        This typically used to gather group other than the scanned region
        """
        a = np.array(self.files)
        b = np.zeros((self.numrow,self.numcol),dtype=a.dtype)
        for i in range(self.numrow):
            for j in range(self.numcol):
                b[i,j] = a[self.retGrid(i,j)]
        #
        self.filNameMatrix = b
        return
    
    def __repr__(self):
        return "numcol=%s numrow=%s exp=%s mode=%s"%(self.numcol, self.numrow, self.expNo,self.mode)
            
    def retGrid(self,I,J):
        """
        returns the gridNo as listed in the database for row I col J of the frameGrid matrix.
        """
        return self.gs - (I*self.numcol + J) -1
        
    def retIJ(self, gridNo):
        """ 
        given gridNo as in the database returns I and J locations of the frame grid matrix.
        The matrix is inverted so that image merger directions are consistent with native
        image coordinate systems at top left corner of each image.
        """
        a = self.gs - gridNo -1
        I =  a/self.numcol
        J =  a%self.numcol
        return (I, J)
    
    def setMPositions(self):
        """
        strictly acquires stagepositions and sets them to member Mx in mm
        """
        
        from scimath.units.length import mm
        stagePositions = np.vstack(self.stagePositions)
        self.Mx = np.zeros((self.numrow, self.numcol,2)) 
        for i in range(self.gs):
            I,J = self.retIJ(i)
            self.Mx[I,J,:]= -1.*stagePositions[i,:2] # multiplication by -1 is a CS conversion
        # take out the initial image coordinates to  physically place the CS
        # origin on the first image
        self.Mx -= self.Mx[0,0,:]
        self.MxUnit = self.Mx*mm
  
        return            
    
    def analyzeOverlaps(self, imageDir=os.environ["DICIMAGEPTH"],errLimit=0.3,
                        bandwidth=500):
        """
        bandwidth = the bandwidth of the overlap regions; a crucial parameter
                    for the analysis to function. Select it too large and the
                    overlapped material regions will be overwhelmed; select it
                    too small and not enough overlapped material. Check out
                    bandwidth output of bdet.patchAnalysis
        errLimit= the error limit for the correlation level as spit out by skimage
        function register_tranaslation. 0.4 or above is bad. 0.25 or below is
        good. It will raise exception if errLimit is exceeded.
        
        DOES:
        characterizes the overlaps of the images through FFT  using 
        bdet.patchAnalysis that uses register_translation.
        Produces
        self.HorizDx_pix: Horizontal Overlaps in the frame grid in pixels  
        
        """

        self.HorizEst = np.zeros((self.numrow,self.numcol-1))
        self.HorizDx_pix = np.zeros((self.numrow, self.numcol,2))
        for I2 in range(self.numrow):
            for J2 in range(self.numcol-1):
                f2 = self.retGrid( I2,J2)
                f1 =  self.retGrid(I2,J2+1)
                im1 , im2 = self.files[f1], self.files[f2]
                im1m   = opim(join(imageDir,im1))
                im2m = opim(join(imageDir,im2)) 
                _res = bdet.patchAnalysis(im1m,im2m, connection='left',
                                          bandwidth=bandwidth,orders=0)
                layers, choppedims, delta,bw = _res
                print "corrected and used bandwidth %s"%(bw,)
                err = layers[0].error[0][0]
                assert err<errLimit, "%s is too large for a good correlation"%(err,) 
                v,u = layers[0].shifts[0,0,:]
                self.HorizDx_pix[I2,J2+1,:] = u+delta[0] , v+delta[1]  
                umDist = (self.Mx[I2,J2+1,0]-self.Mx[I2,J2,0])*1000.
                self.HorizEst[I2,J2]= np.abs(umDist/(u+delta[0]))
        #
        self.VertEst = np.zeros((self.numrow-1,self.numcol))
        self.VertDx_pix = np.zeros((self.numrow, self.numcol,2))
        for I2 in range(self.numrow-1):
            for J2 in range(self.numcol):
                f2 = self.retGrid( I2,J2)
                f1 =  self.retGrid(I2+1,J2)
                im1 , im2 = self.files[f1], self.files[f2]
                im1m   = opim(join(imageDir,im1))
                im2m = opim(join(imageDir,im2)) 
                _res = bdet.patchAnalysis(im1m,im2m, connection='up',  ##!
                                          bandwidth=bandwidth,orders=0)
                layers, choppedims, delta,bw = _res
                err = layers[0].error[0][0]
                assert err<errLimit, "%s is too large for a good correlation"%(err,) 
                v,u = layers[0].shifts[0,0,:]
                self.VertDx_pix[I2+1,J2,:] = u+delta[0] , v+delta[1] 
                umDist = (self.Mx[I2+1,J2,1]-self.Mx[I2,J2,1])*1000.
                self.VertEst[I2,J2]= np.abs(umDist/(v+delta[1]))
       
        return 

    def setoptRes(self, pathOptics="/home/can/meta/optics/configurations.py"):
        """
        oRes =  optical resolution in mm one can overload with input oRes 
        Otherwise the value in the central dictionary will be used. 
        """
        print """optical resolution is not directly taken from pathOptics any 
                more. That is just used as a sanity check. Need to run 
                matchOverlaps beforehand."""
        from scimath.units.length import micrometer
        fo = imp.load_source('ops',pathOptics)
        from ops import optics
        #self.optics = optics
        hm,hs = self.HorizEst.mean(), self.HorizEst.std() 
        vm,vs =self.VertEst.mean(), self.VertEst.std()
        #
        oRes = (hm+vm)/2.
        print """carefully review \n %s+-%s for horizontal and \n %s+-%s for vertical
                 overlap directions. Setting oRes to their average %s"""%(hm,hs,vm,vs,oRes)
        
        valueOnRecord = optics[self.optics]['opticalResolution'] # in micrometer per pixel
        if oRes: # making sure you are not inputting something totally ridiculous
            assert np.abs((valueOnRecord-oRes)/oRes)*100.< 10.
        self.oRes = oRes
        self.oResUnit = self.oRes*micrometer
        return
    
    def _setTop(self):
        # during merger the top left corner of the first image is
        # shifted as needed 
        top = np.array([self.Dx_pix[...,0].min(),self.Dx_pix[...,1].min()])
        self.topPixMerge = -1.*top # now this is the pixel coordinate of top
                                    # left corner after image merger
        return
    def optimizedDx(self,maxlim=3,avlim=1):
        from scimath.units.length import mm
        res_Hpath = np.cumsum(self.HorizDx_pix,axis=1)
        res_Vpath = np.cumsum(self.VertDx_pix, axis=0)
        dumh,dumv = res_Hpath.copy(), res_Vpath.copy() 
        for i in range(self.numcol):
            res_Hpath[:,i,:] += dumv[:,0,:]
        #
        for j in range(self.numrow):
            res_Vpath[j,:,:] += dumh[0,:,:]
        dif = np.abs(res_Hpath-res_Vpath)
        self.res_Hpath = res_Hpath
        self.res_Vpath = res_Vpath
        print """along vertical and horizontal coordinate determination, 
               maximum difference is %s and average difference is %s"""%(dif.max(),dif.mean())
        assert dif.max() < maxlim, "If you are sure the merger is OK, increase maxlim to run."
        assert dif.mean() < avlim, "If you are sure the merger is OK, increase avlim to run."
        
        bestDx_pix = (0.5*(res_Hpath + res_Vpath)).astype(int) 
        self.Dx_pix = bestDx_pix
        #acquire a bit more accuracy by averageing, typically things are within
        #one pixels at this point
        self.Dxunit=bestDx_pix*self.oResUnit
        self.Dx = self.Dxunit/mm # since Dx is traditionally in mm. 
        self._setTop()
        return 
    
    def merge(self,imageDir=os.environ["DICIMAGEPTH"],useMachine=False):
        """This is a poor merge in terms of optimizing stitch quality but
        it will still show if merge coordinates are correct.
        canvas = whole image matrix. Take it. Save it and gimp it to see."""

        exampleIm = opim(join(imageDir,self.files[0]))
        im_nY,im_nX = exampleIm.shape
        if useMachine:
            Mx_pix = (self.MxUnit/self.oResUnit).astype(int)
            Xloc = Mx_pix[...,0].copy()
            Yloc = Mx_pix[...,1].copy()
        else:
            Xloc = self.Dx_pix[...,0].copy()
            Yloc = self.Dx_pix[...,1].copy()
        Xloc -= Xloc.min()
        Yloc -= Yloc.min()
        Xsize = Xloc.max() + im_nX
        Ysize = Yloc.max() + im_nY
        canvas = np.zeros((Ysize,Xsize),dtype=np.uint8)
        for I in range(self.numrow):
            for J in range(self.numcol):
                ii=self.retGrid(I,J)
                im = opim(join(imageDir,self.files[ii]))
                xc,yc = Xloc[I,J], Yloc[I,J]
                xe,ye = xc+im_nX, yc+im_nY
                canvas[yc:ye,xc:xe]=im
        return canvas
    
    def mergeB(self,imageDir=os.environ["DICIMAGEPTH"],useMachine=False,mode="lefttop"):
        """This is a poor merge in terms of optimizing stitch quality but
        it will still show if merge coordinates are correct.
        canvas = whole image matrix. Take it. Save it and gimp it to see."""

        exampleIm = opim(join(imageDir,self.files[0]))
        im_nY,im_nX = exampleIm.shape
        if useMachine:
            Mx_pix = (self.MxUnit/self.oResUnit).astype(int)
            Xloc = Mx_pix[...,0].copy()
            Yloc = Mx_pix[...,1].copy()
        else:
            Xloc = self.Dx_pix[...,0].copy()
            Yloc = self.Dx_pix[...,1].copy()
        Xloc -= Xloc.min()
        Yloc -= Yloc.min()
        Xsize = Xloc.max() + im_nX
        Ysize = Yloc.max() + im_nY
        canvas = np.zeros((Ysize,Xsize),dtype=np.uint8)
        xlim, lowIndx, hiIndx, ylim, lowIndy, hiIndy = limitProvider(
                numrow=self.numrow,numcol=self.numcol,xs=im_nX, ys=im_nY, 
                Xloc=Xloc, Yloc=Yloc, mode=mode)
        for I in range(self.numrow):
            for J in range(self.numcol):
                ii=self.retGrid(I,J)
                im = opim(join(imageDir,self.files[ii]))
                xc,yc = xlim[I,J], ylim[I,J]
                xe,ye = xlim[I,J+1], ylim[I+1,J]
                canvas[yc:ye,xc:xe]=im[ lowIndy[I,J]:hiIndy[I,J] , lowIndx[I,J]:hiIndx[I,J] ]
        return canvas
        
    def mergeExternal(self,externalData, imageDir=os.environ["DICIMAGEPTH"],
                      useMachine=False,mode="lefttop"):
        """
        externalData = data with shape [numGrid;numRow;numCol]
        A similar merger to merge but on external data that is exactly the
        size of the image field. If this a frame Grid over reference image set;
        the external data is a Lagrangian field."""

        exampleIm = opim(join(imageDir,self.files[0]))
        exampleData = externalData[0,...]
        im_nY,im_nX = exampleIm.shape
        dt_nY,dt_nX = exampleData.shape
        assert im_nY == dt_nY
        assert im_nX == dt_nX
        if useMachine:
            Mx_pix = (self.MxUnit/self.oResUnit).astype(int)
            Xloc = Mx_pix[...,0].copy()
            Yloc = Mx_pix[...,1].copy()
        else:
            Xloc = self.Dx_pix[...,0].copy()
            Yloc = self.Dx_pix[...,1].copy()
        Xloc -= Xloc.min()
        Yloc -= Yloc.min()
        Xsize = Xloc.max() + im_nX
        Ysize = Yloc.max() + im_nY
        canvas = np.zeros((Ysize,Xsize),dtype=externalData.dtype)
        xlim, lowIndx, hiIndx, ylim, lowIndy, hiIndy = limitProvider(
                numrow=self.numrow,numcol=self.numcol,xs=im_nX, ys=im_nY, 
                Xloc=Xloc, Yloc=Yloc, mode=mode)
        for I in range(self.numrow):
            for J in range(self.numcol):
                ii=self.retGrid(I,J)
                dt = externalData[ii,...]
                xc,yc = xlim[I,J], ylim[I,J]
                xe,ye = xlim[I,J+1], ylim[I+1,J]
                canvas[yc:ye,xc:xe] = dt[ lowIndy[I,J]:hiIndy[I,J] , lowIndx[I,J]:hiIndx[I,J] ]
        return canvas
    
     
    
    
    

class frameGrid_fiji(frameGrid):

    def _readOut(self,fil):
        """returns a DIC whose keys are file names and values are coordinates"""
        resultDict={}
        with open(fil) as fp:
            for cnt, line in enumerate(fp):
                try:
                    fil, coor = line.split("; ;")
                    coor = coor.strip()
                    exec "coor= np.array(" + coor + ")"
                    resultDict[fil] = coor
                    print fil, coor
                except ValueError:
                    pass
        return resultDict
    
    def _fuse(self, fijicoor, tile_overlap=20,regression_threshold=0.30,
              max_avg_displacement_threshold=2.5,absolute_displacement_threshold=3.50):
        import fijibin
        default_text='''run("Grid/Collection stitching", "type=[Grid: row-by-row] 
        order=[Left & Up] grid_size_x=%s grid_size_y=%s tile_overlap=%s 
        first_file_index_i=%s directory=%s file_names=0000{iiiii}.tif 
        output_textfile_name=%s fusion_method=[Linear Blending] 
        regression_threshold=%s max/avg_displacement_threshold=%s 
        absolute_displacement_threshold=%s compute_overlap 
        computation_parameters=[Save computation time (but use more RAM)] 
        image_output=[Write to disk] output_directory=%s");'''%(self.numcol, 
        self.numrow, tile_overlap, self.firstIndex,self.imageLocalDir, fijicoor+'.txt', 
        regression_threshold, max_avg_displacement_threshold,
        absolute_displacement_threshold,self.imageLocalDir)
        # clean the new lines that are introduced for readability
        runtxt = default_text.replace("\n","") 
        fijibin.macro.run(runtxt)    
        self.fijiResultsFil = join(self.imageLocalDir,fijicoor+".registered.txt")
        return

    
    def setLocal(self, imageLocalDir="default",imageDir=os.environ["DICIMAGEPTH"],
                 root="/home/can/MDM/frameConnections/",**kwds):
        """
        copies all images to the stated localDir as well as setting it to be
        self.imageLocalDir member of the instance
        """
        if imageLocalDir=="default":
            imageLocalDir = join(root,self.default_identifer)
        self.imageLocalDir = imageLocalDir
        if not os.path.isdir( self.imageLocalDir ): 
            os.makedirs( self.imageLocalDir )
        for fil in self.files:
            _from = join(imageDir,fil)
            _to   = join(self.imageLocalDir,fil)
            if not os.path.isfile(_to): # will not overwrite
                shutil.copy2(_from, _to)
        return
        
    
    def analyzeOverlaps(self, imageLocalDir="default",
                        imageDir=os.environ["DICIMAGEPTH"], fijicoor="coor",
                        ):
        """
        roundRes: True: fiji connection coordinates are rounded
                  False:fiji connection coordinates are directly cast as integer
                        ,i.e., truncated; though the first seems preferable
                        what Fiji does in image merger seem truncation
        
                                              
        DOES:
        characterizes the overlaps of the images through FFT  using 
        bdet.patchAnalysis that uses register_translation.
        Produces
        self.HorizDx_pix: Horizontal Overlaps in the frame grid in pixels  
        """
        firstFil = self.files[0]
        a = firstFil.split('.')[0]
        self.firstIndex = int(a) # a trick to get rid of initial zeros
        #
        self.setLocal(imageLocalDir, imageDir=imageDir)
        self._fuse(fijicoor=fijicoor)
        self.fijiResultsD = self._readOut(self.fijiResultsFil)
        self.fiji_Dx_pix = np.zeros((self.numrow, self.numcol,2)) 
        for I2 in range(self.numrow):
            for J2 in range(self.numcol):
                ii = self.retGrid(I2,J2)
                self.fiji_Dx_pix[I2,J2,:] = self.fijiResultsD[self.files[ii]]
                # self.fiji_Dx_pix is a float
        # the following are calculated for optical resolution estimate
        self.HorizEst = 1.e3 * np.diff(self.Mx[...,0],axis=1) / np.diff(self.fiji_Dx_pix[...,0],axis=1)
        self.VertEst = 1.e3 * np.diff(self.Mx[...,1],axis=0) / np.diff(self.fiji_Dx_pix[...,1],axis=0)
        
        return
    
    # setOptRes is kept as is
    
    def optimizedDx(self,roundRes=True):
        """
        roundRes: not only the metrically more true option but also appears to
                    be consistent with the merged image of Fiji.
        """
        if roundRes:
            self.Dx_pix = (np.round(self.fiji_Dx_pix)).astype(int)
        else: 
            self.Dx_pix = self.fiji_Dx_pix.astype(int)
        #
        self.Dxunit=self.Dx_pix.copy()*self.oResUnit
        self.Dx = self.Dxunit/mm # since Dx is traditionally in mm.
        self._setTop()
        return
    
    def storeImages(self, resolutionList=[2,4,8], mod=True):
        """
        mod: save twice so that one set of files can be used for modifications
             in software like gimp or imageJ.
        """
        def fileWork(filName,key,imMat):
            filPath = join(self.imageLocalDir,filName)
            self.imageNames[key] = filPath
            skimage.io.imsave(filPath,imMat)
            return
        #
        self.imageNames = {}
        comb = join(self.imageLocalDir,"img_t1_z1_c1")
        A = skimage.io.imread(comb)
        fileWork("comb_1.tif",1,A)
        if mod:     
            fileWork("comb_1_mod.tif",(1,"mod"),A)
        # 
        for res in resolutionList:
            reducedA = sktr.rescale(A,1.0/res,anti_aliasing=True,
                                    preserve_range=True).astype("uint8")
            name , modname = "comb_%s.tif"%(res,), "comb_%s_mod.tif"%(res,)
            fileWork(name,res,reducedA)
            if mod:     
                fileWork(modname,(res,"mod"),reducedA)
            #
        return
    
    def get_imshow_Inputs(self,res,mod=False):
        """ 
        red = an integer for resolution reduction
        mod = True if you want to work on the routine file; False if not.
        #
        The things that a microstructure overlay needs with imshow are
            0) data
            1) extent coords (xmin, xmax, ymin, ymax)
            2)
        """
        if mod:
            data = skimage.io.imread(self.imageNames[(res,mod)])
        else:
            data = skimage.io.imread(self.imageNames[res])

            
        numPixY, numPixX = data.shape
        from scimath.units.length import mm
        mmPerPix = self.oResUnit/mm * res
        coords = np.array([0.,numPixX,0.,numPixY])
        # take care of corner shift
        xshift,yshift = self.topPixMerge/res
        # minus since the strain coords are at top left corner of the image
        coords[:2] -= xshift 
        coords[2:] -= yshift
        #turn it to mm
        coords *= mmPerPix 
        #
        return (data,coords)
        
        
            

if __name__=="__main__":
    Image.MAX_IMAGE_PIXELS = None # to beat an operational limit in writing large files
    if 0:
        expNo=27
        loadNo=6
        mode = 'microTop'
        #files, gridNos, stagePositions,optics = f(loadNo=loadNo,expNo=expNo,mode=mode)  
        
        fgrid = frameGrid(expNo, loadNo,mode,pthFG= "/home/can/meta/experiments/frameGridDefs.py")
        fgrid.dataBaseOp(f)
        fgrid.setMPositions()
        fgrid.analyzeOverlaps(bandwidth=400)
        fgrid.setoptRes()
        fgrid.optimizedDx(avlim=2,maxlim=6)
        if 0: # persistence operation
            filName = "%s_%s_%s.fgp"%(mode,expNo,loadNo)
            pth = "/home/can/MDM/frameConnections"
            filName = os.path.join(pth,filName)
            pickle.dump(fgrid,open(filName,"w"))
    if 1:
        #expNo=13
        #loadNo=1
        #mode = 'NiTi'
        expNo=27
        loadNo=0
        mode = 'microTop'
        #files, gridNos, stagePositions,optics = f(loadNo=loadNo,expNo=expNo,mode=mode)  
        
        fgridfj = frameGrid_fiji(expNo, loadNo,mode,pthFG= "/home/can/meta/experiments/frameGridDefs.py")
        fgridfj.dataBaseOp(f)
        fgridfj.setMPositions()
        fgridfj.analyzeOverlaps(imageLocalDir = "default")
        fgridfj.setoptRes()
        fgridfj.optimizedDx()
        fgridfj.storeImages()
        if 0: # persistence operation # do it separately; pickle is fickle
            filName = "%s.fgp"%(fgridfj.default_identifer)
            pth = "/home/can/MDM/frameConnections"
            filName = os.path.join(pth,filName)
            pickle.dump(fgridfj,open(filName,"w"))
    
    #fgrid.setoptRes(oRes = 0.1985, pathOptics="/home/can/meta/optics/configurations.py")
    #
    #stagePositionsA = np.vstack(stagePositions)
    #stagePositionsNew = stagePositionsA#
    #
    #fgrid = frameGrid(expNo, loadNo,mode)
    #fgrid.dataBaseOp(f)
    #fgrid.setMPositions(0.0)
    
    #fgrid.setoptRes()
    #fgrid.refineRelativePositions()
    
    #a = fgrid.DxUnit/fgrid.oResUnit # frame locations in pixels.
    #
    #stagePositionsNew[:,:]=stagePositionsA[range(90,96)+range(84,90)+range(78,84)+range(72,78)+range(66,72)+range(60,66)+range(54,60)+range(48,54)+range(42,48)+range(36,42)+range(30,36)+range(24,30)+range(18,24)+range(12,18)+range(6,12)+range(0,6),:]
    #x , y , z = stagePositionsNew[:,0],stagePositionsNew[:,1],stagePositionsNew[:,2]
    #plt.figure()
    #plt.axis('equal')
    #plt.plot(x,y,'x')
