from scipy import *
import numpy as np
import multiprocessing as mp
import os, sys, shutil, pickle, pdb
import os.path as osp
import pylab
import skimage.io

import skimage.io as skio
from dicTools import *
import indRun ; reload(indRun)
import genericUtility.myFileTools as mft ; reload(mft)
import postProcess as pp ; reload(pp)
from PIL import Image, ImageDraw, ImageOps, ImageFont
import warnings
import logging
logger = logging.getLogger(__name__)

import imagePreprocess as improc

def copierWithFuncMod(src,dstdir,impreprocessFunc,impreprocessFuncKwds):
    """
    impreprocessFunc = takes path of an image file or multiple image array returns processed image matrix; e.g. takes 2452x2048x10, averages and returns 2452x2048.
    """
    basename = osp.basename(src)
    #extension = osp.splitext(src)[-1]
    prosName=osp.join(dstdir,basename)
    if os.path.samefile(src,prosName):
        raise ValueError, "Source and destination paths are equivalent; you are in danger of overwriting your rawdata images."
    exec 'impreprocessFunc = improc.%s'%(impreprocessFunc,)
    processedMat= impreprocessFunc(src,**impreprocessFuncKwds)
    # the following produces a file in tiff format. prosName can be *.npy. But that is no
    # problem for the DIC software at all. numpy.save with a the same name would also work.
    skimage.io.imsave(prosName,processedMat)
    return prosName


def sortwithYpriority(mat,xind=0,yind=1,retSorter=False):
    """
    excepts a mat
    sorts over x, y such that x changes fastest
    """
    x = mat[:,xind]
    y = mat[:,yind]
    sorter = np.argsort(x+y*1.e6)
    if retSorter:
        return sorter
    else:
        return mat[sorter,:]

def resizeInPlace(newsize, imPath,resizeKwds={}):
    im = Image.open(imPath)
    im2 = im.resize(newsize,**resizeKwds)
    # overwrite
    im2.save(imPath)
    return

def copier(src,dstdir,overwrite=1,resize=None,resizeKwds={},**kwds):
    """
    resize works on a copied file only as it should (the original file should be preserved.)
    if not None it should be doublet of numbers signifying the new size
    """
    prosName=osp.join(dstdir,osp.basename(src))
    if os.path.exists(prosName):
        if overwrite:
            logger.warn("Warning! %s exists. Overwriting!"%(prosName,))
        else:
            logger.warn("The file exists. Not overwriting. You might be analyzing an old picture.")
    if (not os.path.exists(prosName)) or overwrite==1:
        # assumes this overwrites
        if not shutil._samefile(src,prosName):
            shutil.copyfile(src,prosName)
    else:
        pass
    if resize != None:
        resizeInPlace(resize, prosName, resizeKwds = resizeKwds)
    return prosName


def remover(fname):
    if os.path.exists(fname):
        os.remove(fname)

def retCorners(xy,dxy):
    '''xy and dxy are doublet list or tuples signifying coords and delta
    coords'''
    return (tuple(array(xy)-array(dxy)), tuple(array(xy)+array(dxy)))


def gridPt(im, coords, pointSize, color='cyan'):
    draw = ImageDraw.Draw(im)
    #draw points
    coords=array(coords)
    N = coords.shape[0]
    for i in range(N):
        c = coords[i,:]
        dc = [pointSize,pointSize]
        x0,x1 = retCorners(c,dc)
        draw.ellipse([x0,x1],fill=color)
        if i==0: c0 = c
        if i==N-1: cN = c
    return (draw,c0,cN)

def gridLine(im, coords1, coords2,color='green'):
    draw = ImageDraw.Draw(im)
    coords1 = array(coords1)
    coords2 = array(coords2) 
    N = coords1.shape[0]
    for i in range(N):
        c1 = tuple(coords1[i,:])
        c2 = tuple(coords2[i,:])
        draw.line([c1,c2],fill=color)
    return draw

def gridText(im,coords,textList,font,color='red'):
    draw = ImageDraw.Draw(im)
    #draw points
    coords=array(coords)
    N = coords.shape[0]
    for i in range(N):
        c = coords[i,:]
        draw.text(c,textList[i] ,color,font=font)
    return draw


class defAnalysisGroup:
    def __init__(self, groupSize,basename,imageDir,runDir):
        self.groupSize= groupSize
        self.analyses=[]
        for i in range(groupSize):
            name = basename+'part_%s'%(i,)
            self.analyses.append(defAnalysis(name=name,imageDir=imageDir, runDir=runDir))
        return

    def __repr__(self,**kwds):
        ret=''
        for i, an in enumerate(self.analyses):
            ret += 'Analysis %s\n\n'%(i,)
            ret += an.__repr__(**kwds)
            ret += '\n\n\n'
        return ret
      
    def workOverAn(self,fn,*args,**kwds):
        for an in self.analyses:
            exec 'an.'+fn+'(*args,**kwds)'
        return

    def workIndividual(self,fn,argsList, kwdsList):
        """
        expects to be sent lists of groupSize both for args and kwds
        argsList= list of lists
        kwdsList = list of dictionaries
        """
        for i, an in enumerate(self.analyses):
            if argsList !=None:
                args = argsList[i]
            else:
                args = []
            #
            if kwdsList != None:
                kwds = kwdsList[i]
            else:
                kwds = {}
            #
            exec 'an.'+fn+'(*args,**kwds)'
        return

    def setUndefFiles(self,*args,**kwds):
        self.workOverAn('setUndefFiles',*args,**kwds)
        return 
    def setDefFiles(self,*args,**kwds):
        self.workOverAn('setDefFiles',*args,**kwds)
        return
    def createRunObjs(self,*args,**kwds):
        self.workOverAn('createRunObjs',*args,**kwds)
        return 
    def setppAttrForAll(self,*args,**kwds):
        self.workOverAn('setppAttrForAll',*args,**kwds)
        return
    def setnpAttr(self,*args,**kwds):
        self.workOverAn('setnpAttr',*args,**kwds)
        return
    def createAllFiles(self,*args,**kwds):
        self.workOverAn('createAllFiles',*args,**kwds)
        return
    def setppAttrForAll_ind(self,argsList,kwdsList):
        self.workIndividual('setppAttrForAll',argsList,kwdsList)
        return
    def setppAttrfromList_ind(self,argsList,kwdsList):
        self.workIndividual('setppAttrfromList',argsList,kwdsList)
        return
    def runAll(self,*args,**kwds):
        self.workOverAn('runAll',*args,**kwds)
        return
    def runAllN(self,copy=True,remove=False,numProc=1, **kwds):
        """
        makes the pool work over all self.analyses with all loads under them. 
        runAll makes multiple processes work per self.analyses member for all 
        loads under it. That means if number of loads is low, the cpus are 
        underutilized.  
        """
        if numProc>1:
            if copy:
                self.workOverAn('copyImageFiles',**kwds)
            self.createAllFiles(**kwds)
            maxCpu = mp.cpu_count()-1
            if numProc> maxCpu : raise ValueError,'too high numProc'
            pool = mp.Pool(processes = numProc)
            L = []
            for an in self.analyses:
                for runObj in an.runObjs:
                    L.append(runObj.ppFile)
            #
            pool.map(indRun.execdic, L)
            pool.close()
            if remove:
                self.workOverAn('removeImageFiles',**kwds)
        else:
            self.runAll(copy=copy,remove=remove,numProc=numProc,**kwds)
    
    def rawData(self,*args,**kwds):
        self.workOverAn('rawData',*args,**kwds)
        return
    def showGrid(self,*args,**kwds):
        self.workOverAn('showGrid',*args,**kwds)
        return
    def concatData(self):
        """
        concats the data of all members and sorts with Y priority. It may not be wise to pickle the object after doing this since matrix information will be stored twice.
        """
        bigmat = np.concatenate([ self.analyses[i].rawdatamatrices for i in range(self.groupSize) ], axis=1)
        sorter = sortwithYpriority(bigmat[0,...],xind=0,yind=1,retSorter=True)
        bigmat = bigmat[:,sorter,:]
        self.rawdatamatrices = bigmat
        return
    def pickleSelf(self,selfPickleName='groupAnalysis.p',*args,**kwds):
        fname = os.path.join(self.analyses[0].runDir, selfPickleName)
        self.pickledTo = fname
        pickle.dump(self,open(fname,'w'))
        return

    def runCheckStore(self,enforceStore=False,suppressShowProblem=False,
                      runMode=1,**kwds):
        if runMode==1:
            self.runAll(**kwds)
        elif runMode==2:
            self.runAllN(**kwds)
        self.rawData()
        self.concatData()
        #
        p = pp.postprocess(self.rawdatamatrices,an=self.analyses[0],**kwds)
        #an is sent only for deformation files, so this should do.
        pres =  p.checkStatus()
        print pres
        if pres != 'CLEAN MINIMIZATION':
	    if not suppressShowProblem:
	        p.showProblem(**kwds)
            if not enforceStore:
	        raise ValueError, 'The analysis did not yield clean minimization. Will not store.'
	    else:
	        logger.warn('Warning! The analysis did not yield clean minimization. Only storing because you want me to.')
	        self.pickleSelf(**kwds)
        else:
            self.pickleSelf(**kwds)
        return p

    def check(self,**kwds):
        p = pp.postprocess(self.rawdatamatrices, an = self.analyses[0], **kwds)
        return p.checkStatus()

    


class defAnalysis:
    def __init__(self,name,imageDir,runDir):
        '''This version takes npfile to be constant for the entire analysis.
        Can be easily changed; but don't see any physical reason for
        complication.'''
        self.name = name
        self.imageDir = imageDir
        self.runDir = runDir
        self.npfile = self.name + '.inp'
        self.runObjs=[]

    def __repr__(self,**kwds):
        ret=''
        for i, runObj in enumerate(self.runObjs):
            ret += 'Run object %s\n\n'%(i,)
            ret += runObj.__repr__(**kwds)
            ret += '\n\n\n'
        return ret
#
    def setUndefFiles(self, fList):
        self.undefFiles = []
        for f in fList:
            self.undefFiles.append(f)
        return
#    
    def setDefFiles(self,fList):
        self.defFiles = []
        for f in fList:
            self.defFiles.append(f)
        #weird place
        self.setppnames()
        return
#
    def copyImageFiles(self,impreprocessFunc=None,impreprocessFuncKwds={},**kwds):
        L = self.defFiles
        newL =[]
        for f in L:
            if impreprocessFunc == None:
                newpth = copier(osp.join(self.imageDir,f),self.runDir,**kwds)
            else:
                newpth = copierWithFuncMod(osp.join(self.imageDir,f),self.runDir,
                                  impreprocessFunc,impreprocessFuncKwds)
            newL.append(newpth)
        self.defFiles=newL
        #
        L = self.undefFiles
        newL =[]
        for f in L:
            if impreprocessFunc == None:
                newpth = copier(osp.join(self.imageDir,f),self.runDir,**kwds)
            else:
                newpth = copierWithFuncMod(osp.join(self.imageDir,f),self.runDir,
                                  impreprocessFunc,impreprocessFuncKwds)                
            newL.append(newpth)
        self.undefFiles=newL
        return
    
    def removeImageFiles(self):
        L = self.defFiles + self.undefFiles
        for f in L:
            remover(osp.join(self.runDir,f))
    def createRunObjs(self):
        i=0
        for ppf,df,udf in zip(self.ppnames,self.defFiles,self.undefFiles):
            self.runObjs.append(indRun.runDIC(ppFile=ppf , npFile=self.npfile,
                                           runDir= self.runDir))
            #set the image file parameter
            self.runObjs[i].pp.imFiles = [osp.basename(udf) , osp.basename(df)]
            i = i + 1    
            
            
    def setppnames(self):
        '''Sets self.ppnames and self.rawdatanames and self.resultsnames'''
        self.ppnames = []
        self.rawdatanames = []
        self.resultsnames = []
        for i in range(len(self.defFiles)):
            fname = self.name+'_'+`i+1`
            self.ppnames.append(fname+'.dat')
            self.rawdatanames.append(fname+'.rawdata')
            self.resultsnames.append(fname+'.results')

#

    def setppAttrfromList(self,name,List):
        '''Some attributes should better have independece per run...'''
        if len(List)!=len(self.runObjs):
            raise ValueError, 'The length of list %s is not equal to number of runs %s.'%(len(List),len(self.runObjs))
        for l,runObj in zip(List,self.runObjs):
            runObj.pp.__dict__[name] = l
        return
        
    def setppAttrForAll(self,**kwds):
        '''This is for convenience, if you want to set anything common in the pp
        part of the runObjs for all these guys, you can do it. Like setting all
        icoflag's to 2, etc.'''

        for key,value in kwds.items():
            for runObj in self.runObjs:
                runObj.pp.__dict__[key] = value
        return
    def setnpAttr(self,**kwds):
        for key,value in kwds.items():
            for runObj in self.runObjs:
                runObj.np.__dict__[key] = value
    def createAllFiles(self,**kwds):
        '''Create np once, create pp for all naturally.'''
        self.runObjs[0].createnpFile()
        # set the size of all images reliably; this will override any previous 
        # setting of pp.size ; it assumes all images are the same size
        checkfil = osp.join(self.imageDir, self.undefFiles[0])
        A = skio.imread(checkfil)
        ny,nx = A.shape
        self.setppAttrForAll(size=[nx,ny])
        for runObj in self.runObjs:
            # The following yields he obvious connection between image size and image file
            runObj.createppFile()
        return

    def autoShiftList(self,altNP,**kwds):
        """
        altNP =  a dictionary alternative numerical parameters
        does preliminary runs to get a good shiftList
        does not restore the parameters so you need to (re)run those.
        """
        # first isolate the first grid point
        for runObj in self.runObjs:
            runObj.pp.i0j0List = runObj.pp.i0j0List[:1]
            runObj.pp.npoint =1
        self.setnpAttr(**altNP)
        self.createAllFiles()
        p = self.runCheckStore(selfPickleName='dummy.p',**kwds)
        u = p.pickVar('u').flatten()
        v = p.pickVar('v').flatten()
        for i , runObj in enumerate(self.runObjs):
            runObj.pp.pORshift = [u[i],v[i]]        
        # now here goes
        return p

    def runAll(self,copy=True,remove=False,numProc=1,*args,**kwds):
	"""
        numProc=1, will run all jobs sequentially no attempt at parallel submission
        numProc>1, will attempt using multiprocessing pool process
        """
        
        if copy:
            self.copyImageFiles(**kwds)
        self.createAllFiles()
        # It is not way to expensive to create files even if it is unnecessary. The upside is
        # it will allow you to avoid the changes you make to the object attributes not
        # being reflected on the files and thus the runs.
        if numProc==1:
	    for runObj in self.runObjs:
                runObj.run()
        elif numProc > 1:
            maxCpu = mp.cpu_count()-1
            if numProc> maxCpu : raise ValueError,'too high numProc'
            pool = mp.Pool(processes = numProc)
            pool.map(indRun.execdic, [runObj.ppFile for runObj in self.runObjs]   )
            pool.close()
        else:
            raise ValueError, 'invalid numProc'

        if remove:
            self.removeImageFiles()
        return
################################get-data into the object###############################
    def fillMissingLines(self,matrix):
        """
        matrix = input result matrix
        creates the result columns based on initial grid points
        fills the existing points from matrix; leaves the other points with a
        status code 5.
        """
        nr,nc = matrix.shape
        i0j0List = self.runObjs[0].pp.i0j0List
        sz = len(i0j0List)
        newMat = np.zeros((sz,nc)) 
        newMat[:,:2] = array(i0j0List)
        newMat[:,-3] = 5.
        for i in range(nr):
            ind = where(logical_and(abs(newMat[:,0]-matrix[i,0])<1.e-12,abs(newMat[:,1]-matrix[i,1])<1.e-12) )
            if len(ind[0])>1: raise ValueError,'In analysis, fillingMissingLines, non-unique'
            if len(ind[0])==0: raise ValueError,'In analysis, fillingMissingLines, no answer'
            newMat[ind,:] = matrix[i,:]
        return newMat
            #if abs(matrix[i,0]-newMat[
            #newMat[]
            
        
    def rawData(self):
        self.rawdatamatrices=[]
        for f in self.rawdatanames:
            try:
                fid = open(f,'r')
            except IOError:
                raise IOError,'Could not open rawdata file %s'%(f,)
            matstr=''
            for i,line in enumerate(fid.readlines()):
                if i==0 or i==1:
                    continue
                if i==2:
                    titles=line
                else:
                    matstr = matstr + line
            #
            #
            ok=False
            uglySolutionList=[]
            i=0
            while not ok:
                i+=1
                try:
                    matrix = mft.getMatrix(matstr,uglySolutionList=uglySolutionList)
                    ok=True

                except ValueError as v:
                    if v.message[:35] == 'could not convert string to float: ':
                        uglySolutionList.append(v.message[35:])
                        
                    elif v.message[:29] == 'invalid literal for float(): ':
                        uglySolutionList.append(v.message[29:])
                    else:
                        print "matrix string that could not be converted is \n %s"%(matstr)
                        print "problem file name is %s"%(f,)
                        print v
                        raise ValueError
            #
            matrix = np.atleast_2d(matrix)
            newMat = self.fillMissingLines(matrix)
            #pdb.set_trace()
            if size(matrix,0) != len(self.runObjs[0].pp.i0j0List):
                
                newMat = self.fillMissingLines(matrix)
                print 'Warning! Rawdata Matrix not at proper size'
                #
            else:
                
                newMat= matrix
            #    
            
            matrix = sortwithYpriority(newMat,xind=0,yind=1)
            self.rawdatamatrices.append(matrix)
            
        self.rawdatamatrices = array(self.rawdatamatrices)
        
    # Get a version of this in generic files
    def check(self,**kwds):
        p = pp.postprocess(self.rawdatamatrices, an = self, **kwds)
        self.successRate = p.cleanMask.sum()/float(p.cleanMask.size)
        return p.checkStatus()

##########################################################################################
    def pickleSelf(self,selfPickleName='analysis.p',*args,**kwds):
        fname = os.path.join(self.runDir, selfPickleName)
        self.pickledTo = fname
        pickle.dump(self,open(fname,'w'))
        return

    def runCheckStore(self,enforceStore=False,suppressShowProblem=False,**kwds):
        self.runAll(**kwds)
        self.rawData()
        #keep this a temp variable here
        p = pp.postprocess(self.rawdatamatrices,an=self,**kwds)
        pres =  p.checkStatus()
        print pres
        if pres != 'CLEAN MINIMIZATION':
	    if not suppressShowProblem:
	        p.showProblem(**kwds)
            if not enforceStore:
	        raise ValueError, 'The analysis did not yield clean minimization. Will not store.'
	    else:
	        print 'Warning! The analysis did not yield clean minimization. Only storing because you want me to.'
	        self.pickleSelf(**kwds)
        else:
            self.pickleSelf(**kwds)
        return p
##################################################################################
    def showGrid(self,undefImageNo=0,pointSize=3,**kwds):
        sub = self.runObjs[0].np.sub
        nrt = self.runObjs[0].np.nrt
        nsb = self.runObjs[0].np.nsb
        coords = self.runObjs[0].pp.i0j0List
        #
        imFile = self.undefFiles[0]
        toret = os.getcwd()
        os.chdir(self.imageDir)
        im=Image.open(imFile)

        ### 
        im2= im.convert("RGB")
        draw,c0,cN = gridPt(im2, coords, pointSize, color='cyan')
        draw.rectangle(list(retCorners(c0,sub)),outline='cyan')
        #draw.rectangle(list(retCorners(cN,sub)),outline='cyan')
        ##
        allowance = array(sub) + array([2,2]) + array(nsb)
        draw.rectangle(list(retCorners(c0,allowance)),outline='red')
        #leftBot = retCorners(cN,allowance)[1]
        #draw.rectangle([rightTop,leftBot],outline = 'cyan')
        #im2.show()
        pylab.imshow(im2,aspect='equal',origin='lower')
        os.chdir(toret)
        return
          
    
