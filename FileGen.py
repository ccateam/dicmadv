from dicTools import *
import os
import logging
logger = logging.getLogger(__name__)

import skimage.io as skio
#
class fileGen:
    parList=[]
    def check(self):
        notDefList=[]
        for p in self.parList:
            if p not in self.__dict__:
                notDefList.append(p)
        return notDefList
    def checkRaise(self):            
        notDef = self.check()
        if notDef: #if the notDef list is not empty as it should be
            raise ValueError, 'The following variables are not defined: %s'%(notDef)
    def createStr(self):
        self.checkRaise()
        fileFormat = self.setFileFormat()
        retstr = varToFileStr(fileFormat)
        return retstr
    def createFile(self):
        fid = open(self.fname,'w')
        fid.write(self.createStr())
        fid.close()
    def __repr__(self,lim=600):
        ret = ''
        for p in self.parList:
            exec 'val=self.'+p
            val=str(val)
            if len(val)<lim:
                ret += p +'= '+ val + '\n'
            else:
                ret += p +'= '+ val[:lim] + '....\n'
        return ret

class physicalPar(fileGen):
    def __init__(self,fname):
        '''size is [nsize,msize], imFiles is [undfile,deffile]'''
        self.fname = fname
        self.parList=['icoflag','size','imFiles',
             'inpfile','pORshift','npoint','k0','i0j0List']
    #### I can put a whole bunch of checks for the above, for example icoflag
    #### should be 1, 2 or 3.
        self.setDefaults()
    def setDefaults(self):
        self.icoflag = 2
        self.size = [640,480]
        self.k0=10000 #just a very big number so that I don't deal with it.
    def setFileFormat(self):
        fileFormat = [[self.icoflag,'\n'],[self.size,'\n','  '],
                           [self.imFiles,'\n','\n'],[self.inpfile,'\n'],
                           [self.pORshift,'\n',' '],[self.npoint,' '],
                           [self.k0,'\n']]
        #To avoid more complication in listToString
        for i,i0j0 in enumerate(self.i0j0List):
            fileFormat.append([self.i0j0List[i],'\n',' '])
        #print fileFormat
        return fileFormat

        

class numericalPar(fileGen):
    def __init__(self,fname,extraVar=True):
        '''sub=[nsub,msub] (defines subset region to work on),
        nrt=[nrtx,nrty](coarse opt region),
        nrtinc=[nrtincx,nrtincy] (coarse opt inc.),
        nconf=[nconfx,nconfy] (coarse opt div.),
        nsb=[nsbx,nsby] (def interp. margin on top of sub,
        WATCH OUT nsb SHOULD BE BIG ENOUGH)
        scal=[xscal,yscal,zscal];
        verifyImageData and LSFplane may not be around at all.'''
        
        self.fname=fname
        self.parList=['sub','nrt','nrtinc','nacc','nconf','nsb','ftol','itermax',
                      'imethod_flag','scal','unit','ifile_flag']
        self.extraVar = extraVar
        if extraVar:
            self.addNewPars()
        self.setDefaults()
    def addNewPars(self):
        '''These two dudes do not seem to be there in the original software; so
        I'll add them by modularization. Turn the running of this off if necessary.'''
        self.parList = self.parList + ['verifyImageData','LSFplane']
    def setDefaults(self):
        '''sub, nrt,nsb has no defaults.'''
        self.nrtinc = [1,1]
        self.nacc = 1
        self.nconf = [2,2]
        self.ftol = 0.00001
        self.itermax = 200
        self.imethod_flag = 1
        self.scal = [1,1,1]
        self.unit = 'Pixel'
        self.ifile_flag = 2
        if self.extraVar:
            self.verifyImageData = 0
            self.LSFplane = 0
    def setFileFormat(self):
        fileFormat = [[self.sub,'\n',' '],[self.nrt,'\n',' '],[self.nrtinc,'\n',' '],
                           [self.nacc,'\n'],[self.nconf,'\n',' '],[self.nsb,'\n',' '],
                           [self.ftol,'\n'],[self.itermax,'\n'],[self.imethod_flag,'\n'],
                           [self.scal,'\n',' '],[self.unit,'\n'],[self.ifile_flag,'\n']]
        if self.extraVar:
            fileFormat = fileFormat + [[self.verifyImageData,'\n'],
                                                 [self.LSFplane,'\n']]
        return fileFormat
    
        
        
