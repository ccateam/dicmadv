from scipy import interpolate
import numpy as np
import sys,pickle,pylab,os,glob
import matplotlib.pyplot as plt
from PIL import Image, ImageChops
import os.path as osp

def namenum2fname(basePath,baseName,num,ext='',numZeros=4):
    """ basePath: folder
        baseName: name before zeros"""
    snum = `num`
    lzs =numZeros-len(snum)
    zeros='0'*numZeros
    
    name = baseName + zeros[:lzs]+snum+ext
    # os.path.join works platform independently
    return os.path.join(basePath,name)
    
def trim(im, border=(255,255,255)):
    bg = Image.new(im.mode, im.size, border)
    diff = ImageChops.difference(im, bg)
    bbox = diff.getbbox()
    if bbox:
        return im.crop(bbox)
    else:
        raise ValueError()

def savefig(fname, mergeObject=None,**kwds):
    """This is intended to supplant plt.savefig. It functions exactly as plt.savefig if no mergeObject is sent. But if it is sent, the figure is also pushed into that mergeObjects list."""
    if mergeObject:
        plt.savefig(fname,**kwds)
        mergeObject.push(**kwds)
    else:
        plt.savefig(fname,**kwds)
    return 

        

class merge:
    def __init__(self, directory,ext='.png'):
        if os.path.exists(directory):
            raise ValueError, "The directory should not exist."
        else:
            os.mkdir(directory)
            self.pathDir=directory
        self.ext = ext
        self.names = []

    def standartNaming(self,num):
        """ hardwired 6 zero naming to the data image files """
        # to do : should I straight away convert to .tif
        return namenum2fname(self.pathDir,'', num, ext=self.ext, numZeros=6)
    
    def detectLatest(self):
        globstr = osp.join(self.pathDir, '*'+ self.ext)
        L = glob.glob(globstr)
        intL = [np.int32(osp.splitext( osp.basename(pth) ) [0]) for pth in L]
        if len(intL)==0:
            return 0
        else:
            return np.array(intL).max()
        
    def pathInRow(self):
        num = self.detectLatest() + 1
        pth = self.standartNaming(num)
        #be careful with the returned path, check if it exists and so on.
        return (pth,num)
    
    def push(self,**kwds):
        pth,num = self.pathInRow()
        plt.savefig(pth,**kwds)
        self.names.append(pth)
    
    def pickleSelf(self,name='imageEgg.p'):
        fname = osp.join(self.pathDir,name)
        pickle.dump(self,open(fname,'w'))
        return fname
    
    
    def combineFig(self,numRow, numCol, processDCombine=None, outPic='big.png',**kwds):
        """assumes equal image sizes"""
        numPics=len(self.names)
        for i, name in enumerate(self.names):
            if i==numPics: break
            im = Image.open(name)
            if processDCombine:
                im2 = processDCombine(im,**kwds)
            else:
                im2=im
            szx,szy = im2.size
            if i==0: IM = Image.new("RGB",(szx*numCol,szy*numRow))
            II = i / numCol
            JJ = i % numCol
            IM.paste(im2,(JJ*szx,II*szy))
        #
        IM.save(os.path.join(self.pathDir,outPic))
        return 
        
if __name__=="__main__":
    
    direc='/home/can/MDM/temp/egg3/'
    trymerge = merge(directory=direc)
    x= np.arange(10.)
    a= np.arange(10.)
    ys = np.outer(x,a)
    for i in range(10):
        plt.plot(x, ys[i,:])
        trymerge.push()
        plt.close()
    #
    trymerge.pickleSelf()
    A =pickle.load(open(osp.join(direc,'imageEgg.p')))
    A.combineFig(4,2)
            
            
    
        
