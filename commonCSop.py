import numpy as np
from scipy import misc
import analysis 
reload(analysis)
import postProcess as pp
reload(pp)
import debugTools as dt
import warnings
import logging
logger = logging.getLogger(__name__)
reload(dt)


def watchForOutlier(u,numOrigPoints,fractionSuccess=0.66,expectedMaxError=1.):
    """
    specialized to the expectation of a constant displacement field
    
    fractionSuccess = between fraction of data points required to call the 
                        determination succesful
    expectedMaxError = a measure of expected DIC error among points
    
    """
    def outlierExists(u,expectedMaxError):
        """
        makes use of the difference between average and median
        """
        N=u.size
        av,med,std = np.average(u),np.median(u),np.std(u)
        if np.abs(av-med) < expectedMaxError/((N-1)**0.5):
            return 0
        else:
            return 1
    def dropOutlier(u):
        """ drop the maximum distance point(s) from average"""
        av = np.average(u)
        absdistance = np.abs(u-av)
        abs=np.max(absdistance)
        return u[absdistance != absdistance.max()]
    
    while u.size > float(numOrigPoints)*fractionSuccess:
        if outlierExists(u,expectedMaxError):
            u= dropOutlier(u)
        else:
            return np.average(u)
            
        raise ValueError,"lost too many points"
        
    av,med,std = np.average(u),np.median(u),np.std(u)
    logger.debug("av=%s, med=%s, std=%s"%(av,med,std))
    N=float(len(u))
    if np.abs(av-med) < expectedMaxError/((N-1)**0.5):
        res = av 
    else:
        
        raise ValueError,str(u)
    return res
    

def cob2d(x,y,deltax,deltay,theta):
    """
    DOES:
    stands for change of basis in 2d
    yields the coordinates of a point in a new cs
    when its coordinates in the old cs and the relation between
    old and new  cs are provided.
    new cs is in general translated and rotated with respected to the
    old cs.
    TAKES:
    x,y = coordinates in the old cs
    deltax, deltay = new cs origin coordinates measured in the old cs
    theta = in radians! rotation of the new cs wrt the old cs, defined positive from x to y (ccw).
    RETURNS:
    xnew, ynew= coords in new cs
    """
    xnew = (x-deltax)*np.cos(theta) + (y-deltay)*np.sin(theta)
    ynew = -1.*(x-deltax)*np.sin(theta) + (y-deltay)*np.cos(theta)
    return (xnew, ynew)

def cob2drev(xnew,ynew,deltax,deltay,theta):
    """
    DOES:
    the opposite (reverse) operation of cob2d
    TAKES:
    xnew, ynew= coords in new cs
    deltax, deltay = new cs origin coordinates measured in the old cs
    theta = in radians! rotation of the new cs wrt the old cs, defined positive from x to y (ccw).
    RETURNS:
    x,y = coordinates in the old cs
    """
    x = deltax + xnew*np.cos(theta) - ynew*np.sin(theta)
    y = deltay + xnew*np.sin(theta) + ynew*np.cos(theta)
    return (x, y)
    
def csTranslation(xnew,ynew,x,y,theta):
    """
    calculates the translation amount of two CS; the second rotated by theta wrt the first.
    This is just a juggled form of cob2d.
    xnew,ynew = coords in new cs
    x, y     = coords in old cs
    theta    = rotation of new cs wrt old cs, ccw +
    RETURNS
    deltax, deltay = new cs origin coordinates measured in the old cs
    """
    deltax = x - xnew*np.cos(theta) + ynew*np.sin(theta)
    deltay = y - xnew*np.sin(theta) - ynew*np.cos(theta)
    return (deltax,deltay)




def findRelFrameCoords(deltax2_com,deltay2_com,theta, deltax1_com=0., deltay1_com=0., 
                        frameSize=(2452,2054), searchPars={},numMaxIter=20, 
                        interpolation_points=[0.5],processBy=watchForOutlier,visual=True,fixtheta=False,**kwds):
    """
    There are 3 CS. One is the commonCS. The second is the first image CS whose top left corner is at deltax1_com, deltay1_com measured 
    in the common CS. If deltax1_com=0., deltay1_com=0. the common CS coincides with the top left corner of the first image. 
    Both image CS are rotated by theta wrt common CS. The third CS is the second image's native CS. 
    Physically, rotation by common theta in all images happens when the CCD is not square to the axes of the positioning device.
    TAKES:
    deltax1_com, deltay1_com = image 1 (ref image) origin coordinates in the common CS; no guess this is taken as known.   
    deltax2_com, deltay2_com =  guess (start point) image 2 origin coordinates in the the commonCS; provided in PIXELS 
    theta  =  guess angular misalignment of the CCD provided in RADIANS
    searchPars = all side parameters used in the search algorithm, should be provided in the form of a dictionary
    processBy = function that takes possibly multiple points of analyisis and yields the effective displacement to use.
    """
    def u_v_theta_noiter(deltax2_com,deltay2_com,theta,searchPars,interpolation_points,method,*args):
        logger.debug("input to u_v_theta_noiter\n deltax2_com =  %s ; deltay2_com = %s   ; theta =  %s"%(deltax2_com,deltay2_com,theta))
        # coordinates of the top left corner of the second image in the native CS
        # of the first image
        if method==0:
            revert = False
            icoflag =1
        elif method==1:
            revert = True
            icoflag=1
        elif method==2:
            revert = False
            icoflag= 2
        dx2_1, dy2_1 = cob2d(deltax2_com,deltay2_com,deltax1_com,deltay1_com,theta)
        # stands for overlapCenterx_1, overlapCentery_1; this is the healthiest search area for aligning these frames
        i0j0L =[]
        ocxList_1,ocyList_1, ocxList_2,ocyList_2, ocxList_com, ocyList_com=[], [],[],[],[],[]
        N = len(interpolation_points)
        if revert:
            takeInitial=N-1
        else:
            takeInitial=0
        #
        for i,ip in enumerate(interpolation_points):
            a,b = ip, 1.-ip
            ocx_1 , ocy_1  = a * fx   +   b * dx2_1   ,   a * fy   +   b * dy2_1
            #
            ocx_com , ocy_com = cob2drev(ocx_1,ocy_1,deltax1_com, deltay1_com,theta)
            ocx_2 , ocy_2 = cob2d(ocx_com, ocy_com, deltax2_com, deltay2_com, theta)
            # assumes common theta, also should be consistent with atan(vguess, uguess)
            # at this point (debug pointer)
            if i==takeInitial: # since this will be processed in Fortran DIC engine first
                uguess =  ocx_2 - ocx_1
                #vguess =  -1. * (ocy_2 - ocy_1) # !!another saddle diff between image CS and DIC CS. positive v is motion upward in DIC.                            
                vguess = ocy_2 - ocy_1
                thetaguess = theta#n
            i0j0L.append((np.round(ocx_1).astype(int),np.round(ocy_1).astype(int)))
            
            #
        if revert:
            i0j0L.reverse()
            
        #print ocx_1, ocy_1
        logging.debug(i0j0L)         
        searchPars['ppPars']['i0j0List'] = i0j0L
        searchPars['ppPars']['npoint'] = len(searchPars['ppPars']['i0j0List'])
        searchPars['ppPars']['pORshift'] = [ np.round(uguess).astype(int) , np.round(vguess).astype(int)-30 ] 
        searchPars['ppPars']['icoflag'] = icoflag
        #DIC analysis
        u, v = unitDIC(**searchPars)    
        logger.info("u=%s, v=%s"%(u,v))
        logger.warning('len(u):%s'%( u.size ) )
        umed, vmed = processBy(u,N),processBy(v,N)
        if 0: 
            theta = np.arctan((sinbeta*umed-cosbeta*vmed) / (sinbeta*vmed+cosbeta*umed))
            #theta = np.arctan(-vmed/umed) only true for left-right frames. 
            deltarev = np.linalg.norm([umed,vmed])
            deltax2rev, deltay2rev = deltarev*cosbeta, deltarev*sinbeta
        if 1:
            deltax2rev, deltay2rev = umed, vmed
            
            
        return uguess, vguess, thetaguess, umed,vmed,theta, deltax2rev,deltay2rev
        
    def u_v_theta_iter(deltax2_com,deltay2_com,theta,searchPars,interpolation_points,fixtheta):
        # coordinates of the top left corner of the second image in the native CS
        # of the first image
        dx2_1, dy2_1 = cob2d(deltax2_com,deltay2_com,deltax1_com,deltay1_com,theta)
        # stands for overlapCenterx_1, overlapCentery_1; this is the healthiest search area for aligning these frames
        i0j0L =[]
        ocxList_1,ocyList_1, ocxList_2,ocyList_2, ocxList_com, ocyList_com=[], [],[],[],[],[]
        
        for i,ip in enumerate(interpolation_points):
            a,b = ip, 1.-ip
            ocx_1 , ocy_1  = a * fx   +   b * dx2_1   ,   a * fy   +   b * dy2_1
            #
            ocx_com , ocy_com = cob2drev(ocx_1,ocy_1,deltax1_com, deltay1_com,theta)
            ocx_2 , ocy_2 = cob2d(ocx_com, ocy_com, deltax2_com, deltay2_com, theta)
            # assumes common theta, also should be consistent with atan(vguess, uguess)
            # at this point (debug pointer)
            if i==0: # since this will be processed in Fortran DIC engine first
                uguess =  ocx_2 - ocx_1
                #vguess =  -1. * (ocy_2 - ocy_1) # !!another saddle diff between image CS and DIC CS. positive v is motion upward in DIC.                            
                vguess = ocy_2 - ocy_1
                thetaguess = theta#n
            i0j0L.append((np.round(ocx_1).astype(int),np.round(ocy_1).astype(int)))
            ocxList_1.append(ocx_1),ocyList_1.append(ocy_1), ocxList_2.append(ocx_2)
            ocyList_2.append(ocy_2),ocxList_com.append(ocx_com), ocyList_com.append(ocy_com)
            #
        #print ocx_1, ocy_1
        logging.debug(i0j0L)         
        searchPars['ppPars']['i0j0List'] = i0j0L
        searchPars['ppPars']['npoint'] = len(searchPars['ppPars']['i0j0List'])
        searchPars['ppPars']['pORshift'] = [ np.round(uguess).astype(int) , np.round(vguess).astype(int) ] 
        #DIC analysis
        u, v = unitDIC(**searchPars)
        logger.info("u=%s, v=%s"%(u,v))
        umed, vmed = processBy(u),processBy(v)
        if not fixtheta:
            theta = np.arctan((sinbeta*umed-cosbeta*vmed) / (sinbeta*vmed+cosbeta*umed))
        #theta = np.arctan(-vmed/umed) only true for left-right frames. 
        ocx2_rev, ocy2_rev = np.array(ocx_1)+ umed , np.array(ocy_1) + vmed
        # applying csTranslation to individual subsets and then averaging that
        # would look more natural then the below; however the operation is linear
        # I don't expect any difference. 
        deltax2rev, deltay2rev = csTranslation(np.average(ocx2_rev), 
                                np.average(ocy2_rev),np.average(ocx_com),
                                np.average(ocy_com),  theta)
        return uguess, vguess, thetaguess, umed,vmed,theta, deltax2rev,deltay2rev
    
    def visualconf(u,v,searchPars,plot='all'):
        """if plot= i, i integer ; it will show the ith subset in the i0j0List, 
        if plot= 'all', will show all.""" 
        fil1 = searchPars['undefFil']
        fil2 = searchPars['defFil']
        sub=searchPars['nppars']['sub']
        locs = searchPars['ppPars']['i0j0List']
        if plot=='all':    
            for loc in locs:
                uv = (u,v)
                dt.plotSourceTarget(fil1,fil2,loc=loc,uv=uv,sub=sub,uv2=None)
        else:
            loc=locs[plot]
            dt.plotSourceTarget(fil1,fil2,loc=loc,uv=uv,sub=sub,uv2=None)
        return
        
        
              
    def pcerr(anew,a):  
        if a==0.:
            return np.nan
        else:
            return np.abs((anew-a)/a*100.)
    def err(anew,a):return np.abs(anew-a)

            
    def errpass(dxerr,dyerr, thetaerr, dxLim=0.4, thetaLim=1.e-5,**kwds):
        """setting acceptable errors in pixels with dxLim and thetaLim"""
        if dxerr<dxLim and dyerr<dxLim and thetaerr<thetaLim:
            return True
        else:
            if dxerr>dxLim: logger.info("dxerr=%s, dxlim=%s"%(dxerr,dxLim))
            if dyerr>dxLim: logger.info("dyerr=%s, dylim=%s"%(dyerr,dxLim))
            if thetaerr>thetaLim: logger.info("thetaerr=%s, thetalim=%s"%(thetaerr,thetaLim))
            return False
        
    #
    print "\n\n\n\n\ mike \n\n\n\n\n"
    # in the code _com, _1, and _2 denote the three CS. x is horizontal on images (column counter on image matrix)
    fx,fy = frameSize
    # ddx and ddy have a very important function of assigning the stage motion angle. 
    ddx , ddy = deltax2_com - deltax1_com, deltay2_com - deltay1_com 
    dd = np.linalg.norm([ddx,ddy])
    sinbeta, cosbeta = ddy/dd, ddx/dd
    if 1:
        deltax2_com_old,deltay2_com_old = deltax2_com,deltay2_com
        try:
            problem=0
            method=0
            ug, vg, thetag, u, v, theta, deltax2_com, deltay2_com = u_v_theta_noiter(deltax2_com,deltay2_com,theta,searchPars,interpolation_points=interpolation_points,method=method)        
            logger.info("guesses: %s,%s"%(ug,vg))
        except ValueError: # it is best to replace this with a custom Exception
            problem=1
            try:
                method = 1
                logger.warning("Things did not work. Reverting DIC grid to see if that works with icoflag 1")
                ug, vg, thetag, u, v, theta, deltax2_com, deltay2_com = u_v_theta_noiter(deltax2_com,deltay2_com,theta,searchPars,interpolation_points=interpolation_points,method=method)                    
                logger.info("guesses: %s,%s"%(ug,vg))
            except ValueError:
                method = 2
                logger.warning("Things did not work. Trying with icoflag 2")
                ug, vg, thetag, u, v, theta, deltax2_com, deltay2_com = u_v_theta_noiter(deltax2_com,deltay2_com,theta,searchPars,interpolation_points=interpolation_points,method=method)
                logger.info("guesses: %s,%s"%(ug,vg))
        finally:
            if problem:
                visualconf(u,v,searchPars)
            
        logger.info("\ndeltax2_com=%s, deltax2_com_old=%s,\ndeltay2_com=%s, deltay2_com_old=%s, \ntheta=%s"%(deltax2_com,
                            deltax2_com_old,deltay2_com,deltay2_com_old,theta))
    if 0:
        for i in range(numMaxIter):
            logger.debug("\n\nITERATION %s"%(i,))
            deltax2_com_old,deltay2_com_old = deltax2_com,deltay2_com
            ug, vg, thetag, u, v, theta, deltax2_com, deltay2_com = u_v_theta_iter(deltax2_com,deltay2_com,theta,searchPars,interpolation_points=interpolation_points,fixtheta=fixtheta)
            uerrp,verrp,thetaerrp, dxerrp, dyerrp = pcerr(u,ug), pcerr(v,vg),pcerr(theta,thetag), pcerr(deltax2_com,deltax2_com_old), pcerr(deltay2_com,deltay2_com_old)
            logger.debug("ug=%s,vg=%s, \nu=%s, v=%s"%(ug,vg, u, v))#, deltax2_com, deltay2_com
            #print uerrp,verrp,thetaerrp, dxerrp, dyerrp 
            logger.info("\ndeltax2_com=%s, deltax2_com_old=%s,\ndeltay2_com=%s, deltay2_com_old=%s, \ntheta=%s"%(deltax2_com,
                            deltax2_com_old,deltay2_com,deltay2_com_old,theta))
            dxerr,dyerr, thetaerr = err(deltax2_com,deltax2_com_old), err(deltay2_com,deltay2_com_old), err(theta, thetag)
    
            if errpass(dxerr,dyerr,thetaerr,**kwds): 
                print "solved in %s iterations"%(i)
                if visual: visualconf(u,v,searchPars)
                break
            if i==(numMaxIter-1): 
                visualconf(u,v,searchPars)
                raise ValueError, "No solution"
        
    
                
    return deltax2_com, deltay2_com, theta


def unitDIC(**searchPars):
    """ a very lean DIC application that looks for few (or just one) subsets"""
    an = analysis.defAnalysis('simple', imageDir=searchPars['imageDir'], runDir=searchPars['runDir'])
    an.setUndefFiles([searchPars['undefFil'],])
    an.setDefFiles([searchPars['defFil'],])
    an.createRunObjs()
    ppPars, nppars = searchPars['ppPars'], searchPars['nppars']
    an.setppAttrForAll(**ppPars)
    an.setnpAttr(**nppars)
    an.createAllFiles()
    an.runAll(overwrite=False)
    an.rawData()
    p =  pp.postprocess(an.rawdatamatrices, an = an,colDict = {'X':0,'Y':1,'u':2,'v':3,'ex':4,'ey':5,
    'exy':6,'dudx':7,'dvdy':8,'dudy':9,'dvdx':10,'Theta':11,'w':12,'status':13,
    'iter':14, 'corrcoeff':15}  )
    cm = p.cleanMask
    if not p.cleanMask.all():
        logging.warning(p.checkStatus())
        logging.warning("There is at least one nonconverging point %s"%(p.cleanMask))
        #makes it cry if all points did not converge
    K = an.rawdatamatrices
    #print K
    u,v = K[:,:,2], K[:,:,3]
    return u[cm],v[cm]
    

    

if __name__=="__main__":
    with warnings.catch_warnings(record=True) as w:
        logger.debug('STARTING RUN')
        warnings.simplefilter("ignore")
        cob2dunittest=0
        if cob2dunittest:
            x,y = 40., 40.
            deltax,deltay = 30., 30.
            theta=45. * np.pi/180.
            # expect xnew=14.41, ynew= 0 
            print cob2d(x,y,deltax,deltay,theta)
        #
        intpts = [0.3,0.4,0.5,0.6,0.7]
        intpts = [0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7]
        case=6
        if case==1: # a left to right case with appreciable theta about -0.0215 rad
            searchPars = dict(runDir='/home/can/stitching/',imageDir='/home/can/stitching/',
                            undefFil='000013571.tif',defFil='000013570.tif', 
                        nppars={'sub':[30,30],'nrt':[40,40],'nsb':[5,5],
                            'imethod_flag':1, 'ftol':1.e-6},  
                            ppPars={'icoflag':1,'npoint':0,'i0j0List':[],'size':(2452, 2054) })
        
            dx2,dy2,theta = findRelFrameCoords(2190.,0.,-0.015,frameSize=(2452,2054), 
                        searchPars=searchPars,numMaxIter=10, 
                        interpolation_points=intpts)
        elif case==2: # a left to right case with very limited theta
        
            searchPars = dict(runDir='/home/can/stitching/',imageDir='/home/can/stitching/',
                            undefFil='000039478.tif',defFil='000039477.tif', 
                            nppars={'sub':[30,30],'nrt':[40,40],'nsb':[5,5],
                            'imethod_flag':1, 'ftol':1.e-6},  
                            ppPars={'icoflag':1,'npoint':0,'i0j0List':[],'size':(2452, 2054) })
                        
            dx2,dy2,theta = findRelFrameCoords(1957.,0.,0.,frameSize=(2452,2054), 
                        searchPars=searchPars,numMaxIter=10, 
                        interpolation_points=intpts,dxLim=0.001)
            #print     dx2,dy2,theta

                        
            #dx2,dy2,theta = findRelFrameCoords(1958.,0.,-0.002,frameSize=(2452,2054), 
            #            searchPars=searchPars,numMaxIter=10, 
            #            interpolation_points=intpts,dxLim=0.001,fixtheta=True)
        elif case==3:
            searchPars = dict(runDir='/home/can/stitching/',imageDir='/home/can/stitching/',
                            undefFil='000039478.tif',defFil='000039472.tif', 
                            nppars={'sub':[30,30],'nrt':[40,40],'nsb':[5,5],
                            'imethod_flag':1, 'ftol':1.e-6},  
                            ppPars={'icoflag':1,'npoint':0,'i0j0List':[],'size':(2452, 2054) })
            dx2,dy2,theta = findRelFrameCoords(0.,1580.,0.,frameSize=(2452,2054), 
                        searchPars=searchPars,numMaxIter=10, 
                        interpolation_points=intpts)
            
        elif case==4:
            
            searchPars = dict(runDir='/home/can/stitching/',imageDir='/home/can/stitching/',
                            undefFil='000039457.tif',defFil='000039451.tif', 
                            nppars={'sub':[30,30],'nrt':[40,40],'nsb':[5,5],
                            'imethod_flag':1, 'ftol':1.e-6},  
                            ppPars={'icoflag':2,'npoint':0,'i0j0List':[],'size':(2452, 2054) })
            dx2,dy2,theta = findRelFrameCoords(0,1577.,0.,frameSize=(2452,2054), 
                        searchPars=searchPars,numMaxIter=10, 
                        interpolation_points=intpts)
                        
        elif case==5:
            
            searchPars = dict(runDir='/home/can/stitching/',imageDir='/home/can/stitching/',
                            undefFil='000039447.tif',defFil='000039446.tif', 
                            nppars={'sub':[30,30],'nrt':[40,40],'nsb':[5,5],
                            'imethod_flag':1, 'ftol':1.e-6},  
                            ppPars={'icoflag':1,'npoint':0,'i0j0List':[],'size':(2452, 2054) })
                        
            dx2,dy2,theta = findRelFrameCoords(1957.,0.,0.,frameSize=(2452,2054), 
                        searchPars=searchPars,numMaxIter=10, 
                        interpolation_points=intpts,dxLim=0.001)
                        
        elif case==6: # a left to right case 
        
            searchPars = dict(runDir='/home/can/stitching/',imageDir='/home/can/stitching/',
                            undefFil='000041563.tif',defFil='000041562.tif', 
                            nppars={'sub':[30,30],'nrt':[40,40],'nsb':[5,5],
                            'imethod_flag':1, 'ftol':1.e-6},  
                            ppPars={'icoflag':1,'npoint':0,'i0j0List':[],'size':(2452, 2054) })
                        
            dx2,dy2,theta = findRelFrameCoords(1957.,0.,0.,frameSize=(2452,2054), 
                        searchPars=searchPars,numMaxIter=10, 
                        interpolation_points=intpts,dxLim=0.001)                
                        
        print     dx2,dy2,theta
