import socket
from multiprocessing.managers import BaseManager
import multiprocessing as mp
import Pyro4
import Queue
import time
import glob
import shutil, filecmp
import os
import os.path as osp
import scipy.misc
import imagePreprocess as improc


import indRun ; reload(indRun)


def makedirsFullPermission(pth):
    try:
        original_umask = os.umask(0)
        os.makedirs(pth, 0777)
    finally:
        os.umask(original_umask)


def moveFilesByExtension(srcdir,dstdir,extension):
    
    if not srcdir[-1]=='/':
        srcdir += '/'
    allFiles = glob.glob(srcdir + extension)
    for fil in allFiles:
        prosName=osp.join(dstdir,osp.basename(fil))
        shutil.move(fil,prosName)
    return len(allFiles)

def copyFilesByExtension(srcdir,dstdir,extension):
    
    if not srcdir[-1]=='/':
        srcdir += '/'
    allFiles = glob.glob(srcdir + extension)
    for fil in allFiles:
        copier(fil, dstdir)
    return len(allFiles)



def copier(src,dstdir):
    """
    copies the filw with the same name to the destination directory
    """
    prosName=osp.join(dstdir,osp.basename(src))
	    
    shutil.copyfile(src,prosName)
    return

def copierWithFuncMod(src,dstdir,impreprocessFunc,impreprocessFuncKwds,verbose=True):
    """
    impreprocessFunc = takes path of an image file or multiple image array returns processed image matrix; e.g. takes 2452x2048x10, averages and returns 2452x2048.
    """
    basename = osp.basename(src)
    #extension = osp.splitext(src)[-1]
    prosName=osp.join(dstdir,basename)
    exec 'impreprocessFunc = improc.%s'%(impreprocessFunc,)
    processedMat= impreprocessFunc(src,**impreprocessFuncKwds)
    # the following produces a file in tiff format. prosName can be *.npy. But that is no
    # problem for the DIC software at all. numpy.save with a the same name would also work.
    if verbose: print 'saving to %s'%(prosName,)
    scipy.misc.imsave(prosName,processedMat,'tiff')
    
    return
    
    
    

def copierNoOverWrite(src,dstdir,finalCheck=True,verbose=True):
    if verbose: print "in copierNoOverWrite"
    checkSame = filecmp.cmp   #shutil._samefile  This seemed to have burned us.
    prosName=osp.join(dstdir,osp.basename(src))
    targetExists = os.path.exists(prosName)
    if targetExists:
        if verbose: print "comparing %s and %s"%(src, prosName)
        if verbose: print "The file by name %s exists."%(prosName)
        goodCopy = checkSame(src,prosName)
        time.sleep(0.1)
        if goodCopy:
            if verbose: print "Not copying anything since target exists and checked."
            copy = False
        else:
            print "THIS IS FISHY. Already copied file looks different from original file according to %s. Will attempt again"%(checkSame,)
            raise valueError
            copy = True
        #
    else:
        if verbose: print "The file by name %s DOES NOT exist."%(prosName)
        copy=True
    #
    if copy:
        print 'copying %s to %s'%(src,prosName)
        shutil.copyfile(src,prosName)
        time.sleep(0.1)
        if finalCheck: 
            sm = checkSame(src,prosName)
            time.sleep(0.1)
            if not sm:
                print "File copied but is not equal to the original according to %s"%(checkSame,)
                raise ValueError
        
        return
    

def fq(imFiles,datFile,imageRemotePth,localDir,commonAnalysisPath,verbose=True):
    """ fq = basic operation of DIC clients 
    1) initial copy for images to the local directory 
    2) running the analysis
    3) copying results back
    POSSIBLE TO DO:
    put stages 1 and 3 outside the operation loop.
    Details:
    Keeping the network functions in the basic element of the operation carries the risk of slowing it down or even halting it if network goes down. On the other hand, copying image files in operation has the advantage of only copying the files used by a client. Since, the unit operations processed are distributed dynamically, one does not know what images will be used a priori. An option is copying all images which does not look too appealing. Sometimes though especially if a DIC correlation concerning two images is distributed in 16 parts, the likelihood of many machines needing the same photo is increasing. So, maybe it is not so bad to copy all images a priori. Can switch to that. Regarding copying results back in erms of .rawdata files. One can definitely copy all .rawdata formed in the directory with one command creating a method of DICCalculator that will run after runPool (just as init but running afterwards.) I favored copying here because rawdata files are typically small and mass copying does not keep track of file numbers. Still appropriate checks can be put together for that as well.  

    AS OF MARCH 18 2016, copying functions 1 and 3 are ousted for improving reliablity.

    """
    st = time.time()
    #for imFile in imFiles:
    #    nm = osp.join(imageRemotePth,imFile)
    #    copierNoOverWrite(nm,localDir)
        # I believe in these files staying unique. So, if it came once, say with a previous faulty analysis, do not recopy it and save time.
    # make the analysis
    #print os.getcwd()
    if verbose: print "1. past the file copy in %s"%(datFile,)
    indRun.execdic(datFile)
    if verbose: print "2. returned from execution for %s"%(datFile,)
    # return the results files
    anName = os.path.splitext(datFile)[0]
    #rdat = anName+'.rawdata'
    #print '%s exists => %s'%(rdat, os.path.exists(rdat))
    if verbose: print "3. attempting to copy to common analysis path for %s"%(datFile,)
    #copier(anName+'.rawdata',commonAnalysisPath)
    #copier(anName + '.results',commonAnalysisPath)
    fn = time.time()
    if verbose: print "4. at the end of fq for %s"%(datFile,)
    return (anName, fn-st, time.ctime()) 




class DICCalculator(object):

    def addRelPath(self,rootpth):
        fullpth = os.path.join(rootpth, self.relPath)
        if not(os.path.exists(fullpth)):
            makedirsFullPermission(fullpth)
        else:
            print 'Warning! You might overwrite local files under this directory.'
    
        return fullpth
            
    def checkRelPath(self,rootpth):
        """ you confirm the path exists but don't add it if it does not."""
        fullpth = os.path.join(rootpth, self.relPath)
        if not(os.path.exists(fullpth)):
            raise ValueError, "Path %s does not exist."%(fullpth)
        return fullpth

    def init(self,relPath,allimages,impreprocessFunc=None,impreprocessFuncKwds={}):
        """ 
        impreprocessFunc: the string name of the function under DIC.imagePreprocess module that will allow processing of the image file (by that function) as it is copied to self.localAnalysisDir. The original in self.imageRemotePth is not (to be) altered.
        impreprocessFuncKwds: keyword arguments to pass to this module, expressed as dictionary
        initial operations before running calculations from a central pool
        July 2016, addded impreporcessFunc and impreprocessFuncKwds. 
        """
        self.relPath= relPath
        # need to decide how the above will go.
        self.imageRemotePth = os.environ['DICIMAGEPTH']
        print 'self.imageRemotePth=', self.imageRemotePth
        self.commonAnalysisRemotePth=  self.checkRelPath(os.environ['DICCOMMONANROOT'])
        print 'self.commonAnalysisRemotePth=', self.commonAnalysisRemotePth
        self.localAnalysisDir = self.addRelPath(os.environ['DICCLIENTLOCALROOT'])
        print 'self.localAnalysisDir = ', self.localAnalysisDir
        # stage of copying input files from common network drive to the local drive
        # There is inefficieny in copying all files although a client will typically not analyze all files. But, gets done with it before the analysis starts.
        # It is safer to overwrite all before analyses start.
        copyFilesByExtension(self.commonAnalysisRemotePth, self.localAnalysisDir ,'*.inp')
        copyFilesByExtension(self.commonAnalysisRemotePth, self.localAnalysisDir ,'*.dat')
        for im in allimages:
            nm = os.path.join(self.imageRemotePth, im)
            if impreprocessFunc == None:
                copierNoOverWrite(nm,self.localAnalysisDir)
            else:
                copierWithFuncMod(nm,self.localAnalysisDir,impreprocessFunc,impreprocessFuncKwds)
        return 
    
    def runPool(self,poolsize=5, queueKwds = dict(address=('navier.me.boun.edu.tr', 50000), authkey='karamazof')):
        #
	print "running pool"
        # go to local Analysis directory, eventually for running execDIC
        os.chdir(self.localAnalysisDir)
        #
        hname=socket.gethostname()
        class QueueManager(BaseManager): pass
        QueueManager.register('get_queue')
        m = QueueManager(**queueKwds)
        m.connect()
        queue = m.get_queue()
        
        #to do: getting to the queue should not be here and queue address should be modifiable. At least put that as another method of this class
        #
        pool = mp.Pool(poolsize,maxtasksperchild=1)
        count=0
        submitcount=0
        finishcount=0
        results=[]
        #localq = Queue.Queue()
        localList = []
        #print "queue.empty()=",queue.empty() 
        while not ((queue.empty()) & ((submitcount-finishcount)==0)): # a dizzying logical feat
    #while not ((queue.empty())):    
            count += 1 
            if submitcount - finishcount < poolsize:
                if not queue.empty():
                    im1,im2,datfile = queue.get()
                    print "host "+ hname + " working on "+ datfile
                    #res = pool.apply_async(fq,([im1,im2],datfile,self.imageRemotePth,self.localAnalysisDir,self.commonAnalysisRemotePth),callback=localq.put)
                    res = pool.apply_async(fq,([im1,im2],datfile,self.imageRemotePth,self.localAnalysisDir,self.commonAnalysisRemotePth),callback=localList.append)

                    #localq.put(res.get()) # this will lock unlike a callback
                    submitcount+=1
            finishcountold = finishcount
            #finishcount =  localq.qsize()
            finishcount = len(localList)
            if not (finishcount-finishcountold == 0):
                print 'finished %s analyses'%(finishcount,)
        #print submitcount,finishcount
        #time.sleep(0.1)
        
        print 'out of loop'
        pool.close()
        pool.join()
        processStr=''
        #while not localq.empty():
        #    processStr +=  str(localq.get()) + '  ' + str(hname)+ '\n'
        
        for ln in localList:
            processStr += str(ln) + '  ' + str(hname)+ '\n'
            # added the result copy function here
            anName = ln[0]
            copier(anName+'.rawdata',self.commonAnalysisRemotePth)

        print processStr
        #
        pth = osp.join(self.commonAnalysisRemotePth,"process_%s.rec"%(hname,))
        fid = open(pth,'a')
        fid.write(processStr)
        fid.close()
        #Appending to a single remote file by all clients did not work reliably.
        #pth = osp.join(self.commonAnalysisRemotePth,"process.rec")
        #fid = open(pth,'a')
        #fid.write(processStr)
        #fid.close()
        
        return 
    
    def identify(self):
        return "Hello"



def broadcastPyroDaemon(bname="dicC",nameServerCoords={'host':"navier.me.boun.edu.tr",'port':9005}):
    hname = socket.gethostname()
    daemon = Pyro4.Daemon(host="")      # make a Pyro daemon in the current host
    uri = daemon.register(DICCalculator)   # register the greeting maker as a Pyro object
   
    # register to nameserver 
    ns = Pyro4.locateNS(**nameServerCoords)  
    ns.register(bname+socket.gethostname(), uri)   # register the object with a name in the name server
    daemon.requestLoop()                   # start the event loop of the server to wait for calls 
    return 

if __name__=="__main__":

    broadcastPyroDaemon()
