import socket
from multiprocessing.managers import BaseManager
import multiprocessing as mp
import Pyro4
import Queue
import time
import glob
import shutil, filecmp
import os
import os.path as osp


import indRun ; reload(indRun)

####copy functions

def moveFilesByExtension(srcdir,dstdir,extension):
    
    if not srcdir[-1]=='/':
        srcdir += '/'
    allFiles = glob.glob(srcdir + extension)
    for fil in allFiles:
        prosName=osp.join(dstdir,osp.basename(fil))
        shutil.move(fil,prosName)
    return len(allFiles)

def copyFilesByExtension(srcdir,dstdir,extension):
    
    if not srcdir[-1]=='/':
        srcdir += '/'
    allFiles = glob.glob(srcdir + extension)
    for fil in allFiles:
        copier(fil, dstdir)
    return len(allFiles)

def copierPlain(src,dstdir):
    """
    copies the filw with the same name to the destination directory
    """
    prosName=osp.join(dstdir,osp.basename(src))
	    
    shutil.copyfile(src,prosName)
    return

def copierNoOverWrite(src,dstdir,finalCheck=True,verbose=True):
    if verbose: print "in copierNoOverWrite"
    checkSame = filecmp.cmp   #shutil._samefile  This seemed to have burned us.
    prosName=osp.join(dstdir,osp.basename(src))
    targetExists = os.path.exists(prosName)
    if targetExists:
        if verbose: print "comparing %s and %s"%(src, prosName)
        if verbose: print "The file by name %s exists."%(prosName)
        goodCopy = checkSame(src,prosName)
        time.sleep(0.1)
        if goodCopy:
            if verbose: print "Not copying anything since target exists and checked."
            copy = False
        else:
            print "THIS IS FISHY. Already copied file looks different from original file according to %s. Will attempt again"%(checkSame,)
            raise valueError
            copy = True
        #
    else:
        if verbose: print "The file by name %s DOES NOT exist."%(prosName)
        copy=True
    #
    if copy:
        print 'copying %s to %s'%(src,prosName)
        shutil.copyfile(src,prosName)
        time.sleep(0.1)
        if finalCheck: 
            sm = checkSame(src,prosName)
            time.sleep(0.1)
            if not sm:
                print "File copied but is not equal to the original according to %s"%(checkSame,)
                raise ValueError
        
        return


def copierOverwriteControl(src,dstdir,overwrite):
    """
    resize works on a copied file only as it should (the original file should be preserved.)
    if not None it should be doublet of numbers signifying the new size
    """
    prosName=osp.join(dstdir,osp.basename(src))
    if os.path.exists(prosName):
        if overwrite:
            print "Warning! %s exists. Overwriting!"%(prosName,)
        else:
            print "The file exists. Not overwriting. You might be analyzing an old picture."
    if (not os.path.exists(prosName)) or overwrite==1:
        # assumes this overwrites
        if not shutil._samefile(src,prosName):
            shutil.copyfile(src,prosName)
    else:
        pass
    
    return prosName


class copyWM:
    def __init__(self, src,dstdir,copyKwds={},modifyKwds={}):
        self.src = src
        self.dstdir = dstdir
        self.copy(**copyKwds)
        self.modify(**modifyKwds)
        return

class copyPlain(copyWM):
    """no checks"""
    def copy(self,**kwds):
        copierPlain(self.src, self.dstdir)

class copyOverwriteControl(copyWM):
    def copy(self,overwrite):
        
    

##############modification functions


########################## This has a resize function.

def resizeInPlace(newsize, imPath,resizeKwds={}):
    im = Image.open(imPath)
    im2 = im.resize(newsize,**resizeKwds)
    # overwrite
    im2.save(imPath)
    return

