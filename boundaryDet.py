#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 14:50:27 2018

@author: C. Can Aydiner

implements a gradually refined FFT tracking to a pair of images with function
gfFFTAnalysis. 

The gradual refinement also leads to determination of material
overlap between the two images.
"""

from __future__ import division
import os

import numpy as np
import matplotlib.pyplot as plt
import skimage
import skimage.io
from skimage.feature import register_translation
from skimage.feature.register_translation import _upsampled_dft
import scipy.interpolate

import dicmadv.debugTools as dbg
reload(dbg)

import estimateGrid as estm; reload(estm)
import genericUtility.gridTools as grdt; reload(grdt)

class OutOfBoundsError(Exception):
    pass

class InsufficientOverlapError(Exception):
    pass


class extendedInterp(object):
    """extends LinearNDInterpolator with NearestNDInterpolator extrapolation"""
    def __init__(self, points,values):
        self.funcinterp=scipy.interpolate.LinearNDInterpolator(points,values)
        self.funcnearest=scipy.interpolate.NearestNDInterpolator(points,values)
    def __call__(self,X,Y):
        t=self.funcinterp(X,Y)
        msk = np.isnan(t)
        if msk.any():
            try:
                t[msk] = self.funcnearest(X[msk],Y[msk])
            except TypeError: # e.g., if an individal point is sent
                t = self.funcnearest(X,Y)
        return t


def splitdiv(Ntotal, indices_or_sections):
    """ 
    INPUTS:
    Ntotal = total length of array
    indices_or_sections = number of sections
    OUTPUT:
        div_points: division indices as a list
    
    most functionality copied from numpy.array_split 
    it maximizes uniformity of division points by a clever trick
    it does not leave a very small remainder or anything  like that
    """
    Nsections = int(indices_or_sections)
    if Nsections <= 0:
        raise ValueError('number sections must be larger than 0.')
    Neach_section, extras = divmod(Ntotal, Nsections)
    section_sizes = ([0] +
                         extras * [Neach_section+1] +
                         (Nsections-extras) * [Neach_section])
    # this is the distribution trick
    div_points = np.array(section_sizes).cumsum()
    return div_points

def imsplit(im, nrows,ncols,xorder=True):
    """ 
    im = image input, only uses its shape
    nrows, ncols = the desired number of divisions 
    xorder = True reverse A[i][j] such that the first index corresponds to column
    splits an image intor rectangles and returns rectangle instances in a 
    nested array
    """
    nx,ny=ncols,nrows
    Ny,Nx = im.shape
    divy = splitdiv(Ny,ny)
    divx = splitdiv(Nx,nx)
    # initite a proper size cascaded list
    A=np.zeros((nrows,ncols)).tolist()
    for i in range(ncols):
        for j in range(nrows):
            if xorder:
                A[j][i] = rectangle(divx[i],divy[j],divx[i+1],divy[j+1])
            else:
                A[i][j] = rectangle(divx[i],divy[j],divx[i+1],divy[j+1])
            
            
    return A

def tile_array(a, b0, b1):
    """
    a = input matrix
    b0,b1: upscaling row and column numbers. 
    #
    Upscales the matrix by (b0,b1) copying taking the number for each submatrix
    from the particular location in the input matrix.
    example:
    A = np.array([[1,2],[3,4]])
    tile_array(A,2,2) yields
    array([[1, 1, 2, 2],
       [1, 1, 2, 2],
       [3, 3, 4, 4],
       [3, 3, 4, 4]])
    """
    r, c = a.shape                                    # number of rows/columns
    rs, cs = a.strides                                # row/column strides 
    x = np.lib.stride_tricks.as_strided(a, (r, b0, c, b1), (rs, 0, cs, 0)) 
                                               # view a as larger 4D array
    return x.reshape(r*b0, c*b1) 

def _normalize(A,nominal=127):
    return A-A.mean()+nominal


class rectangle:
    def __init__(self,x1,y1,x2,y2):
        self.box = (x1,y1,x2,y2)
        self.cen = np.array([(x1+x2)/2., (y1+y2)/2.])
        self.w, self.h = x2-x1, y2-y1
    def shiftBox(self,shift):
        """
        shift = (dy,dx) (row,col) 
        return coordinates for a box that is shifted with respect to this
        rectangle"""
        dy,dx = shift
        x1,y1,x2,y2 = self.box
        return(x1+dx, y1+dy,x2+dx, y2+dy)
    def returnImBox(self,im):
        """returns the part of image that falls in this box"""
        x1,y1,x2,y2 = np.asarray(self.box,np.int)   
        return im[y1:y2,x1:x2]
    def showImBox(self,im):
        """returns the part of image that falls in this box"""
        plt.imshow(self.returnImBox(im))
        return
    def __repr__(self):
        return "%s\nw h %s , %s\ncenter %s"%(self.box, self.w, self.h,self.cen)
    
        
def checkBounds(shp,x1,y1,x2,y2,symm = True, allowedPct=40,allowedSize=30):
    """
    symm=True: if has to trim from the right; it trims from the left to keep 
               the center in place; analogously for top and bottom 
    allowedPct = number between [0, 100] if the trimmed box is 
    return: trimmed, boxShape to work with 
    """
    #TODO make a common function to work on either axis and cut down the code    
    ymax , xmax = shp
    allgood = np.array([ (x1>=0)   , (y1>=0),  (x2<=xmax), ( y2<=ymax)    ])
    if allgood.all(): 
        return (False, [x1,y1,x2,y2])
    fullOut = np.array([ (x1>xmax), (y1>ymax), (x2<0),  (y2<0)    ])
    if fullOut.any() : 
        raise OutOfBoundsError
    dx,dy = x2-x1, y2-y1
    #
    x1n,y1n,x2n,y2n=x1, y1, x2, y2
    if x1<0:  
        x1n = 0
        if symm: 
            x2n = x2 + x1 # cut the same amount from the right 
    if x2>xmax:
        x2n = xmax
        if symm:
            x1n = x1 + (x2-xmax) # cut the same amount from the left
    if y1<0:  
        y1n = 0
        if symm: 
            y2n = y2 + y1 # cut the same amount from the right 
    if y2>ymax:
        y2n = ymax
        if symm:
            y1n = y1 + (y2-ymax) # cut the same amount from the left
    trimRatio = np.array([ (x2n-x1n)/dx,  (y2n-y1n)/dy ]) 
    remainingDims = np.array([(x2n-x1n),  (y2n-y1n) ]) 
    A = (trimRatio*100. < allowedPct).any()
    B = (remainingDims < allowedSize).any()
#    if A: 
#        print "trimRatio not satisfactory %s \n %s %s\n %s %s"%(trimRatio,
#                                                        (x2n-x1n),(y2n-y1n),
#                                                        dx, dy)
#    if B: 
#        print "remDims not satisfactory %s \n %s %s\n %s %s"%(remainingDims,
#                                                        (x2n-x1n),(y2n-y1n),
#                                                        dx, dy)
    if (A or B):
        raise InsufficientOverlapError
    
    return (True,[x1n,y1n,x2n,y2n])
    


def searchRec(im1,im2,rect,shift,**kwds):
    """
    searches the location in im1 defined by rect in im2 given an initial 
    displacement of shift. It is aware of image bounds and works around 
    overflows using checkBounds
    """
    sh1 = im1.shape
    sh2 = im2.shape
    x1,y1,x2,y2 = rect.box
    status = 0
    #
    ref_trimFlag,ref_nbox = checkBounds(sh1,*rect.box,**kwds)
    if ref_trimFlag:
        raise ValueError, "no sense to specify the reference box (partly) out"
    try:
        def_trimFlag, def_nbox = checkBounds(sh2,*rect.shiftBox(shift),**kwds)
    except InsufficientOverlapError:
        # catch and keep the estimate as it was.
        shift = 0        
        status = 1
        error, diffphase = np.nan, np.nan
    except OutOfBoundsError:
        #This region likely need not be considered in future subdivisions
        # catch and keep the estimate as it was.
        shift =0
        status = 2
        error, diffphase = np.nan, np.nan
    else: 
        if def_trimFlag:
            def_rect = rectangle(*def_nbox)
            #resize reference rectangle to match what could be found on the other end
            refshiftback = def_rect.shiftBox(-1.*shift) 
            # this should require no checks, meaning it should be within the
            #                        
            ref_rect = rectangle(*refshiftback)
            # during initial debugging check 
            if (ref_rect.cen !=  rect.cen).any(): # redundant check
                raise ValueError, "could not preserve center"
        else:
            def_rect = rectangle(*def_nbox)
            ref_rect = rectangle(*ref_nbox)
            #
        ref_imbox = ref_rect.returnImBox(im1)
        def_imbox = def_rect.returnImBox(im2)
        # TODO: consider dropping averages from each to make it a
        # normalized cross-correlation
        shift, error, diffphase= register_translation(ref_imbox, def_imbox)

    return status,shift,error,diffphase
            
def createInterpNet(x,y,u,v,mode="extendedInterp",**kwds):
    """
    returns interpolation funcitons for u and v based on its values over a net
    the used function does not require a regular net.
    """
    if mode=="interp2d":
        uif = scipy.interpolate.interp2d(x,y,u,**kwds)
        vif = scipy.interpolate.interp2d(x,y,v,**kwds)
    elif mode=="LinearNDInterpolator":
        uif = scipy.interpolate.interp2d((x,y),u,**kwds)
        vif = scipy.interpolate.interp2d((x,y),v,**kwds)
    elif mode=="extendedInterp":
        uif = extendedInterp((x,y),u,**kwds)
        vif = extendedInterp((x,y),v,**kwds)
    else:
        raise ValueError, "invalid interpolation mode"
    return(uif,vif)

class layer:
    """just a structure for a result layer as defined by gfFFTAnalysis"""
    def __init__(self,rects,shifts,status,error,diffphase):
        """
        status = 0, the shift is found on the full box, 
        status = 1, the box has some overlap at the margins of the second image
                    but the overlap is not sufficient based on the given
                    criteria
        status=2, the box is deemed totally out.
        
        shifts: [..,.., 0] is v shift or row shift
                [..,..,1 ] is u shift or column shift 
        """
        self.rects=rects
        self.status=status
        self.shifts=shifts
        self.error=error
        self.diffphase=diffphase
        self.ny, self.nx = self.status.shape
        return 
    def getCenters(self):
        x = np.zeros((self.ny,self.nx))
        y = np.zeros((self.ny,self.nx))
        for i in range(self.nx):
            for j in range(self.ny):
                x[i,j],y[i,j] = self.rects[i][j].cen
        return x,y
        #        
        
    def interpNets(self,interpMode,useCleans=True,**kwds):
        """
        useCleans: if True, 
                   exclude the points that have bad status from
                   shift determination
                   if False, 
                   include these points, it is not the end of the
                   world, since their shifts are kept the same as the last
                   working layer, which basically makes the healthy
                   points padded by the closest neighbors.
        kwds: sent 
        """
        self.interpMode=interpMode
        u = self.shifts[...,1]
        v = self.shifts[...,0] 
        x,y= self.getCenters()
        if useCleans:
            cl = np.logical_not(self.status.astype('bool'))
            self.uif,self.vif = createInterpNet(x[cl],y[cl],u[cl],v[cl],
                                                mode=interpMode,**kwds) 
        else:
            self.uif,self.vif = createInterpNet(x.flatten(),y.flatten(),
                                                u.flatten(),v.flatten(),
                                                mode=interpMode,**kwds)
        return
    
    def visualIJ(self,im1, im2, boxIJ, sub=(50,50)):
        """
        boxIJ: grid location
        """
        I,J = boxIJ
        u = self.shifts[I,J,1]
        v = self.shifts[I,J,0] 
        x,y = self.getCenters()
        cl = np.logical_not(self.status.astype('bool'))
        if cl[I,J]==False: 
            raise ValueError, "should be (partly) out of bounds on im2"
        dbg.plotSourceTarget(im1,im2,loc=(x[I,J],y[I,J]),uv=(u,v),sub=sub)
        return
    def visualXY(self, im1,im2, xy, sub=(50,50)):
        """
        x, y = xy requested pixel coordinates
        this presumes the creation of interpNets i.e. .uif adn .vif are
        callable.
        """
        xr, yr = xy
        try:
            u , v = self.uif(xr,yr)[0], self.vif(xr,yr)[0]
        except IndexError:
            u , v = np.float(self.uif(xr,yr)), np.float(self.vif(xr,yr))
        print u,v
        dbg.plotSourceTarget(im1,im2,loc=xy,uv=(u,v),sub=sub)
        return
                    
        
        
    
def gfFFTAnalysis(im1,im2,orders=0,normalize=False,**kwds):
    """
    im1, im2 = reference and deformed images
    orders =  the number of layers in which the domain will be subdivided in
               factors of 2. order=3 there will be layers 1,2 and 3. These
               will subdivide the domain in 2x2, 4x4 and 8x8 respectively.
               for 5 Mpixel images and order bigger than 6 makes the boxes
               around 30 pixels, i.e., 2000/(2**6)~30. It will likely stop 
               working reliably around this value.  
    normalize: normalize image intensity about the 127 value.
    
    DOES:
    gfFFTAnalysis stands for gradually refined FFT analysis
    Tracking of domains is implemented in gradually divided domains. Zeroth
    layer of analysis searches for the entire im1 in im2. First layer considers
    a 2x2 zone, the second layer 4x4, third layer, 8x8, etc. The initial disp.
    condition of each nth layer box comes from the previous (n-1) layer box 
    that contains this particular nth layer box.
    
    This is a COARSE ANALYSIS in Legacy DIC terms, i.e., all deformation and 
    rotation is unaccounted for; it just searches for shifts.
    """
    if normalize:
        im1, im2 = _normalize(im1), _normalize(im2)
    fullSz = 2**orders
    sh=np.zeros((1,1,2))
    status, error, diffphase = np.zeros((1,1)),  np.zeros((1,1)),  np.zeros((1,1))
    layers=[]
    for order in range(orders+1):
        if order > 0:
            u=tile_array(sh[...,0],2,2)
            v=tile_array(sh[...,1],2,2)
            status,error,diffphase = np.zeros_like(u), np.zeros_like(u),np.zeros_like(u)
            sh = np.dstack((u,v))
        # debug prints will be removed
        #print sh[...,0]
        #print sh[...,1]
        #print '\n  recall shift is in v u order here'
        ncol,nrow = 2**order, 2**order
        rects =  imsplit(im1,ncol,nrow,xorder=True)
        for i in range(ncol):
            for j in range(nrow):  
                shift = sh[i,j,...]
                rect = rects[i][j]
                nstatus, nshift,nerror,ndiffphase = searchRec(im1,im2,rect,shift,**kwds)
                # nshift is the scalar update returned by searchrect
                sh[i,j,...] += -1.* nshift
                status[i,j],error[i,j],diffphase[i,j] = nstatus, nerror, ndiffphase
                
        print sh[...,0]
        print sh[...,1]
        #print '\n\n\n\n recall shift is in v u order here'
        layers.append( layer(rects, sh.copy() , status.copy(), error.copy(), diffphase.copy() ) )
        # copying not to worry about pointer addressing to modified pars
    return layers

def checkImprovement(im1,im2,layers, topLayer=3, loc=(3,3),xy=None, **kwds):
    """ 
    a visual check on how the location estimate of subrectange is improved
    topLayer: the finest division layer considered 
    loc:      grid location in top layer
    xy :    if provided overrides the loc in pixels
    
    DOES:
        calculates the interploated location of the subset from previous layers
        and shows all of them in a box
        Ideally, layer 3 estimate should be better than layer 2 estimate etc.
    """
    if xy:
        x,y = xy
    else:
        x,y = layers[topLayer].rects[loc[0]][loc[1]].cen
    #This is where you want the comparison
    for i in range(topLayer):
        numlayer = topLayer - i
        layers[numlayer].interpNets(**kwds)
        layers[numlayer].visualXY(im1,im2,(x,y))
        plt.title("Layer %s"%(numlayer,) )
    return
        
            
            
def matBoundary(im1, im2, mappingLayer):
    """
    im1 =  ref image
    im2 =  deformed image
    mappingLayer = an instance of the layer class whose u and v estimating
    subfunctions uif and vif are defined.
    #
    DOES:
    Formulates the boundary of the material region in the reference image that  
    actually exists in the deformed image. Roughly, the points outside this
    region do not exist in the coverage of the deformed image. 
    """
    ny1, nx1 = im1.shape
    ny2, nx2 = im2.shape
    x1,y1 = np.meshgrid(np.arange(nx1), np.arange(ny1))
    # The following is an ugly provision needed since different interpolaters 
    # are called with a meshgrid or by the axis arrays.
    if mappingLayer.interpMode=="interp2d": 
        u = mappingLayer.uif(np.arange(nx1), np.arange(ny1))
        v = mappingLayer.vif(np.arange(nx1), np.arange(ny1))      
    elif mappingLayer.interpMode in ["LinearNDInterpolator","extendedInterp"]:
        u = mappingLayer.uif(x1,y1)
        v = mappingLayer.vif(x1,y1)      
    else:
        raise ValueError
    #estimated coordinates of the material domain in deformed image (Eulerian)
    # domain
    x2mat, y2mat = x1+u, y1+v
    #now you can check the conditions on the original coords 
    # in logical terms
    x_in = np.logical_and((x2mat>0),(x2mat<nx2))         
    y_in = np.logical_and((y2mat>0),(y2mat<ny2))         
    xy_in  =np.logical_and(x_in,y_in)
    # Not quite done. But getting xy_in is a decent half-point
    return xy_in
    
def patchAnalysis(im1, im2, connection='up',bandwidth=600,ac_bandwidth=True,normalize=False,**kwds):
    """
    connection = 'up', im1 is overlapping region with its top section that 
                        exists in im2's bottom section.
                        other optins are 'down' 'right' 'left'
    bandwidth =the width of the region that is going to be considered in pixels
    ac_bandwidth = auto correct bandwidth. If bandwith is less than the phys.
                   material overlap, the material region determination gives
                   incomprehensive (!) results. This adjust bandwith to 
                   slightly get over the overlap
    This is intended for two images that are the same size.
    
    
    If images that have huge shifts (and thus little overlap) with each other 
    are run with gfFFTAnalysis; maximum correlation
    does not reliably produce the small overlap subset. In such cases, it is 
    useful to help the analysis by taking the appropriate band of materials
    and run them with respect to each other.  
    """    
    def reg(m1,m2,normalize):
        if normalize:
            return register_translation(_normalize(m1),_normalize(m2))
        else:
            return register_translation(m1,m2)
        
        
    assert im1.shape == im2.shape
    H,W = im1.shape
    #
    if connection=="up":
        im1b = im1[:bandwidth , :]
        im2b = im2[-bandwidth: , :] 
        deltax = 0
        deltay = H - bandwidth 
        if ac_bandwidth:
            shift, error, diffphase = reg(im1b, im2b,normalize=normalize)
            v,u = -1.*shift
            bandwidth= int(bandwidth - v + 50) # 50 is a reasonable allowance to be on the right side
            im1b = im1[:bandwidth , :]
            im2b = im2[-bandwidth: , :] 
            deltay = H - bandwidth 
    elif connection=='down':
        im1b = im1[-bandwidth: , :]
        im2b = im2[:bandwidth , :] 
        deltax = 0
        deltay = -1. * (H - bandwidth )
        if ac_bandwidth:
            shift, error, diffphase = reg(im1b, im2b,normalize=normalize)
            v,u = -1.*shift
            bandwidth= int(bandwidth + v + 50) # 50 is a reasonable allowance to be on the right side
            im1b = im1[-bandwidth: , :]
            im2b = im2[:bandwidth , :] 
            deltay = -1. * (H - bandwidth )
    elif connection=='left':
        im1b = im1[:,:bandwidth ]
        im2b = im2[:,-bandwidth:] 
        deltay = 0
        deltax = W - bandwidth 
        if ac_bandwidth:
            shift, error, diffphase = reg(im1b, im2b,normalize=normalize)
            v,u = -1.*shift
            bandwidth= int(bandwidth - u + 50) # 50 is a reasonable allowance to be on the right side
            im1b = im1[:,:bandwidth ]
            im2b = im2[:,-bandwidth:] 
            deltax = W - bandwidth 
    elif connection=='right':
        im1b = im1[:,-bandwidth:]
        im2b = im2[:,:bandwidth] 
        deltay = 0
        deltax = -1. * (W - bandwidth )
        if ac_bandwidth:
            shift, error, diffphase = reg(im1b, im2b,normalize=normalize)
            v,u = -1.*shift
            bandwidth= int(bandwidth + u + 50) # 50 is a reasonable allowance to be on the right side
            im1b = im1[:,-bandwidth:]
            im2b = im2[:,:bandwidth]   
            deltax = -1. * (W - bandwidth )
        
    else:
        raise NotImplementedError
    #    
    layers = gfFFTAnalysis(im1b,im2b,normalize=normalize,**kwds)
    return layers, (im1b,im2b), (deltax,deltay), bandwidth


def combinedPlot(im1,im2,inbool,grids=None,saveFil=None):
    """
    im1,im2 : images
    inbool  : determined zone (as a binary mask) of im1 material points that
              are still in im2
    grids   : the produced grids that hopefully sit in the material overlap 
              zone   a three-tier list of subgrids and pixel locations
    """    
    f, [ax1,ax2] = plt.subplots(1,2)
    im1c = im1.copy()
    im1c[np.logical_not(inbool)]=255
    ax1.imshow(im1c)
    ax2.imshow(im2)
    if grids:
        # This does not work very well at low resolution.
        grdt.addGridSketches(ax1,grids)
    if saveFil:
        plt.savefig(saveFil)
        plt.close()
    return
    
if __name__=="__main__":
    
    interpM = "extendedInterp"
    #interpM = "interp2d"
    
    imageDir=os.environ["DICIMAGEPTH"]
    def pth(fil):
        return os.path.join(imageDir,fil)
    nrow=6
     

    if 0:
        im1 =skimage.io.imread(pth("0000%s.tif"%(49413,)))
        im2 = skimage.io.imread(pth("0000%s.tif"%(50815,))) 
        # showing the engine with zeroth order operation
        shift, error, diffphase = register_translation(im1, im2)

    if 0: # full image overlap study
        im1 =skimage.io.imread(pth("0000%s.tif"%(49419,)))
        #im2 = skimage.io.imread(pth("0000%s.tif"%(50821,))) 
        im2 = skimage.io.imread(pth("0000%s.tif"%(50055,)))
        layers = gfFFTAnalysis(im1,im2,orders=4,allowedPct=20)
        _layer = layers[-1]
        _layer.interpNets(interpMode=interpM,useCleans=False)
        #plt.figure()
        #layer.visualIJ(im1,im2,(5,5))
        inbool = matBoundary(im1, im2, mappingLayer=_layer)
        plt.figure()
        plt.imshow(inbool)
        f, [ax1,ax2] = plt.subplots(1,2)
        im1c = im1.copy()
        im1c[np.logical_not(inbool)]=255
        ax1.imshow(im1c)
        ax2.imshow(im2)
        lim = estm.prepRectGridLims(inbool, margin=50, shift=(0,0), delta=(10,10))
        grids,centers = grdt.nMultiGrid((8,8),lim,10,10)
        grdt.addGridSketches(ax1,grids)
    if 0: # up-down patch overlap study(these belong to different rows in the frame grid)
        im1 =skimage.io.imread(pth("0000%s.tif"%(49419-6,))) #downRef
        im2 = skimage.io.imread(pth("0000%s.tif"%(50821,))) 
        layers, choppedims, delta,bw = patchAnalysis(im1,im2, connection='up', bandwidth=600, orders=2,allowedPct=20)
        im1b, im2b = choppedims
        _layer = layers[-1]
        _layer.interpNets(interpMode=interpM,useCleans=True)
        #
        inbool = matBoundary(im1b, im2b, mappingLayer=_layer)
        plt.figure()
        plt.imshow(inbool)
        f, [ax1,ax2] = plt.subplots(1,2)
        im1c = im1b.copy()
        im1c[np.logical_not(inbool)]=255
        ax1.imshow(im1c)
        ax2.imshow(im2b)
        print layers[0].error, layers[0].shifts
        lim = estm.prepRectGridLims(inbool, margin=50, shift=(0,0), delta=(10,10))
        grids,centers = grdt.nMultiGrid((4,1),lim,10,10)
        grdt.addGridSketches(ax1,grids)

    if 0: # down-up patch overlap study(these belong to different rows in the frame grid)
        im1 =skimage.io.imread(pth("0000%s.tif"%(49419,)))
        im2 = skimage.io.imread(pth("0000%s.tif"%(50049,))) 
        layers, choppedims, delta,bw = patchAnalysis(im1,im2, ac_bandwidth=True,
                     connection='down', bandwidth=800, orders=2,allowedPct=20)
        im1b, im2b = choppedims
        _layer = layers[-1]
        _layer.interpNets(interpMode=interpM,useCleans=True)
        #
        inbool = matBoundary(im1b, im2b, mappingLayer=_layer)
        plt.figure()
        plt.imshow(inbool)
        f, [ax1,ax2] = plt.subplots(1,2)
        im1c = im1b.copy()
        im1c[np.logical_not(inbool)]=255
        ax1.imshow(im1c)
        ax2.imshow(im2b)
        print layers[0].error, layers[0].shifts
        lim = estm.prepRectGridLims(inbool, margin=50, shift=(0,0), delta=(10,10))
        grids,centers = grdt.nMultiGrid((4,1),lim,10,10)
        grdt.addGridSketches(ax1,grids)
        
    if 1: # left-right patch overlap study(these belong to different rows in the frame grid)
        im1 =skimage.io.imread(pth("0000%s.tif"%(13575,)))
        im2 = skimage.io.imread(pth("0000%s.tif"%(16084,))) 
        layers, choppedims, delta,bw = patchAnalysis(im1,im2, ac_bandwidth=True,
                     connection='left', bandwidth=600, orders=2,allowedPct=20)
        im1b, im2b = choppedims
        _layer = layers[-1]
        _layer.interpNets(interpMode=interpM,useCleans=True)
        #
        inbool = matBoundary(im1b, im2b, mappingLayer=_layer)
        plt.figure()
        plt.imshow(inbool)
        f, [ax1,ax2] = plt.subplots(1,2)
        im1c = im1b.copy()
        im1c[np.logical_not(inbool)]=255
        ax1.imshow(im1c)
        ax2.imshow(im2b)
        print layers[0].error, layers[0].shifts
        lim = estm.prepRectGridLims(inbool, margin=50, shift=(0,0), delta=(10,10))
        grids,centers = grdt.nMultiGrid((1,4),lim,10,10)
        grdt.addGridSketches(ax1,grids)
            
            
            
            
            