#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 11:17:48 2018

Some quick needs for visualization

@author: can
"""
import pickle, os
import matplotlib.pyplot as plt
import skimage.io
import numpy as np


import npostProcess as npp
import nplot
import frameMatrix
import boundaryDet as bdet

def chkf(fil):
    """ a speed/memory function for checking quality of a pickled analysis
    object"""
    an = pickle.load(open(fil))
    K = an.check()
    print K 
    return K

def peek(anfil,imageNum=0,var='Ey',  leveling={'timesStd':1,'numLevels':8,'symm':True},
         contourKwds={'alpha':1.,'extend':'both','cmap':plt.cm.RdBu_r,
                      'origin':'upper',}):
    try:    
        an = pickle.load(open(anfil))
    except:
        an = anfil
    pobj = npp.frameData(an.rawdatamatrices,imageNum=imageNum)
    D, names = pobj.getStrainFrames()
    nplot.plot(D[var],leveling=leveling,contourKwds=contourKwds)
    return D[var]

def peeknpy(anfil,imageNum=0,var='Ey', leveling={'timesStd':1,'numLevels':8,'symm':True},
         contourKwds={'alpha':1.,'extend':'both','cmap':plt.cm.RdBu_r,
                      'origin':'upper'}):
    rd = np.load(anfil)
    pobj = npp.frameData(rd,imageNum=imageNum)
    D, names = pobj.getStrainFrames()
    nplot.plot(D[var],leveling=leveling,contourKwds=contourKwds)
    return D[var]

def peekErr(anfil,imageNum=0,leveling={'timesStd':1,'numLevels':8,'symm':True},contourKwds={'alpha':1.,'cmap':plt.cm.jet,
                      'origin':'upper',}):
    try:    
        an = pickle.load(open(anfil))
    except:
        an = anfil
    pobj = npp.frameData(an.rawdatamatrices,imageNum=imageNum)
    st = pobj.pickVarGrid("status")
    nplot.plot(st,leveling=leveling,contourKwds=contourKwds)
    return

def estimateOptRes(im1,im2,connection="up", addPath=True, errLimit=0.3,
                   bandwidth=600,normalize=True):
    """
     connection = 'up', im1 is overlapping region with its top section that 
                        exists in im2's bottom section.
    addPath: joins the default image directory
    Though this will be go into the context of frameMatrix.frameGrid 
    instances shortly. A quick and dirty function when you send 
    two images that are up and down and use patchAnalysis 
    Ex. when connection="up", im1 = "000049413.tif", im2="000049419.tif"
        when connection="down", im1= "000049419.tif", im2="000049413.tif"
             connection = "right",    "000049416.tif","000049415.tif"
             connection = "left" ,    "000049415.tif","000049416.tif"
    """
    
    imageDir=os.environ["DICIMAGEPTH"]
    opim = skimage.io.imread
    join = os.path.join
    if addPath:
        im1m   = opim(join(imageDir,im1))
        im2m = opim(join(imageDir,im2))
    else:
        im1m = opim(im1)
        im2m = opim(im2)
    #   
    _res = bdet.patchAnalysis(im1m,im2m, connection=connection,bandwidth=bandwidth,
                              normalize=normalize,orders=0,allowedPct=20)
    layers, choppedims, delta,bw = _res
    err = layers[0].error[0][0]
    assert err<errLimit, "%s is too large for a good correlation"%(err,)  
        
    v = layers[0].shifts[0,0,0]
    u = layers[0].shifts[0,0,1]
    print u,v
    print err
    print bw
    #
    mmLoc =  frameMatrix.stagePos4fileName
    diffLoc = mmLoc(im1).flatten() - mmLoc(im2).flatten()
    umDist = diffLoc*1000.
    if connection in ['up','down']:
        pixDist = v+delta[1]
        ret = np.abs(umDist[1]/pixDist)
        
    elif connection in ['left','right']:
        pixDist = u+delta[0]
        ret = np.abs(umDist[0]/pixDist)
    return ret