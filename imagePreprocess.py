import numpy as np
import scipy.misc
from scipy import ndimage
import os.path as osp
import skimage.io

# generic utility

def openByExt(nm):
    "takes path nm returns array"
    extension = osp.splitext(nm)[-1]
    if extension in ['.tif','.tiff']:
        ret = skimage.io.imread(nm)
    elif extension=='.npy':
        ret = np.load(nm)
    #
    return ret


# These functions, by their calling procedure, take the file path of an image (or image array) and return a single image matrix



def framePicker(nm, frameNo):
    """
    picks the frameNo frame of an npy file that has multiple images
    """
    mat = openByExt(nm)
    shp = mat.shape
    if len(shp)==2:
        print "there is only one frame"
        ret = mat # there is no frame to pick; just one
    elif len(shp)==3:
        ret =  mat[...,frameNo]
    else:
        raise ValueError
    #
    return ret
    

def frameAverage(nm):
    """
    puts out the average frame out of all frames included in an npy file
    e.g. a 10 frame npy file has size [2054, 2452, 10]
    """
    mat = openByExt(nm).astype(float)
    ret = np.average(mat, axis=-1).round().astype('uint8')
    return ret


def gaussFilt(nm,sigma,order):
   
    mat = openByExt(nm).astype(float)
    ret = ndimage.gaussian_filter(mat, sigma, order)
    
    return ret

def denoiseGauss(nm,size=3,**kwds):
    """
    taken from Necdet who took it from cv2.GaussianBlur defaults I believe
    """
    mat = openByExt(nm).astype(float)
    sigma = 0.3 * ((size - 1) * 0.5 - 1) + 0.8
    trunc = kwds.pop("truncate", ((size - 1) * 0.5) / sigma)
    filtered_image = ndimage.gaussian_filter(mat, sigma=sigma, truncate=trunc, **kwds)
    return np.uint8(filtered_image + 0.5) #round it to uint8

