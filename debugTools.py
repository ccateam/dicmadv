import numpy as np
import postProcess as pp 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
def crop(fil, loc, sub):
    """ 
    x,y = loc
    nsub,msub = sub
    nsub msub as defined in legacy DIC. This crops a subset from a file searching the file
    with the postProcess tool openImage.
    """
    x,y = loc
    dx,dy = sub
    if type(fil)==str:
        im = pp.openImage(fil)
    else: #try if fil is a matrix
        # the first statement is to invoke an error if fil is not a matrix
        fil.dtype
        im=fil
    try:
        return im[y-dy:y+dy+1,x-dx:x+dx+1,]
    except TypeError:
        return im[int(y-dy):int(y+dy+1),int(x-dx):int(x+dx+1),]
    



def cropSourceTarget(fil1,fil2,loc,uv,sub,uv2=None):
    """
    loc = x,y
    uv  = u,v
    sub = dx,dy (size)
    """
    source = crop(fil1,loc,sub)
    locTarget = np.array(loc)+np.array(uv)
    target =crop(fil2,locTarget,sub)
    if uv2:
        locTarget2 = np.array(loc)+np.array(uv2)
        target2 = crop(fil2,locTarget2,sub)
    else:
        target2=None
    return(source,target,target2)
    
    
def plotSourceTarget(fil1,fil2,*args,**kwds):
    """
    fil1 = undef file
    fil2 =  def file
    args and kwds are for cropSourceTarget
    DOES:
    typically used to show the undeformed subset and the suppesedly found deformed
    subset for a visual confirmation
    
    """
    source,target,target2 = cropSourceTarget(fil1,fil2,*args,**kwds)
    #if target2==None:
    #    plt.imshow(np.hstack([source,target]),cmap=cm.gray)
    #else:
    #    plt.imshow(np.hstack([source,target,target2]),cmap=cm.gray)
    if target2==None:
        f, [ax1,ax2] = plt.subplots(1,2)    
        ax1.imshow(source,cmap=cm.gray)
        ax1.set_title(source)
        #plt.colorbar()
        ax2.imshow(target,cmap=cm.gray)
        ax2.set_title(target)
        #plt.colorbar()
    else: 
        f, [ax1,ax2,ax3] = plt.subplots(1,3)
        ax1.imshow(source,cmap=cm.gray)
        ax1.set_title(fil1)
        #plt.colorbar()
        ax2.imshow(target,cmap=cm.gray)
        ax2.set_title(fil2)
        ax2.colorbar()
        ax3.imshow(target2,cmap=cm.gray)
        ax3.set_title(fil2)
        ax3.colorbar()
    return    
        
    


    
    